@echo off & setLocal EnableDelayedExpansion

for /f "tokens=* delims= " %%a in (IpAddresses.txt) do (
echo %%a
echo y | plink.exe -ssh %%a -m command.txt -l xse -pw $1
)
Pause