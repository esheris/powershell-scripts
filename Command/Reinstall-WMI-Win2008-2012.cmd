REM Do not use with Windows 2003

@echo off

:: complain to mikelee, not eric sheris, about this script

setlocal enabledelayedexpansion
ECHO.
sc query >nul
if %ERRORLEVEL% NEQ 0 ((ECHO SC.EXE not found -- likely this is not a Windows 2008 or above server) & (EXIT /B 1))
IF /I "%windir%" EQU "C:\WINNT" ((ECHO Don''t use this with Win2003) & (EXIT /B 1))

:: SET _servicesToStop=ualsvc,iphlpsvc,ccmexec,winmgmt
SET _servicesToStop="winmgmt"
SET _servicesToDisable="winmgmt"

:: error 1060 means service not installed. The space after the start= before the start param for sc is really needed, not a typo.

for %%A in (%_servicesToDisable%) DO (
	sc query %%A >nul
	if !ERRORLEVEL! NEQ 1060 (
		ECHO Disabling %%A
		sc config %%A start= disabled >nul
		IF !ERRORLEVEL! NEQ 0 ((ECHO FAILED to disable the %%A service) & (EXIT /B 1))
	)
)

for %%A in (%_servicesToStop%) DO (
	sc query %%A >nul
	if !ERRORLEVEL! NEQ 1060 (
		SET _service=%%A
:: Try to stop dependent services first--only to the first level of dependents.
		FOR /F "usebackq tokens=1* delims= " %%A in (`sc enumdepend "!_service!" 10000 ^| FINDSTR /I /C:"SERVICE_NAME"`) DO (
			SC QUERY "%%B" | FINDSTR /I /C:"4  RUNNING" >nul
			IF !ERRORLEVEL! EQU 0 (
				SC STOP "%%B" >nul
				ECHO Stopping %%B service
				SET _ServicesToStop=!_ServicesToStop!,"%%B"
			)
		
		)
		ECHO Stopping !_service! service
		sc stop !_service! >nul
		CALL :WAITFORSERVICESTATUS "!_service!" STOPPED
		IF !ERRORLEVEL! NEQ 0 ((ECHO TIMED OUT stopping !_service! service) & (CALL :ENABLESERVICES) & (EXIT /B 1))
	)

)

PUSHD %windir%\system32\wbem >nul
IF /I "%CD%" NEQ "%windir%\system32\wbem" ((ECHO failed changing to %windir%\system32\wbem) & (EXIT /B 1))

ECHO.
ECHO Removing WMI repository folder
if exist repository rd /S /Q repository >nul
IF %ERRORLEVEL% NEQ 0 ((ECHO Failed removing repository folder) & (EXIT /B 1))

FOR %%A IN (tscfgwmi.dll,scecli.dll,userenv.dll) DO (
	ECHO Registering %%A
	regsvr32 /s %windir%\system32\%%A >nul
	IF !ERRORLEVEL! NEQ 0 ((ECHO FAILED registering %%A with error !ERRORLEVEL!) & (EXIT /B 1))
)

ECHO Registering WMI Provider Service
wmiprvse /regserver >nul
IF %ERRORLEVEL% NEQ 0 ((ECHO Failed registering WMIPRVSE with error %ERRORLEVEL%) & (EXIT /B 1))

ECHO Registering wmisvc.dll
regsvr32 /s wmisvc.dll >nul
IF %ERRORLEVEL% NEQ 0 ((ECHO Failed registering wmisvc.dll with error %ERRORLEVEL%) & (EXIT /B 1))

CALL :ENABLESERVICES


ECHO.
echo Services to re-start: %_servicesToStop%
FOR %%A in (%_servicesToStop%) DO (
	SC query %%A >nul
	if !ERRORLEVEL! NEQ 1060 (
		ECHO Starting %%A service
		sc start %%A >nul
		CALL :WAITFORSERVICESTATUS %%A RUNNING
		IF !ERRORLEVEL! NEQ 0 ((ECHO TIMED OUT starting %%A service) & (EXIT /B 1))
	)

)

:: Search in all subfolders to compile the files listed in the FOR loop below,
:: on the assumption that these files must be compiled first.
:: I make this assumption based on the original script having this assumption,
:: not because I know it actually matters.
:: Otherwise, this loop could be dispensed with because the subdirectories loop
:: below handles these same files again.
ECHO.
FOR /R %%A IN (cimwin32.mof,cimwin32.mfl,rsop.mof,rsop.mfl) DO (
	if exist "%%A" (
		ECHO Compiling %%A
		mofcomp %%A >nul
		IF !ERRORLEVEL! NEQ 0 (
			ECHO Failed compiling %%A with error !ERRORLEVEL!
			SET /A _compileFailCount+=1
		) ELSE (
			SET /A _compileCount+=1
		)
	)
)

ECHO Registering DLL files in subdirectories
FOR /R %%A in (*.DLL) DO (
	regsvr32 /s "%%A" >nul
	IF !ERRORLEVEL! NEQ 0 (
		ECHO Failed registering %%A with error !ERRORLEVEL!
		SET /A _registerFailCount+=1
	) ELSE (
		SET /A _registerCount+=1
	)
)

ECHO Compiling MOF and MFL files in subdirectories
FOR /R %%A in (*.MOF,*.MFL) DO (
	SET _file=%%A
	IF /I NOT "!_file:~0,37!" EQU "C:\Windows\System32\wbem\Autorecover\" (
		mofcomp "%%A" >nul
		IF !ERRORLEVEL! NEQ 0 (
			ECHO Failed compiling %%A with error !ERRORLEVEL!
			SET /A _compileFailCount+=1
		) ELSE (
			SET /A _compileCount+=1
		)
	)
)

ECHO MOF/MFL Compile    Succeeded/Failed: %_compileCount%/%_compileFailCount%
ECHO DLL Registration   Succeeded/Failed: %_registerCount%/%_registerFailCount%
goto :EOF


GOTO :EOF
:WAITFORSERVICESTATUS
SET _serviceName=%~1
SET _serviceStatus=%~2
SC QUERY %_servicename% >nul
IF %ERRORLEVEL% NEQ 1060 FOR /L %%A IN (1,1,30) DO (
	IF /I "%_serviceStatus%" EQU "STOPPED" SC STOP "%_serviceName%" >nul
	IF /I "%_serviceStatus%" EQU "RUNNING" SC START "%_serviceName%" >nul
:: pause for a few seconds. poor man's SLEEP.EXE
	ping 192.168.254.254 -w 1000 -n 10 >nul
	SC QUERY %_servicename% | FINDSTR.EXE /I /C:"%_serviceStatus%" >nul
	IF !ERRORLEVEL! EQU 0 EXIT /B 0
	ECHO Waiting on %_serviceName%
	SC ENUMDEPEND %_servicename% 1000 | FINDSTR /I /C:"4  RUNNING"
	IF !ERRORLEVEL! EQU 0 (
		ECHO Services dependent on %_servicename% are running
		ECHO Run "SC ENUMDEPEND %_servicename% 10000" to find and stop them.

	)
)
EXIT /B 1
GOTO :EOF


GOTO :EOF
:ENABLESERVICES
FOR %%A in (%_servicesToDisable%) DO (
	SC query %%A >nul
	if !ERRORLEVEL! NEQ 1060 (
		ECHO Enabling %%A
		sc config %%A start= auto >nul
		IF !ERRORLEVEL! NEQ 0 ((ECHO FAILED enabling %%A service with error !ERRORLEVEL!) & (EXIT /B 1))
	)
)
GOTO :EOF