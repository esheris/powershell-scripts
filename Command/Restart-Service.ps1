# v1 - 2013-10-22 - mikelee@microsoft.com
# for a given service or list of services, will restart the service and its dependencies in correct order, but only if each service is already running.
# example: Restart-Service -servicesToRestart "winmgmt","plugplay"


param ([String[]]$servicesToRestart)
$script:count = 0
$script:limit = 100

#recursively walks the dependency list for the service, generating an ordered list of services.
function Get-DependencyList {
param ($service)
$script:count++
if ($script:count -gt $script:limit) {Write-Host "Exceeded recursion limit of $script:limit" ; exit 1}
$dependentServices = (Get-Service $service.name).DependentServices
if ($dependentServices)
{
	foreach ($item in $dependentServices) {if ($script:serviceList -notcontains $item.Name) {$script:serviceList += $item.Name}}
	foreach ($dependent in $dependentServices) {Get-DependencyList $dependent}
}

}


$failedList = @()

foreach ($service in $servicesToRestart)
{
	if ($failed) {Remove-Variable failed}
	$topService = Get-Service $service
	Write-Host "`nTrying to restart $($topService.Displayname) `[$($topService.Name)`]"
	if ($topService.Status -ne "Running") {Write-Host "     $($topService.DisplayName) `[$($topService.Name)`] not found or not running. It will not be started." ; continue}
	$script:serviceList = @()
	$serviceList += "$($topService.Name)"
	Get-DependencyList $topService
	$restartList = @()
	foreach ($item in $serviceList)
	{
		if ((Get-Service $item).Status -eq "Running")
		{
			$restartList += $item
		}
		else
		{
			Write-Host "     $item is not currently running. It will not be started."
		}
	}

# When stopping services, you start with the end of the list shut down in correct dependency order;
# When starting services, you start at the beginning of the list.
	
	$stoppedList = @()
	for ($i = $restartList.Count -1 ; $i -ge 0 ; $i--)
	{
		Write-Host "     Stopping $($restartList[$i])"
		Stop-Service $restartList[$i]
		if (!$?) {Write-Host "     Error stopping $($restartList[$i])" ; $failedList += "$($restartList[$i])" ; $failed = $true ; break}
		Start-Sleep -Seconds 2
		$checkCount = 0
		while ((Get-Service $restartList[$i]).Status -ne "Stopped")
		{
			$checkCount++
			if ($checkCount -gt 10)
			{
				Write-Host "     Failed to stop $($restartList[$i])"
				$failedList += "$($restartList[$i])"
				$failed = $true
				Start-Service $restartList[$i]
				break	
			}
			Start-Sleep -Seconds 10
			Write-Host "          Waiting for $($restartList[$i]) to stop"
		}
		$stoppedList += "$($restartList[$i])"
	}
	
# if you fail shutting down any service, abort and restart the ones you just stopped.

	if ($failed) {$restartList = $stoppedList ; Write-Host "     Aborting restart for $service and its dependencies"}
	
	foreach ($item in $restartList)
	{
		Write-Host "     Starting $item"
		Start-Service $item
		if (!$?) {Write-Host "     Error starting $item" ; $failedList += "$item" ; break}
		Start-Sleep -Seconds 2
		$checkCount = 0
		while ((Get-Service $item).status -ne "Running")
		{
			$checkCount++
			if ($checkCount -gt 10) {Write-Host "     Failed to start $item"}
			Start-Sleep -Seconds 10
			Write-Host "          Waiting for $item to start"
		}
	}
}
if ($failedList.Count -ne 0) {Write-Host "`nFailed stopping or starting $failedList`n" ; return 1}
return 999
