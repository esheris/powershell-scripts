@echo off
setlocal enabledelayedexpansion
SET _MACAddress=%~1
SET _StaticIPAddress=%~2
SET _NICName=Team-%~2
SET _SubnetMask=%~3
SET _DefaultGateway=%~4
SET _NICName=Team-%~2
SET _DNS1=__netNic1DnsServerSearchOrder__
SET _DNS2=__netNic1DnsServerSearchOrder2__

:: Get-DNSServer.CMD should already have run before this script, and should have modified
:: the c:\windows\temp copy of this script by replacing 
:: 10.10.83.11 and 10.10.83.12 with actual values
:: of the DNS servers.

IF /I "%_DNS1:~0,2%" EQU "__" (
	Echo You must run GET-DNSServer.CMD to replace DNS server values before running %0
	exit /b 1
)

IF /I "%_DNS2:~0,2%" EQU "__" (
	Echo You must run GET-DNSServer.CMD to replace DNS server values before running %0
	exit /b 1
)


IF NOT DEFINED _DefaultGateway (
	Echo You must specify MACADDRESS IPADDRESS SUBNETMASK DEFAULTGATEWAY on the command line of this script
	exit /b 1
)

SET _outfile=_setnic1.output
SET _errorfile=_setnic1.errors

::
:: You can't have whitespace only between a caret at end of line and a following SET statement.
:: If you do, the SET won't work. But all this stuff here should now be irrelevant--these
:: values should have been passed in on the script command line. Leaving forhistorical/reversion purposes.
::
IF NOT DEFINED _MACAddress SET _MACAddress=__adsAdminMAC__
::
IF NOT DEFINED _StaticIPAddress SET _StaticIPAddress=__adsAdminIP__
::
IF NOT DEFINED _SubnetMask SET _SubnetMask=__netNic1SubnetMask__
::
IF NOT DEFINED _DefaultGateway SET _DefaultGateway=__netNic1DefaultGateway__
::
IF NOT DEFINED _NICName SET _NICName=__netNic1Name__
::



:: insert colons into mac address because WMIC wants them; remove colons and dashes first in case they're already there
SET _MACAddress=%_MACAddress::=%
SET _MACAddress=%_MACAddress:-=%
SET _MACAddress=%_MACAddress:~0,2%:%_MACAddress:~2,2%:%_MACAddress:~4,2%:%_MACAddress:~6,2%:%_MACAddress:~8,2%:%_MACAddress:~10,2%

wmic /output:"%_outfile%" nicconfig where MACAddress="%_MACAddress%" call EnableDHCP
CALL :CHECK4ERROR
ping -n 60 -w 1000 192.168.168.168 >nul
wmic /output:"%_outfile%" nicconfig where MACAddress="%_MACAddress%" call EnableStatic "%_StaticIPAddress%", "%_SubnetMask%"
CALL :CHECK4ERROR
wmic /output:"%_outfile%" nicconfig where MACAddress="%_MACAddress%" call SetGateways "%_DefaultGateway% ", 20
CALL :CHECK4ERROR
wmic /output:"%_outfile%" nicconfig where MACAddress="%_MACAddress%" call SetDNSServerSearchOrder ("%_DNS1%","%_DNS2%")
CALL :CHECK4ERROR
wmic /output:"%_outfile%" nicconfig where MACAddress="%_MACAddress%" call SetDynamicDNSRegistration 1, 1
CALL :CHECK4ERROR
wmic /output:"%_outfile%" nicconfig where MACAddress="%_MACAddress%" call SetTcpipNetbios 1
CALL :CHECK4ERROR

FOR /F "useback delims=" %%A in (`REG.EXE QUERY HKLM\System\CurrentControlSet\Control\Network /s /f "Local Area Connection" /d /e ^| findstr.exe /C:"HKEY_LOCAL_MACHINE"`) DO (
	REG.EXE ADD "%%A" /v Name /d "%_NicName%" /f
	IF !ERRORLEVEL! NEQ 0 (
		ECHO Failed changing Local Area Connection to %_NICName% >>"%_errorfile%"
		SET /A _errors+=1
	)
)




IF DEFINED _Errors (
	echo %_Errors% errors occurred--check the %_errorfile% file
	wmic /output:"%_outfile%" nicconfig where MACAddress="%_MACAddress%" call EnableDHCP	
	exit /b %_Errors%
)






goto :EOF
:Check4Error
TYPE "%_outfile%" > _errorcheck.txt
findstr /i /c:"ReturnValue = 0;" _errorcheck.txt
if %errorlevel% NEQ 0 (
	Type "%_outfile%" >>"%_errorfile%"
	SET /A _Errors+=1
)
if exist "%_outfile%" del "%_outfile%"
if exist _errorcheck.txt del _errorcheck.txt
goto :EOF







