sc config winmgmt start= disabled
net stop ccmexec
net stop winmgmt /y
pause

%systemdrive%
cd %windir%\system32\wbem

rd /S /Q repository

regsvr32 %windir%\system32\tscfgwmi.dll
regsvr32 /s %systemroot%\system32\scecli.dll
regsvr32 /s %systemroot%\system32\userenv.dll

wmiprvse /regserver 
winmgmt /regserver 

sc config winmgmt start= auto
net start winmgmt
net start ccmexec


mofcomp cimwin32.mof
mofcomp cimwin32.mfl
mofcomp rsop.mof
mofcomp rsop.mfl

for /f %%s in ('dir /b /s *.dll') do regsvr32 /s %%s
for /f %%s in ('dir /b *.mof') do mofcomp %%s
for /f %%s in ('dir /b *.mfl') do mofcomp %%s