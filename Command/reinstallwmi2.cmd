@echo off
setlocal enabledelayedexpansion

sc config winmgmt start= disabled
net stop ccmexec

:WAITFORSTOP ccmexec

net stop winmgmt /y

:WAITFORSTOP winmgmt

%systemdrive%
cd %windir%\system32\wbem

rd /S /Q repository

regsvr32 /s %windir%\system32\tscfgwmi.dll
regsvr32 /s %systemroot%\system32\scecli.dll
regsvr32 /s %systemroot%\system32\userenv.dll

wmiprvse /regserver

::pre-win2008
winmgmt /regserver
::win2008+
regsvr32 /s wmisvc.dll 


sc config winmgmt start= auto
net start winmgmt
net start ccmexec


mofcomp cimwin32.mof
mofcomp cimwin32.mfl
mofcomp rsop.mof
mofcomp rsop.mfl

for /f %%s in ('dir /b /s *.dll') do regsvr32 /s %%s
for /f %%s in ('dir /b *.mof') do mofcomp %%s
for /f %%s in ('dir /b *.mfl') do mofcomp %%s

goto :EOF


GOTO :EOF
:WAITFORSTOP
SET _servicename=%~1
SC QUERY %_servicename%
IF %ERRORLEVEL% NEQ 1060 FOR /L %%A IN (1,1,1000) DO (
	SC QUERY %_servicename% | FINDSTR.EXE /C:"STOPPED"
	IF !ERRORLEVEL! EQU 0 EXIT /B
	ping localhost>nul
)