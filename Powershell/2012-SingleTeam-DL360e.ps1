﻿param(
    [Parameter(Mandatory=$true)]
    $ip,
    [Parameter(Mandatory=$true)]
    $netmask,
    [Parameter(Mandatory=$true)]
    $gw
)

function Team-Nic(){
    param(
        [Parameter(Mandatory=$true)]
        $ipaddress
    )
        $teamMembers = (get-netadapter | where {$_.LinkSpeed -ne "0 bps"} | select Name).Name
        if ($teamMembers.count -eq 2){
            $team = new-netlbfoteam -name "Team-$ipaddress" -TeamingMode SwitchIndependent -LoadBalancingAlgorithm TransportPorts -TeamMembers $teamMembers -confirm:$false
            write $team
        } else {
            exit 2
        }
}

function Set-TeamIP(){
    param(
        [Parameter(Mandatory=$true)]
        $teamToIp,
        [Parameter(Mandatory=$true)]
        $ipaddress,
        [Parameter(Mandatory=$true)]
        $gateway,
        [Parameter(Mandatory=$true)]
        $bitlength
    )
    Start-Sleep -Seconds 60

    $CurrentDNSServers = (get-dnsclientserveraddress -interfacealias $teamToIp.Name -addressfamily ipv4).ServerAddresses
    new-netipaddress -ipaddress $ipaddress -defaultgateway $gateway -prefixlength $bitlength -interfacealias $teamToIp.Name
    Set-dnsclientserveraddress -interfacealias $teamToIp.Name -serveraddresses $CurrentDNSServers
    register-dnsclient

}

Function ConvertTo-MaskLength {
  <#
    .Synopsis
      Returns the length of a subnet mask.
    .Description
      ConvertTo-MaskLength accepts any IPv4 address as input, however the output value
      only makes sense when using a subnet mask.
    .Parameter SubnetMask
      A subnet mask to convert into length
  #>
 
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [Alias("Mask")]
    [Net.IPAddress]$SubnetMask
  )
 
  Process {
    $Bits = "$( $SubnetMask.GetAddressBytes() | ForEach-Object { [Convert]::ToString($_, 2) } )" -Replace '[\s0]'
 
    Return $Bits.Length
  }
}


$netmaskBit = ConvertTo-MaskLength -SubnetMask $netmask
$team = Team-Nic -ipaddress $ip
Set-TeamIP -teamToIp $team -ipaddress $ip -gateway $gw -bitlength $netmaskBit 

