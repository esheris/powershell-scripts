﻿param(
    [parameter(mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [Guid]$ImagingBatchID,
    [parameter(mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [string]$emailAddresses
)
$global:ImagingBatchID = $ImagingBatchID
#Region Functions

function Write-OSDLog() {
	param(
		[Parameter(Mandatory=$true)]
		[Guid]$batchID,
		[Parameter(Mandatory=$true)]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText,
		[string]$SqlServer,
		[string]$Database,
		[switch]$nosql
	)
	begin {
		#$LogOutput = [string]::Format("{0}:{1}:{2}",$batchID,$resultType,$resultText)
		
	}
	process {
		#tee-object -InputObject $logoutput -Variable tee			
		#$tee | Out-File -Append -FilePath ".\Add-ServersToSccmFromUrmawm.txt" -Encoding ascii
		if (-not $nosql){
			$dt = Get-Date
			$query = "INSERT INTO t_ServerImportLog (v_BatchID,dt_Inserted,v_MessageType,v_Message) Values ('$batchID','$dt','$resultType','$resultText')"
			$result = Write-Sql -server $SqlServer -database $Database -query $query
		}
	}		
}


    Function New-CMAccount{
     Param(
    [Parameter(Mandatory=$True)]
    $SiteServer,
    [Parameter(Mandatory=$True)]
    $SiteCode,
    [Parameter(Mandatory=$True)]
    $ResourceName,
    [Parameter(Mandatory=$True)]
    $MACAddress
    )

    #Collection query
    $CollectionQuery = Get-WmiObject -Namespace Root\SMS\Site_$SiteCode -Class SMS_Collection -ComputerName $SiteServer -Filter "Name='$CollectionName'"

    #   New computer account information
    $WMIConnection = ([WMIClass]"\\$SiteServer\root\SMS\Site_${SiteCode}:SMS_Site")
    $NewEntry = $WMIConnection.psbase.GetMethodParameters("ImportMachineEntry")
    $NewEntry.MACAddress = $MACAddress
    $NewEntry.NetbiosName = $ResourceName
    $NewEntry.OverwriteExistingRecord = $True
    $Resource = $WMIConnection.psbase.InvokeMethod("ImportMachineEntry",$NewEntry,$null)

    foreach ($i in (1..6)){
        if ($Resource -ne $null){
            return $Resource
        }
        Start-Sleep 30
        $Resource = $WMIConnection.psbase.InvokeMethod("ImportMachineEntry",$NewEntry,$null)
    }
    return $false
    }
 
    function Read-SQL {
    Param (
        [string]$server, 
        [string]$query, 
        [string]$database
    )

	$connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = "<user>"
	$connString.psbase.Password = "<pass>"
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database

	$conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $table = New-Object System.Data.DataTable
	$table.load($sqlCmd.ExecuteReader())
	$conn.Close()
	return $table
    }
    
    function Return-ReaderResults {
        Param (
            $query,
            $server, 
            $database
        )
        $data = Read-SQL $server $query $database
        return $data
    }
    
    function Write-Sql {
        Param (
            $server, 
            $query, 
            $database
        )
        
        $connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
		$connString.psbase.UserID = "<user>"
		$connString.psbase.Password = "<pass>"
		$connString.psbase.IntegratedSecurity = $false
		$connString.psbase.DataSource = $Server
		$connString.psbase.InitialCatalog = $database
        $conn = New-Object System.Data.SqlClient.SqlConnection($connString)
        $conn.Open()
        $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
        $sqlCmd.CommandText = $query
        $sqlCmd.Connection = $conn
        $result = $sqlCmd.ExecuteNonQuery()
        $result
    }

    Function Add-ColonToMac(){
        Param (
            [ValidateNotNullOrEmpty()]
            [string]$Mac
        )
        $mac = $mac.Replace(":","")
        $mac = $mac.Replace(".","")
        $mac = $mac.Replace("-","")
        $macAddress = $mac[0] + $mac[1] + ":" + $mac[2] + $mac[3] + ":" + $mac[4] + $mac[5] + ":" + $mac[6] + $mac[7] + ":" + $mac[8] + $mac[9] + ":" + $mac[10] + $mac[11]
        Write-Output $macAddress
    }

    Function Add-CollectionRuleDirect(){
        Param(
            [Parameter(Mandatory=$True)]
            $SiteServer,
            [Parameter(Mandatory=$True)]
            $SiteCode,
            [Parameter(Mandatory=$True)]
            $ResourceName,
            [Parameter(Mandatory=$True)]
            $CollectionName
        )
            
        $CollectionQuery = Get-WmiObject -Namespace Root\SMS\Site_$SiteCode -Class SMS_Collection -ComputerName $SiteServer -Filter "Name='$CollectionName'"
        $Resource = Get-WmiObject -Namespace Root\SMS\Site_$SiteCode -Class SMS_R_System -ComputerName $SiteServer -filter "Name='$ResourceName'"
        $ResourceID = $Resource.ResourceID
        $CollectionID = $CollectionQuery.CollectionID
        $NewRule = ([WMIClass]"\\$SiteServer\root\SMS\Site_${SiteCode}:SMS_CollectionRuleDirect").CreateInstance()
        $NewRule.ResourceClassName = "SMS_R_SYSTEM"
        $NewRule.ResourceID = $ResourceID
        $NewRule.Rulename = $ResourceName
        $CollectionQuery.AddMemberShipRule($NewRule) | Out-Null
    }

    Function Add-ServersToSCCM{
    param(
        [parameter(mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $servers
    )
		$allResults = @()
        foreach($server in $servers) {
			$info = New-Object -TypeName psobject
            $ServerMac = Add-ColonToMac -Mac $server.v_bemac
            $servername = $server.v_ImagedServerName
			$info | Add-Member -MemberType NoteProperty -Name ServerName -Value $servername
            try {
            $results = Return-ReaderResults -server <server> -database CM_XBL -query "select vrs.Name0 from v_RA_System_MACAddresses mac inner join v_r_system vrs on vrs.ResourceID = mac.ResourceID where MAC_Addresses0 = '$ServerMac'"
            } catch {
                Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Fatal" -resultText "Error retrieving data from SCCM Database (cm_xbl) for $servername. Object will not be created or moved."
                continue
            }
            if ($results -eq $null){ 
                $res = New-CMAccount -ResourceName $servername -MACAddress $ServerMac -SiteServer "<server>" -SiteCode "XBL"
                if ($res -ne $false){
					$info | Add-Member -MemberType NoteProperty -Name ActionTaken -Value <user>
                    Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Informational" -resultText "Created new computer object for $servername"
                    $AddStatus = Add-CollectionRuleDirect -ResourceName $servername -CollectionName "1_Disk_Configuration" -SiteServer "<server>" -SiteCode "XBL"
					Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Informational" -resultText "Attempting to Add $servername to 1_Disk_Configuration"
                }
            } else {
				$info | Add-Member -MemberType NoteProperty -Name ActionTaken -Value "Decom"
				Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Informational" -resultText "Attempting to Add $servername to Decom_From_Windows"
                $AddStatus = Add-CollectionRuleDirect -ResourceName $servername -CollectionName "Quarantine" -SiteServer "<server>" -SiteCode "XBL"
                
            }
			$allResults = $allResults + $info
        }
		return $allResults
    
}

Function Send-Email(){
    param(
        [parameter(mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$emailAddresses,
		[ValidateNotNullOrEmpty()]
		$body
    )
    Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Informational" -resultText "Preparing to send email to $emailaddresses"
    $emailFrom = "Urmawm"
    $emailTo = $emailAddresses
    $subject = "Status of recent build request"
    $smtpServer = "<mailserver>"
	$msgBody = "<H1>Preliminary Results for $global:batch</h1><h2>More detailed results/logs to follow</h2><table><tr><th>Server Name</th><th>Status</th></tr>"
	foreach ($item in $body){
		$sn = $item.ServerName
		$sa = $item.ActionTaken
		$msgBody += "<tr><td>$sn</td><td>$sa</td></tr>"
	}
	$msgBody += "</table>"
    try {
        Send-MailMessage -From $emailFrom -To $emailTo -SmtpServer $smtpServer -Subject $subject -Body $msgBody -BodyAsHtml -Priority High
    } catch {
        Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Error" -resultText "Error sending email"
    }
}

Function Get-ServersFromDatabase(){
	param(
        [parameter(mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$ImagingBatchID
    )
	if (Test-Connection -ComputerName <server> -Quiet){
	try{
		$global:batch = (Return-ReaderResults -server <server> -database Urmawm_Logging -query "select v_BatchName from t_BatchIDNameMapping where v_BatchID = '$global:ImagingBatchID'").v_BatchName
		$servers = Return-ReaderResults -server <server> -database Root_Of_All_Evil -query "select v_ImagedServerName, v_BeMac from Pandoras_Box where v_ImportBatchID = '$global:ImagingBatchID'"
		if ($servers -ne $null){
			return $servers
		} else {
			Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Fatal" -resultText "No Results returnd when querying for batch $global:ImagingBatchID"
			exit 3
		}
	}
	catch {
		Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Fatal" -resultText "Error retrieving data from SQL for Imaging Batch $ImageingBatchID"
        exit 1
	}
	} else {
		Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Fatal" -resultText "Unable to connect to SQL server to retrieve batch list."
	}
}

Function Verify-CollectionMoves(){
	param(
		[ValidateNotNullOrEmpty()]
		$tries
	)
	Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Informational" -resultText "Beginning Validation"
	foreach ($server in $tries){
		$sn = $server.ServerName
		$sa = $server.ActionTaken
		switch ($sa){
			<user>{
				foreach ($i in (1..5)){
					$ServerInBuild = Return-ReaderResults -server <server> -database CM_XBL -query "SELECT * from v_CM_RES_COLL_XBL0000B where Name = '$sn'"
					if ($ServerInBuild -eq $null){
						Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Warning" -resultText "Server not found in BUILD collection - Attempting to Add again"
						Add-CollectionRuleDirect -ResourceName $sn -CollectionName "1_Disk_Configuration" -SiteServer "<server>" -SiteCode "XBL"
						Start-Sleep -Seconds 30
					} else { 
						Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Informational" -resultText "Confirmed Server: $sn found in BUILD collection"
						break 
					}
				}
			}
			"Decom"{
				foreach ($i in (1..5)){
					$ServerInDecom = Return-ReaderResults -server <server> -database CM_XBL -query "SELECT * from v_CM_RES_COLL_XBL00015 where Name = '$sn'"
					if ($ServerInDecom -eq $null){
						Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Warning" -resultText "Server not found in DECOM collection - Attempting to Add again"
						Add-CollectionRuleDirect -ResourceName $sn -CollectionName "1_Disk_Configuration" -SiteServer "<server>" -SiteCode "XBL"
						Start-Sleep -Seconds 30
					} else { 
						Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Informational" -resultText "Confirmed Server: $sn found in DECOM collection"
						break 
					}
				}
			}
			default {}
		}
	}
}

#end region
try {
	$servers = Get-ServersFromDatabase -ImagingBatchID $global:ImagingBatchID
	if ($servers.Count -eq 0){
        Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Fatal" -resultText "No Results returned when querying for batch $global:ImagingBatchID"
        exit 2
    }	
	$counttxt = [String]::Format("Processing {0} servers in batch {1}", $servers.count.ToString(),$global:batch.v_BatchName)
	Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Informational" -resultText $counttxt
	
	$AddServerResults = Add-ServersToSccm -servers $servers 
	Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Informational" -resultText "Finished Processing servers, Sleeping for 5 minutes to let SCCM catch up"
	Start-Sleep -Seconds 300
	Verify-CollectionMoves -tries $addServerResults
	Send-Email -emailAddresses $emailAddresses -body $AddServerResults
} catch {
	Send-Email -emailAddresses $emailAddresses -body "Errors occured while processing your recent build request."
}
& .\Create-DHCPReservation.ps1 -BatchID $global:ImagingBatchID -emailAddresses $emailAddresses