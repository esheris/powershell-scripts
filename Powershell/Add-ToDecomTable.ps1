﻿param(
    [validatenotnullorempty()]
    [string]$servername,
    [string]$pandoraID = "0"
)


function Write-Sql {
    Param (
        $server, 
        $query, 
        $database
    )
    
    $connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = "<user>"
	$connString.psbase.Password = "<pass>"
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database
    $conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $result = $sqlCmd.ExecuteNonQuery()
    $conn.Close()
    $result
}
$query = [String]::Format("INSERT INTO DecommissionedServers (Servername,DecomDate,PandoraID) VALUES ('{0}','{1}','{2}')",$servername,(get-date),$pandoraID)
$return = Write-Sql -server <server> -database <database> -query $query
if ($return -eq 1){ return 999 } else { return 666 }