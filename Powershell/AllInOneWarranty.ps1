function ConvertFrom-Html {
  param(
    [String]$Html
  )

  $HtmlLines = $Html -split "<tr" | Where-Object { $_ -match '<td' }
  $Header = ($HtmlLines[0] -split '<td') -replace '[\w\s\d"=%:;\-]*>|</.*' | Where-Object { $_ -ne '' }

  for ($i = 1; $i -lt $HtmlLines.Count; $i++) {
    $Output = New-Object Object
    $Values = ($HtmlLines[$i] -split '<td') -replace '[\w\s\d"=%:;\-]*>|</.*|<a.+?>'
    for ($j = 1; $j -lt $Values.Count; $j++) {
      $Output | Add-Member NoteProperty $Header[$j - 1] -Value $Values[$j]
    }
    $Output
  }
}
function Select-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$Filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	if ($Show -eq "OK")
	{
		return $objForm.FileName
	}
	else
	{
		return $null
	}
}
function Save-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$extension,[string]$filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.SaveFileDialog
	$objForm.ValidateNames = $true
	$objForm.Filter = $Filter
	$objForm.InitialDirectory = $Directory
	$objForm.AddExtension = $true
	$objForm.DefaultExt = $extension
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	if ($Show -eq "OK")
	{
		return $objForm.FileName
	}
	else
	{
		return $null
	}
}
$importFilePath = Select-FileDialog -Title "Select CSV File" -Directory ".\" -filter "CSV(Comma Delimited) (*.csv) | *.csv"
$import = import-csv $importFilePath
$saveFilePath = Save-FileDialog -Title "Save File" -Directory ".\" -filter "CSV(Comma Delimited) (*.csv) | *.csv" -extension "csv"
$header = "Server,Asset,Serial,MAC,OutOfBand,Model,Start Date,End Date,Days Left"
$header | out-file -filepath $saveFilePath -encoding ascii
foreach ($server in $import){
	$server.Server
	# Check if it's an HP based on the Serial Number, Add more Starts with here for newer servers
	if ($server.Serial.ToUpper().StartsWith("USE") -or $server.Serial.ToUpper().StartsWith("SGA")) {
		[regex]$regex = '\d{2}\s\w{3}\s\d{4}'
		$servicetag = $server.Serial
		$productID = $server.Model
		$url = "http://h20000.www2.hp.com/bizsupport/TechSupport/WarrantyResults.jsp?nickname=&sn=$serviceTag&country=US&lang=en&cc=us&pn=$productID&find=Display+Warranty+Information+%C2%BB&&printver=true"
		$webclient = New-Object System.Net.WebClient
		$string = $webclient.downloadString($url)
		$dates = $regex.matches($string) | % {[datetime]$_.Value}
		$dates[0] = [datetime]"1/1/1900"
		$dates = $dates | sort
		$startDate = $dates[1]
		$endDate = $dates[($dates.length - 1)]
		$daysLeft = ($endDate - (Get-Date)).Days
		if ($daysLeft -lt 0) {$daysLeft = 0 }
		$output = $server.Server + "," + $server.Asset + "," + $server.Serial + "," + $server.MAC + "," + $server.OutOfBand + "," + $server.Model + "," + $startDate.ToShortDateString() + "," + $endDate.ToShortDateString() + "," + $daysLeft
		$output | Out-File -FilePath $saveFilePath -Append -Encoding ascii
	}
	# if its not an hp, its probably a dell, and if it's not a dell, o well! we'll get nothing back then
	else {
		$servicetag = $server.Serial 
		$url = "http://support.dell.com/support/topics/global.aspx/support/my_systems_info/details?c=us&l=en&s=pub&~ck=anavml&servicetag=$servicetag"

		$webclient = New-Object System.Net.WebClient
		$string = $webclient.downloadString($url)
		if ($String -match '<table[\w\s\d"=\d]*[\w\s\d"=%]*contract_table">.+?</table>') {
			# Just in case
			if ($Matches -eq $null) {	
				$output = $server.Server + "," + $server.Asset + "," + $server.Serial + "," + $server.MAC + "," + $server.OutOfBand + "," + $server.Model 
				$output | out-file -filepath $saveFilePath -append -encoding ascii
			} else {
				$Support = ConvertFrom-Html $Matches[0]
				$output = $server.Server + "," + $server.Asset + "," + $server.Serial + "," + $server.MAC + "," + $server.OutOfBand + "," + $server.Model + "," + $Support[0]."Start Date" + "," + $Support[0]."End Date" + "," + $Support[0]."Days Left"
    			$output | out-file -filepath $saveFilePath -append -encoding ascii
			}
		}
		#Probably wasn't a dell :)
		else {
			$output = $server.Server + "," + $server.Asset + "," + $server.Serial + "," + $server.MAC + "," + $server.OutOfBand + "," + $server.Model
			$output | out-file -filepath $saveFilePath -append -encoding ascii
		}
	}
}