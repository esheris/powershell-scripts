﻿#region Read From SQL

function Read-SQL {
    Param (
        [ValidateNotNullOrEmpty()]
        [string]$server, 
        [ValidateNotNullOrEmpty()]
        [string]$query, 
        [ValidateNotNullOrEmpty()]
        [string]$database
    )

	$connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = <user>
	$connString.psbase.Password = "P@`$`$word"
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database

	$conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $table = New-Object System.Data.DataTable
	$table.load($sqlCmd.ExecuteReader())
	$conn.Close()
	return $table
}
#endregion

#region Insert,Update,Delete SQL

function Write-Sql {
        Param (
            $server, 
            $query, 
            $database
        )
        
        $connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
		$connString.psbase.UserID = <user>
		$connString.psbase.Password = "P@`$`$word"
		$connString.psbase.IntegratedSecurity = $false
		$connString.psbase.DataSource = $Server
		$connString.psbase.InitialCatalog = $database
        $conn = New-Object System.Data.SqlClient.SqlConnection($connString)
        $conn.Open()
        $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
        $sqlCmd.CommandText = $query
        $sqlCmd.Connection = $conn
        $result = $sqlCmd.ExecuteNonQuery()
        $result
    }
#endregion

Function ConvertTo-DecimalIP {
  <#
    .Synopsis
      Converts a Decimal IP address into a 32-bit unsigned integer.
    .Description
      ConvertTo-DecimalIP takes a decimal IP, uses a shift-like operation on each octet and returns a single UInt32 value.
    .Parameter IPAddress
      An IP Address to convert.
  #>
   
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [Net.IPAddress]$IPAddress
  )
 
  Process {
    $i = 3; $DecimalIP = 0;
    $IPAddress.GetAddressBytes() | ForEach-Object { $DecimalIP += $_ * [Math]::Pow(256, $i); $i-- }
 
    Return [UInt32]$DecimalIP
  }
}
Function ConvertTo-DottedDecimalIP {
  <#
    .Synopsis
      Returns a dotted decimal IP address from either an unsigned 32-bit integer or a dotted binary string.
    .Description
      ConvertTo-DottedDecimalIP uses a regular expression match on the input string to convert to an IP address.
    .Parameter IPAddress
      A string representation of an IP address from either UInt32 or dotted binary.
  #>
 
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [String]$IPAddress
  )
   
  Process {
    Switch -RegEx ($IPAddress) {
      "([01]{8}\.){3}[01]{8}" {
        Return [String]::Join('.', $( $IPAddress.Split('.') | ForEach-Object { [Convert]::ToUInt32($_, 2) } ))
      }
      "\d" {
        $IPAddress = [UInt32]$IPAddress
        $DottedIP = $( For ($i = 3; $i -gt -1; $i--) {
          $Remainder = $IPAddress % [Math]::Pow(256, $i)
          ($IPAddress - $Remainder) / [Math]::Pow(256, $i)
          $IPAddress = $Remainder
         } )
        
        Return [String]::Join('.', $DottedIP)
      }
      default {
        Write-Error "Cannot convert this format"
      }
    }
  }
}
#region Active Directory

Function Get-AdComputers($domain)
{
	$computers = @()
	$strFilter = "(objectCategory=Computer)"

	$objDomain = New-Object System.DirectoryServices.DirectoryEntry("LDAP://" + $domain)

	$objSearcher = new-object System.DirectoryServices.DirectorySearcher
	$objSearcher.SearchRoot = $objDomain
	$objSearcher.PageSize = 1000
	$objSearcher.Filter = $strFilter
	$objSearcher.SearchScope = "Subtree"

	$colProplist = "name"
	foreach ($i in $colPropList){$objSearcher.PropertiesToLoad.Add($i)}


	$colResults = $objSearcher.FindALL()

	foreach ($objResult in $colResults)
	{
		$objItem = $objresult.Properties
		$computers = $computers + $objItem.name
	}
	return $computers
}

#endregion

#region WMI and Server Connections

Function Get-WmiCustom([string]$server,[string]$namespace='root\cimv2',[string]$class,[int]$timeout=15,[string]$filter)
{
	$ConnectionOptions = new-object System.Management.ConnectionOptions
	$EnumerationOptions = new-object System.Management.EnumerationOptions 

	$timeoutseconds = new-timespan -seconds $timeout
	$EnumerationOptions.set_timeout($timeoutseconds) 

	$assembledpath = "\\" + $server + "\" + $namespace
	#write-host $assembledpath -foregroundcolor yellow 

	$Scope = new-object System.Management.ManagementScope $assembledpath, $ConnectionOptions
	$Scope.Connect() 

	$querystring = "SELECT * FROM " + $class + " " + $filter
	#write-host $querystring 

	$query = new-object System.Management.ObjectQuery $querystring
	$searcher = new-object System.Management.ManagementObjectSearcher
	$searcher.set_options($EnumerationOptions)
	$searcher.Query = $querystring
	$searcher.Scope = $Scope 

	trap [Exception] { write-error "Error connecting to wmi on server: $server";break; } $result = $searcher.get() 

	return $result
} # End Get-WmiCustom
    
function Write-OSDLog() {
	param(
		[Parameter(Mandatory=$true)]
		[int]$resultCode,
		[Parameter(Mandatory=$true)]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText,
		[string]$hostname,
		[string]$SccmResourceID,
		[switch]$nosql
	)
	begin {
		$LogOutput = [string]::Format("{0}:{1}:{2}",$resultCode,$resultType,$resultText)
		
	}
	process {
		tee-object -InputObject $logoutput -Variable tee			
		$tee | Out-File -Append -FilePath $logpath -Encoding ascii
		if (-not $nosql){
			$query = Create-SqlQueryForPatchLogging -resultCode $resultCode -resultType $resultType -resultText $resultText -hostname $hostname -sccmResId $SccmResourceID
			$result = Write-Sql -server $global:SqlServer -database $global:Database -query $query
		}
	}		
}


    
    ################################################################
    ## Parameters:
    ##   $namespace - Name of Namespace to search for
    ##   $server - Name of server to connect to
    ## Takes the name of a namespace and a server. Connects to the root
    ## of WMI and reads the __Namespace class to get a list of all namespaces
    ## on the server that equal the passed in namespace 
    ################################################################
    Function Test-WmiNamespaceExists([string]$namespace,[string]$server)
    {
        $exists = Get-WmiCustom -server $server -namespace "root" -class "__Namespace" -timeout 1 | where {$_.Name -eq $namespace}
        if ($exists -ne $null)
        {
            return $true
        }
        else
        {
            return $false
        }
    }
#endregion

#region Use PSJobs for Paralellism

$s = 0# Handles parallelism
while ($s -lt $servers.length) {
    $threads = Get-Job -State Running
    if ($threads.length -gt 15) {
        Start-Sleep -s 5
        Start-Job -scriptblock $sb -argumentlist $servers[$s] -name $servers[$s] | out-null
        $s ++
    }
    else {
        #Start-Sleep -s 1
        Start-Job -scriptblock $sb -argumentlist $servers[$s] -name $servers[$s] | out-null
        $s ++
    }# End: Parallelism
    $completed = get-job -State Completed
    if ($completed -ne $null) {
        $received = $completed | receive-job
        $received | out-file -filepath $path -append -encoding ascii
        $completed | remove-job
        get-job -State Failed | remove-job
    }
    write-progress -activity "Searching Domain For Virtual Machines" -Status "% Complete:" -percentcomplete ($s/$servers.length*100)
}

#endregion

#region Save and Select File Dialogs

function Select-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$Filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	if ($Show -eq "OK")
	{
		return $objForm.FileName
	}
	else
	{
		return $null
	}
}
function Save-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$extension,[string]$filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.SaveFileDialog
	$objForm.ValidateNames = $true
	$objForm.Filter = $Filter
	$objForm.InitialDirectory = $Directory
	$objForm.AddExtension = $true
	$objForm.DefaultExt = $extension
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	if ($Show -eq "OK")
	{
		return $objForm.FileName
	}
	else
	{
		return $null
	}
}

#endregion

#region Registry

function Test-RegistryValue($regkey, $name) {
	Get-ItemProperty $regkey $name -ErrorAction SilentlyContinue | Out-Null
	$?
}

#endregion

function Get-TrueURL() {
	param (
		[parameter(Mandatory)]
		$url
	)
	$req = [System.Net.WebRequest]::Create($url)
	$req.AllowAutoRedirect = $false
	$req.Method = "GET"

	$resp = $req.GetResponse()
	if ($resp.StatusCode -eq "Found") {
		return $resp.GetResponseHeader("Location")
	}
	else {
		return $resp.responseURI
	}
}
