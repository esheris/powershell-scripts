param(
	[guid]$BatchID
	)
#
$global:ImagingBatchID = $BatchID.ToString()
$global:batch = (Return-ReaderResults -server <server> -database Urmawm_Logging -query "select v_BatchName from t_BatchIDNameMapping where v_BatchID = '$global:ImagingBatchID'").v_BatchName
function Read-SQL {
    Param (
        [string]$server, 
        [string]$query, 
        [string]$database
    )

	$connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = <user>
	$connString.psbase.Password = "P@`$`$word"
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database

	$conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $table = New-Object System.Data.DataTable
	$table.load($sqlCmd.ExecuteReader())
	$conn.Close()
	return $table
    }
 
function Create-DHCPReservation(){
	Param(
	[String]$ScopeId,
	[String]$IPAddress,
	[String]$ClientId,
	[String]$Name,
	[String]$DHCPComputerName
	)
	try {
	    Add-DhcpServerv4Reservation -ScopeId $ScopeId -IPAddress $IPAddress -Name $Name -ClientId $ClientId -ComputerName $DHCPComputerName -ErrorAction Stop
	    Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational "Reservation created for $Name"
    } catch {
        Write-OSDLog -batchID $global:ImagingBatchID -resultType Error -resultText "Failed to create Reservation for $Name"
        throw
    }
}

function Write-OSDLog() {
	param(
		[Parameter(Mandatory=$true)]
		[Guid]$batchID,
		[Parameter(Mandatory=$true)]
        [ValidateSet('Informational','Warning','Error')]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText,
		[string]$SqlServer = "<server>",
		[string]$Database = "Urmawm_Logging",
		[switch]$nosql
	)
	begin {
		#$LogOutput = [string]::Format("{0}:{1}:{2}",$batchID,$resultType,$resultText)
		
	}
	process {
		#tee-object -InputObject $logoutput -Variable tee			
		#$tee | Out-File -Append -FilePath ".\Add-ServersToSccmFromUrmawm.txt" -Encoding ascii
		if (-not $nosql){
			$dt = Get-Date
			$query = "INSERT INTO t_ServerImportLog (v_BatchID,dt_Inserted,v_MessageType,v_Message) Values ('$batchID','$dt','$resultType','$resultText')"
			$result = Write-Sql -server $SqlServer -database $Database -query $query
		}
	}		
}

Function ConvertTo-Mask {
  <#
    .Synopsis
      Returns a dotted decimal subnet mask from a mask length.
    .Description
      ConvertTo-Mask returns a subnet mask in dotted decimal format from an integer value ranging
      between 0 and 32. ConvertTo-Mask first creates a binary string from the length, converts
      that to an unsigned 32-bit integer then calls ConvertTo-DottedDecimalIP to complete the operation.
    .Parameter MaskLength
      The number of bits which must be masked.
  #>
   
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [Alias("Length")]
    [ValidateRange(0, 32)]
    $MaskLength
  )
   
  Process {
    Return ConvertTo-DottedDecimalIP ([Convert]::ToUInt32($(("1" * $MaskLength).PadRight(32, "0")), 2))
  }
}

Function Get-NetworkRange( [String]$IP, [String]$Mask ) {
  If ($IP.Contains("/"))
  {
    $Temp = $IP.Split("/")
    $IP = $Temp[0]
    $Mask = $Temp[1]
  }
 
  If (!$Mask.Contains("."))
  {
    $Mask = ConvertTo-Mask $Mask
  }
 
  $DecimalIP = ConvertTo-DecimalIP $IP
  $DecimalMask = ConvertTo-DecimalIP $Mask
   
  $Network = $DecimalIP -BAnd $DecimalMask
  $Broadcast = $DecimalIP -BOr ((-BNot $DecimalMask) -BAnd [UInt32]::MaxValue)
 
  For ($i = $($Network + 1); $i -lt $Broadcast; $i++) {
    ConvertTo-DottedDecimalIP $i
  }
}

Function ConvertTo-DecimalIP {
  <#
    .Synopsis
      Converts a Decimal IP address into a 32-bit unsigned integer.
    .Description
      ConvertTo-DecimalIP takes a decimal IP, uses a shift-like operation on each octet and returns a single UInt32 value.
    .Parameter IPAddress
      An IP Address to convert.
  #>
   
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [Net.IPAddress]$IPAddress
  )
 
  Process {
    $i = 3; $DecimalIP = 0;
    $IPAddress.GetAddressBytes() | ForEach-Object { $DecimalIP += $_ * [Math]::Pow(256, $i); $i-- }
 
    Return [UInt32]$DecimalIP
  }
}

function Write-Sql {
    Param (
        $server, 
        $query, 
        $database
    )
        
    $connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = <user>
	$connString.psbase.Password = "P@`$`$word"
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database
    $conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $result = $sqlCmd.ExecuteNonQuery()
    $result
}

Function ConvertTo-DottedDecimalIP {
  <#
    .Synopsis
      Returns a dotted decimal IP address from either an unsigned 32-bit integer or a dotted binary string.
    .Description
      ConvertTo-DottedDecimalIP uses a regular expression match on the input string to convert to an IP address.
    .Parameter IPAddress
      A string representation of an IP address from either UInt32 or dotted binary.
  #>
 
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [String]$IPAddress
  )
   
  Process {
    Switch -RegEx ($IPAddress) {
      "([01]{8}\.){3}[01]{8}" {
        Return [String]::Join('.', $( $IPAddress.Split('.') | ForEach-Object { [Convert]::ToUInt32($_, 2) } ))
      }
      "\d" {
        $IPAddress = [UInt32]$IPAddress
        $DottedIP = $( For ($i = 3; $i -gt -1; $i--) {
          $Remainder = $IPAddress % [Math]::Pow(256, $i)
          ($IPAddress - $Remainder) / [Math]::Pow(256, $i)
          $IPAddress = $Remainder
         } )
        
        Return [String]::Join('.', $DottedIP)
      }
      default {
        Write-Error "Cannot convert this format"
      }
    }
  }
}

Function Get-ScopeFromIpAndSubnet(){
param(
    [System.net.Ipaddress]$IpAddress,
    [System.Net.IPAddress]$Subnet
)
    $DecimalIP = ConvertTo-DecimalIP -IPAddress $IpAddress
    $DecimalNetmask = ConvertTo-DecimalIP -IPAddress $Subnet
    $DecimalNetwork = $DecimalIP -band $DecimalNetmask
    $Network = ConvertTo-DottedDecimalIP -IPAddress $DecimalNetwork
    return $Network
}

function Get-DHCPServers(){
    try {
        $dhcpServers = Read-SQL -server <server> -database Root_Of_All_Evil -query "Select v_Colocation, v_DHCPServer from DHCP_Servers"
    } catch {
        return $null
    }
    return $dhcpServers

}

Function ConvertTo-MaskLength {
  <#
    .Synopsis
      Returns the length of a subnet mask.
    .Description
      ConvertTo-MaskLength accepts any IPv4 address as input, however the output value
      only makes sense when using a subnet mask.
    .Parameter SubnetMask
      A subnet mask to convert into length
  #>
 
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [Alias("Mask")]
    [Net.IPAddress]$SubnetMask
  )
 
  Process {
    $Bits = "$( $SubnetMask.GetAddressBytes() | ForEach-Object { [Convert]::ToString($_, 2) } )" -Replace '[\s0]'
 
    Return $Bits.Length
  }
}

function Create-ReservationForPandoraRecord(){
    param(
        $scope,
        $dhcpserver,
        $serverName,
        $serverMac,
        $serverIP
    )
    try{
        Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText "Attempting to create new reservation for IP: $serverIP with Mac: $serverMac in Scope: $scope on DHCPServer: $dhcpserver"
        Create-DHCPReservation -ScopeId $scope -IPAddress $serverIP -ClientId $serverMac -Name $serverName -DHCPComputerName $dhcpserver
        Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText "Successfully created reservation. Triggering replication of failover scope information"
        try {                            
            Invoke-DhcpServerv4FailoverReplication -ComputerName $dhcpserver -ScopeId $scope -Force -ErrorAction Stop
            Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText "Successfully triggered Failover Replication of Scope: $scope"
        } catch {
            Write-OSDLog -batchID $global:ImagingBatchID -resultType Warning -resultText "Error Invoking Failover scope replication, Change should be picked up within an hour by the default replication schedule, or the next time a reservation is added"
        }
    } catch {
        Write-OSDLog -batchID $global:ImagingBatchID -resultType Error -resultText "Error creating DHCP reservation for IP: $serverIP with Mac: $serverMac in Scope: $scope on DHCPServer: $dhcpserver"
    }
}

Function Send-Email(){
    param(
        [parameter(mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$emailAddresses,
		[ValidateNotNullOrEmpty()]
		$body
    )
    Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Informational" -resultText "Preparing to send email to $emailaddresses"
    $emailFrom = "Urmawm"
    $emailTo = $emailAddresses
    $subject = "Status of recent build request"
    $smtpServer = "<mailserver>"
	$msgBody = "<H1>Detailed Results for $global:batch</h1><table><tr><th>Message Type</th><th>Message</th></tr>"
	foreach ($item in $body){
		$sn = $item.v_MessageType
		$sa = $item.v_Message
        switch ($sn){
            "Informational" { $msgBody += "<tr><td>$sn</td><td>$sa</td></tr>"}
            "Warning" {$msgBody += "<tr><td bgcolor=`"#FF6600`">$sn</td><td>$sa</td></tr>"}
            "Error" {$msgBody += "<tr><td bgcolor=`"#FF0000`">$sn</td><td>$sa</td></tr>"}
            }
		
	}
	$msgBody += "</table>"
    try {
        Send-MailMessage -From $emailFrom -To $emailTo -SmtpServer $smtpServer -Subject $subject -Body $msgBody -BodyAsHtml -Priority High
    } catch {
        Write-OSDLog -batchID $global:ImagingBatchID -SqlServer <server> -Database Urmawm_Logging -resultType "Error" -resultText "Error sending email"
    }
}
 
function Main(){
	$servers = Read-SQL -server <server> -database Root_Of_All_Evil -query "select v_rack,v_ImagedServerName, v_BeMac,v_beIP,v_besubnet,v_begateway, v_DomainName, i_bevlan from Pandoras_Box where v_ImportBatchID = '$global:ImagingBatchID'"
    if ($servers -eq $null){
        $i = 0
        $seconds = 30
        while ($i -lt 6){
            Write-OSDLog -batchID $global:ImagingBatchID -resultType Warning -resultText "Failed to get manifest data from Pandora. Sleeping for $seconds seconds and trying again. This will try up to 6 times"
            Start-Sleep -Seconds $seconds
            $servers = Read-SQL -server <server> -database Root_Of_All_Evil -query "select v_rack,v_ImagedServerName, v_BeMac,v_beIP,v_besubnet,v_begateway, v_DomainName, i_bevlan from Pandoras_Box where v_ImportBatchID = '$global:ImagingBatchID'"
            if ($servers -ne $null){break}
            $seconds = $seconds + $seconds 
            $i++
        }
        Write-OSDLog -batchID $global:ImagingBatchID -resultType Error -resultText "Failed to aquire manifest data 6 times. Failed to create Reservations for batch"
        exit 1
    }
    Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText "Preparing to create reservations for $($servers.count) Servers" 
	$DHCPServers = Get-DHCPServers
	foreach ($server in $servers){
        $colo = $server.v_rack.substring(0,3)
        $dhcpserver = ($DHCPServers | where {$_.v_Colocation -eq $colo} | Get-Random).v_DHCPServer
        
        $scope = Get-ScopeFromIpAndSubnet -IpAddress $server.v_BEIP -Subnet $server.v_BESubnet
        $subnetBits = ConvertTo-MaskLength -SubnetMask $server.v_BESubnet
        

        $logEntry = [string]::Format("Preparing to create reservation for {0} on DHCP Server {1} in COLO {2} in scope {3} with bit length of {4}",$server.v_ImagedServerName, $dhcpserver,$colo,$scope,$subnetBits);
        Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText $logEntry

        $scopeExists = Get-DhcpServerv4Scope -ComputerName $dhcpserver -ScopeId $scope

        #Validate Scope Exists on DHCP Server
        if ($scopeExists -ne $null){
            Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText "Found Scope $($scopeExists.ScopeID) with State: $($scopeExists.State) and Lease Duration of $($scopeExists.LeaseDuration)"
            #Validate IP is not already reserved
            $reservationExistsByIP = Get-DhcpServerv4Reservation -ComputerName $dhcpserver -IPAddress $server.v_beip -ErrorAction SilentlyContinue
            
            #See if the mac address has a reservation for a different IP. Need to scan all scopes so takes a few seconds per server.
            $reservationExistsByMac = Get-DhcpServerv4Scope -computerName $dhcpserver | Get-DhcpServerv4Reservation -ComputerName $dhcpserver -ClientId $server.v_BEMac -ErrorAction SilentlyContinue

            if (($reservationExistsByIP -ne $null) -and ($reservationExistsByMac -ne $null)){
                Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText "Found Reservation for IP $($server.v_beip) and Mac $($server.v_bemac)"
                $compareReservations = Compare-Object -ReferenceObject $reservationExistsByIP -DifferenceObject $reservationExistsByMac
                if ($compareReservations -eq $null){
                    Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText "Reservation for Mac and IP are the same reservation. Server does not need a new reservation created. Continuing to next server"
                    continue  
                } else {
                    Write-OSDLog -batchID $global:ImagingBatchID -resultType Error -resultText "Reservation for Mac and IP do NOT match. Unable to create reservation for $($server.v_imagedservername)"
                    continue
                }
            }
            
            if (($reservationExistsByIP -eq $null) -and ($reservationExistsByMac -ne $null)){
                Write-OSDLog -batchID $global:ImagingBatchID -resultType Error -resultText "Found Reservation for Mac: $($server.v_bemac) in a different scope. This should have been removed via decom if this is a rebuild. This script does not delete reservations due to safety issues."
                continue
            }        
            
                if($reservationExists -eq $null){
                    Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText "Did not find existing reservation for IP $($server.v_beip), Creating new reservation"
                    Create-ReservationForPandoraRecord -scope $scope -dhcpserver $dhcpserver -serverName $server.v_imagedservername -serverMac $server.v_bemac -serverIP $server.v_beip
                    
                } else { 
                    Write-OSDLog -batchID $global:ImagingBatchID -resultType Warning -resultText "Found an existing reservation for the IP: $($server.v_beip)"
                    $macFromReservation = $reservationExists.ClientId.replace("-","")
                    if ($macFromReservation -eq $server.v_BeMac){
                        Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText "Mac: $macFromReservation  from previously found reservation for IP: $($server.v_beip) matches Server Mac: $($server.v_bemac). This reservation is for our server. Continuing to next server."
                        continue
                    } else {
                        Write-OSDLog -batchID $global:ImagingBatchID -resultType Error -resultText "Mac: $macFromReservation from previously found reservation for IP: $($server.v_beip) does not match Server Mac: $($server.v_bemac). Unable to create reservation for $($server.v_imagedservername)"
                        continue
                    }
                }            
         } else {
            Write-OSDLog -batchID $global:ImagingBatchID -resultType Warning -resultText "Scope: $scope not found on DHCPServer: $dhcpserver . Checking if scope exists in Active Directory"
            if ($subnetBits -lt 21){
                Write-OSDLog -batchID $global:ImagingBatchID -resultType Error -resultText "Detected scope to be created is a /$subnetBits. We do not create scopes automatically that are larger then /21. Please validate manifest subnet mask is correct or have the build team create this scope manually."
                continue
            }
            $adscope = Get-ADReplicationSubnet "$scope/$subnetbits" -Server
            if ($adscope -ne $null){
                Write-OSDLog -batchID $global:ImagingBatchID -resultType Informational -resultText "Found Scope: $scope/$subnetbits in Active Directory. Creating scope on DHCPServer: $dhcpserver"
                $range = Get-NetworkRange -IP $scope -Mask $subnetBits
                try {
                Add-DhcpServerv4Scope -ComputerName $dhcpserver -Name "$scope/$subnetbits" -StartRange $range[$range.count - 1] -EndRange $range[$range.count - 1] -State Active -Description "Created Automatically during server import" -SubnetMask $server.v_SubnetMask -LeaseDuration 00.08:00:00 -ErrorAction Stop
                } catch {
                    Write-OSDLog -batchID $global:ImagingBatchID -resultType Error -resultText "Error creating new DHCP Scope: $scope/$subnetbits on DHCP Server: $dhcpserver. This is normally due to a scope overlap"
                }
                Create-ReservationForPandoraRecord -scope $scope -dhcpserver $dhcpserver -serverName $server.v_imagedservername -serverMac $server.v_bemac -serverIP $server.v_beip
                
            } else {
                Write-OSDLog -batchID $global:ImagingBatchID -resultType Error -resultText "Scope: $scope/$subnetbits NOT found in Active Directory. Unable to create scope. Unable to create reservation for $($server.v_imagedservername)"
            }
         }        		
	}

    $logs = Read-SQL -server <server> -database Urmawm_Logging -query "select * from t_ServerImportLog where v_BatchID = '$($global:ImagingBatchID)'"

    $logs
}
	
main