# Prompt for the Hyper-V Server to use
$HyperVServer = "." 

# List the available physical network adapters
Write-Host "The following physical network adapters are avaiable for an external virtual network:" 

# Get the collection of external network adapters that are not associated with switches
$query = "Select * From Msvm_ExternalEthernetPort WHERE IsBound=False AND EnabledState=2"
$ExternalEthernetPorts = gwmi -namespace "root\virtualization" -Query $query -computername $HyperVServer 

# Output the elementname for each external nic
foreach ($Nic in $ExternalEthernetPorts)
{                
Write-Host $Nic.Name
}  

$i = 1
do {
# Keep on asking for a physical network adapter until we get a valid answer
$ExternalEthernetPort = $null
  
# Ask for the NIC name to use for the switch
$ExternalEthernetPortName = "Intel(R) 82576 Gigabit Dual Port Network Connection"

# Get the WMI object for the external nic
$query = "Select * From Msvm_ExternalEthernetPort WHERE Name='" + $ExternalEthernetPortName + "' AND IsBound=False AND EnabledState=2"
$ExternalEthernetPort = gwmi -namespace "root\virtualization" -Query $query -computername $HyperVServer

sleep 1
$i++
}
until ($i -gt 15)

# Get friendly name for the external virtual network switch (and internal ethernet port)
$SwitchFriendlyName = "Local Area Connection � Virtual Network"
$InternalEthernetPortFriendlyName = $SwitchFriendlyName 

# Set the friendly name for the internal switch port and external switch port
$InternalSwitchPortFriendlyName = "InternalSwitchPort"
$ExternalSwitchPortFriendlyName = "ExternalSwitchPort" 

# Generate GUIDs for the unique switch name, internal switch port name, internal ethernet port name and external switch port name
$SwitchName = [guid]::NewGuid().ToString()
$InternalSwitchPortName = [guid]::NewGuid().ToString()
$InternalEthernetPortName = [guid]::NewGuid().ToString()
$ExternalSwitchPortName = [guid]::NewGuid().ToString() 

# Setup some other values that will be used
$NumLearnableAddresses = 1024
$ScopeOfResidence = "" 

# Get the Msvm_VirtualSwitchManagementService WMI Object on the system we are going to be working with
$VirtualSwitchManagementService = gwmi Msvm_VirtualSwitchManagementService -namespace "root\virtualization" -computername $HyperVServer 

# Create a new switch with 1024 learnable addresses
$Result = $VirtualSwitchManagementService.CreateSwitch($SwitchName, $SwitchFriendlyName, $NumLearnableAddresses, $ScopeOfResidence)  

# Get the WMI object for the new switch out of the results
$Switch = [WMI]$Result.CreatedVirtualSwitch  

# Create Internal Switch Port 
$Result = $VirtualSwitchManagementService.CreateSwitchPort($Switch, $InternalSwitchPortName, $InternalSwitchPortFriendlyName, $ScopeOfResidence) 

# Get the WMI object for the new switch port out of the results
$InternalSwitchPort = [WMI]$Result.CreatedSwitchPort  

# Create External Switch Port 
$Result = $VirtualSwitchManagementService.CreateSwitchPort($Switch, $ExternalSwitchPortName, $ExternalSwitchPortFriendlyName, $ScopeOfResidence) 

# Get the WMI object for the new switch port out of the results
$ExternalSwitchPort = [WMI]$Result.CreatedSwitchPort  

# Pull it all together with a call to SeutpSwitch
$Result = $VirtualSwitchManagementService.SetupSwitch($ExternalSwitchPort, $InternalSwitchPort, $ExternalEthernetPort, $InternalEthernetPortName, $InternalEthernetPortFriendlyName) 

# Return success if the return value is "0"
if ($Result.ReturnValue -eq 0) 
{write-host "External virtual network created."}  
# If the return value is not "0" or "4096" then the operation failed
elseif ($Result.ReturnValue -ne 4096) 
{write-host "Failed to create external virtual network."} 
else   
{# Get the job object    
$job=[WMI]$Result.job
# Provide updates if the jobstate is "3" (starting) or "4" (running)
while ($job.JobState -eq 3 -or $job.JobState -eq 4) 
{write-host $job.PercentComplete
start-sleep 1
#Refresh the job object
$job=[WMI]$Result.job}
# A jobstate of "7" means success
if ($job.JobState -eq 7)
{write-host "External virtual network created."} 
else
{write-host "Failed to create external virtual network."
write-host "ErrorCode:" $job.ErrorCode
write-host "ErrorDescription" $job.ErrorDescription}
}