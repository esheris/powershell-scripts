﻿param(
    [ValidateNotNullOrEmpty()]
    [string]$dbuser = "build",
    [ValidateNotNullOrEmpty()]
    [string]$dbpass = "P@`$`$word"
)

#Global Variables
Import-Module DHCPServer
$sqlServer = "<server>"
$logfile = ".\CreateReservationsInQuarantine.txt"

function Get-DHCPServers(){
    try {
        $dhcpServers = Read-SQL -server <server> -database Root_Of_All_Evil -query "Select * from DHCP_Servers"
    } catch {
        return $null
    }
    Write $dhcpServers
}

function Write-OSDLog() {
	param(
		[Parameter(Mandatory=$true)]
        [ValidateSet('Informational','Warning','Error')]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText
	)
	begin {
        if (-not(Test-Path -Path $logfile)){ New-Item -ItemType File -Path ".\" -Name $logfile -Force }
		$LogOutput = [string]::Format("{0} : {1} : {2}",$(get-date).ToString(),$resultType,$resultText)
		
	}
	process {
		tee-object -InputObject $LogOutput -Variable tee			
		$tee | Out-File -Append -FilePath ".\Clean-InactiveDHCPReservations.txt" -Encoding ascii
	}		
}

Function ConvertTo-DottedDecimalIP {
  <#
    .Synopsis
      Returns a dotted decimal IP address from either an unsigned 32-bit integer or a dotted binary string.
    .Description
      ConvertTo-DottedDecimalIP uses a regular expression match on the input string to convert to an IP address.
    .Parameter IPAddress
      A string representation of an IP address from either UInt32 or dotted binary.
  #>
 
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [String]$IPAddress
  )
   
  Process {
    Switch -RegEx ($IPAddress) {
      "([01]{8}\.){3}[01]{8}" {
        Return [String]::Join('.', $( $IPAddress.Split('.') | ForEach-Object { [Convert]::ToUInt32($_, 2) } ))
      }
      "\d" {
        $IPAddress = [UInt32]$IPAddress
        $DottedIP = $( For ($i = 3; $i -gt -1; $i--) {
          $Remainder = $IPAddress % [Math]::Pow(256, $i)
          ($IPAddress - $Remainder) / [Math]::Pow(256, $i)
          $IPAddress = $Remainder
         } )
        
        return [String]::Join('.', $DottedIP)
      }
      default {
        Write-Error "Cannot convert this format"
      }
    }
  }
}

Function ConvertTo-Mask {   
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [Alias("Length")]
    [ValidateRange(0, 32)]
    $MaskLength
  )
   
  Process {
    Write ConvertTo-DottedDecimalIP ([Convert]::ToUInt32($(("1" * $MaskLength).PadRight(32, "0")), 2))
  }
}

Function Get-NetworkRange( [String]$IP, [String]$Mask ) {
  If ($IP.Contains("/"))
  {
    $Temp = $IP.Split("/")
    $IP = $Temp[0]
    $Mask = $Temp[1]
  }
 
  If (!$Mask.Contains("."))
  {
    $Mask = ConvertTo-Mask $Mask
  }
 
  $DecimalIP = ConvertTo-DecimalIP $IP
  $DecimalMask = ConvertTo-DecimalIP $Mask
   
  $Network = $DecimalIP -BAnd $DecimalMask
  $Broadcast = $DecimalIP -BOr ((-BNot $DecimalMask) -BAnd [UInt32]::MaxValue)
 
  For ($i = $($Network + 1); $i -lt $Broadcast; $i++) {
    ConvertTo-DottedDecimalIP $i
  }
}

Function ConvertTo-DecimalIP {   
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [Net.IPAddress]$IPAddress
  )
 
  Process {
    $i = 3; $DecimalIP = 0;
    $IPAddress.GetAddressBytes() | ForEach-Object { $DecimalIP += $_ * [Math]::Pow(256, $i); $i-- }
 
    return [UInt32]$DecimalIP
  }
}

Function Get-ScopeFromIpAndSubnet(){
param(
    [System.net.Ipaddress]$IpAddress,
    [System.Net.IPAddress]$Subnet
)
    $DecimalIP = ConvertTo-DecimalIP -IPAddress $IpAddress
    $DecimalNetmask = ConvertTo-DecimalIP -IPAddress $Subnet
    $DecimalNetwork = $DecimalIP -band $DecimalNetmask
    $Network = ConvertTo-DottedDecimalIP -IPAddress $DecimalNetwork
    return $Network
}
function Get-SqlConnection(){
param(
    [string]$server,
    [string]$database
)
    $connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = $dbuser
	$connString.psbase.Password = $dbpass
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database

	$conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    Write $conn
}
function Read-SQL {
    Param (
        [string]$server, 
        [string]$query, 
        [string]$database
    )

	$conn = Get-SqlConnection -server $server -database $database
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $table = New-Object System.Data.DataTable
	$table.load($sqlCmd.ExecuteReader())
	$conn.Close()
	Write $table
}

function Write-Sql {
    Param (
        [string]$server, 
        [string]$query, 
        [string]$database
    )
        
    $conn = Get-SqlConnection -server $server -database $database
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $result = $sqlCmd.ExecuteNonQuery()
    Write $result
}

function Get-DHCPServers(){
    try {
        $dhcpServers = Read-SQL -server <server> -database Root_Of_All_Evil -query "Select * from DHCP_Servers"
    } catch {
        return $null
    }
    Write $dhcpServers
}

function Create-Reservations(){
    param(
        [System.Collections.Generic.List[object]]$reservationInformation
    )
    $dhcpServers = Get-DHCPServers
    foreach ($res in $reservationInformation){
        $rack = (GetInfoFromPandora -mac $res.MacAddress.Replace(":","")).v_Rack
        if ($rack -eq $null){continue}
        $colocation = $rack.substring(0,3)
        $dhcpServer = ($dhcpServers | where {$_.v_Colocation -eq $colocation} | Get-Random).v_DHCPServer
        $dhcpScope = Get-ScopeFromIpAndSubnet -IpAddress $res.IPAddress -Subnet $res.Subnet
        $body = [string]::format("IPAddress={0}&MacAddress={1}&DhcpServer={2}&DhcpScope={3}&ServerName={4}",$res.IPAddress,$res.MacAddress,$dhcpServer,$dhcpScope,$env:COMPUTERNAME);
        try {
            Invoke-WebRequest -Uri "http://urmawmwebservice/api/DHCPReservation/PostNewDHCPReservation" -Method Post -Body $body -ErrorAction Stop 
            $query = [string]::format("Update pandoras_box set v_machinestatus='Successfully Created DHCP Reservation' where v_bemac='{0}'",$res.MacAddress.Replace(":",""))
            Write-Sql -server $sqlServer -database "Root_Of_All_Evil" -query $query
        } catch {
            $query = [string]::format("Update pandoras_box set v_machinestatus='Failed to create Reservation: Scope {1}, IP {2}' where v_bemac='{0}'",$res.MacAddress.Replace(":",""))
            Write-Sql -server $sqlServer -database "Root_Of_All_Evil" -query $query
        }  
    }
}

function GetInfoFromPandora(){
    param(
        [string]$mac
    )
    begin{
        $query = [string]::format("Select v_Rack as [Rack] from Pandoras_box where v_bemac = '{0}'",$mac)
    }
    process{
        $results = Read-SQL -server $sqlServer -database "Root_of_all_evil" -query $query
    }
    end {
        Write $results
    }
}

function Create-IPObject(){
param(
    $nicconfig
)
    $object = New-Object PSObject
    $ipcount = $nicconfig.IPAddress | Measure-Object
    if ($ipcount -eq 1){
        $object | Add-Member -MemberType NoteProperty -Name IPAddress -Value $nicconfig.IPAddress
    } else {
        $ip = ($nicconfig.IPAddress -match "\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b")[0]
        $object | Add-Member -MemberType NoteProperty -Name IPAddress -Value $ip
    }
    $subnetCount = $nicconfig.IPSubnet | Measure-Object
    if ($subnetCount -eq 1){
        $object | Add-Member -MemberType NoteProperty -Name Subnet -Value $nicconfig.IPSubnet
    } else {
        $subnet = ($nicconfig.IPSubnet -match "\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b")[0]
        $object | Add-Member -MemberType NoteProperty -Name Subnet -Value $subnet
    }
    $object | Add-Member -MemberType NoteProperty -Name MacAddress -Value $nicconfig.MacAddress
    $ipList.Add($object)
}


$ipList = new-object 'System.Collections.Generic.List[object]'
$ipNics = Get-WmiObject -Class Win32_networkadapterconfiguration -Filter "IPEnabled='true'"
$nicCount = ($ipNics | measure-object).Count
if ($nicCount -gt 0){
    if ($nicCount -eq 1){
        Create-IPObject -nicconfig $ipNics        
    } else {
        foreach ($nic in $ipNics){
            Create-IPObject -nicconfig $ipNics            
        }
    }
    Create-Reservations -reservationInformation $ipList
    return 999
} else {
    return 666
}

