﻿param(
    [ValidateNotNullOrEmpty()]
    [string]$dbuser = "build",
    [ValidateNotNullOrEmpty()]
    [string]$dbpass = "P@`$`$word"
)

#Global Variables
Import-Module DHCPServer
$smsProvider = "<server>"
$sqlServer = "<server>"
$logfile = ".\Create-ReservationsFor1_Disk.txt"
$SCCMCollectionToProcess = "1_Disk_Configuration"

if (Test-Path -Path $logfile){ Remove-Item -Path $logfile -Force }


function GetCollectionMembership(){
    param(
        [string]$collectionName
    )
    begin{
    #Translate Collection Name to ID
        $collectionID = (Get-CimInstance -Namespace root\sms\site_xbl -ClassName sms_collection -ComputerName $smsProvider -Filter "Name='$collectionName'").CollectionID
    }
    process{
        $serverlist = Get-CimInstance -Namespace root\sms\site_xbl -ClassName sms_cm_res_coll_$collectionID -ComputerName $smsProvider | select Name
    }
    end {
        Write $serverlist
    }
}

function Write-OSDLog() {
	param(
		[Parameter(Mandatory=$true)]
        [ValidateSet('Informational','Warning','Error')]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText
	)
	begin {
        if (-not(Test-Path -Path $logfile)){ New-Item -ItemType File -Path ".\" -Name $logfile -Force }
		$LogOutput = [string]::Format("{0} : {1} : {2}",$(get-date).ToString(),$resultType,$resultText)
		
	}
	process {
		tee-object -InputObject $LogOutput -Variable tee			
		$tee | Out-File -Append -FilePath $logfile -Encoding ascii
	}		
}

Function ConvertTo-DottedDecimalIP {
  <#
    .Synopsis
      Returns a dotted decimal IP address from either an unsigned 32-bit integer or a dotted binary string.
    .Description
      ConvertTo-DottedDecimalIP uses a regular expression match on the input string to convert to an IP address.
    .Parameter IPAddress
      A string representation of an IP address from either UInt32 or dotted binary.
  #>
 
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [String]$IPAddress
  )
   
  Process {
    Switch -RegEx ($IPAddress) {
      "([01]{8}\.){3}[01]{8}" {
        Return [String]::Join('.', $( $IPAddress.Split('.') | ForEach-Object { [Convert]::ToUInt32($_, 2) } ))
      }
      "\d" {
        $IPAddress = [UInt32]$IPAddress
        $DottedIP = $( For ($i = 3; $i -gt -1; $i--) {
          $Remainder = $IPAddress % [Math]::Pow(256, $i)
          ($IPAddress - $Remainder) / [Math]::Pow(256, $i)
          $IPAddress = $Remainder
         } )
        
        return [String]::Join('.', $DottedIP)
      }
      default {
        Write-Error "Cannot convert this format"
      }
    }
  }
}

Function ConvertTo-Mask {   
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [Alias("Length")]
    [ValidateRange(0, 32)]
    $MaskLength
  )
   
  Process {
    return ConvertTo-DottedDecimalIP ([Convert]::ToUInt32($(("1" * $MaskLength).PadRight(32, "0")), 2))
  }
}

Function Get-NetworkRange( [String]$IP, [String]$Mask ) {
  If ($IP.Contains("/"))
  {
    $Temp = $IP.Split("/")
    $IP = $Temp[0]
    $Mask = $Temp[1]
  }
 
  If (!$Mask.Contains("."))
  {
    $Mask = ConvertTo-Mask $Mask
  }
 
  $DecimalIP = ConvertTo-DecimalIP $IP
  $DecimalMask = ConvertTo-DecimalIP $Mask
   
  $Network = $DecimalIP -BAnd $DecimalMask
  $Broadcast = $DecimalIP -BOr ((-BNot $DecimalMask) -BAnd [UInt32]::MaxValue)
 
  For ($i = $($Network + 1); $i -lt $Broadcast; $i++) {
    ConvertTo-DottedDecimalIP $i
  }
}

Function ConvertTo-DecimalIP {   
  [CmdLetBinding()]
  Param(
    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
    [Net.IPAddress]$IPAddress
  )
 
  Process {
    $i = 3; $DecimalIP = 0;
    $IPAddress.GetAddressBytes() | ForEach-Object { $DecimalIP += $_ * [Math]::Pow(256, $i); $i-- }
    $DecimalIP = [UInt32]$DecimalIP
    return $DecimalIP
  }
}

Function Get-ScopeFromIpAndSubnet(){
param(
    [System.net.Ipaddress]$IpAddress,
    [System.Net.IPAddress]$Subnet
)
    $DecimalIP = ConvertTo-DecimalIP -IPAddress $IpAddress
    $DecimalNetmask = ConvertTo-DecimalIP -IPAddress $Subnet
    $DecimalNetwork = $DecimalIP -band $DecimalNetmask
    $Network = ConvertTo-DottedDecimalIP -IPAddress $DecimalNetwork
    return $Network
}
function Get-SqlConnection(){
param(
    [string]$server,
    [string]$database
)
    $connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = $dbuser
	$connString.psbase.Password = $dbpass
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database

	$conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    Write $conn
}
function Read-SQL {
    Param (
        [string]$server, 
        [string]$query, 
        [string]$database
    )

	$conn = Get-SqlConnection -server $server -database $database
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $table = New-Object System.Data.DataTable
	$table.load($sqlCmd.ExecuteReader())
	$conn.Close()
	Write $table
}

function Write-Sql {
    Param (
        [string]$server, 
        [string]$query, 
        [string]$database
    )
        
    $conn = Get-SqlConnection -server $server -database $database
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $result = $sqlCmd.ExecuteNonQuery()
    Write $result
}

function GetInfoFromPandora(){
    param(
        [string]$serverName
    )
    begin{
        $query = [string]::format("Select v_Rack as [Rack], v_BEMac as [MacAddress], v_BEIP as [IP], v_besubnet as [Subnet], v_begateway as [Gateway] from Pandoras_box where v_imagedservername = '{0}'",$serverName)
    }
    process{
        $results = Read-SQL -server $sqlServer -database "Root_of_all_evil" -query $query
    }
    end {
        Write $results
    }
}

function Get-DHCPServers(){
    try {
        $dhcpServers = Read-SQL -server <server> -database Root_Of_All_Evil -query "Select * from DHCP_Servers"
    } catch {
        return $null
    }
    Write $dhcpServers
}

function Main(){
    Write-OSDLog -resultType Informational -resultText "---------- Beginning Run of Create-Reservations.ps1 ----------"
    $dhcpServers = Get-DHCPServers
    $servers = GetCollectionMembership -collectionName $SCCMCollectionToProcess
    Write-OSDLog -resultType Informational -resultText "Processing $(($servers | measure-object).Count) Servers from $SCCMCollectionToProcess"
    foreach ($server in $servers){
        Write-OSDLog -resultType Informational -resultText "----------Processing $($server.Name)----------"
        $serverInformation = GetInfoFromPandora -serverName $server.Name
        $serverInformation | ConvertTo-Csv -NoTypeInformation | Out-File -FilePath $logfile -Encoding ascii -Append
        $colocation = $serverInformation.Rack.SubString(0,3)
        $dhcpServer = ($dhcpServers | where {$_.v_Colocation -eq $colocation} | Get-Random).v_DHCPServer
        Write-OSDLog -resultType Informational -resultText "Picked $dhcpServer in $colocation to perform actions against"
        $scope = Get-ScopeFromIpAndSubnet -IpAddress $serverInformation.IP -Subnet $serverInformation.Subnet
        Write-OSDLog -resultType Informational -resultText "Found that Servers IP $($serverInformation.ip) exists in Scope $scope"
        try {
            Write-OSDLog -resultType Informational -resultText "Attempting to create reservation  in Scope: $($scope) for IP: $($serverInformation.IP) and Mac Address: $($serverInformation.MacAddress)"
            Add-DhcpServerv4Reservation -ComputerName $dhcpServer -ScopeId $scope -IPAddress $serverInformation.IP -ClientId $serverInformation.MacAddress -Name $server.Name -Type Both -ErrorAction Stop
            $query = "Update Pandoras_Box Set v_machinestatus = 'Created Reservation for IP: $($serverInformation.IP).' where v_bemac = '$($serverInformation.MacAddress)'"
            Write-Sql -server $sqlServer -database "Root_of_All_Evil" -query $query
        }
        catch {
            Write-OSDLog -resultType Warning -resultText "Failed to create reservation. This normally happens when either a reservation already exists for the IP in the scope or the Mac is associated with a different IP"
            $existingReservation = Get-DhcpServerv4Reservation -ComputerName $dhcpServer -ScopeId $scope | where {$_.IPAddress -eq $serverInformation.IP}
            if ($existingReservation -eq $null){
                Write-OSDLog -resultType Warning -resultText "No Existing Reservation found for the IP address Looking for a Lease"
                for ($i=0;$i -lt 6; $i++){
                    try {
                        $existingLease = Get-DHCPServerv4Lease -ComputerName $dhcpserver -ScopeId $scope -ClientId $($serverInformation.MacAddress) -ErrorAction Stop
                        break
                    } catch { continue }
                }
            } else {
                Write-OSDLog -resultType Warning -resultText "Found existing reservation for $($serverInformation.IP), finding the associate lease"
                $existingLease = Get-DhcpServerv4Lease -ComputerName $dhcpServer -ScopeId $scope -ClientId $existingReservation.ClientID
            }
            if ($existingLease.AddressState -eq "InactiveReservation") {
                Write-OSDLog -resultType Warning -resultText "Found Active lease marked as InactiveReservation, Deleteing"
                Remove-dhcpserverv4Reservation -ComputerName $dhcpServer -ScopeId $scope -ClientId $existingLease.ClientID 
                Write-OSDLog -resultType Informational -resultText "Attempting to create reservation  in Scope: $($scope) for IP: $($serverInformation.IP) and Mac Address: $($serverInformation.MacAddress)"
                Add-DhcpServerv4Reservation -ComputerName $dhcpServer -ScopeId $scope -IPAddress $serverInformation.IP -ClientId $serverInformation.MacAddress -Name $server.Name -Type Both -ErrorAction Stop | Out-Null
                $query = "Update Pandoras_Box Set v_machinestatus = 'Created Reservation for IP: $($serverInformation.IP).' where v_bemac = '$($serverInformation.MacAddress)'"
                Write-Sql -server $sqlServer -database "Root_of_All_Evil" -query $query
            } else {
                Write-OSDLog -resultType Error -resultText "Found Active Lease for IP: $($serverInformation.IP)"
                $query = "Update Pandoras_Box Set v_machinestatus = 'Unable to create Reservation for IP: $($serverInformation.IP). An existing ACTIVE LEASE was found for a different machine' where v_bemac = '$($serverInformation.MacAddress)'"
                Write-Sql -server $sqlServer -database "Root_of_All_Evil" -query $query
            }
        }
        Write-OSDLog -resultType Informational -resultText "Invoking Replication of scope $scope"
        Invoke-DhcpServerv4FailoverReplication -ComputerName $dhcpServer -ScopeId $scope -Force | Out-Null
    }
}
Main