﻿# ------------------------------------------------------------------------------
# Create VM Template Wizard Script
# ------------------------------------------------------------------------------
# Script generated on Tuesday, July 10, 2012 11:14:00 PM by Virtual Machine Manager
# 
# For additional help on cmdlet usage, type get-help <cmdlet name>
# ------------------------------------------------------------------------------


New-SCVirtualScsiAdapter -VMMServer <server> -JobGroup d4582fdb-aef3-4db9-b9fe-722b0f7eaabf -AdapterID 7 -ShareVirtualScsiAdapter $false -ScsiControllerType DefaultTypeNoType 


New-SCVirtualDVDDrive -VMMServer <server> -JobGroup d4582fdb-aef3-4db9-b9fe-722b0f7eaabf -Bus 1 -LUN 0 


New-SCVirtualNetworkAdapter -VMMServer <server> -JobGroup d4582fdb-aef3-4db9-b9fe-722b0f7eaabf -MACAddressType Dynamic -VLanEnabled $false -EnableVMNetworkOptimization $false -EnableMACAddressSpoofing $false -IPv4AddressType Dynamic -IPv6AddressType Dynamic 


Set-SCVirtualCOMPort -NoAttach -VMMServer <server> -GuestPort 1 -JobGroup d4582fdb-aef3-4db9-b9fe-722b0f7eaabf 


Set-SCVirtualCOMPort -NoAttach -VMMServer <server> -GuestPort 2 -JobGroup d4582fdb-aef3-4db9-b9fe-722b0f7eaabf 


Set-SCVirtualFloppyDrive -RunAsynchronously -VMMServer <server> -NoMedia -JobGroup d4582fdb-aef3-4db9-b9fe-722b0f7eaabf 

$CPUType = Get-CPUType -VMMServer <server> | where {$_.Name -eq "3.60 GHz Xeon (2 MB L2 cache)"}


New-SCHardwareProfile -VMMServer <server> -CPUType $CPUType -Name "Profile349b32df-03f9-4511-a7f2-ea90c88dee82" -Description "Profile used to create a VM/Template" -CPUCount 4 -MemoryMB 2048 -DynamicMemoryEnabled $true -DynamicMemoryMaximumMB 16384 -DynamicMemoryBufferPercentage 20 -MemoryWeight 5000 -VirtualVideoAdapterEnabled $false -CPUExpectedUtilizationPercent 20 -DiskIops 0 -CPUMaximumPercent 100 -CPUReserve 0 -NetworkUtilizationMbps 0 -CPURelativeWeight 100 -HighlyAvailable $false -NumLock $false -BootOrder "IdeHardDrive", "PxeBoot", "CD", "Floppy" -CPULimitFunctionality $false -CPULimitForMigration $false -JobGroup d4582fdb-aef3-4db9-b9fe-722b0f7eaabf 


$VirtualHardDisk = Get-SCVirtualHardDisk -VMMServer <server> | where {$_.Location -eq "\\<server>\MSVMMLibrary\VHDs\04.09.12\SCCMVBASEINHR2_disk_1.vhd"} | where {$_.HostName -eq "<server>"}

New-SCVirtualDiskDrive -VMMServer <server> -IDE -Bus 0 -LUN 0 -JobGroup d4582fdb-aef3-4db9-b9fe-722b0f7eaabf -VirtualHardDisk $VirtualHardDisk -VolumeType BootAndSystem 

$HardwareProfile = Get-SCHardwareProfile -VMMServer <server> | where {$_.Name -eq "Profile349b32df-03f9-4511-a7f2-ea90c88dee82"}
$GuestOSProfile = Get-SCGuestOSProfile -VMMServer <server> | where {$_.Name -eq "2008 R2 Default"}

$AnswerFile = Get-Script -VMMServer <server> | where {$_.SharePath -eq "\\<server>\MSVMMLibrary\ApplicationFrameworks\unattend.xml"}
$OperatingSystem = Get-SCOperatingSystem -ID a4959488-a31c-461f-8e9a-5187ef2dfb6b | where {$_.Name -eq "64-bit edition of Windows Server 2008 R2 Enterprise"}

$template = New-SCVMTemplate -Name "2008-R2-INH_04.12" -RunAsynchronously -HardwareProfile $HardwareProfile -GuestOSProfile $GuestOSProfile -JobGroup d4582fdb-aef3-4db9-b9fe-722b0f7eaabf -ComputerName "*" -FullName "" -OrganizationName "" -TimeZone 91 -Workgroup "WORKGROUP"  -AnswerFile $AnswerFile -MergeAnswerFile $false -OperatingSystem $OperatingSystem 


