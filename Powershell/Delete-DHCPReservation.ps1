﻿param(
    [ValidateNotNullOrEmpty()]
    [int]$PandoraID
)


if ((Get-Module -Name DhcpServer) -eq $null){
    try {
        import-module -Name DHCPServer -ErrorAction Stop
    } catch {
        "Error loading DHCP Module"
    }
}

function Read-SQL {
    Param (
        [string]$server,
        [string]$query,
        [string]$database
    )

	$connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = <user>
	$connString.psbase.Password = <password>
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database

	$conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $table = New-Object System.Data.DataTable
	$table.load($sqlCmd.ExecuteReader())
	$conn.Close()
	return $table
}

function Get-PandoraRecord()
{
    param(
        [int]$PandoraID
    )
    $result = Read-SQL -server "<server>" -database "Root_Of_All_Evil" -query
}
