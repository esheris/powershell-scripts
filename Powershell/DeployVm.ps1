# ------------------------------------------------------------------------------
# Create Virtual Machine Wizard Script
# ------------------------------------------------------------------------------
# Script generated on Monday, May 21, 2012 7:29:05 PM by Virtual Machine Manager
#
# For additional help on cmdlet usage, type get-help <cmdlet name>
# ------------------------------------------------------------------------------
param([string]$serverName,[string]$hostServer = "",[switch]$dualNic)
if ((get-module) -match "VirtualMachineM"){Remove-Module -Name VirtualMachineManager }
Import-Module -Name VirtualMachineManager

if ($hostServer -eq ""){
	$hostServers = New-Object System.Collections.ArrayList
	Write-Host "Select a VM Host" -foregroundColor "Green"
	$hosts = Get-SCVMHost -VMMServer <server> | where {$_.ComputerState -eq "Responding"}
	foreach ($vmhost in $hosts){
		$hostServers.add($vmhost.ComputerName) | out-null
	}
	$hostServers.Sort()
	for ($i=0; $i -le $hostServers.Count - 1; $i++){
		[String]::Format("`t{0}: {1}", $i,$hostServers.Item($i))
	}
	$selectedHost = Read-host "Select a Server:"
	$hostServer = $hostServers.item($selectedHost)
	write-host "$hostServer has been selected" -foregroundcolor "Yellow"
}

$jobGroup = [guid]::NewGuid()

New-SCVirtualScsiAdapter -VMMServer <server> -JobGroup $jobGroup -AdapterID 7 -ShareVirtualScsiAdapter $false -ScsiControllerType DefaultTypeNoType


New-SCVirtualDVDDrive -VMMServer <server> -JobGroup $jobGroup -Bus 1 -LUN 0

$hostAdapters = Get-SCVMHostNetworkAdapter -VMHost $hostServer | where {$_.LogicalNetworks -ne $null}
$adapters = New-Object System.Collections.ArrayList
if ($hostAdapters.Count -gt 1){
	Write-Host "Select a Logical Network for the Back End Network" -foregroundcolor "Green"
	foreach ($ln in $hostAdapters){
		$logicalNetworks = $ln.LogicalNetworks
		$i = 1
		foreach ($logicalNetwork in $logicalNetworks){
			$adapters.add($logicalNetwork.Name) | out-null
		}
	}
	for ($i=0; $i -le $adapters.Count � 1; $i++){
		[String]::Format("`t{0}: {1}",$i,$adapters.Item($i))
	}
	$selection = read-host "Select a Logical Network:"
	$BElogicalNetwork = $adapters.Item($selection)
	write-host "$logicalNetwork has been selected" -foregroundcolor "Yellow"

} else {
	$BElogicalNetwork = $hostAdapters.LogicalNetworks[0].Name
}

New-SCVirtualNetworkAdapter -VMMServer <server> -JobGroup $jobGroup -MACAddressType Dynamic -VLanEnabled $false -EnableVMNetworkOptimization $false -EnableMACAddressSpoofing $false -IPv4AddressType Dynamic -IPv6AddressType Dynamic -logicalNetwork $BElogicalNetwork

if ($dualnic){
$feAdapter = New-Object System.Collections.ArrayList
if ($hostAdapters.Count -gt 1){
	Write-Host "Select a Logical Network for the Front End Network"
	foreach ($ln in $hostAdapters){
		$logicalNetworks = $ln.LogicalNetworks
		$i = 1
		foreach ($logicalNetwork in $logicalNetworks){
			$feAdapter.add($logicalNetwork.Name) | out-null
		}
	}
	for ($i=0; $i -le $feadapters.count � 1; $i++){
		[String]::Format("{0}: {1}",$i,$feadapters.item($i))
	}
	$selection = read-host "Select a Front End Network"
	$FElogicalNetwork = $feAdapter.item($selection)


} else {
	$FElogicalNetwork = $hostAdapters.LogicalNetworks[0].Name
}
New-SCVirtualNetworkAdapter -VMMServer <server> -JobGroup $jobGroup -MACAddressType Dynamic -VLanEnabled $false -EnableVMNetworkOptimization $false -EnableMACAddressSpoofing $false -IPv4AddressType Dynamic -IPv6AddressType Dynamic -logicalNetwork $FElogicalNetwork
}

Set-SCVirtualCOMPort -NoAttach -VMMServer <server> -GuestPort 1 -JobGroup $jobGroup


Set-SCVirtualCOMPort -NoAttach -VMMServer <server> -GuestPort 2 -JobGroup $jobGroup


Set-SCVirtualFloppyDrive -RunAsynchronously -VMMServer <server> -NoMedia -JobGroup $jobGroup

$CPUType = Get-CPUType -VMMServer <server> | where {$_.Name -eq "3.60 GHz Xeon (2 MB L2 cache)"}

$CapabilityProfile = Get-SCCapabilityProfile -VMMServer <server> | where {$_.Name -eq "Hyper-V"}

New-SCHardwareProfile -VMMServer <server> -CPUType $CPUType -Name "Profile$jobGroup" -Description "Profile used to create a VM/Template" -CPUCount 4 -MemoryMB 2048 -DynamicMemoryEnabled $true -DynamicMemoryMaximumMB 16384 -DynamicMemoryBufferPercentage 20 -MemoryWeight 5000 -VirtualVideoAdapterEnabled $false -CPUExpectedUtilizationPercent 20 -DiskIops 0 -CPUMaximumPercent 100 -CPUReserve 0 -NetworkUtilizationMbps 0 -CPURelativeWeight 100 -HighlyAvailable $false -NumLock $false -BootOrder "CD", "IdeHardDrive", "PxeBoot", "Floppy" -CPULimitFunctionality $false -CPULimitForMigration $false -CapabilityProfile $CapabilityProfile -JobGroup $jobGroup


$Template = Get-SCVMTemplate -VMMServer <server> -All | where {$_.Name -eq "2008-R2-INH-MGMT"}
$HardwareProfile = Get-SCHardwareProfile -VMMServer <server> | where {$_.Name -eq "Profile$jobGroup"}

$AnswerFile = Get-Script -VMMServer <server> | where {$_.SharePath -eq "\\<server>\MSVMMLibrary\ApplicationFrameworks\unattend.xml"}
$OperatingSystem = Get-SCOperatingSystem -ID a4959488-a31c-461f-8e9a-5187ef2dfb6b | where {$_.Name -eq "64-bit edition of Windows Server 2008 R2 Enterprise"}

New-SCVMTemplate -Name "Temporary Template$jobGroup" -Template $Template -HardwareProfile $HardwareProfile -JobGroup $jobGroup -ComputerName $serverName -TimeZone 91  -AnswerFile $AnswerFile -MergeAnswerFile $false -OperatingSystem $OperatingSystem



$template = Get-SCVMTemplate -All | where { $_.Name -eq "Temporary Template$jobGroup" }
$virtualMachineConfiguration = New-SCVMConfiguration -VMTemplate $template -Name $serverName
Write-Output $virtualMachineConfiguration
$vmHost = Get-SCVMHost -ComputerName "$hostServer"
Set-SCVMConfiguration -VMConfiguration $virtualMachineConfiguration -VMHost $vmHost
Update-SCVMConfiguration -VMConfiguration $virtualMachineConfiguration
Update-SCVMConfiguration -VMConfiguration $virtualMachineConfiguration
New-SCVirtualMachine -Name $serverName -VMConfiguration $virtualMachineConfiguration -Description "" -BlockDynamicOptimization $false -StartVM -JobGroup $jobGroup -RunAsynchronously -StartAction "TurnOnVMIfRunningWhenVSStopped" -StopAction "SaveVM" -DelayStartSeconds "0"
