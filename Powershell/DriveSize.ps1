##################################################################################################################################################
## DriveSize.ps1 - Powershell Script to gather logical drive information on remote systems
##
## Written By Eric Sheris - Xbox Operations Center - LSGOI SME
##
## Maintenance:
## Date        By   Comments
## ----------  ---  ------------------------------------------------------------------------------------------------------------------------------
## 11/3/2010   EAS  Created
##
##################################################################################################################################################
$showHelp = @"
Purpose:
  This script will retrieve logical drive information for a given server
 
Usage:
    ./DriveSize server hostlist.txt [-?|-H|-Help]
 
Where:
    -? or -H             displays this help
 
Comments:
     From the PowerShell command-line, invoke the script, passing any combination
     of hostnames and filenames (text file(s) containing a hostname listing
     Any combinations and number of arguments can be used
     if an argument is a filename:
        Each line in the file will processed as a hostname
     if an argument is not a filename:
         The argument will processed as a hostname
 
Example:
     ./DriveSize server1 hostlist.txt server2 server3 
"@
################################################################
## Parameters: 
##   $computername - Name or IP of server to connect to
##   $namespace - Wmi Namespace to connect to, defaults to root\cimv2
##   $class - Wmi class to query
##   $timeout - Time(in seconds) to wait for Wmi to respond
##   $filter - Where clause for Wmi Query
##
## This is used instead of the built in Get-WmiObject because Get-WmiObject does not take
## a timeout parameter and can cause a script to hang indefinately
##
################################################################
Function Get-WmiCustom([string]$computername,[string]$namespace='root\cimv2',[string]$class,[int]$timeout=15,[string]$filter)
{
	$ConnectionOptions = new-object System.Management.ConnectionOptions
	$EnumerationOptions = new-object System.Management.EnumerationOptions 

	$timeoutseconds = new-timespan -seconds $timeout
	$EnumerationOptions.set_timeout($timeoutseconds) 

	$assembledpath = "\\" + $computername + "\" + $namespace
	#write-host $assembledpath -foregroundcolor yellow 

	$Scope = new-object System.Management.ManagementScope $assembledpath, $ConnectionOptions
	$Scope.Connect() 

	$querystring = "SELECT * FROM " + $class + " " + $filter
	#write-host $querystring 

	$query = new-object System.Management.ObjectQuery $querystring
	$searcher = new-object System.Management.ManagementObjectSearcher
	$searcher.set_options($EnumerationOptions)
	$searcher.Query = $querystring
	$searcher.Scope = $Scope 

	trap { $_ } $result = $searcher.get() 

	return $result
} # End Get-WmiCustom

################################################################
## Takes a string that is the server or IP to ping
## Returns True if the server responds to ping, else returns false
################################################################
function isOnline([string]$hostname)
{
    $erroractionpreference = "SilentlyContinue"
    $hostname = $hostname.trimend(".")
    $fqdn = [System.Net.Dns]::GetHostEntry($hostname).HostName
    $ping = new-object System.Net.NetworkInformation.Ping
    $reply = $ping.send($hostname)
    if ($reply.status -eq "Success"){
        return $true
    }
    else{
        return $false
    }
} # End isOnline

################################################################
## Parameters:
##   $server - Server name or Ip address to connect to
## 
## Tests if the server is online using the isOnline function
## If the server is online, use Get-WmiCustom function to connect to the root/cimv2 namespace and read from win32_logicaldisk where the drivetype is not 2 or 5
## $a is an expression used to calculate and format the table generated for output This can be written as 1 long line, but i have broken it out for readability.
## If isOnline returns false, the function writes output stating it was unable to connect to the server.
## 
################################################################
function Get-LogicalDriveInformation([string]$server)
{
    $status = isOnline $server
    if ($status){    
        $disks = Get-WmiCustom -class win32_logicaldisk -computername $server -timeout 2 | where {$_.DriveType -ne 2} | where {$_.DriveType -ne 5}
        $a = @{
                Expression={};
                Label=$server.ToUpper();
                width=15
            },
             @{
                Expression={
                    $_.DeviceID
                };
                Label="Drive";
                Width=12;
                Align="Center";
            },
            @{
                Expression={
                    $_.VolumeName
                };
                Label="Name";
                Width=12
            },
            @{
                Expression={
                    ($_.Size / 1gb).tostring("0.##")
                };
                Label="Size(GB)";
                Width=8;
                Align="Right";
            },
            @{
                Expression={
                    ($_.FreeSpace / 1gb).tostring("0.##")
                };
                Label="Free Space(GB)";
                Width=16;
                Align="Center";
            },
            @{
                Expression={
                    (($_.FreeSpace / $_.Size) * 100).ToString("0")
                };
                Label="% Free";
                Width=8;
                Align="Center";
            }
            
        $disks | format-table $a
        
    }
    else
    {
        write-host "Unable to connect to server: $server" -foregroundcolor "Red"
    }
} # End Get-LogicalDriveInformation

################################################################
## Main
################################################################

## check if any arguments have been passed
if ($args.count)
{
    foreach ($item in $args)
    {
        ## if any of the arguments are -h,-?, or -help, show the help file and exit
        if (($item.ToUpper() -eq "-H") -or ($item.ToUpper() -eq "-?") -or ($item.ToUpper() -eq "-help"))
        {
            $ShowHelp
            exit
        }
    }
}
## Check if any arguments have been passed
if ($args.count)
{
    ## Loop through all the args, checking if they are a filename or a server.
    foreach ($item in $args)
    {
        ## Check if any of the items are files
        if (test-path($item))
        {
            ## If any of the items are files, loop through the content of the file calling the Get-LogicalDriveInformation function for each item
            foreach ($server in get-content $item)
            {
                Get-logicalDriveInformation $server
            }
        }
        ## If the items are not a file, assume they are a server name or IP and call Get-LogicalDriveInformation for the arg
        else
        {
            Get-LogicalDriveInformation $item
        }
    }
}
## If no arguments have been passed, show the help file.
else
{
    $ShowHelp
    exit
}