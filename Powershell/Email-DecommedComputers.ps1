﻿function Read-SQL {
    Param (
        [ValidateNotNullOrEmpty()]
        [string]$server, 
        [ValidateNotNullOrEmpty()]
        [string]$query, 
        [ValidateNotNullOrEmpty()]
        [string]$database
    )

	$connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = <user>
	$connString.psbase.Password = "P@`$`$word"
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database

	$conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $table = New-Object System.Data.DataTable
	$table.load($sqlCmd.ExecuteReader())
	$conn.Close()
	return $table
}
$query = "Select ServerName from DecommissionedServers where DecomDate > DATEADD(Day,-1,getdate()) order by ServerName"

$DecommedComputers = Read-SQL -server <server> -query $query -database Root_of_all_evil

if ($DecommedComputers){
    if ($DecommedComputers.Count -gt 1){
        $body = "<table><tr><th>Server Name</th></tr>"
        foreach ($dc in $DecommedComputers){
           $body += "<tr><td>$($dc.ServerName)</td></tr>"
        }
        $body += "</table>"
        $junkvar = Send-Email -Server xmail -Subject "Servers Decommissioned in the previous 24 hours" -To "v-sbsers@microsoft.com" -From "LSDDecom@Microsoft.com" -MessageHTML $body
    } else {
        $body = "<table><tr><th>Server Name</th></tr><tr>$($DecommedComputers.ServerName)</tr></table>"
        $junkvar = Send-Email -Server xmail -Subject "Servers Decommissioned in the previous 24 hours" -To "v-sbsers@microsoft.com" -From "LSDDecom@microsoft.com" -MessageHTML $body
    }
}