$success = "Final status of script`r`n"
function Invoke-GPUpdate([string]$hostname = "."){
    $status = Invoke-WmiMethod -ComputerName $hostname -Path win32_process -Name create -ArgumentList "gpupdate /force /wait:0" 
    return $status            
}

################################################################
## Takes a string that is the server or IP to ping
## Returns True if the server responds to ping, else returns false
################################################################
function isOnline([string]$hostname)
{
    $erroractionpreference = "SilentlyContinue"
    $hostname = $hostname.trimend(".")
    $fqdn = [System.Net.Dns]::GetHostEntry($hostname).HostName
    $ping = new-object System.Net.NetworkInformation.Ping
    $reply = $ping.send($hostname)
    if ($reply.status -eq "Success"){
        return $true
    }
    else{
        return $false
    }
    $erroractionpreference = "Continue"
} # End isOnline

Function do-work([string]$hostname){
    if (isOnline($hostname)){
    try{
        $path = "\\" + $hostname + "\c$\ProgramData\Microsoft\Group Policy\History\*"
        if (Test-Path $path){
            Remove-item $path  -recurse
            $status = Invoke-GPUpdate($hostname)
            switch ($status.ReturnValue){
        "0" {
            $success = $success + "Successfully ran Gpupdate /force on $hostname`r`n" 
        }
        "2" {
            $success = $success + "Error running Gpupdate /force on $hostname: Access Denied`r`n"
        }
        "3" {
            $success = $success + "Error running Gpupdate /force on $hostname: Insufficient Privileges`r`n"
        }
        "8" {
            $success = $success + "Error running Gpupdate /force on $hostname: Unknown failure `r`nPlease log into the computer and run GPUpdate /force`r`n"
        }
        "9" {
            $success = $success + "Error running Gpupdate /force on $hostname: Path Not Found`r`n"
        }
        "21" {
            $success = $success + "Error running Gpupdate /force on $hostname: Invalid Parameter`r`n"
        }
    }
        }Else{
            $success = $success + "Can not find path: $path`r`n"
        }
    }catch{ 
        $success = $success + "Unable to remove files, stoping processing for this server: $hostname`r`n"
        break
    }
    }
}

if ($args.count)
{
    $i = 0
    ## Loop through all the args, checking if they are a filename or a server.
    foreach ($item in $args)
    {
        ## Check if any of the items are files
        if (test-path($item))
        {
            $j = 0
            $list = get-content $item
            ## If any of the items are files, loop through the content of the file calling the Get-LogicalDriveInformation function for each item
            foreach ($server in $list)
            {
                do-work $server
                $j ++
                Write-Progress -activity "Correcting 0KB file group policy issue" -status "Progress:" -percentcomplete ($j/$list.length*100) -id 2 -currentOperation "Servers in Files"
                
            }
        }
        ## If the items are not a file, assume they are a server name or IP and call Get-LogicalDriveInformation for the arg
        else
        {
            do-work $item
            $i ++
            Write-Progress -activity "Correcting 0KB file group policy issue" -status "Progress:" -percentcomplete ($i/$args.count*100) -id 1 -currentOperation "Single Servers"
            
        }
        
    }
}
else
{
    Write-Host "No Arguments were passed" -foregroundcolor Red
}
$success | tee-object output.txt