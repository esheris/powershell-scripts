#Jason Ochoa 4/10/10
#This script removes the blank line that may show up in Windows 2008 gateway config registry values when set by netsh.
#Network interfaces must be reset or system must be rebooted for changes to take effect.
$srv = "."
$key  = "SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Interfaces"
$type = [Microsoft.Win32.RegistryHive]::LocalMachine
$regKey = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey($type, $Srv)
$regKey = $regKey.OpenSubKey($key)

foreach ($sub in $regKey.GetSubKeyNames())
{
	$Interfaces = ($regkey.OpenSubKey($sub)).GetValue("DefaultGateway")
	
	if (($Interfaces | ?{$_ -eq ""}) -ne $null){
		$Interfaces = $Interfaces | ?{$_ -ne ""}		
		($regkey.OpenSubKey($sub, $true)).setValue("DefaultGateway",([string[]]($Interfaces)))
		write-host "$sub was modified for the following gateway: $interfaces"
	}
}
