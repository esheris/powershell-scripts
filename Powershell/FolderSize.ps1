function Select-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$Filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	If ($Show -eq "OK")
	{
		Return $objForm.FileName
	}
	Else
	{
		Return $null
	}
}

Function Get-WmiCustom([string]$computername,[string]$namespace='root\cimv2',[string]$class,[int]$timeout=15,[string]$filter)
{
	$ConnectionOptions = new-object System.Management.ConnectionOptions
	$EnumerationOptions = new-object System.Management.EnumerationOptions 

	$timeoutseconds = new-timespan -seconds $timeout
	$EnumerationOptions.set_timeout($timeoutseconds) 

	$assembledpath = "\\" + $computername + "\" + $namespace
	#write-host $assembledpath -foregroundcolor yellow 

	$Scope = new-object System.Management.ManagementScope $assembledpath, $ConnectionOptions
	$Scope.Connect() 

	$querystring = "SELECT * FROM " + $class + " " + $filter
	#write-host $querystring 

	$query = new-object System.Management.ObjectQuery $querystring
	$searcher = new-object System.Management.ManagementObjectSearcher
	$searcher.set_options($EnumerationOptions)
	$searcher.Query = $querystring
	$searcher.Scope = $Scope 

	trap { $_ } $result = $searcher.get() 

	return $result
}

function folderSize
{
    $file = Select-FileDialog -title "Select Server List" -Directory ".\" -filter "Text Files (.txt)|*.txt"
    $serverlist = get-content $file

    $drive = read-host "Please enter a drive letter"
    if ($drive.contains(":")){ $drive = $drive.replace(":","") }
    if ($drive.contains("\")){ $drive = $drive.replace("\","") }
    if (!($drive.contains("$"))) { $drive = $drive + "$" }    

    $path = read-host "please enter a path"

    $arrObj = @()
    foreach ($server in $serverlist)
    {
        $fso = new-object -com "Scripting.FileSystemObject"
        $folder = $fso.getfolder("\\$server\$drive\$path")
        $size = $folder.size
        $obj = new-object Object
        $obj | Add-Member Noteproperty "Name" -value $server
        $obj | Add-Member Noteproperty "Size" -value $size
        $arrObj = $arrObj + $obj
    }
    $a = @{Expression={($_.Name).padright(10)};Label="Server Name";width=30},@{Expression={($_.Size / 1gb).tostring("0.##").padleft(10)};Label="Folder Size(GB)";width=20}
    $arrObj | ft $a
}

function Get-LogicalDriveInformation
{
    param([string]$server)
    $disks = Get-WmiCustom -class win32_logicaldisk -computername $server -timeout 2 | where {$_.DriveType -ne 2} | where {$_.DriveType -ne 5}
    $a = @{Expression={$_.DeviceID};Label="Drive Letter"},@{Expression={$_.VolumeName};Label="Name"},@{Expression={($_.Size / 1gb).tostring("0.##")};Label="Size"},
    @{Expression={($_.FreeSpace / 1gb).tostring("0.##")};Label="Free Space"},@{Expression={(($_.FreeSpace / $_.Size) * 100).tostring(0)};Label="% Free"}
    $disks | format-table $a
}

write-host "Menu:"
write-host "1. Get Drive Information"
write-host "2. Get the size of a specific folder"
write=host ""
$option = read-host "Please enter a menu option"


