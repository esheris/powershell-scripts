<#
.SYNOPSIS
   <A brief description of the script>
.DESCRIPTION
   <A detailed description of the script>
.PARAMETER <paramName>
   <Description of script parameter>
.EXAMPLE
   <An example of using the script>
#>

# Main Script Area. This function is called at the bottom of the script to begin actual execution.
function main() {
	Write-Host "This script will gather information for submitting a bug for issues found with SCCM task sequences. It will gather information from the SCCM instance (error logs) and manifest information. If possible, it will also gather logs and information from the remote server.`n" -ForegroundColor green
	$servername = Read-Host "Please enter the server name to gather data against"
	$desc = Read-Host "Input a description of the problem"
	$blocking = Read-Host "What is impacted by this issue (e.g. All imaging, HP imaging, Dell Imaging, Blades...): "
	$manifestQuery = [String]::Format("Select * from Pandoras_box where v_imagedservername = '{0}'", $servername)
	$manifest = Return-ReaderResults -query $manifestQuery -server "<server>" -database "Root_Of_All_Evil"
	$manifestMac = $manifest.v_BeMAC
	

	if ( Test-Path $pwd\$servername) { Remove-Item -Force -Path $pwd\$servername\ -Recurse ; New-Item -ItemType directory -Name $servername -Path $pwd | Out-Null } else { New-Item -ItemType directory -Name $servername -Path $pwd | Out-Null }
	# verify we can connect to the server
	if (Test-Connection -ComputerName $servername -Quiet) {
		# Verify we can connect to WMI on the server	
		New-Item -ItemType directory -Name "ClientLogs" -Path $pwd\$servername | Out-Null
		Copy-Item -Path "\\$servername\c`$\Windows\SysWow64\CCM\Logs\*" -Destination $pwd\$servername\ClientLogs\ -Recurse -Force
		$ipinfo = get-wmiCustom -server $servername -class win32_NetworkAdapterConfiguration -timeout 2 -filter "Where IPEnabled='$true'" | fl *
	} else {
		$pingStatus = "Unable to connect to the server $servername"
	}
	$sccmServerLogsQuery = "select vadv.AdvertisementName,tes.Step, tes.ActionName, tes.GroupName, tes.ExitCode, tes.ActionOutput from v_TaskExecutionStatus as tes inner join v_R_System as vrs on tes.ResourceID = vrs.ResourceID inner join v_Advertisement as vadv on vadv.AdvertisementID = tes.AdvertisementID where ExecutionTime > DATEADD(hour,-2,getdate()) and vrs.Name0 = '$servername' order by ExecutionTime desc"
	$sccmServerLogs = Return-ReaderResults -query $sccmServerLogsQuery -server "<server>" -database "SMS_CEN"
	$collQuery = "select vcol.Name from v_FullCollectionMembership as fcm inner join v_Collection as vcol on vcol.CollectionID = fcm.CollectionID inner join v_R_System as vrs on vrs.ResourceID = fcm.ResourceID where vrs.Name0 ='$servername'"
	$sccmCollections = Return-ReaderResults -query $collQuery -server "<server>" -database "SMS_CEN"

	$outputFile = "$pwd\$servername\1_$servername.txt"
	$sccmTESlog = "$pwd\$servername\TES_$servername.txt"

	"Description of issue: " | Out-File -FilePath $outputFile -Encoding ASCII
	Out-File -InputObject $desc -FilePath $outputFile -Encoding ASCII -Append

	"What is impacted: " | Out-File -FilePath $outputFile -Encoding ASCII -Append
	Out-File -InputObject $blocking -FilePath $outputFile -Encoding ASCII -Append

	"Variables in manifest:" | Out-File -FilePath $outputFile -Encoding ASCII -Append
	Out-File -InputObject $manifest -FilePath $outputFile -Encoding ASCII -Append

	"Listing the WMI provided network adapter configuration for $servername: " | Out-File -FilePath $outputFile -Encoding ASCII -Append
	Out-File -InputObject $pingStatus -FilePath $outputFile -Encoding ASCII -Append 

	Out-File -InputObject $wmiStatus -FilePath $outputFile -Encoding ASCII -Append 

	Out-File -InputObject $ipinfo -FilePath $outputFile -Encoding ASCII -Append

	"Listing All Sccm Collections $servername is a member of: " | Out-File -FilePath $outputFile -Encoding ASCII -Append
	Out-File -InputObject $sccmCollections -FilePath $outputFile -Encoding ASCII -Append
	
	"Listing the last 2 hour of task execution status for $servername: " | Out-File -FilePath $sccmTESlog -Encoding ASCII -Append
	Out-File -InputObject $sccmServerLogs -FilePath $sccmTESlog -Encoding ASCII -Append
	
	Write-Host "Zipping folder $pwd\$servername to $pwd\$servername.zip"
	if ( Test-Path $pwd\$servername\$servername.zip) { Remove-Item -Force -Path $pwd\$servername.zip }
	dir $pwd\$servername\ | add-Zip $pwd\$servername.zip

}

# usage: dir c:\demo\files\*.* -Recurse | add-Zip c:\demo\myzip.zip
function Add-Zip
{
	param([string]$zipfilename)

	if(-not (test-path($zipfilename)))
	{
		set-content $zipfilename ("PK" + [char]5 + [char]6 + ("$([char]0)" * 18))
		(dir $zipfilename).IsReadOnly = $false	
	}
	
	$shellApplication = new-object -com shell.application
	$zipPackage = $shellApplication.NameSpace($zipfilename)
	
	foreach($file in $Input) 
	{ 
            $zipPackage.CopyHere($file.FullName)
            Start-sleep -milliseconds 500
	}
}

function Test-WmiNamespaceExists([string]$namespace,[string]$server)
{
	$exists = Get-WmiCustom -server $server -namespace "root" -class "__Namespace" -timeout 1 | where {$_.Name -eq $namespace}
	if ($exists -ne $null)
	{
		return $true
	}
	else
	{
		return $false
	}
}

function Get-WmiCustom([string]$server,[string]$namespace = 'root\cimv2',[string]$class,[int]$timeout = 15,[string]$filter)
{
	$ConnectionOptions = new-object System.Management.ConnectionOptions
	$EnumerationOptions = new-object System.Management.EnumerationOptions 

	$timeoutseconds = new-timespan -seconds $timeout
	$EnumerationOptions.set_timeout($timeoutseconds) 

	$assembledpath = "\\" + $server + "\" + $namespace
	#write-host $assembledpath -foregroundcolor yellow 

	$Scope = new-object System.Management.ManagementScope $assembledpath, $ConnectionOptions
	$Scope.Connect() 

	$querystring = "SELECT * FROM " + $class + " " + $filter
	#write-host $querystring 

	$query = new-object System.Management.ObjectQuery $querystring
	$searcher = new-object System.Management.ManagementObjectSearcher
	$searcher.set_options($EnumerationOptions)
	$searcher.Query = $querystring
	$searcher.Scope = $Scope 

	trap [Exception] { write-error "Error connecting to wmi on server: $server";break; } $result = $searcher.get() 

	return $result
} # End Get-WmiCustom

function Read-SQL {
	param ($server, $query, $database)
	$conn = new-object ('System.Data.SqlClient.SqlConnection')
	$connString = "Server=$server;Integrated Security=SSPI;Database=$database"
	$conn.ConnectionString = $connString
	$conn.Open()
	$sqlCmd = New-Object System.Data.SqlClient.SqlCommand
	$sqlCmd.CommandText = $query
	$sqlCmd.Connection = $conn
	$Rset = $sqlCmd.ExecuteReader()
	,$Rset ## The comma is used to create an outer array, which PS strips off automatically when returning the $Rset
}

function Return-ReaderResults {
	param ($query,$server, $database)
	$data = Read-SQL $server $query $database
	while ($data.read() -eq $true) {
		$max = $data.FieldCount -1
		$obj = New-Object Object
		for ($i = 0; $i -le $max; $i++) {
			$name = $data.GetName($i)
			$obj | Add-Member Noteproperty $name -value $data.GetValue($i)
		}
		$obj
	}
}

main