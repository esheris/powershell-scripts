<#
.SYNOPSIS
   <A brief description of the script>
.DESCRIPTION
   <A detailed description of the script>
.PARAMETER <paramName>
   <Description of script parameter>
.EXAMPLE
   <An example of using the script>
#>

param ($forest = "<domain>", $outfile = "AD-$forest.csv")


$header = "ComputerName,Domain,FQDN,LastAccountResetTime,LastLogonTime,logonCount,Status,AccountCreatedTime,LastAccountChangeTime,OS,OSVersion,ServicePack,ADPath"
$forest = "LDAP://DC=$($forest.Substring(0,$forest.IndexOf("."))),DC=$($forest.Substring($forest.indexOf(".") + 1))"
$forestRoot = New-Object System.DirectoryServices.DirectoryEntry $forest
$forestSearcher = New-Object System.DirectoryServices.DirectorySearcher

$forestSearcher.SearchRoot = $forestRoot
$forestSearcher.PageSize = 200
$forestSearcher.SearchScope = "subtree"
$forestSearcher.Filter = "(objectCategory=computer)"
$computerList = $forestSearcher.FindAll()

function Pause ($Message = "Press any key to continue...") {
	Write-Host -NoNewLine $Message
	$null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
	Write-Host ""
}

function Convert-Timestamp {
	param ($ticktock)
		try {
			$fulldate = [system.DateTime]::FromFileTimeUtc([string]$ticktock)
			$year = [string]$fulldate.year
			$month = ([string]$fulldate.month).Padleft(2,"0")
			$day = ([string]$fulldate.day).Padleft(2,"0")
			[string[]]$datestring = "$year$month$day"
	}
	catch {$datestring = "ERROR"}
	return $datestring
}

Out-File -Filepath $outfile -Encoding ascii -InputObject $header

foreach ($computer in $computerList)
{

	$uac = $computer.Properties.Item("userAccountControl")
	$uacMask = [system.Convert]::ToString([string]$uac,2)
	if ($uacMask[$uacMask.Length - 2] -eq "1") {$status = "Disabled"} else {$status = "Enabled"}
	$computerName = $computer.Properties.Item("name")
	write-host "$computerName"
	[string]$computerDNSHostName = $computer.Properties.Item("dnshostname")
	$computerDomain = $computerDNSHostName.Substring($computerDNSHostname.IndexOf(".") + 1)
	$computerpwdLastSet = Convert-Timestamp $computer.Properties.Item("pwdLastSet")
	$computerTimestamp = Convert-Timestamp $computer.Properties.Item("lastlogontimestamp")
	$createdTime = $computer.Properties.Item("whenCreated")
	$changedTime = $computer.Properties.Item("whenChanged")
	$logonCount = $computer.Properties.Item("logonCount")
	$OS = $computer.Properties.Item("operatingSystem") -replace ",",";"
	$OSVersion = $computer.Properties.Item("operatingSystemVersion") -replace ",",";"
	$servicePack = $computer.Properties.Item("operatingSystemServicePack") -replace ",",";"
	$ldapPath = $computer.Properties.Item("aDSPath") -replace ",","/"


	
	[string[]]$outString += "$computerName,$computerDomain,$computerDNSHostName,$computerpwdLastSet,$computerTimestamp,$logonCount,$status,$createdTime,$changedTime,$OS,$OSVersion,$servicePack,$ldapPath"
}
Out-File -Filepath $outfile -Append -Encoding ascii -InputObject $outString