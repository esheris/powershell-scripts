function ConvertFrom-Html {
  param(
    [String]$Html
  )

  $HtmlLines = $Html -split "<tr" | Where-Object { $_ -match '<td' }
  $Header = ($HtmlLines[0] -split '<td') -replace '[\w\s\d"=%:;\-]*>|</.*' | Where-Object { $_ -ne '' }

  for ($i = 1; $i -lt $HtmlLines.Count; $i++) {
    $Output = New-Object Object
    $Values = ($HtmlLines[$i] -split '<td') -replace '[\w\s\d"=%:;\-]*>|</.*|<a.+?>'
    for ($j = 1; $j -lt $Values.Count; $j++) {
      $Output | Add-Member NoteProperty $Header[$j - 1] -Value $Values[$j]
    }
    $Output
  }
}
$import = import-csv Prod.csv
$header = "Server,Asset,Serial,MAC,OutOfBand,Start Date,End Date,Days Left"
$header | out-file -filepath "warranty.csv" -encoding ascii
foreach ($server in $import){
$server.Server
$servicetag = $server.Serial 
$url = "http://support.dell.com/support/topics/global.aspx/support/my_systems_info/details?c=us&l=en&s=pub&~ck=anavml&servicetag=$servicetag"

$webclient = New-Object System.Net.WebClient
$string = $webclient.downloadString($url)

if ($String -match '<table[\w\s\d"=\d]*[\w\s\d"=%]*contract_table">.+?</table>') {
	if ($Matches -eq $null) {	
		$output = $server.Server + "," + $server.Asset + "," + $server.Serial + "," + $server.MAC + "," + $server.OutOfBand
		$output | out-file -filepath "warranty.csv" -append -encoding ascii
	} else {
		$Support = ConvertFrom-Html $Matches[0]
		$output = $server.Server + "," + $server.Asset + "," + $server.Serial + "," + $server.MAC + "," + $server.OutOfBand + "," + $Support[0]."Start Date" + "," + $Support[0]."End Date" + "," + $Support[0]."Days Left"
    	$output | out-file -filepath "warranty.csv" -append -encoding ascii
	}
} else {
	$output = $server.Server + "," + $server.Asset + "," + $server.Serial + "," + $server.MAC + "," + $server.OutOfBand
	$output | out-file -filepath "warranty.csv" -append -encoding ascii
	}
}