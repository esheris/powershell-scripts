Function QueryWmi ($computername)
{
        $ip1 = $null
        $ip2 = $null
        $nics = get-wmicustom -class win32_networkadapterconfiguration -namespace "root\cimv2" -computername $computername |? {$_.IpEnabled.compareto($false)}
        if ($nics.length -gt 1)
        {
            $ip1 = $nics[0].ipaddress
            $ip2 = $nics[1].ipaddress
        }
        else
        {
            $ip1 = $nics.ipaddress
        }
        
        
        # I modify this line depending on what i want to return that day
		$data = $computername + "," + $ip1 + "," + $ip2
		return $data
        
}

Function pingComputer ($computername)
{
	$ping = get-wmicustom -class win32_pingstatus -namespace "root\cimv2" -computername $computername -filter "Where Address='$computername'" | select-object statuscode
	return $ping
}

Function GetAdComputers($domain)
{
	$computers = @()
	$strFilter = "(objectCategory=Computer)"

	$objDomain = New-Object System.DirectoryServices.DirectoryEntry("LDAP://" + $domain)

	$objSearcher = new-object System.DirectoryServices.DirectorySearcher
	$objSearcher.SearchRoot = $objDomain
	$objSearcher.PageSize = 1000
	$objSearcher.Filter = $strFilter
	$objSearcher.SearchScope = "Subtree"

	$colProplist = "name"
	foreach ($i in $colPropList){$objSearcher.PropertiesToLoad.Add($i)}


	$colResults = $objSearcher.FindALL()

	foreach ($objResult in $colResults)
	{
		$objItem = $objresult.Properties
		$computers = $computers + $objItem.name
	}
	return $computers
}

Function Get-WmiCustom([string]$computername,[string]$namespace,[string]$class,[int]$timeout=15,[string]$filter)
{
	$ConnectionOptions = new-object System.Management.ConnectionOptions
	$EnumerationOptions = new-object System.Management.EnumerationOptions 

	$timeoutseconds = new-timespan -seconds $timeout
	$EnumerationOptions.set_timeout($timeoutseconds) 

	$assembledpath = "\\" + $computername + "\" + $namespace
	#write-host $assembledpath -foregroundcolor yellow 

	$Scope = new-object System.Management.ManagementScope $assembledpath, $ConnectionOptions
	$Scope.Connect() 

	$querystring = "SELECT * FROM " + $class + " " + $filter
	#write-host $querystring 

	$query = new-object System.Management.ObjectQuery $querystring
	$searcher = new-object System.Management.ManagementObjectSearcher
	$searcher.set_options($EnumerationOptions)
	$searcher.Query = $querystring
	$searcher.Scope = $Scope 

	trap { $_ } $result = $searcher.get() 

	return $result
}

$domain = read-host "Please enter the domain you wish to query using ldap format (e.g. dc=test,dc=live)"
$computerlist = GetAdComputers $domain
$currentLocation = Get-Location
$filename = "IpInformation.csv"
$path = $currentLocation.tostring() + "\" + $filename
write-host $path

$header = "Computer Name,Ip 1,Ip 2"
$header | Out-File -filepath $path

foreach ($computer in $computerlist)
{
	$computer
	$status = pingComputer $computer
	If ($status.statuscode -eq 0)
	{		
			$info = QueryWmi $computer
			$info | Out-File -filepath $path -append

	}
	else
	{
		$info = $computer.trim() + "," + "OFFLINE"
		$info | out-File -filepath $path -append
		
	}
}