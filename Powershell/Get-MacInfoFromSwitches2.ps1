﻿param(
    [array]$switches
)

if ((Get-Module -Name NetCmdlets) -eq $null){
	try{
        Import-Module NetCmdlets -ErrorAction Stop
    } catch {
        Write-Host -ForegroundColor Red -Object "Unable to load NetCmdlets Module"
        exit 1
    }
}
if ((Get-Module -Name "Microsoft.XBOX.LsdNet.HPNA.PSCommandLet") -eq $null){
	try{
        Import-Module "Microsoft.XBOX.LsdNet.HPNA.PSCommandLet" -ErrorAction Stop
    } catch {
        Write-Host -ForegroundColor Red -Object "Unable to load HPNA Module"
        exit 2
    }
}
function Write-Sql {
        Param (
            $server,
            $query,
            $database
        )

        $connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
		$connString.psbase.UserID = <user>
		$connString.psbase.Password = "<password."
		$connString.psbase.IntegratedSecurity = $false
		$connString.psbase.DataSource = $Server
		$connString.psbase.InitialCatalog = $database
        $conn = New-Object System.Data.SqlClient.SqlConnection($connString)
        $conn.Open()
        $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
        $sqlCmd.CommandText = $query
        $sqlCmd.Connection = $conn
        $result = $sqlCmd.ExecuteNonQuery()
        $conn.Close()
        $result
    }
#$DebugPreference = Continue
$secPass = ConvertTo-SecureString "<password>" -AsPlainText -Force

$cred = New-Object System.Management.Automation.PSCredential("<user>",$secPass)

#$secPass = ConvertTo-SecureString "ij=R?dHTbi\YMm@s,ks^" -AsPlainText -Force

#$cred = New-Object System.Management.Automation.PSCredential("mgmt\v-sbsers",$secPass)

#$cred = Get-Credential
$CDPtext = New-Object System.Text.StringBuilder;
[int] $runTimes = 0;


if($switches.Count -eq 0){
    try {
	$devices = Get-DevicesByGroup -HpnaServer lsdhpna -Credential $cred -Group "Inventory";
} catch {
"error logging in"
}
} else {
	foreach($switch in $switch){
		$devices += Get-Device -HpnaServer lsdhpna -Credential $cred -HostnameOrIP $switch
	}
}

foreach($device in $devices){
	$time = Get-Date;
	$macCommand = "";
	[bool]$tryToConnect = $false;
	switch -regex ($device.hostName)
	{
		"CO2-ACC-CIS-4948" {
			$macCommand = "show mac-address-table | i _Giga";
			$tryToConnect = $true;
			break;
			}
		"HLF-CIS-3064" {
			$macCommand = "show mac address-table | i Eth1/[1-9]$|Eth1/[1-3][0-9]$|Eth1/4[0-2]$";
			$tryToConnect = $true;
			break;
			}
		"CO2-ACC-CIS-31" {
			$macCommand = "show mac address-table | i Gi[1-8]/0/[1-9]$|Gi[1-8]/0/1[0-6]$";
			$tryToConnect = $true;
			break;
			}
		"BLU-ACC-CIS-3048" {
			$macCommand = "show mac address-table | i Eth1/[1-9]$|Eth1/[1-3][0-9]$|Eth1/4[0-8]$";
			$tryToConnect = $true;
			break;
			}
		"B11-ACC-CIS-4948" {
			$macCommand = "show mac address-table | i GigabitEthernet1/[1-9]____$|GigabitEthernet1/[1-3][0-9]___$|GigabitEthernet1/4[0-6]__$";
			$tryToConnect = $true;
			break;
			}
		"ACC-CIS-5" {
			$macCommand = "show mac address-table | i 1[0-2][0-9]/";
			$tryToConnect = $true;
			break;
			}
	}
	if($tryToConnect){

		try{
		$thisHLFsession = Connect-SSH -Force -Credential $cred -Server $device.hostname -port 22 -Timeout 10 -ShellPromptExpression "^.*#\s$|^.*#$" -Config ("TerminalWidth=132", "TerminalHeight=0");
		Invoke-SSH -Connection $thisHLFsession -Command "term length 0" -ShellPromptExpression "^.*#\s$|^.*#$";
		$macAddresses = Invoke-SSH -Connection $thisHLFsession -Command $macCommand -ShellPromptExpression "^.*#\s$|^.*#$";
		Disconnect-SSH -Connection $thisHLFsession;
			}
		Catch{
		Write-Host "Connection Failed" $device.hostName;
		Disconnect-SSH -Connection $thisHLFsession;
		}

		[bool]$isPartialLine = $false;
		[string]$partialLine = "";


		foreach($line in $macAddresses){
			if ($line.Text.Length -eq 0){continue}
			Clear-Variable -Name lineText
			Clear-Variable -Name splitLine
			Clear-Variable -Name vlan
			Clear-Variable -Name mac
			Clear-Variable -Name port
			$lineText = $line.Text;
			$lineText = $lineText.trim()


			if($isPartialLine -eq $true){
				$lineText = [string]::Format("{0}{1}",$partialLine,$lineText);
				$lineText = $lineText.trim()
				$lineText = $lineText.Replace("* ","")
				while ($lineText.contains("  ")){
					$lineText = $lineText -replace "  "," "
				}
				$lineText = $lineText.trim()
				$splitLine = $lineText.split(" ")
				$vlan = $splitLine[0]
				$mac = $splitLine[1].Replace(".","")
				$port = $splitLine[$splitLine.count - 1]
				$query = [string]::Format("UPDATE SwitchDump Set dt_update = '{0}', i_vlan = '{1}', v_Mac = '{2}', v_port = '{3}', v_switch = '{4}' where v_switch = '{4}' and v_Mac = '{2}' IF @@Rowcount = 0 Insert into SwitchDump (dt_update, i_vlan, v_mac, v_port, v_switch) VALUES ('{0}','{1}','{2}','{3}','{4}')", (Get-Date), $vlan, $mac, $port, $device.hostname)
		 		Write-Sql -server <server> -database root_of_all_evil -query $query

				$isPartialLine = $false

			}
			elseif($line.EOL -eq $false){
				$partialLine = $lineText;
				$isPartialLine = $true;
			}
			else
			{
				$lineText = [string]::Format("{0}{1}",$partialLine,$lineText);
				$lineText = $lineText.trim()
				$lineText = $lineText.Replace("* ","")
				while ($lineText.contains("  ")){
					$lineText = $lineText -replace "  "," "
				}
				$lineText = $lineText.trim()
				$splitLine = $lineText.split(" ")
				$vlan = $splitLine[0]
				$mac = $splitLine[1].Replace(".","")
				$port = $splitLine[$splitLine.count - 1]
				$query = [string]::Format("UPDATE SwitchDump Set dt_update = '{0}', i_vlan = '{1}', v_Mac = '{2}', v_port = '{3}', v_switch = '{4}' where v_switch = '{4}' and v_Mac = '{2}' IF @@Rowcount = 0 Insert into SwitchDump (dt_update, i_vlan, v_mac, v_port, v_switch) VALUES ('{0}','{1}','{2}','{3}','{4}')", (Get-Date), $vlan, $mac, $port, $device.hostname)
		 		Write-Sql -server <server> -database root_of_all_evil -query $query

			}
		}

	}

}
