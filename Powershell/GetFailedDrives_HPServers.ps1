Function Get-WmiCustom([string]$computername,[string]$namespace,[string]$class,[int]$timeout=15,[string]$filter)
{
	$ConnectionOptions = new-object System.Management.ConnectionOptions
	$EnumerationOptions = new-object System.Management.EnumerationOptions 

	$timeoutseconds = new-timespan -seconds $timeout
	$EnumerationOptions.set_timeout($timeoutseconds) 

	$assembledpath = "\\" + $computername + "\" + $namespace
	#write-host $assembledpath -foregroundcolor yellow 

	$Scope = new-object System.Management.ManagementScope $assembledpath, $ConnectionOptions
	$Scope.Connect() 

	$querystring = "SELECT * FROM " + $class + " " + $filter
	#write-host $querystring 

	$query = new-object System.Management.ObjectQuery $querystring
	$searcher = new-object System.Management.ManagementObjectSearcher
	$searcher.set_options($EnumerationOptions)
	$searcher.Query = $querystring
	$searcher.Scope = $Scope 

	trap { $_ } $result = $searcher.get() 

	return $result
}

$currentLocation = Get-Location
$inputpath = $currentlocation.tostring() + "\" + "serverlist.txt"
$serverlist = get-content $inputpath

$filename = "DiskInfo.csv"
$path = $currentLocation.tostring() + "\" + $filename


$header = "Server Name, Disk Location, Status Number"
$header | Out-file -filepath $path
foreach ($server in $serverlist){
    $disks = get-wmicustom -computername $server -namespace "root\hpq" -class hpsa_diskdrive
    foreach ($disk in $disks){
        if ($disk.operationalstatus -ne 2) {
            $output = $server + "," + $disk.elementname + "," + $disk.operationalstatus
            $output | out-file -filepath $path -append
        }
    }
}