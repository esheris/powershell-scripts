<#
.SYNOPSIS
   <A brief description of the script>
.DESCRIPTION
   <A detailed description of the script>
.PARAMETER <paramName>
   <Description of script parameter>
.EXAMPLE
   <An example of using the script>
#>
function get-info {
$i = $null
$ilos = $null
$ilos = gc .\ipAddresses.txt
foreach ($i in $ilos){
	$i
	$plinkCmd = "Y" | .\plink.exe -ssh $i -m .\command2.txt -l Administrator -pw GFSILOGFSILO
}
foreach ($i in $ilos){
	$plinkCmd = .\plink.exe -ssh $i -m .\command.txt -l Administrator -pw GFSILOGFSILO
	if ($plinkCmd -ne $null){
		[string]$getMatches = $plinkCmd -match "port1nic"
		$FullMac = $getMatches.substring($getMatches.indexof("=") + 1,17)
		$mac = ($FullMac.replace(":","")).toupper()
		Write-Host "$mac - $i"
	} else {
		Write-Host "Issue accessing $i, see above"
	}
}
}
