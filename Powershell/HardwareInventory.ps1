Function GetAdComputers($domain)
{
	$computers = @()
	$strFilter = "(objectCategory=Computer)"

	$objDomain = New-Object System.DirectoryServices.DirectoryEntry("LDAP://" + $domain)

	$objSearcher = new-object System.DirectoryServices.DirectorySearcher
	$objSearcher.SearchRoot = $objDomain
	$objSearcher.PageSize = 1000
	$objSearcher.Filter = $strFilter
	$objSearcher.SearchScope = "Subtree"

	$colProplist = "name"
	foreach ($i in $colPropList){$objSearcher.PropertiesToLoad.Add($i)}


	$colResults = $objSearcher.FindALL()

	foreach ($objResult in $colResults)
	{
		$objItem = $objresult.Properties
		$computers = $computers + $objItem.name
	}
	return $computers
}

$domain = read-host "Please enter the domain you wish to query using ldap format (e.g. dc=test,dc=live)"
$servers = GetAdComputers $domain
$currentLocation = Get-Location
$filename = "AssetInformation.csv"
$path = $currentLocation.tostring() + "\" + $filename

$header = "Computer Name,Serial Number"
$header | Out-File -filepath $path -encoding ASCII

################################################################
## Begin Definition of Script Block for Start-Job Command. This 
## is large because code running inside a scriptblock cannot call
## functions outside itself, so I had to put all the required
## functions inside it.
################################################################
$sb = {

    Function QueryWmi ($server)
    {
		#$asset = get-wmicustom -class Win32_SystemEnclosure -namespace "root\cimv2" -server $server -timeout 2
        $serial = get-wmicustom -class Win32_Bios -namespace "root\cimv2" -server $server -timeout 2
		#$model = get-wmicustom -class Win32_ComputerSystem -namespace "root\cimv2" -server $server -timeout 2
        #$manufacturer = $model.Manufacturer.trim()
#        switch ($manufacturer) {
#            "Dell Inc." {
#                $manufacturer = "Dell"
#                }
#            "Dell Computer Corporation" {
#                $manufacturer = "Dell"
#                }
#            "HP" {
#                $manufacturer = "HP"
#                }
#            "Hewlett-Packard" {
#                $manufacturer = "HP"
#                }
#            "Microsoft Corporation" {
#               $manufacturer = "Microsoft Corporation"
#                }
#            default {
#                $outOfBandIP = "This Failed"
#                }
#            }
#        switch ($manufacturer){
#            "Dell" {
#                $drac = get-wmicustom -class Dell_RemoteAccessServicePort -namespace "root\cimv2\dell" -server $server -timeout 2
#                $outOfBandIp = $drac.Accessinfo
#                }
#            "HP" {
#                $ilo = get-wmicustom -class HP_ManagementProcessor -namespace "root\hpq" -server $server -timeout 2
#                $outOfBandIp = $ilo.IPAddress
#                }
#            "Microsoft Corporation" {
#                $outOfBandIp = "Virtual Machine"
#                }
#            }
        
        # I modify this line depending on what i want to return that day
		$data = $server + ","  + $serial.SerialNumber
		return $data        
    }

    ################################################################
    ## Parameters:
    ##   $server - name of server to ping
    ##
    ## Takes a string that is the server or IP to ping
    ## Returns True if the server responds to ping, else returns false
    ################################################################
    function isOnline([string]$server)
    {
        $erroractionpreference = "SilentlyContinue"
        $hostname = $server.trimend(".")
        $fqdn = [System.Net.Dns]::GetHostEntry($hostname).HostName
        $ping = new-object System.Net.NetworkInformation.Ping
        $reply = $ping.send($hostname)
        if ($reply.status -eq "Success"){
            return $true
        }
        else{
            return $false
        }
    } # End isOnline


    ################################################################
    ## Parameters: 
    ##   $server - Name or IP of server to connect to
    ##   $namespace - Wmi Namespace to connect to, defaults to root\cimv2
    ##   $class - Wmi class to query
    ##   $timeout - Time(in seconds) to wait for Wmi to respond
    ##   $filter - Where clause for Wmi Query
    ##
    ## This is used instead of the built in Get-WmiObject because Get-WmiObject does not take
    ## a timeout parameter and can cause a script to hang indefinately
    ################################################################
    Function Get-WmiCustom([string]$server,[string]$namespace='root\cimv2',[string]$class,[int]$timeout=15,[string]$filter)
    {
    	$ConnectionOptions = new-object System.Management.ConnectionOptions
    	$EnumerationOptions = new-object System.Management.EnumerationOptions 

    	$timeoutseconds = new-timespan -seconds $timeout
    	$EnumerationOptions.set_timeout($timeoutseconds) 
    
    	$assembledpath = "\\" + $server + "\" + $namespace
    	#write-host $assembledpath -foregroundcolor yellow 
    
    	$Scope = new-object System.Management.ManagementScope $assembledpath, $ConnectionOptions
    	$Scope.Connect() 
    
    	$querystring = "SELECT * FROM " + $class + " " + $filter
    	#write-host $querystring 
    
    	$query = new-object System.Management.ObjectQuery $querystring
    	$searcher = new-object System.Management.ManagementObjectSearcher
    	$searcher.set_options($EnumerationOptions)
    	$searcher.Query = $querystring
    	$searcher.Scope = $Scope 
    
    	trap [Exception] { write-error "Error connecting to wmi on server: $server";break; } $result = $searcher.get() 
    
    	return $result
    } # End Get-WmiCustom
    
    Function Get-Inventory([string]$server)
    {
        $status = isOnline $server
        if ($status) {
            $info = QueryWmi $server
            return $info
        }
    }
    
    $name = $args[0]
    Get-Inventory $name
}



# Main
$start = get-date
if ((get-job).length -gt 0){
    get-job | remove-job
}

$s = 0
while ($s -lt $servers.length) {
    $threads = Get-Job -State Running
    if ($threads.length -gt 30) {
        Start-Sleep -s 5
        Start-Job -scriptblock $sb -argumentlist $servers[$s] -name $servers[$s] | out-null
        $s++
    }
    else {
        #Start-Sleep -s 1
        Start-Job -scriptblock $sb -argumentlist $servers[$s] -name $servers[$s] | out-null
        $s ++
    }
    $completed = get-job -State Completed
    if ($completed -ne $null) {
        $received = $completed | receive-job
        $received | out-file -filepath $path -append -encoding ascii
        $completed | remove-job
        get-job -State Failed | remove-job
    }
    write-progress -activity "Searching Domain For Virtual Machines" -Status "% Complete:" -percentcomplete ($s/$servers.length*100)
}

$end = get-date
$elapsed = $end - $start
$count = $servers.length
write-host "Processed $count Servers in $elapsed"
# End: Main