﻿param(
    [ValidateNotNullOrEmpty()]
    [System.Net.IPAddress]$ilo
)
if((Get-Module -Name NetCmdlets) -eq $null){
try{
Import-Module netcmdlets -ErrorAction Stop
} catch {
    Write-Host -ForegroundColor Red -Object "Unable to load NETCMDLETs module. This Module is required to use this script."
    exit 1
}
}

Function Get-DataFromIlo($iloIP, $username, $password){
    $info = New-Object -TypeName psobject
    $info | Add-Member -MemberType NoteProperty -Name LoggedInWith -Value $username
    $serverinfo = (Invoke-SSH -AuthMode password -Server $iloIP -User $username -Password $password -Force -Command "Show /System1/").text
    $NicInfo = (Invoke-SSH -AuthMode password -Server $iloIP -User $username -Password $password -Force -Command "Show System1/network1/integrated_nics").text
    foreach ($line in $serverinfo){
        if($line.contains(" name=")){ $info | Add-Member -MemberType NoteProperty -Name Model -Value $line.split("=")[1]}
        if($line.contains(" number=")){ $info | Add-Member -MemberType NoteProperty -Name Serial -Value $line.split("=")[1]}
        if($line.contains("oemhp_server_name=")){ $info | Add-Member -MemberType NoteProperty -Name IloServerName -Value $line.split("=")[1]}
    }

    foreach ($line in $NicInfo){
        if($line.contains("Port1NIC_MACAddress=")){ $info | Add-Member -MemberType NoteProperty -Name Nic1Mac -Value $line.split("=")[1]}
        if($line.contains("Port2NIC_MACAddress=")){ $info | Add-Member -MemberType NoteProperty -Name Nic2Mac -Value $line.split("=")[1]}
        if($line.contains("Port3NIC_MACAddress=")){ $info | Add-Member -MemberType NoteProperty -Name Nic3Mac -Value $line.split("=")[1]}
        if($line.contains("Port4NIC_MACAddress=")){ $info | Add-Member -MemberType NoteProperty -Name Nic4Mac -Value $line.split("=")[1]}
    }
    return $info
}

if (Test-Connection -Quiet -ComputerName $ilo){

try {
    $connection = Connect-SSH -AuthMode password -Force -User "xse" -Password "L1veops@" -Server $ilo -ErrorAction Stop | Out-Null
    Get-DataFromIlo -iloIP $ilo -username "xse" -password "L1veops@"
} catch {
    try {
        $connection = Connect-SSH -AuthMode password -Force -User "Administrator" -Password "GFSILOGFSILO" -Server $ilo -ErrorAction Stop | Out-Null
        Get-DataFromIlo -iloIP $ilo -username "Administrator" -password "GFSILOGFSILO"
    } catch {
        "Failed to log in to the Ilo with any known credentials. This occasionally happens when the Ilo is locked up and needs a reset."
    }
}
} else {
    "Unable to ping ilo ip provided"
}
    