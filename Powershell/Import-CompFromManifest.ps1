function Select-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$Filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	If ($Show -eq "OK")
	{
		Return $objForm.FileName
	}
	Else
	{
		Return $null
	}
}

function Create-Csv($xlfile,$csvfile)
{
    $saveas = 6
    $xlsx = $xlfile
    $xl = New-Object -com "Excel.Application"
    $wb = $xl.WorkBooks.open($xlsx)
    $wb.SaveAs($csvfile,$saveas)
    $xl.displayalerts = $false
    $xl.quit()
    if (ps excel){kill -name excel}
}

function Pause ($Message="Press any key to continue...")
{
    Write-Host -NoNewLine $Message
    $null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    Write-Host ""
}

function get-site($domain)
{
switch ($domain) {
    "prod" {
        return "QyInfSccmAdm201"
        }
    "RR" {
        return "RRInfSccmAdm001"
        }
    }
}

function FixMacs($mac)
{
    $macLength = 12
    $macaddress = $mac
    $macaddress = $macaddress.replace(".","")
    $macaddress = $macaddress.replace(":","")
    $macaddress = $macaddress.replace("-","")
    for(
    
}

param([string]$domain,[string]$sitecode)
$admsvr = get-site($domain)
$ws = new-webserviceproxy("http://$admserver/deploymentwebservice/sccm.asmx?WSDL")

[string]$csv = get-content env:temp
$csvfile = $csv + "\temp.csv"
Write-Host "Please Select a Manifest File"
$xlfile = Select-FileDialog -Title "Select Excel File" -Directory ".\" -filter "Excel Files (*.xlsx *.xls) | *.xlsx;*.xls"

$create = Create-Csv $xlfile $csvfile

$import = Import-Csv $csvfile
$log = "Status of the Import Process`r`n"
foreach ($server in $import)
{
    $resourceID = $ws.AddComputer($server.adsadminmac,"",$server.winhostname,$sitecode)
    
    if ($resourceID -eq 0)
    {
        $log += "Error importing server $server.winhostname`r`n"
    }
    else
    {
        $log += "Imported Server: $server.winhostname`r`n"
        $log += "Server Mac Address: $server.adsadminmac`r`n"
    }
}   