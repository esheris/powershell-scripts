﻿function New-ImportMachine()
{
	param (
	[parameter(Mandatory=$true)]
	$hostname,
	[parameter(Mandatory=$true)]
	$MacAddress,
	[parameter(Mandatory=$false)]
	$overwrite = $true,
	[parameter(Mandatory=$false)]
	$ManagementPoint = "<server>",
    [parameter(Mandatory=$false)]
    $collectionToAddTo
	)
	begin { 
		$smsProvider = gwmi -Namespace root\sms -ComputerName $managementpoint -query "select * from sms_providerlocation"
        $namespacePath = $smsProvider.NamespacePath
        $method = "ImportMachineEntry"
	}
	process {
		if ($namespacePath -ne $null) {
			$site = [wmiclass]"$namespacePath`:SMS_Site"
            $inParams = $site.GetMethodParameters($method)
            $inParams.MacAddress = $MacAddress
            $inParams.NetbiosName = $hostname
            $inParams.OverwriteExistingRecord = $overwrite
            $result = $site.ImportMachineEntry($inParams.NetBiosName,$inParams.SMSBIOSGUID,$inParams.MACAddress,$inParams.OverwriteExistingRecord,$inParams.FQDN,$inParams.IsAMTMachine,$inParams.MEBxPassword,$inParams.AdminPassword,$inParams.AddToCollection,$inParams.CollectionRule,$inParams.CollectionId,$inParams.WTGUniqueKey)
            if (-not $result.returnvalue -eq 0){ write-host "Error Adding computer to site";exit 1 }
            if ($collectionToAddTo){
                Write-host "Preparing to add to collection"
                $CollectionID = (gwmi -namespace root\sms\site_xbl -ComputerName $smsProvider.Machine -Class sms_collection -filter "Name='$collectionToAddTo'").CollectionID
                $collection = [wmi]"$namespacePath`:SMS_Collection.CollectionID='$collectionID'"                
                $collRule = ([wmiclass]"$namespacePath`:SMS_CollectionRuleDirect").createinstance()
                $collRule.RuleName = $hostname
                $collRule.ResourceClassName = "SMS_R_System"
                $collRule.ResourceID = $result.ResourceID
                $AddToCollRes = $collection.AddMembershipRule($collRule)
            }
			
			if ($AddToCollRes.ReturnValue -eq 0){
                Write-Host "Successfully added computer to collection"
				exit 0
			} else {
                Write-host "Error Adding computer to collection"
				exit 2
			}
		} else { exit 4 }
	}
}

New-ImportMachine -hostname testagain1 -MacAddress "00:21:32:43:54:65" -collectionToAddTo "1_Disk_Configuration"