<#
.SYNOPSIS
   For use at end of SCCM computer build out, move from build OU to PreDeployment OU. Can be used to move any to any if command line params are supplied
.DESCRIPTION
   <A detailed description of the script>
.PARAMETER <paramName>
   -currentOU    OU where the server is supposed to be now, not including forest or server name
   -destinationOU   like above, but different
   
.EXAMPLE
   <An example of using the script>
#>

param (
$currentOU = "OU=ServerBuild,OU=Build" ,
$destinationOU = "OU=PreDeployment"
)

# don't really need this right now--may be useful in future
$propertyList = "name","pwdLastSet","lastLogonTimestamp","isCriticalSystemObject","objectCategory","userAccountControl"

function Pause ($Message = "Press any key to continue...") {
	Write-Host -NoNewLine $Message
	$null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
	Write-Host ""
}

# Get domain that the computer belongs to
$computerSystem = Get-WMIObject -Class win32_computersystem
$computerName = $computerSystem.Name
$forestName = $computerSystem.domain
$ldapForestName = "DC=$($forestName.Substring(0,$forestName.IndexOf("."))),DC=$($forestName.Substring($forestName.indexOf(".") + 1))"

# connect to AD and find the computer
# TODO: wrap in try/catch
$forestRoot = New-Object System.DirectoryServices.DirectoryEntry "LDAP://$ldapForestName"
$forestSearcher = New-Object System.DirectoryServices.DirectorySearcher
$forestSearcher.SearchRoot = $forestRoot
$forestSearcher.PageSize = 200
$forestSearcher.SearchScope = "subtree"
$forestSearcher.PropertiesToLoad.AddRange($propertyList)
$forestSearcher.Filter = "(&(objectCategory=computer)(name=$computerName))"
$adComputer = $forestSearcher.FindAll()

# make sure you found 1 and exactly 1 object in AD
if ($adComputer.Count -eq 0) {Write-Host "NOT PROCESSED -- $ADComputer not found" ; exit 1}
if ($adComputer.Count -gt 1) {"NOT PROCESSED -- $ADComputer found more than 1 object" ; exit 2}

# make sure the computer is in the expected OU before moving it
[string]$adComputerDN = $adComputer[0].Properties.Item("aDSPath")
$expectedADComputerDN = "LDAP://CN=$computerName,$currentOU,$ldapForestName"
if ($adComputerDN -ne $expectedADComputerDN) {Write-Host "Mismatch: $adComputerDN -- $expectedADComputerDN" ; exit 1}

$computer2Move = New-Object System.DirectoryServices.DirectoryEntry $adComputerDN
$computer2Move.PSBase.MoveTo("LDAP://$destinationOU,$ldapForestName")

# make sure it worked, waiting up to 60 seconds for AD replication to finish before failing
$computerWasMoved = New-Object System.DirectoryServices.DirectoryEntry "LDAP://CN=$computerName,$destinationOU,$ldapForestName"
for ($count = 1 ; $count -le 12 ; $count++) {
	Write host "$count - sleeping 5 seconds"
	Start-Sleep -Seconds 5
	if ($computerWasMoved.Name -eq "$computerName") {exit 0}
}
Write-Host "Move failed for $computerName" ; exit 1
