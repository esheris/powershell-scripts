$sitecode = "CEN"
$webservice = New-WebServiceProxy -Uri http://<server>:81/sccm.asmx
$webservice.UseDefaultCredentials = $true

function Move-Computer ($delColl, $addColl, $res, $name) {
	$addResult = $webservice.AddComputerToCollectionByID($res,$addColl,$Name)
	if ($addResult){
		Log-Result -resultText "Added $name with Resource ID $res to $addColl"
		$delResult = $webservice.RemoveComputerFromCollectionByID($res,$delColl,$Name)
		if ($delResult) {
			Log-Result -resultText "Removed $Name with Resource ID $res from $delColl"
		} else {
			Log-Result -resultText "Failed to remove $Name with Resource ID $res from $delColl"
		}
	} else {
		Log-Result -resultText "Failed to add $Name with Resource ID $res to $addColl"
	}
}

function Read-SQL {
    Param ($server, $query, $database)
    $conn = new-object ('System.Data.SqlClient.SqlConnection')
    $connString = "Server=$server;Integrated Security=SSPI;Database=$database"
    $conn.ConnectionString = $connString
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $Rset = $sqlCmd.ExecuteReader()
    ,$Rset ## The comma is used to create an outer array, which PS strips off automatically when returning the $Rset
}

function Return-ReaderResults {
    Param ($query,$server = "<server>", $database = "SMS_CEN")
    $data = Read-SQL $server $query $database
    while ($data.read() -eq $true) {
        $max = $data.FieldCount -1
        $obj = New-Object Object
        For ($i = 0; $i -le $max; $i++) {
            $name = $data.GetName($i)
            $obj | Add-Member Noteproperty $name -value $data.GetValue($i)
     }
     $obj
    }
}
function Log-Result()
{
	param(
		[parameter(Mandatory=$false)]
		[string]$logpath = "$pwd\Move-Collections.log",
		[Parameter(Mandatory=$true)]
		[string]$resultText
	)
	begin {
		$logSize = (gci $logpath).Length
		$LogOutput = [string]::Format("{0} {1} {2}",(get-date).ToShortDateString(),(get-date).ToLongTimeString(),$resultText)
	}
	process {		
		Tee-Object -InputObject $LogOutput -Variable tee
		Out-File -FilePath $logpath -InputObject $tee -Append
	}
}

function Get-LastStatus(){
	param(
		[Parameter(Mandatory=$true)]
		[string]$ResourceID,
		[Parameter(Mandatory=$true)]
		[string]$collection
	)
	begin {
		switch ($collection) {
			"CEN00021" {$advertID = "CEN20000"}
			"CEN00022" {$advertID = "CEN20001"}
			"CEN00023" {$advertID = "CEN20002"}
			"CEN00024" {$advertID = "CEN20003"}
			"CEN00026" {$advertID = "CEN20004"}
			"CEN0008A" {$advertID = "CEN2001B"}
			"CEN0008C" {$advertID = "CEN2001C"}
		}
	}
	process {
		$results = Return-ReaderResults "Select LastStatusMessageID from v_ClientAdvertisementStatus where ResourceID = '$ResourceID' and AdvertisementID = '$advertID'"
		return $results.LastStatusMessageID
	}
}

	
# main Script Area
Function Main(){
	$collectionIDs = "CEN00021","CEN00022","CEN00023","CEN00024","CEN00026","CEN0008A","CEN0008C"
	foreach ($collection in $collectionIDs){
		$collList = $webservice.GetCollectionMembers($collection,$sitecode)
		foreach ($computer in $collList){
			$lastStatus = Get-LastStatus -ResourceID $computer.ResourceID -collection $collection
			$compName = $computer.NetbiosName
			Log-Result -resultText "Last Status Message for $compName is $lastStatus"
			if ($lastStatus -eq 11143 -or $lastStatus -eq 11171 -or $lastStatus -eq 10040){
				switch ($collection) {
					"CEN00021" { Move-Computer $collection "CEN00022" $computer.ResourceID $computer.Name}
					"CEN00022" { Move-Computer $collection "CEN00023" $computer.ResourceID $computer.Name}
					"CEN00023" { Move-Computer $collection "CEN00024" $computer.ResourceID $computer.Name}
					"CEN00024" { Move-Computer $collection "CEN00026" $computer.ResourceID $computer.Name}
					"CEN00026" { Move-Computer $collection "CEN00025" $computer.ResourceID $computer.Name}
					"CEN0008A" { Move-Computer $collection "CEN0008C" $computer.ResourceID $computer.Name}
					"CEN0008C" { Move-Computer $collection "CEN0008B" $computer.ResourceID $computer.Name}
				}
			}
		}
	}
}

Main