param(
	[ValidateNotNullOrEmpty()]
	[string]$serverlist
)
$domains = "<domain>",","non<domain>","<domain>","<domain>","<domain>"
import-module ActiveDirectory
$servers = gc $serverlist
foreach ($server in $servers){
	foreach ($domain in $domains){
	$ldapdomain = [String]::format("dc={0},dc={1}",$domain.split(".")[0],$domain.split(".")[0])
	$adcomp = get-adcomputer -LDAPFilter "(name=$server)" -server $domain
	if ($adcomp -ne $null){
		if ($adcomp.DistinguishedName.contains("Inventory")){
			write-host "$server found in inventory ou - Moving to staging"
			$adcomp | Move-ADObject -TargetPath "ou=staging,$ldapdomain"
			$server | out-file -filepath ".\SafeToDeleteFromScom.txt" -encoding ascii -append
			continue
		} elseif ($adcomp.DistinguishedName.contains("Staging")){
			write-host "$server is already in the staging OU"
			$server | out-file -filepath ".\SafeToDeleteFromScom.txt" -encoding ascii -append
			continue
		} else {
			$ds = $adcomp.DistinguishedName
			write-host "$server was found here: $ds"
			$server | out-file -filepath ".\DONOT-DeleteFromScom.txt" -encoding ascii -append
			continue
		}
	} else {
		write-host "Unable to find $server in current $domain"
	}
	}
}

# foreach ($server in $serverlist){ Get-SCOMAgent -DNSHostName $server | Uninstall-SCOMAgent }