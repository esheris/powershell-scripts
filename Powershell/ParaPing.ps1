$path = "$pwd\PingResults.csv"
##############################################################################
## ParaPing.ps1 - powershell script to ping systems in Paralell
## Maintenance:
## Date        By   Comments
## ----------  ---  ----------------------------------------------------------
## 2/28/2011   EAS  Added Progress Bar and made run in parallel
##
##############################################################################
$scriptUsage = @"
Purpose:
  This script will retrieve a quick (one ping) network response
 
Usage:
    ./ping server hostlist.txt [-?|-H]
 
Where:
    -? or -H         displays this help
 
Comments:
     From the PowerShell command-line, invoke the script, passing any combination
     of hostnames and filenames (text file(s) containing a hostname listing
     Any combinations and number of arguments can be used
     if an argument is a filename:
        each line in the file will processed as a hostname
     if an argument is not a filename:
         the argument will processed as a hostname
 
Example:
     ./ping server1 hostlist.txt server2 server3
"@
 
##############################################################################
 
function ShowUsage(){
	$scriptUsage
	}   
 
function WMIDateStringToDate($Bootup) {
	[System.Management.ManagementDateTimeconverter]::ToDateTime($Bootup)
	}
$sb = {
function DoPing ([string]$hostname){
	$erroractionpreference = "SilentlyContinue"
    $hostname = $hostname.trimend(".")
 
    #$fqdn = [System.Net.Dns]::GetHostEntry($hostname).HostName
 
	$ping = new-object System.Net.NetworkInformation.Ping
	$reply = $ping.send($hostname)
	if ($reply.status -eq "Success"){
		$output = [string]::Format("OK,{0},{2}",$reply.Address.ToString(),$reply.RoundtripTime,$hostname)
		}
	else
        {
        $z = [system.net.dns]::gethostaddresses($hostname)[0].ipaddresstostring
		$output = [string]::Format("FAIL,{0},{2}",$z,"***",$hostname)
        }
	$output
	}
 $hostname = $args[0]
 DoPing $hostname
 }
######################################################
## MAIN
######################################################
##
## Process command-line arguments
##
if ($args.count) {
 
    # Check for command-line flags
	foreach ($item in $args) {
		#$item
		if (($item.toupper() -eq "-H") -or ($item.toupper() -eq "-?")){
			ShowUsage
			exit
		}
	}
}
 
if ($args.count) {
 	$list = @()
	"LN,HostName,PingStatus,IP,HostName" | out-file -filepath $path -encoding ascii
	
 
	# Loop through each argument
    foreach ($item in $args) {
        #
        # if argument is a filename, process the contents of the file, ignoring any comment lines
        #
        if (test-path($item)) {
			$list = $list + (Get-Content $item)
     	}
        #
        # if argument is NOT a filename, treat the argument as a hostname
        #
   		else {
			$list += $item			
 		}

	}
	
	if ((get-job).length -gt 0){
    get-job | remove-job
	}
	$count = $list.length
	Write-Host "Pinging $count Servers" -ForegroundColor Green
	Write-Host "Writing results to $path" -ForegroundColor Green
	$s = 0
	while ( $s -lt $list.length) {
		$threads = Get-Job -State Running
		if ($threads.length -ge 20) {
	    	Start-Sleep -Seconds 7
        	Start-Job -scriptblock $sb -argumentlist $list[$s] -name $list[$s] | out-null
        	$s++
    	}
		elseif (($threads.length -gt 15) -or ($threads.length -lt 20)) {
			Start-Sleep -Seconds 1
			Start-Job -scriptblock $sb -argumentlist $list[$s] -name $list[$s] | out-null
			$s++
		}
    	else {
        	#Start-Sleep -s 1
        	Start-Job -scriptblock $sb -argumentlist $list[$s] -name $list[$s] | out-null
        	$s ++
    	}
    	$completed = get-job -State Completed
    	if ($completed -ne $null) {
        	$received = $completed | receive-job
        	$received | out-file -filepath $path -append -encoding ascii
        	$completed | remove-job
        	get-job -State Failed | remove-job
    	}
		
    	write-progress -activity "Pinging Servers:" -Status "% Complete:" -percentcomplete ($s/$list.length*100)
	}
	Write-Host "Waiting for jobs to complete and finishing writing to file $path" -ForegroundColor Yellow
	while ((Get-Job -state Running) -ne $null){ Start-Sleep 1}
	$completed = get-job -State Completed
	if ($completed -ne $null) {
        	$received = $completed | receive-job
        	$received | out-file -filepath $path -append -encoding ascii
        	$completed | remove-job
        	get-job -State Failed | remove-job
    	}
 	}
	Write-Host "Finished writing file $path" -ForegroundColor Green

Write-Host "Please note that results will be out of order due to the parallel nature of this script!" -ForegroundColor Yellow
exit
