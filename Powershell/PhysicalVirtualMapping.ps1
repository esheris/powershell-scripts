Start-Transcript -Path "$PWD\PhysicalVirtualMapping.log"
################################################################
## Connects to Active Directory and returns the property 'name'
## for all computer objects as an array  
################################################################
Function Get-AdComputers($domain)
{
	$computers = @()
	$strFilter = "(objectCategory=Computer)"

	$objDomain = New-Object System.DirectoryServices.DirectoryEntry("LDAP://" + $domain)

	$objSearcher = new-object System.DirectoryServices.DirectorySearcher
	$objSearcher.SearchRoot = $objDomain
	$objSearcher.PageSize = 1000
	$objSearcher.Filter = $strFilter
	$objSearcher.SearchScope = "Subtree"

	$colProplist = "name"
	foreach ($i in $colPropList){$objSearcher.PropertiesToLoad.Add($i)}


	$colResults = $objSearcher.FindALL()

	foreach ($objResult in $colResults)
	{
		$objItem = $objresult.Properties
		$computers = $computers + $objItem.name
	}
	return $computers
}

## Prep work before entering loop
$path = "$pwd\PhysicalVirtualMapping.csv"
$servers = Get-AdComputers("dc=halowaypoint,dc=live")
$header = "PhysicalServer,VirtualServer"
$header | out-file -filepath $path -encoding ascii

################################################################
## Begin Definition of Script Block for Start-Job Command. This 
## is large because code running inside a scriptblock cannot call
## functions outside itself, so I had to put all the required
## functions inside it.
################################################################
$sb = {
    ################################################################
    ## Parameters: 
    ##   $server - Name or IP of server to connect to
    ##   $namespace - Wmi Namespace to connect to, defaults to root\cimv2
    ##   $class - Wmi class to query
    ##   $timeout - Time(in seconds) to wait for Wmi to respond
    ##   $filter - Where clause for Wmi Query
    ##
    ## This is used instead of the built in Get-WmiObject because Get-WmiObject does not take
    ## a timeout parameter and can cause a script to hang indefinately
    ################################################################
    Function Get-WmiCustom([string]$server,[string]$namespace='root\cimv2',[string]$class,[int]$timeout=15,[string]$filter)
    {
    	$ConnectionOptions = new-object System.Management.ConnectionOptions
    	$EnumerationOptions = new-object System.Management.EnumerationOptions 

    	$timeoutseconds = new-timespan -seconds $timeout
    	$EnumerationOptions.set_timeout($timeoutseconds) 
    
    	$assembledpath = "\\" + $server + "\" + $namespace
    	#write-host $assembledpath -foregroundcolor yellow 
    
    	$Scope = new-object System.Management.ManagementScope $assembledpath, $ConnectionOptions
    	$Scope.Connect() 
    
    	$querystring = "SELECT * FROM " + $class + " " + $filter
    	#write-host $querystring 
    
    	$query = new-object System.Management.ObjectQuery $querystring
    	$searcher = new-object System.Management.ManagementObjectSearcher
    	$searcher.set_options($EnumerationOptions)
    	$searcher.Query = $querystring
    	$searcher.Scope = $Scope 
    
    	trap [Exception] { write-error "Error connecting to wmi on server: $server";break; } $result = $searcher.get() 
    
    	return $result
    } # End Get-WmiCustom
    
	Function Test-ServerConnection(){
		param([string]$hostname)
		$errorPreferenceHolding = $ErrorActionPreference
		$ErrorActionPreference = "SilentlyContinue"
		$hostname = $hostname.trimend(".")
		$fqdn = [System.Net.Dns]::GetHostEntry($hostname).HostName
		$ping = New-Object System.Net.NetworkInformation.Ping
		$reply = $ping.send($hostname)
		if ($reply.Status -eq "Success"){
			return $true
		} else {
			return $false
		}
		$ErrorActionPreference = $errorPreferenceHolding
	}

    
    ################################################################
    ## Parameters:
    ##   $namespace - Name of Namespace to search for
    ##   $server - Name of server to connect to
    ## Takes the name of a namespace and a server. Connects to the root
    ## of WMI and reads the __Namespace class to get a list of all namespaces
    ## on the server that equal the passed in namespace 
    ################################################################
    Function Test-WmiNamespaceExists([string]$namespace,[string]$server)
    {
        $exists = Get-WmiCustom -server $server -namespace "root" -class "__Namespace" -timeout 1 | where {$_.Name -eq $namespace}
        if ($exists -ne $null)
        {
            return $true
        }
        else
        {
            return $false
        }
    } # End Test-WmiNamespaceExists
	
    
    ###############################################################
    ## Parameters: 
    ##   $server - Name of server to connect to
    ## 
    ## Returns a coma seperated list containing the server queried
    ## and any vm's found
    ##############################################################
    
    Function Get-VirtualMachines([string]$server) {
        $status = Test-ServerConnection $server
        if ($status){
            $namespaceExists = Test-WmiNamespaceExists -namespace "virtualization" -server $server
            if ($namespaceExists){
                $vmName = Get-WmiCustom -server $server -namespace 'root\virtualization' -class Msvm_ComputerSystem -timeout 1 -filter 'WHERE Caption = "Virtual Machine"' | Select ElementName
                if ($vmName -ne $null){
                    $output = $server
                    foreach ($element in $vmname){
                        $output = $output + "," + $element.elementname                    
                    }
                    $output
                }
            } 
        } else {
			Write-Error -Message "Unable to connect to the server: $server" -CategoryActivity ObjectNotFound 
		}
    } # End Get-VirtualMachines
    ## Needed because of the use of the script block
    $name = $args[0]
    Get-VirtualMachines $name
} # End Definition of Script Block (variable $sb).


# Main
$start = get-date
if ((get-job).length -gt 0){
    get-job | remove-job
}

$s = 0# Handles parallelism
while ($s -lt $servers.length) {
    $threads = Get-Job -State Running
    if ($threads.length -gt 15) {
        Start-Sleep -s 5
        Start-Job -scriptblock $sb -argumentlist $servers[$s] -name $servers[$s] | out-null
        $s ++
    }
    else {
        #Start-Sleep -s 1
        Start-Job -scriptblock $sb -argumentlist $servers[$s] -name $servers[$s] | out-null
        $s ++
    }# End: Parallelism
    $completed = get-job -State Completed
    if ($completed -ne $null) {
        $received = $completed | receive-job
        $received | out-file -filepath $path -append -encoding ascii
        $completed | remove-job
        get-job -State Failed | remove-job
    }
    write-progress -activity "Searching Domain For Virtual Machines" -Status "% Complete:" -percentcomplete ($s/$servers.length*100)
}
while ((Get-Job -state Running) -ne $null){ Start-Sleep 1}
	$completed = get-job -State Completed
	if ($completed -ne $null) {
        	$received = $completed | receive-job
        	$received | out-file -filepath $path -append -encoding ascii
        	$completed | remove-job
        	get-job -State Failed | remove-job
    	}
	Write-Host "Finished writing file $path" -ForegroundColor Green
$end = get-date
$elapsed = ($end - $start).TotalSeconds
$count = $servers.length
write-host "Processed $count Servers in $elapsed"
Stop-Transcript
# End: Main