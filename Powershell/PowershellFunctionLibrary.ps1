$_debug = $false
$regPath = "HKLM:\SOFTWARE\LSGOI\OSDeploy"
function Write-OSDLog()
{
	param(
		[Parameter(Mandatory=$true)]
		[int]$resultCode,
		[Parameter(Mandatory=$true)]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText
	)
	begin {
		$LogOutput = [string]::Format("{0}:{1}:{2}:{3}",$logCount,$resultCode,$resultType,$resultText)
	}
	process {
		$logOutput | Out-File -Append -FilePath $logpath -Encoding ascii
		$logCount++
	}		
}
# Tests if a registry value exists. Returns True or False
function Test-RegistryValue($regkey, $name) {
	Get-ItemProperty $regkey $name -ErrorAction SilentlyContinue | Out-Null
	$?
}
function Get-AdComputerContainer(){
	if ($_debug = $true){ log-result 0 Information "Entering $MyInvocation.MyCommand.Name"}
}

function Get-DomainInfo(){
	if ($_debug = $true){ log-result 0 Information "Entering $MyInvocation.MyCommand.Name"}
}
# Returns an INT
function Get-Day(){
	if ($_debug -eq $true){ log-result 0 Information "Entering $MyInvocation.MyCommand.Name"}
	Write-Output ((Get-Date) - ([datetime]"1/1/1900")).days
}
# Returns an INT
function Get-Hour(){
	if ($_debug = $true){ log-result 0 Information "Entering $MyInvocation.MyCommand.Name"}
	Write-Output (Get-Date).Hour
}

function Set-Ok2Build(){
	if ($_debug = $true){ log-result 0 Information "Entering $MyInvocation.MyCommand.Name"}
	if (Test-Path $regPath -ErrorAction SilentlyContinue){
		if (Test-RegistryValue $regPath 
		Remove-ItemProperty
	}
}

function Get-Ok2BuildStatus(){
	if ($_debug = $true){ log-result 0 Information "Entering $MyInvocation.MyCommand.Name"}
	if (Test-Path $regPath -ErrorAction SilentlyContinue){
		if (Test-RegistryValue $regPath "ServerBuildStatus"){
			foreach ($value in (Get-ItemProperty $regPath).ServerBuildStatus){
				
		}
	} else {
		log-result 0 
	}
	
}
# Verifies if specific combinations of registry keys exist
function Get-QuarantineRegKey(){
	if ($_debug = $true){ log-result 0 Information "Entering $MyInvocation.MyCommand.Name"}
	if (Test-Path $regPath -ErrorAction SilentlyContinue){ 
		log-result 0 INFORMATION "OSDeploy_REGISTRY_KEY_EXISTS")
	} else {
		md $regPath -Force | Out-Null
	}
	$QTS = Test-RegistryValue $regPath "QuarantineTimeStamp"
	$QTZ = Test-RegistryValue $regPath "QuarantineTimeZone"
	$SBS = Test-RegistryValue $regPath "ServerBuildStatus"
	if (!($QTS -and $QTZ -and $SBS)) {log-result 0 INFORMATION "OSDeploy_REG_KEYS_COMBINATION_OK_000"; break;}
	if ($QTS -and $QTZ -and -not $SBS) { log-result 0 INFORMATION "OSDeploy_REG_KEYS_COMBINATION_OK_110"; break;}
	if (!($QTS -and $QTZ) -and $SBS) { log-results 0 INFORMATION "OSDeploy_REG_KEYS_COMBINATION_OK_001"; break;}
	log-results 1 FATAL "INVALID_OSDeploy_REG_KEY_COMBINATION"
}

function Set-QuarantineRegKey(){
	if ($_debug = $true){ log-result 0 Information "Entering $MyInvocation.MyCommand.Name"}
}

function Get-UniversalVariable(){
	if ($_debug = $true){ log-result 0 Information "Entering $MyInvocation.MyCommand.Name"}
}