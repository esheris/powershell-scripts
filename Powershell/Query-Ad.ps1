#usage: Query-AD.ps1 -servername <servername>
param([string]$servername,[string]$domain)
$servername = $servername.toupper()
$strFilter = "(&(objectCategory=Computer))"
$strDomain = $domain.split(".")
$ldapString = [string]::format("LDAP://DC={0},DC={1}",$strDomain[0],$strDomain[1])
write-host($ldapString)
$ldap = $ldapString
$objDomain = New-Object System.DirectoryServices.DirectoryEntry($ldap)

$objSearcher = New-Object System.DirectoryServices.DirectorySearcher
$objSearcher.SearchRoot = $objDomain
$objSearcher.PageSize = 1000
$objSearcher.Filter = $strFilter
$objSearcher.SearchScope = "Subtree"


$objSearcher.PropertiesToLoad.Add("name") | out-null

$colResults = $objSearcher.FindAll() | WHERE {$_.path.contains($servername)}
$path = $colResults.path.toupper()
$path
$find = "CN=$servername,OU=Staging".toupper()
$find

if ($path.contains($find)){
	return 0
} else {
	return 1
}