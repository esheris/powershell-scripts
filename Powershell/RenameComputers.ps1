function Select-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$Filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	If ($Show -eq "OK")
	{
		Return $objForm.FileName
	}
	Else
	{
		Return $null
	}
}

function Rename-Computer([string]$old,[string]$new,[switch]$reboot)
{
    $ComputerSystem = Get-WmiObject -class win32_computersystem -namespace "ROOT\CIMV2" -computer $old
    $ComputerSystem.Rename($new)
    if ($reboot){
        $OperatingSystem = Get-WmiObject -Class Win32_OperatingSystem -namespace "ROOT\CIMV2" -computer $old
        $OperatingSystem.Reboot()
    }
}


$csvFile = Select-FileDialog -title "Select Server List" -Directory ".\" -filter "CSV (Comma delimited) (.csv)|*.csv"
$computerNames = import-csv $csvFile

