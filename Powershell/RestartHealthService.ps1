﻿$ScriptUsage = @"
Purpose:
	This script will restart the SCOM service on the given server
	and/or list of servers

Usage:
	./RestartHealthService.ps1 server1 server2 serverlist.txt [-?|-H]
	
Where:
	server1 = Name of a server you can directly connect to
	serverlist.txt = a text file containing a list of servers, one per line
	-? or -H = Displays this help
	
Comments:


Example:
	./RestartHealthService.ps1 server2 listofservers.txt server3
	
"@

function Stop-HealthService(){
	param ([string]$hostname)
	# Stop Service
	$healthService = Get-WmiObject -class Win32_Service -ComputerName $hostname | where {$_.Name -eq "HealthService"}
	if ($healthService.State -eq "Stopped") { 
		return $true
	} else {
		$status = ((Get-WmiObject -class Win32_Service -ComputerName $hostname | where {$_.Name -eq "HealthService"}).StopService()).ReturnValue
		if ($status -ne 0){
			return $false
		}
		$i = 0
		do{			
			Start-Sleep -Seconds 5
			$i++
			if ($i -gt 5){
				return $false
			}
		} while ((Get-WmiObject -class Win32_Service -ComputerName $hostname | where {$_.Name -eq "HealthService"}).State -ne "Stopped")			
	}
	if ((Get-WmiObject -class Win32_Service -ComputerName $hostname | where {$_.Name -eq "HealthService"}).State -eq "Stopped") {
		return $true
	} else { 
		return $false
	}
}
Function Start-HealthService(){
	param ([string]$hostname)
	# Start Service	
	$healthService = Get-WmiObject -class Win32_Service -ComputerName $hostname | where {$_.Name -eq "HealthService"}
	if ($healthService.State -eq "Running") {
		return $true
	} else {
		$status = ((Get-WmiObject -class Win32_Service -ComputerName $hostname | where {$_.Name -eq "HealthService"}).StartService()).ReturnValue
		if ($status -ne 0){
			return $false
		}
		$j = 0
		do{			
			Start-Sleep -Seconds 5
			$j++
			if ($j -gt 5){
				return $false
			}
		} while ((Get-WmiObject -class Win32_Service -ComputerName $hostname | where {$_.Name -eq "HealthService"}).State -ne "Running")
	}
	if ((Get-WmiObject -class Win32_Service -ComputerName $hostname | where {$_.Name -eq "HealthService"}).State -eq "Running") {
		return $true
	} else {
		return $false
	}
}

Function Test-ServerConnection(){
	param([string]$hostname)
	$errorPreferenceHolding = $ErrorActionPreference
	$ErrorActionPreference = "SilentlyContinue"
	$hostname = $hostname.trimend(".")
	$fqdn = [System.Net.Dns]::GetHostEntry($hostname).HostName
	$ping = New-Object System.Net.NetworkInformation.Ping
	$reply = $ping.send($hostname)
	$ErrorActionPreference = $errorPreferenceHolding
	if ($reply.Status -eq "Success"){
		return $true
	} else {
		return $false
	}		
}

Function Restart-HealthService(){
	param ([string]$hostname)
	$pingStatus = Test-ServerConnection $hostname
	if ($pingStatus){
		$stopStatus = Stop-HealthService $hostname
		if ($stopStatus){
			$startStatus = Start-HealthService $hostname
			if ($startStatus){
				Write-Host "Successfully Restarted the health service on $hostname" -ForegroundColor Green
			} else {
				Write-Host "Failed to START the health service on $hostname" -ForegroundColor Red
			}
		} else {
			Write-Host "Failed to Stop the Health Service on $hostname" -ForegroundColor Red
		}
	} else {
		Write-Host "Failed to connect to $hostname. Please verify the server is online" -ForegroundColor Red
	}
}

if ($args.count) {
	foreach ($item in $args) {
		if (($item.ToUpper() -eq "-H") -or ($item.ToUpper() -eq "-?")){
			$ScriptUsage
			exit
		}
	}
}

if ($args.count) {
	$list = @()
	foreach ($item in $args) {
		if (Test-Path($item)){
			$list = $list + (Get-Content $item)
		} else {
			$list += $item
		}
	}
	$k = 0
	foreach ($s in $list){
		Write-Progress -Activity "Restarting Health Service" -Status "% Complete:" -PercentComplete ($k/$list.length*100)
		Restart-HealthService $s		
		$k++
	}
} else { $ScriptUsage }