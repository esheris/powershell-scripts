param(
	[string]$server
	)
<# Powershell Script for Running a ConfigMgr Task Sequence On-Demand
	Written By: Jeremy Young (jeremy.young@microsoft.com)
	http://myitforum.com/cs2/blogs/jeremyyoung/
#>

<#
	.SYNOPSIS
		Initiates a ConfigMgr task sequence to run immediateley on a target machine.

	.DESCRIPTION
		Initiates a ConfigMgr task sequence to run immediateley on a target machine.

	.PARAMETER  ComputerName
		The server name to target.

	.PARAMETER  TaskSequenceName
		The name of the ConfigMgr task sequence to run

	.EXAMPLE
		PS C:\> Invoke-ConfigMgrTaskSequence.ps1 -ComputerName "Foo" -TaskSequenceName "My ConfigMgr Task Sequence"

#>

function Invoke-ConfigMgrTaskSequence {
	[CmdletBinding()]
	[OutputType([System.Int32])]
	param(
		[Parameter(Position=0)]
		[System.String]		
		$ComputerName=(get-content env:computername), #defaults to local computer name

		[Parameter(Position=1,Mandatory=$true)]
		[System.String]
		$TaskSequenceName		
	)
	try
	{		
		Write-Host "Searching for ads for [$TaskSequenceName] on [$ComputerName]"
		Get-WmiObject -Namespace root\ccm\policy\machine\actualconfig -Query "Select * from CCM_SoftwareDistribution where PKG_Name = '$TaskSequenceName' and PRG_ProgramID = '*'" -ComputerName $ComputerName | ForEach-Object{

			$adID=$_.ADV_AdvertisementID
			$pkgID=$_.PKG_PackageID
			Write-Host "Found ad [$adID] for [$TaskSequenceName] with pkgID [$pkgID]"
			
			Get-WmiObject -Namespace root\ccm\policy\machine\actualconfig -Query "Select * from CCM_Scheduler_ScheduledMessage where ScheduledMessageID like '$($adID)-$($pkgID)-%'" -ComputerName $ComputerName | ForEach-Object{
				$policyID=$_.ScheduledMessageID
				Write-Host "Found policy [$policyID] for ad [$adID]"
			}
		}
		Write-Host "Connecting to cm client on [$ComputerName]"
		$cmclient = [wmiclass] "\\$ComputerName\root\ccm:SMS_Client"
		Write-Host "Triggering policy."
		$out=$cmclient.TriggerSchedule($policyID)
		Write-Host "Done."
	}
	catch
	{
		continue
	}
}

function Select-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$Filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	if ($Show -eq "OK")
	{
		return $objForm.FileName
	}
	else
	{
		return $null
	}
}

Invoke-ConfigMgrTaskSequence -ComputerName $server -TaskSequenceName GPUpdate