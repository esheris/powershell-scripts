[CmdletBinding()]
param(
	[Parameter(Mandatory=$true)]
	[string]$rfc,
	[string]$machines
	)
$global:rfc = $rfc
$global:guid = ([guid]::NewGuid()).tostring()
$global:date = [datetime]::now
$global:SqlServer = "<server>"
$global:Database = "Root_Of_All_Evil"
$debug = $false
$taskSequence = "Install_Updates_Now"
<# Powershell Script for Running a ConfigMgr Task Sequence On-Demand
	Written By: Jeremy Young (jeremy.young@microsoft.com)
	http://myitforum.com/cs2/blogs/jeremyyoung/

	v-sbsers - Added Features, Error handling, And Logging
#>

<#
	Changes in this version.
New changes:
Script logs all output to a file created in the same directory. The log file will either be named based off the date and time the script is run, or, if the �rfc switch is used, will be created using the rfc number as the log name.

New switches:
-rfc
	If provided, will create the script log file using the rfc number as its name
-machines
	Can take a single machine or a path to a file. If �machines is not provided, the user will be prompted for a file.

Examples:
.\Run-Install_Updates_Now.ps1 �rfc 12121212
Creates a log file named 12121212.txt and prompts for an input file
.\Run-Install_Updates_Now.ps1 �rfc 12121212 �machines .\patching.txt
Creates a log file named 12121212.txt and reads the patching.txt file to find machines to patch.
.\Run-Install_Updates_Now.ps1 �machines
Creates a log file named based off the current date and attempts to batch the server .
.\Run-Install_Updates_Now.ps1
Creates a log file based off the current date and prompts for an input file


#>

function Invoke-ConfigMgrTaskSequence {
	[CmdletBinding()]
	[OutputType([System.Int32])]
	param(
		[Parameter(Position=0,Mandatory=$true)]
		[System.String]
		$ComputerName,

		[Parameter(Position=1,Mandatory=$true)]
		[System.String]
		$TaskSequenceName
	)
	try	{
		$ResourceID = Get-SccmResourceId -hostname $ComputerName
		if (Test-Connection -ComputerName $computerName -Quiet){
			if (Test-WmiNamespaceExists -namespace "ccm" -ComputerName $computerName){
				if (Test-WmiNamespaceExists -parent "root\ccm" -namespace "LocationServices" -ComputerName $computerName){
					[array]$MP = gwmi -namespace root\ccm\locationservices -class sms_mpinformation
					if ($MP.Count -eq 1){
						if (($MP.MP -eq [string]::empty) -or ($MP.MP -eq $null)){
							If ($debug) { Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 0 -resultType "Informational" -resultText "Searching for ads for [$TaskSequenceName] on [$ComputerName]" }
							$softwareDist = Get-WmiCustom -Namespace root\ccm\policy\machine\actualconfig -Query "Select * from CCM_SoftwareDistribution where PKG_Name = '$TaskSequenceName' and PRG_ProgramID = '*'" -server $ComputerName -timeout 2
							ForEach ($sd in $softwareDist){
								$adID=$sd.ADV_AdvertisementID
								$pkgID=$sd.PKG_PackageID
								If ($debug) {Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 0 -resultType "Informational" -resultText "Found ad [$adID] for [$TaskSequenceName] with pkgID [$pkgID]" }

								$schedules = Get-WmiCustom -Namespace root\ccm\policy\machine\actualconfig -Query "Select * from CCM_Scheduler_ScheduledMessage where ScheduledMessageID like '$($adID)-$($pkgID)-%'" -server $ComputerName -timeout 2
								ForEach ($schedule in $schedules){
									$policyID=$schedule.ScheduledMessageID
									if ($policyID){
										If ($debug) { Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 0 -resultType "Informational" -resultText "Found policy [$policyID] for ad [$adID]" }
									} else {
										Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 8 -resultType "Error" -resultText "Schedule Policy not found for [$TaskSequenceName] on [$ComputerName]."
									}
								}
							}
							If ($debug) { Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 0 -resultType "Informational" -resultText "Connecting to cm client on [$ComputerName]" }
							$cmclient = [wmiclass] "\\$ComputerName\root\ccm:SMS_Client"
							If ($debug) { Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 0 -resultType "Informational" -resultText "Triggering policy on [$ComputerName]" }
							try {
								$ErrorActionPreference = "Stop"
								$out=$cmclient.TriggerSchedule($policyID)
								Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 0 -resultType "Informational" -resultText "Successfully Triggered Sccm Policy on [$ComputerName]"
							} catch {
								Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 7 -resultType "Error" -ResultText "Error Triggering Policy. [$ComputerName] may require a reboot prior to starting job"
								continue
							} finally {
								$ErrorActionPreference = "Continue"
							}
						} else {
							Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 6 -resultType "Error" -resultText "No Management Point is configured for [$ComputerName]. SCCM client should be reinstalled."
						}
					} else {
						Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 5 -resultType "Error" -resultText "Multiple Management Points Detected on [$ComputerName]. SCCM client should be reinstalled"
					}
				} else {
					Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 4 -resultType "Error" -resultText "Wmi Namespace [root\ccm\LocationServices] for SCCM does not Exist on [$ComputerName]. The SCCM Client needs to be repaired."
				}
			} else {
				Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 2 -resultType "Error" -resultText "Unable to connect to [root\ccm] namespace on [$ComputerName]. Either we can not access WMI or the SCCM Client is not installed."
			}
		} else {
			Write-OSDLog -hostname $computerName -SccmResourceID $ResourceID -resultCode 1 -resultType "Error" -resultText "Unable to ping to [$ComputerName]"
		}
	}
	catch {
		continue
	}
}

function Write-OSDLog() {
	param(
		[Parameter(Mandatory=$true)]
		[int]$resultCode,
		[Parameter(Mandatory=$true)]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText,
		[string]$hostname,
		[string]$SccmResourceID,
		[switch]$nosql
	)
	begin {
		$LogOutput = [string]::Format("{0}:{1}:{2}",$resultCode,$resultType,$resultText)

	}
	process {
		tee-object -InputObject $logoutput -Variable tee
		$tee | Out-File -Append -FilePath $logpath -Encoding ascii
		if (-not $nosql){
			$query = Create-SqlQueryForPatchLogging -resultCode $resultCode -resultType $resultType -resultText $resultText -hostname $hostname -sccmResId $SccmResourceID
			$result = Write-Sql -server $global:SqlServer -database $global:Database -query $query
		}
	}
}

function Create-SqlQueryForPatchLogging(){
param(
	[string]$resultCode,
	[string]$resultType,
	[string]$resultText,
	[string]$hostname,
	[string]$rfc,
	[string]$sccmResID
)
	$startTime = $global:Date
	$batchID = $global:guid
	$rfc = $global:rfc
	$query = "INSERT INTO SccmPatchingStatus (BatchID, RFC, StartTime, ServerName, ErrorNumber, ResultType, ResultText, SccmResourceID) "
	if ($hostname -eq [string]::empty){
		$query += "VALUES ('$batchID','$rfc','$startTime',$null,'$resultCode','$resultType','$resultText','$sccmResID')"
	} else {
		$query += "VALUES ('$batchID','$rfc','$startTime','$hostname','$resultCode','$resultType','$resultText','$sccmResID')"
	}
	return $query
}

function Get-SccmResourceId()
{
	param (
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$hostname
	)
	$resource = Get-WmiObject -ComputerName <server> -Class SMS_R_System -Namespace root\sms\site_CEN -Filter "NetbiosName='$hostname'"
	if ($resource -ne $null){
		Write-Output $resource.resourceid
	} else { write-output 0 }
}


Function Test-WmiNamespaceExists() {
	param(
		[Parameter(Mandatory=$true)]
		[string]$namespace,
		[Parameter(Mandatory=$true)]
		[string]$ComputerName,
		[string]$parent = "root"
	)
	begin {
		$ErrorActionPreference = "Stop"
		try {
			$exists = Get-WmiCustom -server $ComputerName -namespace $parent -class "__Namespace" -timeout 1 | where {$_.Name -eq $namespace}
		}
		catch {
			$exists = $null
		}
		finally {
			$ErrorActionPreference = "Continue"
		}
	}
	process {
        if ($exists -ne $null)
        {
            return $true
        }
        else
        {
            return $false
        }
	}
}

function Write-Sql {
    Param ($server, $query, $database)
    $conn = new-object ('System.Data.SqlClient.SqlConnection')
    $connString = "Server=$server;Integrated Security=SSPI;Database=$database"
    $conn.ConnectionString = $connString
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $result = $sqlCmd.ExecuteNonQuery()
    $result
}

Function Get-WmiCustom([string]$server,[string]$namespace='root\cimv2',[string]$class,[int]$timeout=15,[string]$filter, [string]$query)
{
	$ConnectionOptions = new-object System.Management.ConnectionOptions
	$EnumerationOptions = new-object System.Management.EnumerationOptions

	$timeoutseconds = new-timespan -seconds $timeout
	$EnumerationOptions.set_timeout($timeoutseconds)

	$assembledpath = "\\" + $server + "\" + $namespace
	#write-host $assembledpath -foregroundcolor yellow

	$Scope = new-object System.Management.ManagementScope $assembledpath, $ConnectionOptions
	$Scope.Connect()
	if ($query){
		$querystring = $query
	} else {
		$querystring = "SELECT * FROM " + $class + " " + $filter
	}
	#write-host $querystring
	$query = new-object System.Management.ObjectQuery $querystring
	$searcher = new-object System.Management.ManagementObjectSearcher
	$searcher.set_options($EnumerationOptions)
	$searcher.Query = $querystring
	$searcher.Scope = $Scope

	trap [Exception] { write-error "Error connecting to wmi on server: $server";break; } $result = $searcher.get()

	return $result
}

function Select-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$Filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	if ($Show -eq "OK")
	{
		return $objForm.FileName
	}
	else
	{
		return $null
	}
}

if ($rfc){ $global:logpath = "$pwd\$rfc.log"}
else {
	$datepart = $global:date.ToShortDateString().replace("/",".")
	$timepart = $global:date.ToShortTimeString().replace(":",".")
	$global:logpath = "$pwd\$datepart-$timepart.log"
}
Write-OSDLog -resultCode 0 -resultType "Informational" -resultText "Starting Batch at $global:date" -nosql
#If $machines is defined we either have a path to a file or are an individual server
if ($machines){
	# test path to the machines variable to determine if we were given a file path
	if (test-path -Path $machines){
		Write-OSDLog -resultCode 0 -resultType "Informational" -resultText "Found file at $machines" -nosql
		$serverlist = gc $machines
	}
	#Appears to be a server instead of a list.
	else {
		$serverlist = $machines
		Write-OSDLog -resultCode 0 -resultType "Warning" -resultText "[-machines] appears to be a single server." -nosql
	}
}
# $machines is not defined, show the select file dialog
else {
	$file = Select-FileDialog -Title "Select a Server List" -Directory ".\" -Filter "Text files (*.txt)|*.txt"
	#verify the user chose a file. if $file is null the user canceled out
	if ($file){
		$serverlist = gc $file
	} else {
		Write-OSDLog -resultCode 4 -resultType "Error" -resultText "User Canceled out of Select File Dialog"
	}
}

# Validate we got something in the serverlist
if ($serverlist){
	# Figure out if we have 1 server or an array of servers so we can show/log the count of machines we are touching.
	if ($serverlist.GetType().Name -eq "String"){
		$serverCount = 1
	} else {
		$serverCount = $serverlist.Count
	}
		Write-OSDLog -resultCode 0 -resultType "Informational" -resultText "Currently Processing $serverCount Machine(s)" -nosql
	#loop through each machine triggering the task sequence
	foreach ($server in $serverlist){
		Invoke-ConfigMgrTaskSequence -ComputerName $server -TaskSequenceName $taskSequence
	}
}
