# $siteCode variable needs to be defined globaly.
# Finds the resource of a machine by its name in SCCM. This id is needed for other actions such as adding and removing from collection
$logpath = $env:systemroot + "\temp\OSDeploy.log"
$logCount = 1
function Write-OSDLog()
{
	param(
		[Parameter(Mandatory=$true)]
		[int]$resultCode,
		[Parameter(Mandatory=$true)]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText
	)
	begin {
		$LogOutput = [string]::Format("{0}:{1}:{2}:{3}",$logCount,$resultCode,$resultType,$resultText)
	}
	process {
		$logOutput | Out-File -Append -FilePath $logpath -Encoding ascii
		$logCount++
	}
}
function Get-SccmResourceId()
{
	param (
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$hostname
	)
	begin {
		# Determine Environment and set $siteCode correctly. Uses domain of the machine the script is being run from, so this should be run 'in' the environment.
		# if this is to be used as an all in one module, this should be extracted into it's own function.
		switch (([System.DirectoryServices.ActiveDirectory.Domain]::GetComputerDomain()).Name) {
			"<domain>" {$siteCode = "RR1"}
			"<domain>" {$siteCode = "Q01"}
			default {$siteCode = $null}
		}
	}
	process {
		if ($siteCode -ne $null) {
			$resource = Get-WmiObject -Class SMS_R_System -Namespace root\sms\site_$siteCode -Filter "Name = '$hostname'"
			if ($resource -ne $null){
				Write-Output $resource.resourceid
			}
		} else { Write-Output "Error determining Site Code" }
	}
}

# This doesn't seem to useful for a non-GUI app, but i'm including it anyway
function Get-SccmCollectionMembers()
{
	param (
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$collectionID
	)
	begin {
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"}
			"<domain>" {$siteCode = "Q01"}
			default {$siteCode = $null}
		}
	}
	process {
		if ($siteCode -ne $null){
			$collectionMembers = Get-WmiObject -Class SMS_CM_RES_COLL_$collectionID -Namespace root\sms\site_$siteCode | select Name
			if ($collectionMembers -ne $null){
				Write-Output $collectionMembers
			}
		} else { Write-Output "Error determining Site Code" }
	}
}

# Finds the CollectionID of a collection by its name in SCCM. This id is needed for other actions
function Get-SccmCollectionID()
{
	param (
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$collectionName
	)
	begin {
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"}
			"<domain>" {$siteCode = "Q01"}
			default {$siteCode = $null}
		}
	}
	process {
		if ($siteCode -ne $null){
			$collection = Get-WmiObject -Class SMS_Collection -Namespace root\sms\site_$siteCode -Filter "Name = '$collectionName'"
			if ($collection -ne $null){
				Write-Output $collection.CollectionID
			}
		} else { Write-Output "Error determining Site Code" }
	}
}
# Remember, we import computers to the PRI not the ADM
# ImportMachineEntry([string]NetbiosName,[string]SMBIOSGUID,[string]MacAddress,[bool]OverwriteExistingRecord,[string]FQDN,[bool]isAMTMachine,[string]MEBxPassword,[string]AdminPassword)
function New-ImportMachine()
{
	param (
	[parameter(Mandatory=$true)]
	$hostname,
	[parameter(Mandatory=$true)]
	$MacAddress,
	[parameter(Mandatory=$false)]
	$overwrite = $true,
	[parameter(Mandatory=$false)]
	$ManagementPoint = "<server>",
    [parameter(Mandatory=$false)]
    $collectionToAddTo
	)
	begin {
		$smsProvider = gwmi -Namespace root\sms -ComputerName $managementpoint -query "select * from sms_providerlocation"
        $namespacePath = $smsProvider.NamespacePath
        $method = "ImportMachineEntry"
	}
	process {
		if ($namespacePath -ne $null) {
			$site = [wmiclass]"$namespacePath`:SMS_Site"
            $inParams = $site.GetMethodParameters($method)
            $inParams.MacAddress = $MacAddress
            $inParams.NetbiosName = $hostname
            $inParams.OverwriteExistingRecord = $overwrite
            $result = $site.ImportMachineEntry($inParams.NetBiosName,$inParams.SMSBIOSGUID,$inParams.MACAddress,$inParams.OverwriteExistingRecord,$inParams.FQDN,$inParams.IsAMTMachine,$inParams.MEBxPassword,$inParams.AdminPassword,$inParams.AddToCollection,$inParams.CollectionRule,$inParams.CollectionId,$inParams.WTGUniqueKey)
            if (-not $result.returnvalue -eq 0){ write-host "Error Adding computer to site";exit 1 }
            if ($collectionToAddTo){
                Write-host "Preparing to add to collection"
                $CollectionID = (gwmi -namespace root\sms\site_xbl -ComputerName $smsProvider.Machine -Class sms_collection -filter "Name='$collectionToAddTo'").CollectionID
                $collection = [wmi]"$namespacePath`:SMS_Collection.CollectionID='$collectionID'"
                $collRule = ([wmiclass]"$namespacePath`:SMS_CollectionRuleDirect").createinstance()
                $collRule.RuleName = $hostname
                $collRule.ResourceClassName = "SMS_R_System"
                $collRule.ResourceID = $result.ResourceID
                $AddToCollRes = $collection.AddMembershipRule($collRule)
            }

			if ($AddToCollRes.ReturnValue -eq 0){
                Write-Host "Successfully added computer to collection"
				exit 0
			} else {
                Write-host "Error Adding computer to collection"
				exit 2
			}
		} else { exit 4 }
	}
}

function Add-SccmComputerToCollectionByName()
{
	param(
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$ResourceName,
	[parameter(Mandatory=$true)]
	[string]$CollectionName
	)
	begin {
		$ResourceID = Get-SccmResourceId -hostname $ResourceName
		$CollectionId = Get-SccmCollectionID -CollectionName $CollectionName
	}
	process{
		if ($ResourceID -eq $null) { Write-Host "ResourceID Is Null" }
		if ($CollectionId -eq $null) { Write-Host "CollectionId Is Null" }
		$i = 1
		while ((Add-SccmComputerToCollectionByID -ResourceId $ResourceID -CollectionID $CollectionID) -ne $true) {
			if ($i -lt 6){
				Start-Sleep -Seconds 5
			} else {
				Write-Host "Failed to Add $ResourceName to $CollectionName"
			}
		}
	}

}

# Adds a computer (by resource id) to a given collection (by collection id)
function Add-SccmComputerToCollectionByID()
{
	param(
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$resourceID,
	[parameter(Mandatory=$true)]
	[string]$collectionID
	)
	begin {
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"}
			"<domain>" {$siteCode = "Q01"}
			default {$siteCode = $null}
		}
	}
	process {
		if ( $siteCode -ne $null){
			$collection = [wmi]"\\.\root\sms\site_$sitecode`:SMS_Collection.CollectionID='$collectionID'" # the : needs to be escaped or things go badly
			$directRule = [WmiClass]"\\.\root\SMS\Site_$siteCode`:SMS_CollectionRuleDirect" # the : needs to be escaped or things go badly
			$directInstance = $directRule.CreateInstance()
			$directInstance.ResourceClassName = "SMS_R_System"
			$directInstance.ResourceID = $resourceID
			$result = $collection.AddMembershipRule($directInstance)
			if ($result.ReturnValue -eq 0){
				# If this fails, it's not the end of the world, the server has been added to the collection, it just might take a bit longer to show up, also doesnt generally fail
				$collection.RequestRefresh() | Out-Null
				Write-Output $true
			}
			else{
				Write-Output $false
			}
		} else { Write-Output "Error determining Site Code" }
	}
}

function Remove-SccmComputerFromCollectionByName()
{
	param(
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$ResourceName,
	[parameter(Mandatory=$true)]
	[string]$CollectionName
	)
	begin {
		$ResourceID = Get-SccmResourceId -hostname $ResourceName
		$CollectionId = Get-SccmCollectionID -CollectionName $CollectionName
	}
	process{
		if ($ResourceID -eq $null) { Write-Host "ResourceID Is Null" }
		if ($CollectionId -eq $null) { Write-Host "CollectionId Is Null" }
		$i = 1
		while ((Remove-SccmComputerFromCollectionByID -ResourceId $ResourceID -CollectionID $CollectionID) -ne $true) {
			if ($i -lt 6){
				Start-Sleep -Seconds 5
			} else {
				Write-Host "Failed to Add $ResourceName to $CollectionName"
			}
		}
	}
}

# Removes a computer (by resource id) from a collection (by collection id)
function Remove-SccmComputerFromCollectionByID()
{
	param(
	# not sure how to take these both from the pipeline yet and i need them both, so i'm leaving 'ValueFromPipeline' out for now
	[parameter(Mandatory=$true)]
	[string]$resourceID,
	[parameter(Mandatory=$true)]
	[string]$collectionID
	)
	begin {
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"}
			"<domain>" {$siteCode = "Q01"}
			default {$siteCode = $null}
		}
	}
	process {
		if ($siteCode -ne $null){
			$collection = [wmi]"root\sms\site_$sitecode`:SMS_Collection.CollectionID='$collectionID'" # the : needs to be escaped or things go badly
			$rule = $collection.CollectionRules | where {$_.ResourceID -eq $resourceID}
			$result = $collection.DeleteMembershipRule($rule)
			if ($result.ReturnValue -eq 0){
				# If this fails, it's not the end of the world, the server has been added to the collection, it just might take a bit longer to show up, also doesnt generally fail
				$collection.RequestRefresh() | Out-Null
				Write-Output $true
			}
			else{
				Write-Output $false
			}
		} else { Write-Output "Error determining Site Code" }
	}
}

function Remove-AllComputersFromCollection()
{
	param(
	[parameter(Mandatory=$true)]
	[string]$collectionID
	)
	begin {
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"}
			"<domain>" {$siteCode = "Q01"}
			default {$siteCode = $null}
		}
	}
	process {
		if ($siteCode -ne $null) {
			$Method = "DeleteAllMembers"
			$collection = Get-WmiObject -Class SMS_Collection -Namespace root\sms\site_$siteCode -Filter "CollectionID = '$collectionID'"
			$removed = $collection.psbase.InvokeMethod($Method, $null)
			Write-Host "Removed $removed servers from the collection"
		} else { Write-Output "Error determining Site Code" }
	}
}

# these work, but by default would probably point to the admin server. Clearing PXE on the admin server causes a delay because the request has to be send
# along to the PRI then PXE is cleared. Could add some logic here to figure out what the environment is and connect to the PRI for clearing PXE
# the problem with connecting to the PRI is it seems like MACHINES have different resource id's on ADM and PRI... example follows.
#PS C:\Users\TEMP> $resource = Get-WmiObject -Class SMS_R_System -Namespace root\sms\site_rr1 -filter "Name='<server>'"
#PS C:\Users\TEMP> $resource.resourceid
#450
#PS C:\Users\TEMP> $resource = Get-WmiObject -Class SMS_R_System -Namespace root\sms\site_rr2 -filter "Name=''" -computer <server>
#PS C:\Users\TEMP> $resource.resourceid
#312
# collectionID's appear to be uniform across servers so ClearLastPXEforCollections can be pointed at the pri easily

# Clears PXE for an entire collection (by collection ID)
function Clear-LastPXEforCollectionByName()
{
	param (
	[Parameter(Mandatory=$true)]
	[string]$CollectionName
	)
	begin {
		$collectionID =
	}
}
function Clear-LastPXEforCollectionById()
{
	param (
	[parameter(Mandatory=$true)]
	[string]$collectionID
	)
	begin {
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"}
			"<domain>" {$siteCode = "Q01"}
			default {$siteCode = $null}
		}
	}
	process {
		if ($siteCode -ne $null) {
			$collection = [wmi]"root\sms\site_$siteCode`:SMS_Collection.CollectionID='$collectionID'" # the : needs to be escaped or things go badly
			$result = $collection.ClearLastNBSAdvForCollection()
		} else { Write-Output "Error determining Site Code" }
	}
}

# Clears PXE for a specific computer (by resource id)
function Clear-LastPXEforMachine()
{
	param (
	[parameter (Mandatory=$true,ValueFromPipeline=$true)]
	$resourceID
	)
	begin {
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"}
			"<domain>" {$siteCode = "Q01"}
			default {$siteCode = $null}
		}
	}
	process {
		if ($siteCode -ne $null){
			$collection = [wmiclass]"root\sms\site_$siteCode`:SMS_Collection" # the : needs to be escaped or things go badly
			$result = $collection.ClearLastNbsAdvForMachines($resourceID) #this can take an array of resource id's, tho i havn't tested it yet
			if ($result.StatusCode -eq 0)
			{
				Write-Host "Successfully cleared PXE for Resource: $resourceID"
			}
			else
			{
				Write-Host "Failed to Clear PXE for Resource: $resourceID" -ForegroundColor Red
			}
		} else { Write-Output "Error determining Site Code" }
	}
}
