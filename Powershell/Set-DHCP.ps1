﻿$ErrorActionPreference = "Stop"
"-------------------------------------------------" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
"Beginning new run of Set-DHCP Script at $(get-date)" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
[array]$nics = gwmi win32_networkadapterconfiguration -Filter "DHCPEnabled='false'" | where {$_.DefaultIPGateway -ne $null}
"Attempting to set $($nics.Count) Adapters to DHCP" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
$nicsBackup = $nics

function Set-toDHCP()
{
param(
    [validatenotnullorempty()]
    [array]$nics
)
    foreach ($nic in $nics){
        "Attempting to set adapter $($nic.Caption) to DHCP" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
        "Current IP: $($nic.ipaddress[0])" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
        "Current Gateway: $($nic.DefaultIPGateway[0])" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
        $nic.EnableDHCP() | out-null
        $nic.SetDNSServerSearchOrder() | out-null
	$adapter = gwmi win32_networkAdapter -filter "DeviceID='$($nic.Index)'"
	$adapter.Disable() | out-null
	$adapter.Enable() | out-null
	start-sleep 15
        $nicTest = gwmi win32_networkadapterconfiguration -filter "Index='$($nic.Index)'"
        if ($nicTest.DHCPEnabled -eq $true){
            "Nic is set to DHCP Successfully" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
            $ipv4Ip = $nicTest.IPAddress | where {$_.length -le 15}
		"New IP Address is $ipv4ip"
            if ($ipv4Ip -eq $null -or $ipv4Ip.StartsWith("169.")){
                "After setting adapter $($nictest.Caption) to DHCP, we were unable to aquire a valid ip address from DHCP" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
                Write-Output $false
            } else {
                "Successfully aquired a valid ip address for adapter $($nicTest.Caption)" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
                Write-Output $true
            }
        } else {
            "Failed to enable dhcp for adapter $($nicTest.Caption)" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
            Write-Output $false
        }
    }
}

function Restore-NICConfig()
{
param(
    [validatenotnullorempty()]
    [array]$nicsBackup
)
    "Beginning attempt to restore ip configuration" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
    foreach ($nic in $nicsBackup){
        "Restoring IP configuration for adapter $($nic.Caption)" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
        try{
            $nicToRestore = gwmi win32_networkadapterconfiguration -filter "Index='$($nic.Index)'"
            $nicToRestore.EnableStatic($nic.IPAddress, $nic.IPSubnet)
            $nicToRestore.SetGateways($nic.DefaultIPGateway, $nic.GatewayCostMetric)
            $nicToRestore.SetDNSServerSearchOrder($nic.DNSServerSearchOrder)
        } catch {
            "Failed to revert dhcp" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
        }
    }    
}
if ($nics.Count -gt 0){
    "Attempting to set static nics to DHCP" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
    [array]$results = Set-toDHCP -nics $nics
    if ($results -contains $true){
        "Successfully set $(($results | where {$_ -eq $true}).Count) adapter(s) to DHCP" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
        return 999
    } else {
        "Failed setting any adapters to DHCP, Restoring Original Configuration" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
        Restore-NICConfig -nicsBackup $nicsBackup
        return 666
    }
} else {
    "All Nics on this machine are currently configured as DHCP" | Out-File -FilePath .\Set-DHCP.log -Encoding ascii -Append
    return 999
}