<#
.SYNOPSIS
   Script to change DNSServerSearchOrder
.DESCRIPTION
   This script changes the DNSServerSearchOrder on servers it is run against. The nic that is changed will be the nic on the server that has a pre-existing DNSServerSearchOrder set and DHCP is not Enabled
.PARAMETER <paramName>
   -DNSServers - An array of dns servers to set as the DNSServerSearchOrder
.EXAMPLE
   .\Set-DnsServers.ps1 -DNSServers "10.187.226.146","10.187.226.147"
   PowerShell -command ".\set-DNSServers.ps1 -DNSSERVERS "10.187.226.146","10.187.226.147";exit $LASTEXITCODE"
#>
param([string[]]$DNSServers)
# Dump DNS Cache to variable. $DNSCache will be an array
$DNSCache = ipconfig /displaydns | select-string 'A \(Host\) Record' | foreach-object { $_.ToString().Split(' ')[-1] }

# Get the nic we will be operating on. find a nic on the server where the existing DnsServerSearchOrder is not null (we don't set dns on FE nics) and DHCP is not Enabled
$nic = Get-WmiObject Win32_NetworkAdapterConfiguration  | where {$_.DnsServerSearchOrder -ne $null -and $_.DHCPEnabled -eq $false}
# If nic has some value
if ($nic){
	# If more then 1 nic, exit. Count only works if there is more then 1 entry which makes $nic into an array instead of a single object
	if ($nic.Count -gt 1){
		# More then 1 nic was returned
		write-host "More then 1 nic was found that met the criteria for changing the DNS"
		exit 2
	} else {
		# Get the current DNSServerSearchOrder in case we need to roll back
		$currentdns = $nic.DnsServerSearchOrder
		write-host "Current DNS Servers: "
		$currentdns
		# Set the DNSServerSearchOrder to the new dns servers
		$result = ($nic.SetDNSServerSearchOrder($DNSServers)).ReturnValue
		if ($result -eq 0){
			# Flush DNS
			ipconfig /flushdns
			# Set ErrorActionPreference to 'Stop' so Try/Catch/Finally works...
			$ErrorActionPreference = "Stop"
			# Loop through the entries in dns cache
			write-host "Attempting to resolve A Record entries from dns cache"
			foreach ($dnsEntry in $DNSCache){
				try {
					# Try to resolve each entry. the Resolve method either works or throws an exception which is why we are using Try/Catch/Finally
					[system.net.dns]::GetHostEntry($dnsEntry)
					write-host "Happy Happy Joy Joy" -ForeGroundColor "Green"
				}
				catch {
					# If Resolve threw an execption, set the DNS servers back to what they were
					write-host "Failed to resolve $dnsentry, reverting dns changes" -ForeGroundColor "Red"
					$nic.SetDNSServerSearchOrder($currentdns)
					#set ErrorActionPreference Back
					$ErrorActionPreference = "Continue"
					# and exit with a 1
					exit 1
				}
			}
		} elseif($result.ReturnValue -eq 1){
			write-host "Server thinks it needs a reboot, panic!!" -ForeGroundColor "Red"
			$ErrorActionPreference = "Continue"
			exit 666
		} else {
			write-host "Failed to set dns servers correctly" -ForeGroundColor "Red"
			$ErrorActionPreference = "Continue"
			exit $result
		}
	}		
} else {
	# No Nics were returned that met the criteria required
	write-host "No Nics were found that met the criteria for changing the DNS"
	exit 3
}
