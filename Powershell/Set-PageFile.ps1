#region Functions
function Write-OSDLog()
{
	param(
		[parameter(Mandatory=$false)]
		[string]$logpath = $env:systemroot + "\temp\OSDeploy.log",
		[Parameter(Mandatory=$true)]
		[int]$resultCode,
		[Parameter(Mandatory=$true)]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText
	)
	begin {
		$LogOutput = [string]::Format("{0}:{1}:{2}:{3}",$logCount,$resultCode,$resultType,$resultText)
	}
	process {		
		Tee-Object -InputObject $LogOutput -Variable tee
		Out-File -FilePath $logpath -InputObject $tee -Append
	}		
}
function Disable-AutomaticPageFile()
{
	begin {
		Write-OSDLog -resultCode 0 -resultType "Informational" -resultText "Starting Disable-AutomaticPageFile. This will turn off the `'Automatically manage paging file size for all drives`' setting"
	}
	process {
		$pc = Get-WmiObject -Class Win32_ComputerSystem -EnableAllPrivileges -Impersonation 3
		$pc.AutomaticManagedPagefile=$false
		$pc.put()
		if ((Get-WmiObject -Class Win32_ComputerSystem -EnableAllPrivileges -Impersonation 3).AutomaticManagedPagefile = $true){
			Write-Output $false
		} else {
			Write-Output $true
		}
	}
}

function Set-PagefileSize()
{
	param(
	[Parameter(Mandatory=$true)]
	[int]$ram,
	[Parameter(Mandatory=$true)]
	[int]$bits
	)
	begin {
		Write-OSDLog -resultCode 0 -resultType "Informational" -resultText "Starting Set-PageFileSize Function. This will attempt to set the page file to $ram + 1024MB for 64 bit servers and 4096 for 32 bit servers."
		$pagefile = get-wmiobject win32_pagefilesetting
	}
	process {
		switch ($bits.tostring()) {
			"64" {
				$pfSize = [int]$ram + 1024
				$pagefile.initialsize = $pfSize
				$pagefile.maximumsize = $pfSize
				$pagefile.put() 
				if ((Get-WmiObject -class win32_pagefilesetting).maximumsize -ne ([int]$ram + 1024)){
					Write-OSDLog -resultCode 1 -resultType "FATAL" -resultText "Error Setting Pagefile to $pfSize"
					exit 1
				} else {
					Write-OSDLog -resultCode 0 -resultType "ExitSuccess" -resultText "Successfully Set Pagefile to $pfSize"
					exit 0
				}
			}
			"32" {
				$pfSize = 4096
				$pagefile.initialsize = $pfSize
				$pagefile.maximumsize = $pfSize
				$pagefile.put()
				if ((Get-WmiObject -class win32_pagefilesetting).maximumsize -ne 4096){
					Write-OSDLog -resultCode 1 -resultType "FATAL" -resultText "Error Setting Pagefile to $pfSize"
					exit 1
				} else {
					Write-OSDLog -resultCode 0 -resultType "ExitSuccess" -resultText "Successfully Set Pagefile to $pfSize"
					exit 0
				}
			}
			default {
				Write-OSDLog -resultCode 1 -resultType "FATAL" -resultText "Error determining processor bit size (32-64)"
				exit 1
			}
		}
	}
}
#endregion

#region Main
if ((Get-WmiObject -Class Win32_ComputerSystem -EnableAllPrivileges -Impersonation 3).AutomaticManagedPagefile = $true){
	$result = Disable-AutomaticPageFile
	if ($result -eq $false){
		Write-OSDLog -resultCode 1 -resultType "FATAL" -resultText "Error Disabling Automatic "
	} else {
		Write-OSDLog -resultCode 0 -resultType "Success" -resultText "Successfully Disabled Automatically Managed Pagefile Setting"
	}
}
$winProcessor = get-wmiobject win32_processor
$bits = $winProcessor[0].Addresswidth

$ram = (get-wmiobject win32_operatingsystem).TotalVisibleMemorySize
$ram = ($ram / 1kb).tostring("F00")

$freeSpace = ((get-wmiobject Win32_LogicalDisk -filter "DeviceID='C:'").freespace / 1mb).tostring("F00")
$freespace = [int]$freespace * .8 #make sure to leave 20% free space
if ([int]$ram -gt [int]$freespace) { $ram = $freespace }

set-pagefilesize -ram $ram -bits $bits

#endregion