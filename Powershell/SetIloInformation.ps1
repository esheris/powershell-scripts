﻿param(
    [ValidateNotNullOrEmpty()]
    [string]$BuildBatch
)

try{
	import-module -name netcmdlets
} catch {
	write-host "Need to be able to access the netcmdlets powershell modules. please run from tls801"
	exit 666
}

$iloUsername = "Administrator"
$iloPassword = "GFSILOGFSILO"
"$(get-date) - Starting New run of SetIloInformation Script." | Out-File -FilePath .\setIloInformation.log -Encoding ascii -Append

function Read-SQL {
    Param (
        [ValidateNotNullOrEmpty()]
        [string]$server, 
        [ValidateNotNullOrEmpty()]
        [string]$query, 
        [ValidateNotNullOrEmpty()]
        [string]$database
    )

	$connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = <user>
	$connString.psbase.Password = "P@`$`$word"
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database

	$conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $table = New-Object System.Data.DataTable
	$table.load($sqlCmd.ExecuteReader())
	$conn.Close()
	return $table
}

function Set-IloInformation(){
    param(
        [ValidateNotNullOrEmpty()]
        [System.Net.IPAddress]$iloIP,
        [ValidateNotNullOrEmpty()]
        [string]$username,
        [ValidateNotNullOrEmpty()]
        [string]$password,
        [ValidateNotNullOrEmpty()]
        [string]$serverNameFromManifest
    )
    $ip = $iloIP.IPAddressToString
    $xseset=0
    try{
    # Log into ilo with Administrator Account configured by GFS and create our xse account
    write-host "Adding xse account to $serverNameFromManifest" -ForegroundColor Green
    Invoke-SSH -AuthMode password -Server $ip  -User $username -Password $password -Force -Command "create /map1/accounts1 username=xse password=L1veops@lsgoi name=xse group=admin,config,oemhp_rc,oemhp_power,oemhp_vm" -ErrorAction Stop | Out-Null
    $xseset = 1
    } catch {
        Write-Host "Error logging into $ip with user $username and Pass: $password" -ForegroundColor Red
        "$(get-date) - Error logging into $ip with user $username and Pass: $password" | Out-File -FilePath .\setIloInformation.log -Encoding ascii -Append
    }
    if ($xseset -eq 1){
        # set nic to first boot device this is slightly painful since booting from the external nic would be bootsource6 but bootsource6 doesnt display in the ilo. no real way to tell which we should use, leaving commented out.
        #Invoke-SSH -AuthMode password -Server $iloIP -User "xse" -Password "L1veops@lsgoi" -Force -Command "set /system1/bootconfig1/bootsource6 bootorder=1"
        # delete Administrator account set by GFS
        Write-Host "Deleting Administrator account from ilo using recently created xse account" -ForegroundColor Green
        Invoke-SSH -AuthMode password -Server $iloIP -User "xse" -Password "L1veops@lsgoi" -Force -Command "delete /map1/accounts1/Administrator" | Out-Null

        Write-Host "Setting Ilo DNS Hostname to $serverNameFromManifest per manifest" -ForegroundColor Green
        $dnsNameCmd = [string]::Format("set /map1/dnsendpt1 Hostname=OOB-{0}",$serverNameFromManifest)
        # Unfortunately, this always throws an error due to the ilo resetting immediately after the command runs
        Invoke-SSH -AuthMode password -Server $iloIP -User "xse" -Password "L1veops@lsgoi" -Force -Command $dnsNameCmd -ErrorAction SilentlyContinue | Out-Null
        
    } 
}

$iloServers = Read-SQL -server "<server>" -query "Select v_imagedServerName, v_outofbandip from Pandoras_Box where v_imagingbatchid = '$BuildBatch'" -database Root_of_all_evil
$serversInBatch = $iloServers.count
Write-Host "Processing $serversInBatch Servers" -ForegroundColor Green
$i = 1
foreach ($server in $iloServers){
    $ilo = $server.v_outofbandip
    $name = $server.v_imagedservername

    Write-Host "Executing against server number $i of $serversInBatch : $name with ilo ip of $ilo" -ForegroundColor Green
    if (Test-Connection -Quiet -ComputerName $ilo){

        Set-IloInformation -iloIP $ilo -username $iloUsername -password $iloPassword -serverNameFromManifest $name
        
    } else {
        write-host "Unable to ping ilo ip: $ilo" -ForegroundColor Red
        "$(get-date) - Unable to ping ilo ip: $ilo" | Out-File -FilePath .\setIloInformation.log -Encoding ascii -Append
    }
    $i++
}

Write-Host "Please check the log file - setIloInformation.log - located in the same directory as this script for errors" -ForegroundColor Cyan
