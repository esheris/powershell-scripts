$pagefile = get-wmiobject win32_pagefilesetting
# $tsenv = New-Object -COMObject Microsoft.SMS.TSEnvironment
# $pageFileSize = $tsenv.Value("PageFileMin")
$winProcessor = get-wmiobject win32_processor
$bits = $winProcessor[0].Addresswidth

$ram = get-wmiobject win32_operatingsystem | select TotalVisibleMemorySize
$ram = ($ram.TotalVisibleMemorySize / 1kb).tostring("F00") 
switch ($bits.tostring()) {
	"64" {
		$pagefile.initialsize = [int]$ram + 1024
		$pagefile.maximumsize = [int]$ram + 1024
		$pagefile.put()
	}
	"32" {
		$pagefile.initialsize = 4096
		$pagefile.maximumsize = 4096
		$pagefile.put()
	}
	default {
		"Error: Unable to set pagefile"
	}
}

