function ConnectSQL {
    Param ($server, $query, $database)
    $conn = new-object ('System.Data.SqlClient.SqlConnection')
    $connString = "Server=$server;Integrated Security=SSPI;Database=$database"
    $conn.ConnectionString = $connString
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $result = $sqlCmd.ExecuteNonQuery()
    $result
}

function QuerySQL {
    Param ([string]$query, [string]$server = "RRInfSccmAdm001.core.live", [string]$database = "Build_Data")
    $data = ConnectSQL $server $query $database
    $data
}
    
$computer = get-wmiobject -class Win32_ComputerSystem
$sccmServer = ""
if ($computer.domain -eq "prod.live") { $sccmServer = "QyInfSccmAdm201.prod.live" }
if ($computer.domain -eq "core.live") { $sccmServer = "RRInfSccmAdm001.core.live" }
if ($sccmServer -eq "") { write-host "Unable to determine domain. Exiting.." -foregroundColor Red -backgroundColor White; exit;}

$server = read-host "Please enter the name of the server to update the MAC address for"
$mac = read-host "Please enter Nic 1 MAC address"
if (($mac.Contains(":")) -or ($mac.Contains("."))){
    $mac = $mac.Replace(".","")
    $mac = $mac.Replace(":","")
}

$query = "Update Computer_Data SET adsAdminMac = '$mac', netNic1MacAddress = '$mac' WHERE winhostname = '$server'"

$success = querySQL $query $sccmServer

if ($success -eq 1) { write-host "Successfully updated MAC Address for $server to $mac" -ForegroundColor Green }
else { write-host "Failed to update MAC Address for $server" -ForegroundColor Red -BackgroundColor White }