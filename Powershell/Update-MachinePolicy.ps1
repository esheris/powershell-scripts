function Select-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$Filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	If ($Show -eq "OK")
	{
		Return $objForm.FileName
	}
	Else
	{
		Return $null
	}
}

$file = Select-FileDialog -title "Select Server List" -Directory ".\" -filter "Text Files (.txt)|*.txt"
If ($file.Length -eq 0){
exit
}
$serverlist = get-content $file

foreach ($server in $serverlist) {
    $class = [WmiClass]"\\$server\root\ccm:SMS_Client"
    $class.RequestMachinePolicy()
    $class.EvaluateMachinePolicy()
}