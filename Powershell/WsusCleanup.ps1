﻿function Write-Sql {
        Param (
            $server, 
            $query, 
            $database
        )
        
        $connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
		$connString.psbase.UserID = <user>
		$connString.psbase.Password = "<password>"
		$connString.psbase.IntegratedSecurity = $false
		$connString.psbase.DataSource = $Server
		$connString.psbase.InitialCatalog = $database
        $conn = New-Object System.Data.SqlClient.SqlConnection($connString)
        $conn.Open()
        $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
        $sqlCmd.CommandText = $query
        $sqlCmd.Connection = $conn
        $result = $sqlCmd.ExecuteNonQuery()
        $result
    }

Function RunWsusCleanupAndReport(){
    [reflection.assembly]::LoadWithPartialName("Microsoft.UpdateServices.Administration") | out-null 
    $wsus = [Microsoft.UpdateServices.Administration.AdminProxy]::GetUpdateServer(); 
    $cleanupScope = new-object Microsoft.UpdateServices.Administration.CleanupScope; 
    $cleanupScope.DeclineSupersededUpdates = $true        
    $cleanupScope.DeclineExpiredUpdates         = $true 
    $cleanupScope.CleanupObsoleteUpdates     = $true 
    $cleanupScope.CompressUpdates                  = $true 
    $cleanupScope.CleanupObsoleteComputers = $true 
    $cleanupScope.CleanupUnneededContentFiles = $true 
    $cleanupManager = $wsus.GetCleanupManager(); 
    $results = $cleanupManager.PerformCleanup($cleanupScope)
    
    $superceded = $results.SupercededUpdatesDeclined
    $expired = $results.ExpiredUpdatesDeclined
    $obsoleteDeleted = $results.ObsoleteUpdatesDeleted
    $compressed = $results.UpdatesCompressed
    $ObsoleteComputersDeleted = $results.ObsoleteComputersDeleted
    [int]$diskSpaceFreed = $results.DiskSpaceFreed / 1mb
    $computername = $env:COMPUTERNAME
    $date = get-date
    $query = "Insert Into WsusCleanupHistory (dt_Created, v_ServerName, i_SupercededUpdatesDeclined, i_ExpiredUpdatesDeclined, i_ObsoleteUpdatesDeleted, i_UpdatesCompressed, i_ObsoleteComputersDeleted, i_DiskSpaceFreedMB) "
    $query += [String]::Format("Values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",$date,$computername,$superceded,$expired,$obsoleteDeleted,$compressed,$ObsoleteComputersDeleted,$diskSpaceFreed)

    try{
        $sqlres = Write-Sql -server <server> -query $query -database Root_of_all_evil
        if ($sqlres -eq 1){ exit 0 } else { exit 1}
    } catch {
        $query | Out-File -FilePath ".\WsusCleanupLog.log" -Encoding ascii -Append
    }
}

RunWsusCleanupAndReport
