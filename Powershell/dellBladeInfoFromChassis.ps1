﻿function CreateSerialObjects($serialInfo)
{
    foreach($s in $serialInfo){
        $temp = [regex]::Split($s.trim(),"\s+")
        $info = New-Object -TypeName psobject
        $info | Add-Member -MemberType NoteProperty -Name Slot -Value $temp[0].split("-")[1]
        if ($temp[1] -eq $null){ $info | Add-Member -MemberType NoteProperty -Name SerialNumber -Value "Unknown" } else { $info | Add-Member -MemberType NoteProperty -Name SerialNumber -Value $temp[1]}
        Write-Output $info
    }
}

function CreateHostNameObjects($hostNameInfo, $dracIP)
{
    foreach($h in $hostNameInfo){
        $temp = [regex]::Split($h.trim(),"\s+")
        $info = New-Object -TypeName psobject
        $info | Add-Member -MemberType NoteProperty -Name Slot -Value $temp[0]
        $info | Add-Member -MemberType NoteProperty -name ChassisIP -Value $dracIP
        if ($temp[2] -eq $null) { $info | Add-Member -MemberType NoteProperty -Name Name -Value "Unknown" } else {$info | Add-Member -MemberType NoteProperty -Name Name -Value $temp[2]}
        Write-Output $info
    }
}

function CreateMacAddressObjects($macAddressInfo)
{
    foreach($m in $macAddressInfo){
        $temp = [regex]::Split($m.trim(),"\s+")
        $info = New-Object -TypeName psobject
        $info | Add-Member -MemberType NoteProperty -Name Slot -Value $temp[0].split("-")[1]
        $info | Add-Member -MemberType NoteProperty -Name Nic1 -Value $temp[3]
        $info | Add-Member -MemberType NoteProperty -Name Nic2 -Value $temp[4]
        Write-Output $info
    }
}

function Join-Object {
    Param(
       [Parameter(Position=0)]
       $First
    ,
       [Parameter(Position=1,ValueFromPipeline=$true)]
       $Second
    )
    BEGIN {
       [string[]] $p1 = $First | gm -type Properties | select -expand Name
    }
    Process {
       $Output = $First | Select $p1
       foreach($p in $Second | gm -type Properties | Where { $p1 -notcontains $_.Name } | select -expand Name) {
          Add-Member -in $Output -type NoteProperty -name $p -value $Second."$p"
       }
       $Output
    }
 }

 function CombineInfo($hostinfo,$serverinfo,$macinfo){
    for($i = 0;$i -lt $hostinfo.count -1; $i++){
        $hostAndSer = Join-Object -First $hostinfo[$i] -Second $serverinfo[$i]
        $allInfo = Join-Object -First $hostAndSer -Second $macinfo[$i]
        Write-Output $allInfo
    }
}

function Select-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$Filter)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	if ($Show -eq "OK")
	{
		return $objForm.FileName
	}
	else
	{
		return $null
	}
}
$content =Get-Content -Path (Select-FileDialog -Title "Select File" -Directory ".\" -Filter "Text | *.txt")
if ($content -eq $null) {exit 1} else {$dracips = $content}
foreach ($dracIP in $dracips){
try{
Connect-SSH -AuthMode password -Force -Password "calvin" -User "root" -Server $dracIP -ErrorAction Stop | Out-Null
$user = "root"
$pass = "calvin"
} catch {
$user = "xse"
$pass = "<password>"
}
$serverSerials = (Invoke-SSH -AuthMode password -Force -Password $pass -User $user -Command "getsvctag" -server $dracIP).Text | where {$_.contains("Server")}
$serverSerialsArray = CreateSerialObjects $serverSerials
$hostNames = (Invoke-SSH -AuthMode password -Force -Password $pass -User $user -Command "getslotname" -server $dracIP).Text  | where {-not $_.contains("<")}
$hostNamesArray = CreateHostNameObjects -hostNameInfo $hostNames -dracIP $dracip

$macAddresses = (Invoke-SSH -AuthMode password -Force -Password $pass -User $user -Command "getmacaddress" -server $dracIP).Text | where {$_.contains("Server")}
$macAddressesArray = CreateMacAddressObjects -macAddressInfo $macAddresses

$allInfo += CombineInfo -hostinfo $hostNamesArray -serverinfo $serverSerialsArray -macinfo $macAddressesArray

}

$allInfo | Export-Csv -Path ".\test.csv" -Encoding ASCII -NoTypeInformation -Force