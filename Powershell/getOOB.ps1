function ConnectSQL {
    Param ($server, $query, $database)
    $conn = new-object ('System.Data.SqlClient.SqlConnection')
    $connString = "Server=$server;Integrated Security=SSPI;Database=$database"
    $conn.ConnectionString = $connString
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $Rset = $sqlCmd.ExecuteReader()
    ,$Rset ## The comma is used to create an outer array, which PS strips off automatically when returning the $Rset
}

function QuerySQL {
    Param ($query,$server = "<server>.<domain>", $database = "Build_Data")
    $data = ConnectSQL $server $query $database
    while ($data.read() -eq $true) {
        $max = $data.FieldCount -1
        $obj = New-Object Object
        For ($i = 0; $i -le $max; $i++) {
            $name = $data.GetName($i)
            $obj | Add-Member Noteproperty $name -value $data.GetValue($i)
     }
     $obj
    }
}

function FindServer {
    Param ($server)
    $query = "SELECT winHostName, OOBIpAddress FROM dbo.Computer_Data WHERE winHostName = '" + $server + "'"
    $table = QuerySQL $query
    if ($table -eq $null){
        write-host -foreground red "Unable to find server:" $server 
        break
    }
    else {
        return $table
        }
}

function Pause ($Message="Press any key to continue...")
{
Write-Host -NoNewLine $Message
$null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
Write-Host ""
}

While ($true){
$server = read-host "Enter a Server name"
$OOBInfo = FindServer $server
$OOBInfo
}
