﻿function Read-SQL {
    Param (
        [ValidateNotNullOrEmpty()]
        [string]$server, 
        [ValidateNotNullOrEmpty()]
        [string]$query, 
        [ValidateNotNullOrEmpty()]
        [string]$database
    )

	$connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = <user>
	$connString.psbase.Password = "P@`$`$word"
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database

	$conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $table = New-Object System.Data.DataTable
	$table.load($sqlCmd.ExecuteReader())
	$conn.Close()
	return $table
}
$filepath = ".\controllerdriverversions.txt"
$servers = Read-SQL -server <server> -query "select Name from [v_CM_RES_COLL_XBL00080]" -database CM_XBL
"ServerName,Description,DriverVersion" | Out-File -Encoding ascii -FilePath $filepath -Force
foreach ($server in $servers){
    if(Test-Connection -Quiet -ComputerName $($server.Name)){
        try{
            $pnpSignedDriver = get-ciminstance -ClassName win32_pnpsigneddriver -ComputerName $($server.Name) -filter "Description like 'Smart Array%'" -OperationTimeoutSec 5 -ErrorAction Stop
            if ($pnpSignedDriver -eq $null) { 
                    $string = [String]::Format("{0},{1}",$($server.Name),"Not an HP Server")
                    $string | Out-File -Encoding ascii -FilePath $filepath -Append
                continue 
            }
            if ($pnpSignedDriver.Count -gt 0){
                foreach ($driver in $pnpSignedDriver){
                    $string = [String]::Format("{0},{1},{2}",$($server.Name),$driver.Description,$driver.DriverVersion)
                    $string | Out-File -Encoding ascii -FilePath $filepath -Append
                }
            } else {
                    $string = [String]::Format("{0},{1},{2}",$($server.Name),$pnpSignedDriver.Description,$pnpSignedDriver.DriverVersion)
                    $string | Out-File -Encoding ascii -FilePath $filepath -Append
            }
        } catch {
            $string = [String]::Format("{0},{1}",$($server.Name),"Error accessing WMI")
            $string | Out-File -Encoding ascii -FilePath $filepath -Append
        }
    } else {
            $string = [String]::Format("{0},{1}",$($server.Name),"Unable to ping server")
            $string | Out-File -Encoding ascii -FilePath $filepath -Append        
    }
}