$path = ""
$cert1 = "1"
$cert2 = "2"
$cert3 = "3"
$pass1 = ""
$pass2 = ""
$pass3 = ""


function Import-PfxCertificate {
 
	param([String]$certPath,[String]$certRootStore = �LocalMachine�,[String]$certStore = �My�,$pfxPass = $null)
	$pfx = new-object System.Security.Cryptography.X509Certificates.X509Certificate2    
	if ($pfxPass -eq $null) {$pfxPass = read-host "Enter the pfx password" -assecurestring}    
	$pfx.import($certPath,$pfxPass,"PersistKeySet")    
	$store = new-object System.Security.Cryptography.X509Certificates.X509Store($certStore,$certRootStore)    
	$store.open("MaxAllowed")    
	$store.add($pfx)    
	$store.close()    
}

function Remove-PfxCertificate {
	param([String]$certName,[String]$certRootStore = "LocalMachine",[String]$certStore = "My")
	$cert = dir ("cert:\" + $certRootStore + "\" + $certStore) | where {$_.Subject.Contains($certName)}
	$store = new-object System.Security.Cryptography.x509certificates.x509Store($certStore,$certRootStore)
	$store.open("MaxAllowed")
	$store.remove($cert)
	$store.close()
}

Remove-PfxCertificate $cert1.replace(".pfx","")
Remove-PfxCertificate $cert2.replace(".pfx","")
Remove-PfxCertificate $cert3.replace(".pfx","")

Import-PfxCertificate -certPath ($path + $cert1) -pfxPass $pass1
Import-PfxCertificate -certPath ($path + $cert2) -pfxPass $pass2
Import-PfxCertificate -certPath ($path + $cert3) -pfxPass $pass3

iisreset /stop
Start-Sleep 10
iisreset /start