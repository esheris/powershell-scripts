PARAM (
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,HelpMessage="MSI Database Filename",ValueFromPipeline=$true)]
        [Alias("Filename","Path","Database","Msi")]
        $msiDbName
    )
# Load some TypeData
$SavedEA = $Global:ErrorActionPreference
$Global:ErrorActionPreference = "SilentlyContinue"
Update-TypeData -AppendPath ((Split-Path -Parent $MyInvocation.MyCommand.Path) + "\comObject.types.ps1xml")
$Global:ErrorActionPreference = $SavedEA
 
function get-msiproperties {
    PARAM (
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,HelpMessage="MSI Database Filename",ValueFromPipeline=$true)]
        [Alias("Filename","Path","Database","Msi")]
        $msiDbName
    )
 
    # A quick check to see if the file exist
    if(!(Test-Path $msiDbName)){
        throw "Could not find " + $msiDbName
    }
 
    # Create an empty hashtable to store properties in
    $msiProps = @{}
     
    # Creating WI object and load MSI database
    $wiObject = New-Object -com WindowsInstaller.Installer
    $wiDatabase = $wiObject.InvokeMethod("OpenDatabase", (Resolve-Path $msiDbName).Path, 0)
     
    # Open the Property-view
    $view = $wiDatabase.InvokeMethod("OpenView", "SELECT * FROM Property")
    $view.InvokeMethod("Execute")
     
    # Loop thru the table
    $r = $view.InvokeMethod("Fetch")
    while($r -ne $null) {
        # Add property and value to hash table
        $msiProps[$r.InvokeParamProperty("StringData",1)] = $r.InvokeParamProperty("StringData",2)
         
        # Fetch the next row
        $r = $view.InvokeMethod("Fetch")
    }
 
    $view.InvokeMethod("Close")
     
    # Return the hash table
    return $msiProps
}

get-msiproperties -msiDbName $msidbname