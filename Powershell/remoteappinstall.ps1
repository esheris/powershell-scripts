#assumptions: Software location: \\fileserver\software\appname\appname.msi Server to install on: Server1
$installdir = �D:\Software\appname�
$creds = Get-Credential
$msipath = "\\fileserver\software\appname\appname.msi"
$computerToInstallOn = �Server1�
$scriptblock = [scriptblock]::Create(�msiexec.exe /I `�$msipath`� REBOOT=ReallySuppress INSTALLDIR=`�$installdir`��
Invoke-Command �ComputerName $computerToInstallOn �Credential $cred �ScriptBlock $scriptblock
