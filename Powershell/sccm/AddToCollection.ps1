﻿function Add-ToCollection()
{
	param (
	[parameter(Mandatory=$true)]
	$hostname,
	[parameter(Mandatory=$false)]
	$ManagementPoint = "<server>",
    [parameter(Mandatory=$false)]
    $collectionToAddTo
	)
	begin { 
		$smsProvider = gwmi -Namespace root\sms -ComputerName $managementpoint -query "select * from sms_providerlocation"
        $namespacePath = $smsProvider.NamespacePath
	}
	process {
		if ($namespacePath -ne $null) {
                Write-host "Preparing to add to collection"
                $CollectionID = (gwmi -namespace root\sms\site_xbl -ComputerName $smsProvider.Machine -Class sms_collection -filter "Name='$collectionToAddTo'").CollectionID
                $resource = gwmi -namespace root\sms\site_xbl -ComputerName $smsProvider.Machine -class sms_r_system -filter "NetbiosName='$hostname'"
                if ($CollectionID -eq $null){ exit 2 }
                if ($resource -eq $null){ exit 1 }
                $collection = [wmi]"$namespacePath`:SMS_Collection.CollectionID='$collectionID'"                
                $collRule = ([wmiclass]"$namespacePath`:SMS_CollectionRuleDirect").createinstance()
                $collRule.RuleName = $resource.NetbiosName
                $collRule.ResourceClassName = "SMS_R_System"
                $collRule.ResourceID = $resource.ResourceID

                $AddToCollRes = $collection.AddMembershipRule($collRule)
			
			if ($AddToCollRes.ReturnValue -eq 0){
                Write-Host "Successfully added computer to collection"
				exit 0
			} else {
                Write-host "Error Adding computer to collection"
				exit 2
			}
		} else { exit 4 }
	}
}

Add-ToCollection -hostname testagain1 -collectionToAddTo "1_Disk_Configuration"