function Clean-SccmDuplicates()
{
	begin { 
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"}
			"<domain>" {$siteCode = "Q01"}
			default {$siteCode = $null}
		} 
	}
	process {
		$list = @()
		$duplicates = @()
		$resources = gwmi -class sms_r_system -namespace Root\SMS\Site_rr1
		foreach ($l in $resources) {
			if ($list -contains $l.name) {
				$duplicates += $l.Name 
			} else {
				$list += $l.name
			}
		}

		foreach ($d in $duplicates) {
			$res = gwmi -Class sms_r_system -Namespace Root\SMS\Site_rr1 -Filter "Name='$d'"
			foreach ($r in $res){
				if (($r.AgentName[0] -eq "SMS_AD_SYSTEM_DISCOVERY_AGENT") -and ($r.Client -ne '1')){
					$r.Delete()
					$r.Name
				} 
			}
		}
	} 
}