﻿param(
    [ValidateNotNullOrEmpty()]
    $hostname
)

function Get-SccmResource()
{
	param (
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$hostname
	)
	process {
            $secPass = ConvertTo-SecureString "<password>" -AsPlainText -Force
            $cred = New-Object System.Management.Automation.PSCredential("<user>",$secPass)

			$resource = Get-WmiObject -ComputerName <server> -Class SMS_R_System -Namespace root\sms\site_xbl -Filter "Name='$hostname'" -Credential $cred -Impersonation Impersonate
			if ($resource -ne $null){
				Write-Output $resource
			}
	}
}

function Delete-SccmResource()
{
param(
    $resources
)
    if ($resource.count -gt 1){
        foreach($res in $resource){
            $res.delete()
        }
    } else {
        $resource.delete()
    }
}

function main()
{
    param(
    $hostname
    )

    for($i=1;$i -lt 5; $i++){
        $resource = Get-SccmResource -hostname $hostname
        if ($resource -ne $null){
            Delete-SccmResource -resources $resource
        }
        $resourceCheck = Get-SccmResource -hostname $hostname
        if ($resourceCheck -eq $null){
            return 999
        }
        Start-Sleep -Seconds 5
    }
    return 666
}

main -hostname $hostname
