<#
.SYNOPSIS
   <A brief description of the script>
.DESCRIPTION
   <A detailed description of the script>
.PARAMETER <paramName>
   <Description of script parameter>
.EXAMPLE
   <An example of using the script>
#>
#$logpath = $env:systemroot + "\temp\OSDeploy.log"

$logCount = 1
function Write-OSDLog()
{
	param(
		[parameter(Mandatory=$false)]
		[string]$logpath = $env:systemroot + "\temp\OSDeploy.log",
		[Parameter(Mandatory=$true)]
		[int]$resultCode,
		[Parameter(Mandatory=$true)]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText
	)
	begin {
		$LogOutput = [string]::Format("{0}:{1}:{2}:{3}",$logCount,$resultCode,$resultType,$resultText)
	}
	process {		
		Tee-Object -InputObject $LogOutput -Variable tee
		Out-File -FilePath $logpath -InputObject $tee -Append
		$logCount++
	}		
}

function Get-SccmCollectionID()
{
	param (
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$collectionName
	)
	begin { 
		Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Entering Get-SccmCollectionId Fuction"
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"; $siteServer = "RRInfSccmAdm001"}
			"<domain>" {$siteCode = "Q01"; $siteServer = "<server>"}
			default {$siteCode = $null}
		} 
		
	}
	process {
		if ($siteCode -ne $null){
			Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Determined Sitecode of the Administration Server to be $siteCode"
			$collection = Get-WmiObject -Class SMS_Collection -Namespace root\sms\site_$siteCode -Filter "Name = '$collectionName'"
			if ($collection -ne $null){
				Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Determined Collection ID for $collectionName to be $collection.CollectionID"
				Write-Output $collection.CollectionID
			} else {
				Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Failed to determine Collection ID for $collectionName"
				exit 1
			}
		} else { Write-OSDLog -resultCode 1 -resultType "Fatal" -resultText "Failed to Determine SiteCode for Administration Server"; exit 1 }
	} 
}

function Get-SccmResourceId()
{
	param (
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$hostname
	)
	begin {
		Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Entering Get-SccmResourceID Fuction"
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"; $siteServer = "RRInfSccmAdm001"}
			"<domain>" {$siteCode = "Q01"; $siteServer = "<server>"}
			default {$siteCode = $null}
		} 
	}
	process {
		if ($siteCode -ne $null) {
			Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Determined Sitecode of the Administration Server to be $siteCode"
			$resource = Get-WmiObject -Class SMS_R_System -Namespace root\sms\site_$siteCode -Filter "Name = '$hostname'" -ComputerName $siteServer
			if ($resource -ne $null){
				Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Determined Collection ID for $collectionName to be $resource.resourceID"
				Write-Output $resource.resourceid
			} else {
				Write-OSDLog -resultCode 1 -resultType "Fatal" -resultText "Failed to determine Resource ID for $hostname"
				exit 1
			}
		} else { Write-OSDLog -resultCode 1 -resultType "Fatal" -resultText "Failed to Determine SiteCode for Administration Server"; exit 1 }
	}
}

function Add-SccmComputerToCollectionByName()
{
	param(
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$ResourceName,
	[parameter(Mandatory=$true)]
	[string]$CollectionName
	)
	begin {
		Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Entering Add-SccmComputerToCollectionByName Fuction"
		$ResourceID = Get-SccmResourceId -hostname $ResourceName
		Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Resource Name: $resourceName - Resource ID: $ResourceID"
		$CollectionId = Get-SccmCollectionID -CollectionName $CollectionName
		Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Collection Name: $CollectionName - Collection ID: $CollectionID"
	}
	process{
		$i = 1
		Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Entering while loop to add computer to collection. Will Try 6 times."
		while ((Add-SccmComputerToCollectionByID -ResourceId $ResourceID -CollectionID $CollectionID) -ne $true) {
			if ($i -lt 6){
				Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Try $i Adding $ResourceName to $CollectionName"
				Start-Sleep -Seconds 5
			} else {
				Write-OSDLog -resultCode 1 -resultType "Fatal" -resultText "Failed to add Resoure: $resourceID to Collection: $CollectionID after 6 attempts"
				exit 1
			}
			$i++
		}
	}
		
}

# Adds a computer (by resource id) to a given collection (by collection id)
function Add-SccmComputerToCollectionByID()
{
	param(
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$resourceID, 
	[parameter(Mandatory=$true)]
	[string]$collectionID
	)
	begin {
		Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Entering Add-SccmComputerToCollectionByID Function"
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"; $siteServer = "RRInfSccmAdm001"}
			"<domain>" {$siteCode = "Q01"; $siteServer = "<server>"}
			default {$siteCode = $null}
		}
		Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Site Server: $siteServer - Site Code: $siteCode"
	}
	process {
		if ( $siteCode -ne $null){
			$collection = [wmi]"\\$siteServer\root\sms\site_$sitecode`:SMS_Collection.CollectionID='$collectionID'" # the : needs to be escaped or things go badly
			$directRule = [WmiClass]"\\$siteServer\root\SMS\Site_$siteCode`:SMS_CollectionRuleDirect" # the : needs to be escaped or things go badly
			$directInstance = $directRule.CreateInstance()
			$directInstance.ResourceClassName = "SMS_R_System"
			$directInstance.ResourceID = $resourceID
			$result = $collection.AddMembershipRule($directInstance)
			if ($result.ReturnValue -eq 0){
				# If this fails, it's not the end of the world, the server has been added to the collection, it just might take a bit longer to show up, also doesnt generally fail
				$collection.RequestRefresh() | Out-Null
				Write-Output $true 
			}
			else{
				Write-Output $false
			}
		} else { Write-OSDLog -resultCode 1 -resultType "Fatal" -resultText "Error determining SiteCode." }
	}
}

function Remove-SccmComputerFromCollectionByName()
{
	param(
	[parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$ResourceName,
	[parameter(Mandatory=$true)]
	[string]$CollectionName
	)
	begin {
		Write-OSDLog -resultCode 2 -resultType "Informational" -resultText "Entering Remove-SccmComputerFromCollectionByName Fuction"
		$ResourceID = Get-SccmResourceId -hostname $ResourceName
		Write-OSDLog -resultCode 2 -resultType "Informational" -ResultText "Resource Name: $resoureName - Resource ID: $ResourceID"
		$CollectionId = Get-SccmCollectionID -CollectionName $CollectionName
		Write-OSDLog -resultCode 2 -resultType "Informational" -ResultText "Collection Name: $CollectionName - Collection ID: $CollectionID"
	}
	process{
		if ($ResourceID -eq $null) { Write-OSDLog -resultCode 1 -resultType "Fatal" -resultText "Resource ID is NULL"; exit 1 }
		if ($CollectionId -eq $null) { Write-OSDLog -resultCode 1 -resultType "Fatal" -resultText "Collection ID is NULL"; exit 1 }
		$i = 1
		while ((Remove-SccmComputerFromCollectionByID -ResourceId $ResourceID -CollectionID $CollectionID) -ne $true) {
			if ($i -lt 6){
				Start-Sleep -Seconds 5
			} else {
				Write-Host "Failed to Add $ResourceName to $CollectionName"
			}
			$i++
		}
	}		
}

function Remove-SccmComputerFromCollectionByID()
{
	param(
	# not sure how to take these both from the pipeline yet and i need them both, so i'm leaving 'ValueFromPipeline' out for now
	[parameter(Mandatory=$true)]
	[string]$resourceID,
	[parameter(Mandatory=$true)]
	[string]$collectionID
	)
	begin { 
		switch ((Get-WmiObject Win32_ComputerSystem).Domain) {
			"<domain>" {$siteCode = "RR1"; $siteServer = "RRInfSccmAdm001"}
			"<domain>" {$siteCode = "Q01"; $siteServer = "<server>"}
			default {$siteCode = $null}
		} 
	}
	process {
		if ($siteCode -ne $null){
			$collection = [wmi]"\\$siteServer\root\sms\site_$sitecode`:SMS_Collection.CollectionID='$collectionID'" # the : needs to be escaped or things go badly
			$rule = $collection.CollectionRules | where {$_.ResourceID -eq $resourceID}
			$result = $collection.DeleteMembershipRule($rule)
			if ($result.ReturnValue -eq 0){
				# If this fails, it's not the end of the world, the server has been added to the collection, it just might take a bit longer to show up, also doesnt generally fail
				$collection.RequestRefresh() | Out-Null
				Write-Output $true 
			}
			else{
				Write-Output $false
			}
		} else { Write-Output "Error determining Site Code" }
	} 
}

$servers = "CaptureIIS-R2","CaptureINH-R2","CaptureSQL-R2","CaptureVM-INH","CaptureVM-IIS","CaptureVM-SQL"
$collections = "1_Build_2008_Base", "2_Build_2008_IPAK", "3a_Build_2008_IIS_Install","3b_Build_2008_SQL"

