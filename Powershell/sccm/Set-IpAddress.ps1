function ConnectSQL {
    Param ([string]$server, [string]$query, [string]$database)
    $conn = new-object ('System.Data.SqlClient.SqlConnection')
    $connString = "Server=$server;Uid=build;Pwd=<password>;Database=$database"
    $conn.ConnectionString = $connString
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $Rset = $sqlCmd.ExecuteReader()
    ,$Rset ## The comma is used to create an outer array, which PS strips off automatically when returning the $Rset
}

function QuerySQL {
    Param ([string]$query, [string]$server = "<server>.<domain>", [string]$database = "Build_Data")
    $data = ConnectSQL $server $query $database
    while ($data.read() -eq $true) {
        $max = $data.FieldCount -1
        $obj = New-Object Object
        For ($i = 0; $i -le $max; $i++) {
            $name = $data.GetName($i)
            $obj | Add-Member Noteproperty $name -value $data.GetValue($i)
     }
     $obj
    }
}

function Set-IPAddress {
		param([string]$networkinterface,[string]$ip,[string]$mask,[string]$gateway,[string]$dns1,[string]$dns2,[string]$registerDns = "TRUE")
		$dns = $dns1
		if($dns2){$dns = $dns1,$dns2}
		$index = (gwmi Win32_NetworkAdapter | where {$_.netconnectionid -eq $networkinterface}).InterfaceIndex
		$NetInterface = Get-WmiObject Win32_NetworkAdapterConfiguration | where {$_.InterfaceIndex -eq $index}
        $NetInterface.EnableStatic($ip,$mask)
        if ($gateway -ne ""){$NetInterface.SetGateways($gateway)}
        if ($dns -ne ""){$NetInterface.SetDNSServerSearchOrder($dns)}
		$NetInterface.SetDynamicDNSRegistration($registerDns)
		
}
$nicInfo = gwmi Win32_NetworkAdapter | where {$_.PhysicalAdapter -eq 'TRUE'}
$nic1Mac = $nicinfo[0].MacAddress.replace(":","")
# $Nic2Mac = $nicinfo[1].MacAddress.replace(":","")
$nic1Name = $nicinfo[0].netConnectionID
#$nic2Name = $nicinfo[1].netConnectionID

$sqlQuery = "SELECT * FROM dbo.Computer_Data WHERE adsAdminMac = '" + $nic1Mac + "'"
$ipinfo = querysql $sqlQuery

# set nic 1 variables to make names nicer
$nic1Ip = $ipinfo.netNic1IpAddress.tostring()
$nic1Mask = $ipinfo.netNic1SubnetMask.tostring()
$nic1Gw = $ipinfo.netNic1DefaultGateway.tostring()
$nic1Dns1 = $ipinfo.netNic1DnsServerSearchOrder.tostring()
$nic1Dns2 = $ipinfo.netNic1DnsServerSearchOrder2.tostring()

# set nic 2 variables to make names nicer
#$nic2Ip = $ipinfo.netNic2IpAddress.tostring()
#$nic2Mask = $ipinfo.netNic2SubnetMask.tostring()
#$nic2Gw = $ipinfo.netNic2DefaultGateway.tostring()
#$nic2Dns1 = $ipinfo.netNic2DnsServerSearchOrder.tostring()
#$nic2Dns2 = $ipinfo.netNic2DnsServerSearchOrder2.tostring()

Set-IpAddress $nic1Name $nic1Ip $nic1Mask $nic1Gw $nic1Dns1 $nic1Dns2
#Set-IpAddress $nic2Name $nic2Ip $nic2Mask $nic2Gw $nic2Dns1 $nic2Dns2 $false