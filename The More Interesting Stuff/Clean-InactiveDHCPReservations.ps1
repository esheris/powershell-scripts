﻿Import-Module DhcpServer
function Get-DHCPServers(){
    try {
        $dhcpServers = Read-SQL -server <server> -database Root_Of_All_Evil -query "Select Distinct(v_DHCPServer) from DHCP_Servers"
    } catch {
        return $null
    }
    return $dhcpServers
}

function Write-OSDLog() {
	param(
		[Parameter(Mandatory=$true)]
        [ValidateSet('Informational','Warning','Error')]
		[string]$resultType,
		[Parameter(Mandatory=$true)]
		[string]$resultText
	)
	begin {
        if (-not(Test-Path -Path ".\Clean-InactiveDHCPReservations.txt")){ New-Item -ItemType File -Path ".\" -Name ".\Clean-InactiveDHCPReservations.txt" -Force }
		$LogOutput = [string]::Format("{0} : {1} : {2}",$(get-date).ToString(),$resultType,$resultText)
		
	}
	process {
		tee-object -InputObject $LogOutput -Variable tee			
		$tee | Out-File -Append -FilePath ".\Clean-InactiveDHCPReservations.txt" -Encoding ascii
	}		
}

function Read-SQL {
    Param (
        [string]$server, 
        [string]$query, 
        [string]$database
    )

	$connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
	$connString.psbase.UserID = <user>
	$connString.psbase.Password = <password>
	$connString.psbase.IntegratedSecurity = $false
	$connString.psbase.DataSource = $Server
	$connString.psbase.InitialCatalog = $database

	$conn = New-Object System.Data.SqlClient.SqlConnection($connString)
    $conn.Open()
    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
    $sqlCmd.CommandText = $query
    $sqlCmd.Connection = $conn
    $table = New-Object System.Data.DataTable
	$table.load($sqlCmd.ExecuteReader())
	$conn.Close()
	return $table
}


if (Test-Path -Path ".\Clean-InactiveDHCPReservations.txt"){ Remove-Item -Path ".\Clean-InactiveDHCPReservations.txt" -Force }
Write-OSDLog -resultType Informational -resultText "---------------Beginning cleanup of dhcp scopes--------------"
$dhcpServers = Get-DhcpServers
if ($dhcpServers -ne $null){
foreach ($entry in $dhcpServers){
    $server = $entry.v_DHCPServer
    if (Test-Connection -ComputerName $server -Quiet){
        $scopes = get-Dhcpserverv4scope -ComputerName $server
        foreach ($scope in $scopes){
            Write-OSDLog -resultType Informational -resultText "----------Processing Scope $($scope.ScopeId) on DHCP server $server----------"
            $inactiveLeases = Get-DhcpServerv4Lease -ComputerName $server -ScopeId $($scope.ScopeId) | where {$_.AddressState -eq "InactiveReservation"}
            if ($inactiveLeases -ne $null){
                Write-OSDLog -resultType Informational -resultText "Removing $($inactiveLeases.Count) Inactive Leases"
                foreach ($inactiveLease in $inactiveLeases){
                    Write-OSDLog -resultType Informational -resultText "Removing inactive lease $($inactiveLease.IPAddress)"
                    $inactiveLease | Remove-DhcpServerv4Reservation -ComputerName $server
                }
            Invoke-DhcpServerv4FailoverReplication -ComputerName $server -ScopeId $($scope.ScopeId) -Force
            } else {
                Write-OSDLog -resultType Informational -resultText "No Inactive Leases found"
            }
            Write-OSDLog -resultType Informational -resultText "----------Finished Processing Scope $($scope.ScopeId)----------"
            
        }
    } else {
        Write-OSDLog -resultType Error -resultText "Unable to connect to $server"
    }
}
} else {
    Write-OSDLog -resultType Warning -resultText "!!!!!Unable to retrieve list of DHCP Servers from SQL!!!!!"
}
Write-OSDLog -resultType Informational -resultText "---------------Finished cleanup of dhcp scopes--------------"