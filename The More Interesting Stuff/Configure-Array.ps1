﻿param (
    [array]$configuration
)
function ParseDisks($DriveShowAll){
    $disks = [string]::Empty
    foreach ($line in $DriveShowAll){
        $line = $line.trim()
        if ($line.startswith("physicaldrive")){
            $disks += $line.Split(" ")[1] + ","
        }
    }
    $disks = $disks.TrimEnd(",")
    if ($disks -eq [string]::Empty){ $disks = "None" }
    return $disks
}
function MakeDiskString([array]$disks){
    $string = [string]::Empty
    foreach ($disk in $disks){
        $string += $disk + ","
    }
    return $string.TrimEnd(",")
}

function GetRaidType(){
param(
    [string]$i
)
    $retval = [string]::Empty
    switch ($i){
        "1" { $retval = "1";break;}
        "5" {$retval = "5";break;}
        "10" {$retval = "1+0";break;}
    }
    return $retval
}

$ctrlList = new-object 'System.Collections.Generic.List[object]'
$controllers = .\hpacucli.exe ctrl all show
foreach ($ctrl in $controllers){
    if ($ctrl -ne [string]::Empty){
        $splitctrl = $ctrl.split(" ")
        $controller = $splitctrl[$splitctrl.IndexOf("Array") + 1]
        $slot = $splitctrl[$splitctrl.IndexOf("Slot") +1]
        $diskString = .\hpacucli.exe controller slot=$slot physicaldrive all show
        $ssdString = .\hpacucli.exe controller slot=$slot ssdphysicaldrive all show
        $physicalDisks = ParseDisks($diskString)
        $ssdDisks = ParseDisks($ssdString)
        $obj = New-Object PSObject
        $obj | Add-Member -MemberType NoteProperty -Name ControllerType -Value $controller
        $obj | Add-Member -MemberType NoteProperty -Name ControllerSlot -Value $slot
        $obj | Add-Member -MemberType NoteProperty -Name PhysicalDisks -Value $physicalDisks
        $obj | Add-Member -MemberType NoteProperty -Name SSDDisks -Value $ssdDisks
        $ctrlList.Add($obj)
    }
}

$diskCount = 0
foreach ($controller in $ctrlList){
    $pdisks = $controller.PhysicalDisks
    if ($pdisks -ne "None"){ $diskCount += $pdisks.Split(",").Count }    
}
$requiredDiskCount = 0
$configIssues = [string]::Empty
foreach ($config in $configuration){
    $c = $config.Split(",")
    switch ($c[1]){
        1 { if ($c[0] % 2 -ne 0) { $configIssues += "A raid 1 requires an even number of disks: Config Part - $config`r`n" }}
        5 { if ($c[0] -lt 3){ $configIssues += "A raid 5 requires at least 3 disks: Config Part- $config`r`n"}}
        10 { if ($c[0] % 2 -ne 0) { $configIssues += "A raid 10 requires an even number of disks - Config Part: $config`r`n" }}
        default {$configIssues +="Raid $($c[1]) is not currently supported - Config Part: $config`r`n"}
    }
    $requiredDiskCount += $c[0]
}
if (($requiredDiskCount -le $diskCount) -and ($configIssues -eq [string]::Empty)){
    #Lets Make some arrays!
    $disks = new-object System.Collections.ArrayList
    $correctController = $ctrlList | where {$_.PhysicalDisks -ne "None"}
    if ($requiredDiskCount -lt $diskCount){ $addSpare = $true } else {$addSpare = $false }
    
    if (($correctController | Measure).Count -ne 1){return 666}
    $actualDisks = $correctController.PhysicalDisks
    $actualDisks.Split(",") | foreach { $disks.add($_) | out-null}
    $slot = $correctController.ControllerSlot
    #Set Cache to default values
    
    foreach ($config in $configuration){
        $c = $config.Split(",")
        $raid = $c[1]
        $raidType = GetRaidType($raid)        
        
        $disksToConfigure = $disks | select -first $c[0]
        $diskString = MakeDiskString($disksToConfigure)

        if ($disksToConfigure.Count -gt 2 -and $raidType -eq 1){$raidType = "1+0"}
        
        $result = .\HPACUCLI.exe controller slot=$slot create type=ld drives=$diskString raid=$raidType
        if ($result -ne $null){Write-Host "Error Creating Array`r`n" + $result; return 666; }
        $disksToConfigure | foreach { $disks.Remove($_)}
        
    }
    if ($addSpare){
        $spare = $disks | select -Last 1
        $spareString = MakeDiskString($spare)
        .\hpacucli.exe controller slot=$slot array all add spares=$spareString
    }
    $cachRes = .\hpacucli.exe controller slot=$slot modify cacheratio=25/75
    if ($cachRes -ne $null){ Write-Host "Error modifying cache ratios`r`n" + $cachRes; return 666;}
}