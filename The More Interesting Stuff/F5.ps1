# Begin Web Configuration
  $config_WebLB_NumberOfWebRequestsToCheck = 1
# End Web Configuration

function BackupLoadBalancerConfiguration( $lbServer)
{
	$serverType = GetServerType -appServer $lbServer
	if ($serverType -ne 'LoadBalancer')
	{	LogInfo "Aborting... $(GetIP $lbServer) is not a LoadBalancer!" $colors_Error; return $false	}
	
	$ignoreConsole = ''
}
 
Set-Alias GetLBPools GetLoadBalancerPools
<#
.SYNOPSIS
Returns a list of Pools from an F5 Load Balancer
.DESCRIPTION
Uses tmsh to get a list of pools from the F5 and parses the output into objects 
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.EXAMPLE
PS C:\PowerShell> GetLoadBalancerPools lbX
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function GetLoadBalancerPools ( $lbServer, [string]$filter = [string]::Empty)
{
	$rawText = RunCommand 'tmsh -q show ltm pool field-fmt' $null $lbServer
	if ([string]::IsNullOrEmpty($filter)){
		return ParseTextToObjects $rawText '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
	} else {
		$results = ParseTextToObjects $rawText '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
		if ($null -eq $results){
			return $null
		} else {
			return $results | where-object {$_.name.tolower().contains($filter.tolower()) }
		}
	}
}

Set-Alias GetLBPools GetLoadBalancerPools
<#
.SYNOPSIS
Returns an object showing Pool information and an array of Nodes with statistics from an F5 Load Balancer
.DESCRIPTION
Uses tmsh to get information and statistics for a given Pool on an F5 Load Balancer and parses the output into an object with child Node objects
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER PoolName
Name of the Pool for which to query on
.EXAMPLE
PS C:\PowerShell> GetLoadBalancerPools lbX PoolABC123
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function GetLoadBalancerPoolInfo ( $lbServer, [string] $poolName)
{
	$rawText = RunCommand "tmsh -q show ltm pool $poolName members field-fmt" $null $lbServer
	
	$poolText = @()
	$nodesText = @()
	$foundNodeSectionStart = $false
	$foundNodeSectionEnd = $false
	foreach ($line in $rawText)
	{
		if ($foundNodeSectionStart)
		{
			if ($foundNodeSectionEnd)
			{	$poolText += $line	}
			elseif ($line -match '^\s{4}\}')
			{	$foundNodeSectionEnd = $true; continue 	}
			else
			{	$nodesText += $line	}
		}
		elseif ($line -match '^\s{4}members\s+\{')
		{	$foundNodeSectionStart = $true; continue	}
		else
		{	$poolText += $line	}
	}
	$poolInfoObj = ParseTextToObjects $poolText '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
	$nodeObjs = ParseTextToObjects $nodesText '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
	AddProperty $poolInfoObj 'Nodes' $nodeObjs

	return $poolInfoObj
}

Set-Alias GetLBIRules GetLoadBalancerIRules

<#
.SYNOPSIS
Returns a list of IRules from the LB
.DESCRIPTION
Uses tmsh to get a list of the current IRules from the LB
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER Filter
(optional) filter the irule based on its name
.EXAMPLE
PS C:\PowerShell> GetLoadBalancerPools lbX
PS C:\PowerShell> GetLoadBalancerPools lbX API
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function GetLoadBalancerIRules([string]$lbserver, [string]$filter = [string]::Empty)
{
	if (AreAnyRequiredArgumentsNull @('lbserver'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh show ltm rule field-fmt"
    $rawText = RunCommand $command $null $lbServer
	if ([string]::IsNullOrEmpty($rawText)){return $null}
	if ([string]::IsNullOrEmpty($filter)){
    	return ParseTextToObjects $rawText '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
	} else {
		$results = ParseTextToObjects $rawText '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
		return $results | where-object {$_.name.ToLower().contains($filter.ToLower())}
	}
}

Set-Alias CreateLBIRule CreateLoadBalancerIRule
<#
.SYNOPSIS
Creates a new IRule on the LB
.DESCRIPTION
Uses tmsh to create a new IRule on the LB
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER ServiceName
Name of the service the IRule will be applied to
.PARAMETER vipIP
SNAT IP address
.PARAMETER serverGroup (optional)
ServerGroup for this pool
.PARAMETER prefix (optional)
network prefix to use for irule creation
.EXAMPLE
PS C:\PowerShell> CreateLoadBalancerIRule lbX API 10.0.20.20 Web1
PS C:\PowerShell> CreateLoadBalancerIRule lbX API 10.0.20.20 10.0.20.0/24
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function CreateLoadBalancerIRule([string]$lbserver,[string]$serviceName, [string]$vipIP, [string]$serverGroup=$null, [string]$prefix=$null)
{
	if (AreAnyRequiredArgumentsNull @('lbserver','serviceName','vipIP'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    if ($serverGroup -eq $null -and $prefix -eq $null)
    { LogInfo "Please provide either serverGroup or Prefix" $colors_Error; return $false }

	if ($null -eq $prefix){
    	$s = Get-Random(getservers $null  $serverGroup)
		if ($null -eq $s ){return $false }
    	$prefix = GetPublicNetworkPrefix $s
		if ($null -eq $prefix){return $false }
	}
    $ruleName = "iRule-SNAT-API-$serviceName"
    $command = "tmsh -q create ltm rule $ruleName "
    $command += "when CLIENT_ACCEPTED { "
    $command += "if { [IP::addr [IP::client_addr] equals $prefix] }{ "
    $command += "snat $vipIP } }"
    $rawText = RunCommand $command $null $lbServer
    $rule = GetLoadBalancerIRules -lbserver $lbServer -filter $ruleName
	if ($null -eq $rule){
		return $false
	}
	return $true
}

set-alias GetLBMon GetLoadBalancerMonitors
<#
.SYNOPSIS
Gets the load balancer monitors
.DESCRIPTION
Uses tmsh to get the monitors from the load balancer
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER serviceName
The name of the service the monitor is being created for.
.PARAMETER filter
filter the list of returned monitors based on the monitor name
.EXAMPLE
PS C:\PowerShell> GetLoadBalancerMonitors lb
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function GetLoadBalancerMonitors([string]$lbserver, [string]$filter = [string]::Empty)
{
if (AreAnyRequiredArgumentsNull @('lbserver'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q list ltm monitor"
    $monitors = RunCommand $command $null $lbServer
	if ($null -eq $monitors){return $false}
	[String[]]$monitorNames = $monitors -match '^ltm\s' | %{ $_.Split()[3] }
	if ($monitorNames -eq $null){return $false }
	$objects = ParseTextToObjects $monitors '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
	if ($monitorNames.Count -eq $objects.Count){
		if ($monitorNames.Count -gt 1){
			$fullMonitorObjects = 0..($objects.Count - 1) | %{AddProperty $objects[$_] "Name" $monitorNames[$_]}
		} else {
			$fullMonitorObjects = AddProperty $objects "Name" $monitorNames
		}
	} else {
		return $false
	}
	if ([string]::IsNullOrEmpty($filter)){
    	return $objects
	} else {
		return $objects | where-object{$_.Name.ToLower().Contains($filter.ToLower())}
	}
    
}

Set-Alias CreateLBHttpMon CreateLoadBalancerHttpMonitor
<#
.SYNOPSIS
Creates the HTTP monitor for the specified service in the load balancer
.DESCRIPTION
Uses tmsh to create the HTTP monitor for the specified service in the load balancer
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER serviceName
The name of the service the monitor is being created for.
.PARAMETER port
port the monitor should monitor
.EXAMPLE
PS C:\PowerShell> CreateLoadBalancerHTTPMonitor lb Service 48999
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function CreateLoadBalancerHttpMonitor([string]$lbserver, [string]$serviceName, [string]$port)
{
if (AreAnyRequiredArgumentsNull @('lbserver','serviceName','port'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $monitorName = "HTTP-$serviceName-$port"
    $command = "tmsh -q create ltm monitor http $monitorName { "
    $command += "defaults-from http "
    $command += "destination *`:$port "
    $command += "interval 10 "
    $command += "timeout 31 "
    $command += "time-until-up 0 "
    $command += "send 'GET /keepalives/$($serviceName).ashx HTTP/1.0\r\n\r\n' "
    $command += "recv robloxup "
    $command += "}"
	#Write-Host $command
    $rawText = RunCommand $command $null $lbServer
	$monitor = GetLoadBalancerMonitors $lbServer $monitorName
    if ($null -eq $monitor){return $false}
    return $true
}

Set-Alias CreateLBHttpsMon CreateLoadBalancerHttpsMonitor
<#
.SYNOPSIS
Creates the HTTPS monitor for the specified service in the load balancer
.DESCRIPTION
Uses tmsh to create the HTTPS monitor for the specified service in the load balancer
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER serviceName
The name of the service the monitor is being created for.
.EXAMPLE
PS C:\PowerShell> CreateLoadBalancerHTTPSMonitor lb Service 
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function CreateLoadBalancerHttpsMonitor([string]$lbserver, [string]$serviceName)
{
if (AreAnyRequiredArgumentsNull @('lbserver','serviceName'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $monitorName = "HTTPS-$serviceName"
    $command = "tmsh -q create ltm monitor https $monitorName { "
    $command += "cipherlist DEFAULT:+SHA:+3DES:+kEDH "
    $command += "compatibility enabled "
    $command += "defaults-from https "
    $command += "destination *`:* "
    $command += "interval 10 "
    $command += "timeout 31 "
    $command += "time-until-up 0 "
    $command += "send 'GET /keepalives/newservice.ashx HTTP/1.1\r\nHost: $serviceName.api.roblox.com\r\nConnection: Close\r\n\r\n' "
    $command += "recv robloxup "
    $command += "}"
    $rawText = RunCommand $command $null $lbServer
	$monitor = GetLoadBalancerMonitors $lbServer $monitorName
    if ($null -eq $monitor){return $false}
    return $true
}

Set-Alias CreateLBHttpPool CreateLoadBalancerHttpPool
<#
.SYNOPSIS
Creates the HTTP and HTTPS pools for the specified service in the load balancer
.DESCRIPTION
Uses tmsh to create the HTTP and HTTPS pools for the specified service in the load balancer
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER serviceName
The name of the service the pool is being created for.
.PARAMETER port
The port for the HTTP pool
.EXAMPLE
PS C:\PowerShell> CreateLoadBalancerPools lb Service 48999
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function CreateLoadBalancerHttpPool([string]$lbserver, [string]$serviceName, [string]$port)
{
	if (AreAnyRequiredArgumentsNull @('lbserver','serviceName','port'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $poolName = "POOL-API-$serviceName-$port"
    $command = "tmsh -q create ltm pool $poolName "
    $command += "monitor HTTP-$serviceName-$port "
    $command += "load-balancing-mode predictive-member "
    $command += "service-down-action reset "
    $command += "slow-ramp-time 60 "

    $rawText = RunCommand $command $null $lbServer
	$httpPool = GetLoadBalancerPools $lbServer $poolName
    if ($null -eq $httpPool){ return $false }
    
    return $true
}
<#
.SYNOPSIS
Creates the HTTPS pool for the specified service in the load balancer
.DESCRIPTION
Uses tmsh to create the HTTPS pools for the specified service in the load balancer
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER serviceName
The name of the service the pool is being created for.
.PARAMETER port
The port for the HTTPS pool
.EXAMPLE
PS C:\PowerShell> CreateLoadBalancerPools lb Service 
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
Set-Alias CreateLBHttpsPool CreateLoadBalancerHttpsPool

function CreateLoadBalancerHTTPSPool([string]$lbserver, [string]$serviceName, [string]$port){
	if (AreAnyRequiredArgumentsNull @('lbserver','serviceName','port'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
	$poolNameHttps = "POOL-API-$serviceName-$port "
	$command = "tmsh create ltm pool $poolNameHttps "
    $command += "monitor HTTPS-$serviceName "
    $command += "load-balancing-mode least-connections-member "
    $command += "service-down-action reset "
    $command += "slow-ramp-time 60"
	
	$rawText = RunCommand $command $null $lbServer
	$httpsPool = GetLoadBalancerPools $lbServer $poolNameTcp
    if ($null -eq $httpsPool) { return $false }
	return $true
}

Set-Alias AddLBNodeToPool AddLoadBalancerNodeToPool
<#
.SYNOPSIS
Adds a node to a pool
.DESCRIPTION
Uses tmsh to add a node to a pool
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER poolName
The name of the pool to add the node to
.PARAMETER node
Node to add
.PARAMETER port
Port on the node to direct traffic to
.EXAMPLE
PS C:\PowerShell> AddLoadBalancerNodeToPool lb lbPoolName Node Port
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function AddLoadBalancerNodeToPool([string]$lbserver, [string]$poolName, [string]$node, [string]$port)
{
	if (AreAnyRequiredArgumentsNull @('lbserver','poolName','node','port'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q modify ltm pool $poolName members add { $node`:$port }"
    $rawText = RunCommand $command $null $lbServer
	$pool = GetLoadBalancerPoolInfo $lbServer $poolName
	foreach ($n in $pool.Nodes){
		if ($n.addr.Trim() -eq $node.Trim()){return $true}
	}
	return $false
}

Set-Alias RemoveLBNodeFromPool RemoveLoadBalancerNodeFromPool
<#
.SYNOPSIS
Removes a node from a pool
.DESCRIPTION
Uses tmsh to remove a node from a pool
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER poolName
The name of the pool to remove the node from
.PARAMETER node
Node to remove
.EXAMPLE
PS C:\PowerShell> AddLoadBalancerNodeToPool lb lbPoolName Node Port
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function RemoveLoadBalancerNodeFromPool([string]$lbserver, [string]$poolName, [string]$node, [string]$port)
{
	if (AreAnyRequiredArgumentsNull @('lbserver','poolName','node','port'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q modify ltm pool $poolName members delete { $node`:$port }"
    $rawText = RunCommand $command $null $lbServer
	$pool = GetLoadBalancerPoolInfo $lbServer $poolName
	foreach ($n in $pool.Nodes){
		if ($n.addr.Trim() -eq $node.Trim()){return $false}
	}
	return $false
}

Set-Alias GetLBVirtualServers GetLoadBalancerVirtualServers
<#
.SYNOPSIS
Gets the Virtual Servers from the Load Balancer
.DESCRIPTION
Uses tmsh to get the  virtual servers list
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.EXAMPLE
PS C:\PowerShell> GetLoadBalancerVirtualServers lb
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function GetLoadBalancerVirtualServers([string]$lbserver, [string]$filter = [string]::Empty)
{
	if (AreAnyRequiredArgumentsNull @('lbserver'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q show ltm virtual field-fmt"
    $rawText = RunCommand $command $null $lbServer
	if ([string]::IsNullOrEmpty($rawText)){return $null }
	$vsObjects = ParseTextToObjects $rawText '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
	if ([string]::IsNullOrEmpty($filter)){
    	return $vsObjects
	} else {
		return $vsObjects | Where-Object { $_.Name.ToLower().Contains($filter.ToLower()) }
	}
}

Set-Alias CreateLBVirtualServer CreateLoadBalancerVirtualServer
<#
.SYNOPSIS
Creates a Virtual Server in the Load Balancer
.DESCRIPTION
Uses tmsh to create a virtual server
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER serviceName
Name of the service the virtual server is being created for
.PARAMETER destination
destination for the virtual server
.PARAMETER pool
Name of the pool to add to the virtual server
.PARAMETER rule
Name of the IRule to add to the virtual server
.PARAMETER httpOrHttps
False for http, True for https, defaults to false
.EXAMPLE
PS C:\PowerShell> CreateLoadBalancerVirtualServer lb Service Destination Pool Rule $true 
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function CreateLoadBalancerVirtualServer([string]$lbserver, [string]$serviceName, [string]$destination, [string]$pool, [string]$rule, [bool]$httpOrHttps = $false)
{
	if (AreAnyRequiredArgumentsNull @('lbserver','serviceName','destination','pool','rule'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $vsName = "VS-API-$serviceName"
    if (-not $httpOrHttps){ $vsName += "-80" }
    $command = "tmsh -q create ltm virtual $vsName { "
    if (-not $httpOrHttps){ $command += "destination $destination`:80 " }
    else { $command += "destination $destination`:443 " }
    $command += "ip-protocol tcp "
    $command += "pool $pool "
    $command += "profiles add { tcp { } "
    if (-not $httpOrHttps){$command += "http { } } " }
    else { $command += "} " }
    $command += "rules { $rule } "
    $command += "source 0.0.0.0/0 "
    $command += "translate-address enabled "
    $command += "translate-port enabled "
    $command += "}"
    $rawText = RunCommand $command $null $lbServer
	$vs = GetLoadBalancerVirtualServers $lbserver $vsName
	if ($null -eq $vs){return $false }
	return $true
}

Set-Alias GetLBVirtualAddresses GetLoadBalancerVirtualAddresses
<#
.SYNOPSIS
Gets the Virtual Addresses from the load balancer
.DESCRIPTION
Uses tmsh to get Virtual Addresses
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.EXAMPLE
PS C:\PowerShell> GetLoadBalancerVirtualAddresses lb
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function GetLoadBalancerVirtualAddresses([string]$lbserver, [string]$filter = [string]::Empty)
{
	if (AreAnyRequiredArgumentsNull @('lbserver'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q show ltm virtual-address field-fmt"
    $rawText = RunCommand $command $null $lbServer
	if ([string]::IsNullOrEmpty($rawText)){return $null}
	$results = ParseTextToObjects $rawText '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
	if ([string]::IsNullOrEmpty($filter)){
    	return $results
	} else {
		return $results | Where-Object {$_.name.ToLower().Contains($filter.ToLower())}
	}
}
Set-Alias GetLBTrafficGroups GetLoadBalancerTrafficGroups
<#
.SYNOPSIS
Gets the Traffic Groups from the load balancer
.DESCRIPTION
Uses tmsh to get traffic groups
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.EXAMPLE
PS C:\PowerShell> GetLoadBalancerTrafficGroups lb
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function GetLoadBalancerTrafficGroups([string]$lbserver)
{
	if (AreAnyRequiredArgumentsNull @('lbserver'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q show cm traffic-group field-fmt"
    $rawText = RunCommand $command $null $lbServer
    return ParseTextToObjects $rawText '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
}

Set-Alias CreateLBVirtualAddress CreateLoadBalancerVirtualAddress
<#
.SYNOPSIS
Creates a Virtual Address on an F5 load balancer
.DESCRIPTION
Uses tmsh to create the virtual server
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER IP Address
IP for virtual address
.PARAMETER Traffic Group
Traffic Group name to use - Use GetLoadBalancerTrafficGroups
.EXAMPLE
PS C:\PowerShell> CreateLodBalancerVirtualAddress lb 10.0.0.0 <traffic group name>
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function CreateLoadBalancerVirtualAddress([string]$lbserver, [string]$ip, [string]$trafficGroup)
{
	if (AreAnyRequiredArgumentsNull @('lbserver','ip','trafficGroup'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q create ltm virtual-address $ip { "
    $command += "address $ip "
    $command += "mask 255.255.255.255 "
    $command += "traffic-group $trafficGroup "
    $command += "}"
    $rawText = RunCommand $command $null $lbServer
	$va = GetLoadBalancerVirtualAddresses $lbServer $ip
	if ($null -eq $va){return $false }
    return $true
}

Set-Alias CreateLBNode CreateLoadBalancerNode
<#
.SYNOPSIS
Creates a Node object (member server) on an F5 load balancer
.DESCRIPTION
Uses tmsh to create the node
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER NodeName
Name of the Pool for which to query on
.PARAMETER IPAddress
IPAddress to use to create the Node
.EXAMPLE
PS C:\PowerShell> CreateLoadBalancerNode lbX web123.roblox.com 209.15.x.x
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function CreateLoadBalancerNode ( $lbServer, [string] $nodeName, [string] $ipAddress)
{
	$rawText = RunCommand "tmsh -q create ltm node $ipAddress description $nodeName address $ipAddress" $null $lbServer
	
	return $rawText
}

Set-Alias GetLBNode GetLoadBalancerNode
<#
.SYNOPSIS
Retrieves a Node object (member server) on an F5 load balancer
.DESCRIPTION
Uses tmsh to get the node information and then parses it into an object
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER nodeServer
Server to get information about - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.EXAMPLE
PS C:\PowerShell> GetLoadBalancerNode lbX webY
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function GetLoadBalancerNode( $lbServer, $nodeServer)
{ 
	$nodeServer = GetIP $nodeServer
	$rawText = RunCommand "tmsh -q show ltm node $nodeServer" $null $lbServer
	if ($rawText -match 'The requested node address.*was not found')
	{	$rawText = RunCommand "tmsh -q show ltm node $((GetServer $nodeServer).PublicIPAddress)" $null $lbServer	}
	
	if ($rawText -match 'The requested node address.*was not found')
	{	LogInfo "Node $(GetHostName $nodeServer) does not exist on LB $(GetHostName $lbServer)!" $colors_Warning; return $null	}
	
	return ParseTextToObjects $rawText '^\s+\w+.*\s+[\d\w]+' '\s' '\}'
}

Set-Alias FindLBWebs FindLoadBalancedWebServers
function FindLoadBalancedWebServers([string] $url, [string] $webServerGroup, [string] $websiteName, [string] $farmName, [int] $numberOfRequestsToCheck = -1, [bool] $searchIISLogsIfNeeded = $true, [string] $username, [string] $password)
{
	#Find the WebServer which served the request - useful only when browsing a load-balanced website
	$results = @()
	if ([string]::IsNullOrEmpty($url))
	{	$url = GetInternetDomainByDataCenter	}
	if ($numberOfRequestsToCheck -lt 1)
	{	$numberOfRequestsToCheck = $config_WebLB_NumberOfWebRequestsToCheck	}
	
	for ($i=0; $i -lt $numberOfRequestsToCheck; $i++)
	{
		$response = BrowseWebPage $url $username $password
		$machineID_Index = $response.IndexOf("MachineID")
		if ($machineID_Index -gt 0)
		{
			$machineID = $response.SubString($machineID_Index,25).Split()[1]
			if (![string]::IsNullOrEmpty($machineID) -and $results -notcontains $machineID)
			{	$results += $machineID	}
		}
	}
	
	if ($searchIISLogsIfNeeded -and ([string]::IsNullOrEmpty($results) -or $results.Count -eq 0))
	{
		if ([string]::IsNullOrEmpty($farmName) -or [string]::IsNullOrEmpty($webServerGroup))
		{	LogInfo "Skipping IISLogs search because neither a Farm nor a ServerGroup were given!" $colors_Warning		}
		else
		{
			#Add a GUID to the QueryString and then search for the Request in the IIS Logs on every server 
			$testGUID = GetGUID 0
			if ($url.Contains('?'))
			{	$testURL = "$($url)&LBGUID=$($testGUID)"	}
			else
			{	$testURL = "$($url)?LBGUID=$($testGUID)"	}

			for ($i=0; $i -lt $numberOfRequestsToCheck; $i++)
			{
				$response = BrowseWebPage $testURL $username $password
			}
			
			Wait 10 "Waiting for IIS Logs to commit to disk"
			#Now look for the response
			$totalResponsesFound = 0
			foreach ($webServer in (FindWebServersByKeepAliveFiles $farmName -webServerGroup $webServerGroup -websiteName $websiteName))
			{
				$logCount = GetIISLogCount (GetIP $webServer) last1 "" "LBGUID=$testGUID" -websiteName $websiteName
				if ($logCount -gt 0 -and $results -notcontains $webServer.Name)
				{	
					$totalResponsesFound += $logCount
					$results += $webServer.Name	
				}
				#If we've already found every request that was submitted then end the loop
				if ($totalResponsesFound -ge $numberOfRequestsToCheck)
				{	break	}
			}
			
			#Try one more time if unsuccessful (sometimes it takes longer for IIS to commit to disk - or maybe the log just rolled over)
			if ([string]::IsNullOrEmpty($results) -or $results.Count -eq 0)
			{
				for ($i=0; $i -lt $numberOfRequestsToCheck; $i++)
				{
					$response = BrowseWebPage $testURL $username $password
				}
				
				Wait 30 "Waiting for IIS Logs to commit to disk"
				#Now look for the response
				$totalResponsesFound = 0
				foreach ($webServer in (FindWebServersByKeepAliveFiles $farmName -webServerGroup $webServerGroup -websiteName $websiteName))
				{
					$logCount = GetIISLogCount (GetIP $webServer) last1 "" "LBGUID=$testGUID" -websiteName $websiteName
					if ($logCount -gt 0 -and $results -notcontains $webServer.Name)
					{	
						$totalResponsesFound += $logCount
						$results += $webServer.Name	
					}
					#If we've already found every request that was submitted then end the loop
					if ($totalResponsesFound -ge $numberOfRequestsToCheck)
					{	break	}
				}
			}
		}
	}
	
	return $results
}

function VerifyLoadBalancerOnlineOffline([string] $url, [string] $webServerGroup, [string] $websiteName, [string] $farmName, [int] $numberOfRequestsToCheck = -1, [bool] $searchIISLogsIfNeeded = $true, [string] $username, [string] $password)
{
	$result = $false
	
	if ([string]::IsNullOrEmpty($webServerGroup) -and [string]::IsNullOrEmpty($websiteName) -and [string]::IsNullOrEmpty($farmName))
	{	LogInfo "Aborting... not enough arguments were passed in, please specify at least ServerGroup, Website, and/or Farm" $colors_Error; return $result	}
	if ($numberOfRequestsToCheck -lt 1)
	{	$numberOfRequestsToCheck = $config_WebLB_NumberOfWebRequestsToCheck	}
		
	$webServers = GetServers "web" $webServerGroup
	if ([string]::IsNullOrEmpty($webServers.Count) -or $webServers.Count -lt 2)
	{	LogInfo "Aborting... not enough web servers are available in group '$webServerGroup' to test with - only found $($webServers.Count)" $colors_Error; return $result	}
	
	LogInfo " ...Finding the current webserver... " $colors_Warning
	$firstWebServer = ""
	while ([string]::IsNullOrEmpty($firstWebServer))
	{	$firstWebServer = @(FindLoadBalancedWebServers $url $webServerGroup $websiteName $farmName 1 $searchIISLogsIfNeeded $username $password)[0]; wait 1	}
	
	LogInfo " ...Taking first webserver offline ($($firstWebServer))... " $colors_Warning
	if ((IsServerInProductionEnvironment $firstWebServer))
	{
		$message = "$(GetServerIdentification $firstWebServer 0) is being taken offline for a few minutes during LoadBalancer testing of the resource `n$url `n`nAnother email will follow when the server is brought back online once the testing has completed."
		SendEmail "Taking $(GetServerIdentification $firstWebServer 1) offline temporarily" $message  
	}
	$firstWebOffline = $false
	while (!$firstWebOffline)
	{	$firstWebOffline = TakeWebsiteOfflineUsingKeepAliveFiles $firstWebServer $websiteName -offlineExtension $defaultTemporarilyOfflineExtension; wait 10 	}
	
	LogInfo " ...Finding the current webserver... " $colors_Warning
	$nextWebServers = @()
	while ([string]::IsNullOrEmpty($nextWebServers) -or $nextWebServers.Count -eq 0)
	{	$nextWebServers += @(FindLoadBalancedWebServers $url $webServerGroup $websiteName $farmName $numberOfRequestsToCheck $searchIISLogsIfNeeded $username $password); wait 1	}
	
	$result = ($nextWebServers -notcontains $firstWebServer)
	
	LogInfo " ...Bringing the first webserver back online ($($firstWebServer))... "
	while ($firstWebOffline)
	{	$firstWebOffline = (!(BringWebsiteOnlineUsingKeepAliveFiles $firstWebServer $websiteName -offlineExtension $defaultTemporarilyOfflineExtension)); wait 10 	}
	if ((IsServerInProductionEnvironment $firstWebServer))
	{
		$message = "$(GetServerIdentification $firstWebServer 0) has been brought back online because the LoadBalancer tests have completed for the resource `n$url"
		SendEmail "$(GetServerIdentification $firstWebServer 1) is back online" $message  
	}

	if ($result)
	{	LogInfo "Online/Offline test succeeeded! Traffic was verified to have switched from $($firstWebServer) to $($nextWebServers)!" $colors_Success	}
	else
	{	LogInfo "Online/Offline test failed! Traffic was found on $($firstWebServer) and then on $($nextWebServers)!" $colors_Error	}
	
	return $result	
}

function VerifyLoadBalancerVIPBrowsing([int] $websiteID, [string] $sourceWebServerGroup, [int] $numberOfServersToCheck = -1, [string] $username, [string] $password)
{
	$result = $false
	
	if ($websiteID -lt 1)
	{	LogInfo "Aborting... WebsiteID is required and must be a positive integer ($websiteID was given)" $colors_Error; return $result	}
	$websiteFromDB = GetWebsiteFromDB $websiteID
	$serverGroup = GetServerGroups $websiteFromDB.ServerGroupID
	if ([string]::IsNullOrEmpty($sourceWebServerGroup ))
	{	$sourceWebServerGroup = $serverGroup.Name	}
	
	$sourceWebServers = GetServers "web" $sourceWebServerGroup
	$webServerBehindVIP = (GetServers "web" $serverGroup.Name)[0]
	$websiteName = GetWebsiteName $webServerBehindVIP $websiteFromDB.IISWebSiteID
	
	if ([string]::IsNullOrEmpty($serverGroup))
	{	LogInfo "Aborting... could not determine the ServerGroup of websiteID '$websiteID'" $colors_Error; return $result	}
	if ([string]::IsNullOrEmpty($sourceWebServers))
	{	LogInfo "Aborting... could not find any Servers for the ServerGroup '$sourceWebServerGroup'" $colors_Error; return $result	}
	if ([string]::IsNullOrEmpty($webServerBehindVIP))
	{	LogInfo "Aborting... could not find a Server for the ServerGroup '$($serverGroup.Name)'" $colors_Error; return $result	}
	if ([string]::IsNullOrEmpty($websiteName))
	{	LogInfo "Aborting... could not find WebsiteID '$websiteID' on $($sourceWebServers[0].Name)" $colors_Error; return $result	}
	
	if ($numberOfServersToCheck -lt 1)
	{	$numberOfServersToCheck = $config_WebLB_NumberOfWebRequestsToCheck	}
	
	$numberOfServersToCheck = [Math]::Min($numberOfServersToCheck, ($sourceWebServers.Count))
		
	LogInfo " ...Testing $numberOfServersToCheck webservers to see if they can browse the VIP for website '$websiteName' ... " $colors_Warning
	$healthChecks = @(GetWebsiteHealthChecks $webServerBehindVIP $websiteName -testPrivateIP 0 -testPublicIP 0 -testServerDNSName 0 -testSSLDNSName 1)
	
	for ($i=0; $i -lt $numberOfServersToCheck; $i++)
	{
		$rand = GenerateRandomNumber 0 ($sourceWebServers.Count - 1)
		$testWebServer = $sourceWebServers[$rand]
		LogInfo " ... Verifying VIP Browsing from $($testWebServer.Name) to website '$websiteName'..." $colors_Warning 
		
		foreach ($healthCheck in $healthChecks)
		{
			$testResult = RunPowershellCommands "CheckWebPage '$($healthCheck.URL)' '$($healthCheck.ExpectedContent)'" $testWebServer -includeRobloxLibrary 1
			$result = $false
			$result = $testResult[-1] -like 'true'
			
			if ($result)
			{	LogInfo "$($testWebServer.Name) -> '$($healthCheck.URL)' succeeded!" $colors_Success	}
			else
			{	LogInfo "$($testWebServer.Name) -> '$($healthCheck.URL)' failed!" $colors_Error; break	}
		}
	}

	if ($result)
	{	LogInfo "VIP Browsing test succeeeded! $numberOfServersToCheck servers were verfied to have been able to browse the VIP!" $colors_Success	}
	else
	{	LogInfo "VIP Browsing test failed! $($testWebServer.Name) was not able to browse the VIP!" $colors_Error	}
	
	return $result	
}

<#
.SYNOPSIS
Marks a node disabled globally
.DESCRIPTION
Uses tmsh to disabled a node globally
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER node
Node to remove
.EXAMPLE
PS C:\PowerShell> EnableLoadBalancerNodeGlobal lb Node
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function EnableLoadBalancerNodeGlobal([string]$lbserver, [string]$node)
{
	if (AreAnyRequiredArgumentsNull @('lbserver','node'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q modify ltm node $node session user-enabled"
    $rawText = RunCommand $command $null $lbServer
	$node = GetLoadBalancerNode $lbServer $node
	if (($node.session -eq "monitor-enabled") -or ($node.session -eq "user-enabled")){return $true }
	return $false
}

Set-Alias DisableLBNodeGlobal DisableLoadBalancerNodeGlobal
<#
.SYNOPSIS
Marks a node disabled globally
.DESCRIPTION
Uses tmsh to disabled a node globally
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER node
Node to remove
.EXAMPLE
PS C:\PowerShell> DisableLoadBalancerNodeGlobal lb Node
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function DisableLoadBalancerNodeGlobal([string]$lbserver, [string]$node)
{
	if (AreAnyRequiredArgumentsNull @('lbserver','node'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q modify ltm node $node session user-disabled"
    $rawText = RunCommand $command $null $lbServer
	$node = GetLoadBalancerNode $lbServer $node
	if (($node.session -eq "monitor-enabled") -or ($node.session -eq "user-enabled")){return $false }
	return $false
}

Set-Alias OnlineLBNodeGlobal OnlineLoadBalancerNodeGlobal
<#
.SYNOPSIS
Marks a node online globally
.DESCRIPTION
Uses tmsh to online a node globally
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER node
Node to remove
.EXAMPLE
PS C:\PowerShell> OnlineLoadBalancerNodeGlobal lb Node
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function OnlineLoadBalancerNodeGlobal([string]$lbserver, [string]$node)
{
	if (AreAnyRequiredArgumentsNull @('lbserver','node'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q modify ltm node $node state user-up"
    $rawText = RunCommand $command $null $lbServer
	$node = GetLoadBalancerNode $lbServer $node
	if ($node.state -eq "up"){return $false }
	return $false
}

Set-Alias OfflineLBNodeGlobal OfflineLoadBalancerNodeGlobal
<#
.SYNOPSIS
Marks a node offline globally
.DESCRIPTION
Uses tmsh to offline a node globally
.PARAMETER lbServer
F5 Load Balancer for which to query - can be a CommonName (WebX), a HostName, an IPAddress, or a Server Record (GetServer)
.PARAMETER node
Node to offline
.EXAMPLE
PS C:\PowerShell> OfflineLoadBalancerNodeGlobal lb Node
.LINK
http://support.f5.com/content/kb/en-us/products/big-ip_ltm/manuals/product/bigip_tmsh_refguide/_jcr_content/pdfAttach/download/file.res/Traffic_Management_Shell_(tmsh)_Reference_Guide.pdf
#>
function OfflineLoadBalancerNodeGlobal([string]$lbserver, [string]$node)
{
	if (AreAnyRequiredArgumentsNull @('lbserver','node'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
    $command = "tmsh -q modify ltm node $node state user-down"
    $rawText = RunCommand $command $null $lbServer
	$node = GetLoadBalancerNode $lbServer $node  
	if ($node.state -ne "up"){return $false }
	return $false
}