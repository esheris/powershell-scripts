#Assumptions: webserver name: server01 - Sitename: testsite - Sites are stored in D:\ - Logs are stored in E:\logroot - IIS logs are cycled daily - 
#New log files are created at 20 mb - certs are stored in \\cert\path and the cert name is certname.pfx

Import-Module WebAdministration
# Define scriptblock with function for import of certficiates
$sb = [scriptblock]::create("
function Import-PfxCertificate {
 
	param([String]$certPath,[String]$certRootStore = �LocalMachine�,[String]$certStore = �My�,$pfxPass = $null)
	$pfx = new-object System.Security.Cryptography.X509Certificates.X509Certificate2    
	if ($pfxPass -eq $null) {$pfxPass = read-host "Enter the pfx password for cert at $certPath" -assecurestring}    
	$pfx.import($certPath,$pfxPass,"PersistKeySet")    
	$store = new-object System.Security.Cryptography.X509Certificates.X509Store($certStore,$certRootStore)    
	$store.open("MaxAllowed")    
	$store.add($pfx)    
	$store.close()    S
}
$certPath = "\\Cert\Path"
$certName = "certname.pfx"
$webserver = "server01"
$sitename = "testsite"
$logpath = "e:\logroot"
$pfxPass = Get-PFXPassword -certname $certname
New-Item -type directory -path "D:\" -name $sitename
New-WebAppPool -Name "$sitename-pool"
New-Website -Name $sitename -Port 80 -HostHeader $sitename -PhysicalPath "d:\testsite"
Set-WebConfigurationProperty -Filter /system.webserver/security/authentication/AnonymousAuthentication -name enabled -value false -Location testsite
Set-WebConfigurationProperty "/system.applicationhost/sites/sitedefaults" -Name logfile.enabled -Value True
Set-WebConfigurationProperty "/system.applicationhost/sites/sitedefaults" -Name logfile.directory -Value $logpath
Set-WebConfigurationProperty "/system.applicationhost/sites/sitedefaults" -Name logfile.period -Value Daily
set-WebConfigurationProperty "/system.applicationhost/sites/sitedefaults" -Name logfile.truncateSize -Value 20971520
Import-PfxCertificate -certPath ($path + $cert1) -pfxPass $pfxPass
"
Invoke-Command -ComputerName $webserver -ScriptBlock $sb