$config_Linux_DefaultPackages = @('nano','sysstat','iotop','iptraf','wireshark','wget','gcc','automake') #,'mytop' MyTop is not in the standard repo so it always fails, we can add it back in once we make the install code smarter to deal with this
$SoftwareRepositoriesServers_GroupName = "SoftwareRepositories"
$SoftwareRepository_Java_FolderName = "Java"
$linuxSwappiness = 1
$linuxCachePressure = 50
$linuxElevator = "deadline"

function SetLinuxOptimizationsForSSDs( $lappServer)
{
	if (AreAnyRequiredArgumentsNull @('lappServer'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
	$errors = @()
	if (-not(SetLinuxFstab $lappServer)){ $errors += "Failed setting fstab" }
	if (-not(SetLinuxSwappinessSetting $lappServer)){ $errors += "Failed setting swappiness" }
	if (-not(SetLinuxCachePressureSetting $lappServer)){ $errors += "Failed setting Cache Pressure" }
	if (-not(SetLinuxKernelParams $lappServer)){ $errors += "Failed setting Kernel Params" }
	if ($errors.Count -eq 0)
	{ return $true }
	LogInfo $errors $colors_Error
	return $false
}

function GetLinuxFstab($lappServer)
{
	if (AreAnyRequiredArgumentsNull @('lappServer'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $null	}
	return RunCommand "cat /etc/fstab | grep -v \#" '' $lappServer
}

function SetLinuxFstab($lappServer)
{
	if (AreAnyRequiredArgumentsNull @('lappServer'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
	$fstab = GetLinuxFstab $lappServer
	foreach ($line in $fstab){
		if ($line.contains("RobloxVolGroup-lv_root") -and (-not($line.contains("noatime"))))
		{
			RunCommand "sed -i -e '/^\/dev\/mapper\/RobloxVolGroup-lv_root/s/defaults/defaults,noatime,nodiratime/g' /etc/fstab" '' $lappServer
		}
		if ($line.contains("RobloxVolGroup-lv_home") -and (-not($line.contains("noatime"))))
		{
			RunCommand "sed -i -e '/^\/dev\/mapper\/RobloxVolGroup-lv_home/s/defaults/defaults,noatime,nodiratime/g' /etc/fstab" '' $lappServer
		}
		if ($line.contains("/boot") -and (-not($line.contains("noatime"))))
		{
			RunCommand "sed -i -e '/\/boot/s/defaults/defaults,noatime,nodiratime/g' /etc/fstab" '' $lappServer
		}
	}
	$fstab = GetLinuxFstab $lappServer
	foreach ($line in $fstab){
		if ($line.contains("RobloxVolGroup-lv_root") -and (-not($line.contains("noatime"))))
		{
			return $false
		}
		if ($line.contains("RobloxVolGroup-lv_home") -and (-not($line.contains("noatime"))))
		{
			return $false
		}
		if ($line.contains("/boot") -and (-not($line.contains("noatime"))))
		{
			return $false
		}
	}
	return $true
}
function GetLinuxSwappinessSetting($lappServer)
{
	if (AreAnyRequiredArgumentsNull @('lappServer'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $null	}
	$rawText = RunCommand "cat /etc/sysctl.conf | grep vm.swappiness" '' $lappServer
	if ([string]::IsNullOrEmpty($rawText))
	{
		return $null
	}
	return $rawText.split("=")[1]
}
function SetLinuxSwappinessSetting($lappServer)
{
	if (AreAnyRequiredArgumentsNull @('lappServer'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
	$currentSwappiness = GetLinuxSwappinessSetting $lappServer
	if ($currentSwappiness -eq $null)
	{ 
		RunCommand 'echo "vm.swappiness=$linuxSwappiness" >> /etc/sysctl.conf' '' $lappServer 
		return ((GetLinuxSwappinessSetting $lappServer) -eq $linuxSwappiness)
	}
	if ($currentSwappiness -ne $linuxSwappiness)
	{
		RunCommand "sed -i -e 's/vm.swappiness=$currentSwappiness/vm.swappiness=$linuxSwappiness/g' /etc/sysctl.conf" '' $lappServer
		return ((GetLinuxSwappinessSetting $lappServer) -eq $linuxSwappiness)
	}
	return $true
}
function GetLinuxCachePressureSetting($lappServer)
{
	if (AreAnyRequiredArgumentsNull @('lappServer'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $null	}
	$rawText = RunCommand "cat /etc/sysctl.conf | grep vm.vfs_cache_pressure" '' $lappServer
	if ([string]::IsNullOrEmpty($rawText))
	{
		return $null
	}
	return $rawText.split("=")[1]
}

function SetLinuxCachePressureSetting($lappServer)
{
	if (AreAnyRequiredArgumentsNull @('lappServer'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
	$currentCachePressure = GetLinuxCachePressureSetting $lappServer
	if ($currentCachePressure -eq $null)
	{ 
		RunCommand 'echo "vm.vfs_cache_pressure=$linuxCachePressure" >> /etc/sysctl.conf' '' $lappServer 
		return ((GetLinuxCachePressureSetting $lappServer) -eq $linuxCachePressure)
	}
	if ($currentCachePressure -ne $linuxCachePressure)
	{
		RunCommand "sed -i -e 's/vm.vfs_cache_pressure=$currentCachePressure/vm.vfs_cache_pressure=$linuxCachePressure/g' /etc/sysctl.conf" '' $lappServer
		return ((GetLinuxCachePressureSetting $lappServer) -eq $linuxCachePressure)
	}
	return $true
}
function GetLinuxKernelParams($lappServer)
{
	if (AreAnyRequiredArgumentsNull @('lappServer'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $null	}
	$rawText = RunCommand " cat /etc/grub.conf | grep kernel | grep \# -v" '' $lappServer
	return $rawText.split()
}

function SetLinuxKernelParams($lappServer)
{
	if (AreAnyRequiredArgumentsNull @('lappServer'))
	{	LogInfo "Aborting $($myInvocation.InvocationName)... Required arguments were not given!" $colors_Error; return $false	}
	$currentParams = GetLinuxKernelParams $lappServer
	$elevator = $currentParams -match "elevator="
	if ([string]::IsNullOrEmpty($elevator[0]))
	{
		RunCommand "sed -i -e '/kernel/s/$/ elevator=deadline/g' /etc/grub.conf" '' $lappServer
		return ((GetLinuxKernelParams $lappServer) -contains "elevator=deadline")
	}
	if ((-not([string]::IsNullOrEmpty($elevator[0]))) -and ($elevator[0] -ne "elevator=deadline"))
	{
		 $currentsetting = $elevator[0].split("=")[1]
		 RunCommand "sed -i 's/elevator=$currentsetting/elevator=deadline/g' /etc/grub.conf" '' $lappServer
		 return ((GetLinuxKernelParams $lappServer) -contains "elevator=deadline")
	}
	return $true
}
