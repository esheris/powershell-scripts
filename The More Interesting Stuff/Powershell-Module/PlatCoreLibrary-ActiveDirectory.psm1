﻿Function Get-VCentersFromAD
{
<#
        .SYNOPSIS
        Uses an LDAP query to locate our Vcenter Servers within AD
        .DESCRIPTION
        Performs an LDAP query of the local Active Directory to locate all non disabled computer objects,
        whose description contains the phrase "vcent".
        This allows us to automatically discover the Virtual Center Servers within our environment
        .EXAMPLE
        Get-VcentersFromAD -type vCenter
		Datacenter      : IL1
		ServerName      : IL1-VC-STD04
		Description     : vCenter:prod
		OperatingSystem : Windows Server 2008 R2 Standard
		IPv4            : 10.98.120.32
		
		Datacenter      : IL1
		ServerName      : IL1-VC-M01
		Description     : vCenter:prod
		OperatingSystem : Windows Server 2008 R2 Standard
		IPv4            : 10.98.120.34
        ....
        .PARAMETER type
        vCenters, vCenterDatabase, or All
        .NOTES
        Requires domain connectivity.
    #>
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$false)]
        [validateset("vCenter","vDB","All")]
        [string]$Type,
        [parameter(Mandatory=$false)]
		[string]$datacenter
    )
    begin {
        if ((gwmi win32_computersystem).partofdomain -eq $false){Write-Host "This Cmdlet expects the machine it is running on to be domain joined. Please run from a domain joined machine" -ForegroundColor Red; continue }
        switch  ($Type)
        {
            "vCenter" {[array]$filter = "OU=VCenter Servers,OU=Servers,DC=t3n,DC=dom"}
            "vDB" {[array]$filter = "OU=VCDB Servers,OU=Servers,DC=t3n,DC=dom"}
            "All" {[array]$filter = "OU=VCenter Servers,OU=Servers,DC=t3n,DC=dom","OU=VCDB Servers,OU=Servers,DC=t3n,DC=dom"}
            default {[array]$filter = "OU=VCenter Servers,OU=Servers,DC=t3n,DC=dom","OU=VCDB Servers,OU=Servers,DC=t3n,DC=dom"}
        }
    }
    process {
        $colResults = @()
        foreach ($f in $filter){
            $Searcher = New-Object DirectoryServices.DirectorySearcher -Property @{
                Filter = "(&(objectCategory=computer)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))"
                SearchRoot = "LDAP://$f"
                PageSize = 10000
            }
            $colResults += $Searcher.FindAll()
        }
        $results = @()

        foreach ($objResult in $colResults)
        {
            $vcenter = new-object psobject
            $description = [string]::Empty
            try {
                $description = $objResult.Properties.description[0]
            } catch {}

			$vcenter | Add-Member -MemberType NoteProperty -Name "Datacenter" -Value $($objResult.Properties.name | select -first 1).substring(0,3).ToString()
			$vcenter | Add-Member -MemberType NoteProperty -Name "ServerName" -Value $($objResult.Properties.name | select -first 1).ToString()
            $vcenter | Add-Member -MemberType NoteProperty -Name "Description" -Value $description
			$vcenter | Add-Member -MemberType NoteProperty -Name "OperatingSystem" -Value $($objResult.Properties.operatingsystem | select -first 1).ToString()
            $vcenter | Add-Member -MemberType NoteProperty -Name IPv4 -Value $([system.net.dns]::GetHostAddresses($($objResult.Properties.name | select -first 1))).ToString()
            $results += $vcenter
        }
    }
    end {
        $Searcher.Dispose()
		if ([string]::IsNullOrEmpty($datacenter)){
			return $results 
		} else {
			return $results | where {$_.Datacenter -eq $datacenter} 
		}
        
    }
}