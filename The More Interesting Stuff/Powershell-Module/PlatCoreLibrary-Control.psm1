Function Get-ServerCredentials {
 Param(
 <#
     .SYNOPSIS
    Gets VM Credentials from control for a specified VM
    .DESCRIPTION
    Gets VM Credentials from control for a specified VM
    .EXAMPLE
    $vm = Get-VM -Name "example"
    Get-ServerCredentials -VMObject $vm
    .PARAMETER VMObject
    Valid Virtual Machine object from Get-VM
    .NOTES
    Requires the user to have valid Control API Access
 #>
   [Parameter(Mandatory = $True)]
   [VMware.Vim.VirtualMachine]$VMObject
    )
        if (!$bearerHeader){
        Write-host "Please provide a username and password for Platform login"
        $user = Read-host "Control V2 API Username" 
        $pass = Read-host "Control V2 API Password"        
        Connect-ToPlatform -ControlUsername $user -ControlPassword $pass
        } 
        if ($bearerHeader){
            $folder = $VMObject.folder.name
            $servername = $VMObject.name
            
            #log into platform and get the credentials.              
            $servercredsURL = "https://api.ctl.io/v2/servers/$folder/$servername/credentials"
    
            $serverCreds = Invoke-RestMethod  -Method GET -Headers $bearerHeader -Uri $servercredsURL -ContentType "application/json" -SessionVariable "theSession"
            $serverCreds
        }
                
}

Function Connect-ToPlatform {
 <#
     .SYNOPSIS
    Generates an authenticated session with Control
    .DESCRIPTION
    Generates an authenticated session with Control
    .EXAMPLE
    Connect-ToPlatform -ControlUsername $user -ControlPassword $pass
    .PARAMETER ControlUsername
    Control Username
    .PARAMETER ControlPassword
    Control Password
    .NOTES
    Requires the user to have valid Control API Access
 #>
 Param(
   [Parameter(Mandatory = $True)]
   [ValidateNotNullOrEmpty()]
   $ControlUsername,
    [Parameter(Mandatory = $True)]
    [ValidateNotNullOrEmpty()]
   $ControlPassword
    )

            $body = @{
                username = $ControlUsername
                password = $ControlPassword
            }
            $newpassword = "Blank"
            $body = $body | ConvertTo-Json 
            $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
            #log into platform and get back a bearerToken
            $logonUrl  = "https://api.ctl.io/v2/authentication/login"
            $logonResponse = Invoke-RestMethod  -Method Post -Headers $headers -Uri $logonUrl -Body $body -ContentType "application/json" -SessionVariable "theSession" -ErrorAction continue
            $bearer = $logonResponse.bearerToken
            $bearer = " Bearer " + $bearer
            $headers.Add("Authorization",$bearer)
            
  }
