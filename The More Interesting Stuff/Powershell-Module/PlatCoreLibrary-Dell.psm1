$CTLSqlServer = "wa1t3nessql01.t3n.dom";
function New-DellServiceRequest{
<#
    .SYNOPSIS
    Create a new Dell Service Request
    .DESCRIPTION
    Create a new Dell Service Request for hardware replacement
    .EXAMPLE
    New-DellServiceRequest -ServiceTag 5HTJVH1  -Datacenter WA1 -PrimaryContactName "Ray Salamon" -SecondaryContactName "Kevin Kang" -Part OTR -onsite $true -TroubleshootingNotes "Failed Bootable SD CARD (8gb) in DRAC"
    .PARAMETER ServiceTag
    The service tag to retrieve parts for
    .PARAMETER Datacenter
    The Datacenter the server is located in. Valid values are: CA1,CA2,CA3,DE1,DE2,GB1,GB3,IL1,IL2,LB1,LB2,LB3,NE1,NJ1,NY1,PD1,SG1,UC1,UT1,VA1,WA1
    .PARAMETER PrimaryContactName
    First and Last name of the primary contact (normally yourself) Current valid values are Ray Salamon, Kevin Kang, Kelly Mendenhall, Eric Sheris
    The name is used for a database lookup to find the appropriate email address and phone number
    .PARAMETER SecondaryContactName
    Same as PrimaryContactName
    .PARAMETER Part
    Valid Part Code from Get-ValidDellParts
    .PARAMETER onsite
    $true if you want to have dell schedule a tech to go to the datacenter to replace the part, $false to just send the part to the datacenter
    .PARAMETER TroubleshootingNotes
    Notes on what was done to diagnose the problem. Dell will accept or reject the request depending on this. Also put information here if your part is OTR (other)
#>
    param(
        [Parameter(Mandatory = $True)]
        [ValidateLength(5,7)]
        [string]$ServiceTag,
        [Parameter(Mandatory = $True)]
        [ValidateSet("CA1","CA2","CA3","DE1","DE2","GB1","GB3","IL1","IL2","LB1","LB2","LB3","NE1","NJ1","NY1","PD1","SG1","UC1","UT1","VA1","WA1")]
        [string]$Datacenter,
        [Parameter(Mandatory = $True)]
        [ValidateNotNullOrEmpty()]
        [string]$PrimaryContactName,
        [Parameter(Mandatory = $True)]
        [ValidateNotNullOrEmpty()]
        [string]$SecondaryContactName,
        [Parameter(Mandatory = $True)]
        [ValidateNotNullOrEmpty()]
        [string]$Part,
        [Parameter(Mandatory = $True)]
        [ValidateNotNullOrEmpty()]
        [Boolean]$onsite,
        [Parameter(Mandatory = $True)]
        [ValidateNotNullOrEmpty()]
        [string]$TroubleshootingNotes
    )

    $validParts = Get-ValidDellParts -ServiceTag $ServiceTag
    $Part = $Part.ToUpper()
    if ($validParts.PartNumber -contains $Part){
        $data = @{
            serial = $ServiceTag
            datacenter = $Datacenter
            primary = $PrimaryContactName
            alternate = $SecondaryContactName
            part = $Part
            troubleshootingNotes = $TroubleshootingNotes
            onsite = $onsite.ToString()
        } | ConvertTo-Json

        $result = Invoke-RestMethod -Method Post -Uri "http://wa1t3nesiis02.t3n.dom/api/Dispatch/" -Body $data -ContentType "application/json"
        Write-Log -Message $($result | out-string) -Level Info
        $result
    } else {
        Write-Log -Message "Invalid Part Specified. Please use a PartNumber from the below list of valid parts." -Level Warn
        $validParts | Select PartTypeCode, PartNumber, PartDescription
    }
}

function Get-ValidDellParts{
<#
    .SYNOPSIS
    Retrieves a list of valid parts for a given service tag
    .DESCRIPTION
    Retrieves a list of valid parts for a given service tag
    .EXAMPLE
    Get-ValidDellParts -ServiceTag <ServiceTag for server here>
    .PARAMETER ServiceTag
    The service tag to retrieve parts for
#>
    param(
        [Parameter(Mandatory = $True)]
        [ValidateLength(5,7)]
        [string]$ServiceTag
    )
    
    $baseURL = "http://wa1t3nesiis02.t3n.dom/api/Dispatch/Parts"
    $fullUrl = "$baseURL/$ServiceTag/"
    Invoke-RestMethod -Method Get -Uri $fullUrl 
}

function Get-DellDispatchStatus {
<#
    .SYNOPSIS
    Retrieves the current status of a specified dispatch from Dell
    .EXAMPLE
    Get-DellDispatchStatus -SRNumber SR123455
    .PARAMETER SRNumber
    Dell Service Request Number
    .NOTES 
    Requires a valid Dell TechDirect account
#>
    param(
        [Parameter(Mandatory = $True)]
        [ValidateNotNullOrEmpty()]
        [string]$SRNumber
    )

    $baseURL = "http://wa1t3nesiis02.t3n.dom/api/Dispatch"
    $fullUrl = "$baseURL/$SRNumber/"
    $response = Invoke-RestMethod -Method Get -Uri $fullUrl
    return $response
}

function Get-DellDispatches {
    <#
        .SYNOPSIS
        Gets all dispatches from the dispatch database
        .NOTES
        Dispatch Database information is updated nightly and may not always show the most recent data
    #>
    return Invoke-RestMethod -uri "http://wa1t3nesiis02.t3n.dom/api/Dispatch/" -Method Get
}

function Update-DellDispatchStatus {
<#
    .SYNOPSIS
    Updates the Dispatch status to the specified status
    .PARAMETER RowID
    SQL Row ID to update
    .PARAMETER status
    Status text to update the Status column to.
#>
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [int]$RowID,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [String]$status
    )
    Invoke-SqlStoredProcedure -server $CTLSqlServer -database ToolingData -SP_Name Update_DellDispatchStatus -parameters @{ "ID"=$RowID;"Status"=$status}
}
