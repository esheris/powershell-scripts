﻿ $CTLSqlServer = "wa1t3nessql01.t3n.dom";
 function Write-Log
{
<#
.Synopsis
   Write-Log writes a message to a specified log file with the current time stamp.
.DESCRIPTION
   The Write-Log function is designed to add logging capability to other scripts.
   In addition to writing output and/or verbose you can write to a log file for
   later debugging.

   By default the function will create the path and file if it does not
   exist.

.EXAMPLE
   Write-Log -Message "Log message"
   Writes the message to  the same location as the current script.
.EXAMPLE
   Write-Log -Message "Restarting Server"
   Writes the content to the log file and creates the path and file specified.
.EXAMPLE
   Write-Log -Message "Does not exist" -Path -Level Error
   Writes the message to the log file as an error message, and writes the message to the error pipeline.
#>
    [CmdletBinding()]
    #[Alias('wl')]
    [OutputType([int])]
    Param
    (
        # The string to be written to the log.
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,

        [Parameter(Mandatory=$false,
                    ValueFromPipelineByPropertyName=$true, 
                    Position=3)]
        [ValidateSet("Error","Warn","Info")]
        [string]$Level="Info",

        [Parameter(Mandatory=$false)]
        [switch]$NoClobber
    )
    Begin
    {
      $BasePath="$(Split-Path $psscriptroot -parent)\PlatCoreLibLogs"
      $currentUser = $env:username
      $Path = "$BasePath\$currentUser-PSLog.log"
    }
    Process
    {

        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Warning "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
            }

        # If attempting to write to a log file in a folder/path that doesn't exist
        # to create the file include path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
            }

        else {
              $file = get-Item $Path
              if ($file.Length -gt 1mb){
                if (Test-Path "$Path.old"){
                  remove-item "$Path.old" -Force
                }
                Move-Item -Path $Path -destination "$Path.old" -Force
                $NewLogFile = New-Item $Path -Force -ItemType File
              }
            }

        # Now do the logging and additional output based on $Level
        switch ($Level) {
            'Error' {
                Write-Error $Message
                Write-Output "$(Get-Date -Format "yyyy-MM-dd HH:mm:ss") ERROR: $Message" | Out-File -FilePath $Path -Append
                }
            'Warn' {
                Write-Warning $Message
                Write-Output "$(Get-Date -Format "yyyy-MM-dd HH:mm:ss") WARNING: $Message" | Out-File -FilePath $Path -Append
                }
            'Info' {
                Write-Verbose $Message
                Write-Output "$(Get-Date -Format "yyyy-MM-dd HH:mm:ss") INFO: $Message" | Out-File -FilePath $Path -Append
                }
            }
    }
    End
    {
    }
}

function ConvertFrom-Timestamp {
<#
    .SYNOPSIS
    Converts timestamps to standard datetime objects
    .DESCRIPTION
    Converts timestamps generally found in ActiveDirectory or WMI to normal datetime objects
    .LINK
    (CIM_DATETIME Documentation) https://msdn.microsoft.com/en-us/library/aa387237(v=vs.85).aspx
    (FromFileTimeUTC Documentation) https://msdn.microsoft.com/en-us/library/system.datetime.fromfiletimeutc(v=vs.110).aspx
    .EXAMPLE
    Active Directory Last Logon style timestamp
        ConvertFrom-Timestamp -timestamp "129948127853609000"
        Monday, October 15, 2012 10:13:05 PM
    WMI Style LastBootUpTime
        ConvertFrom-Timestamp -timestamp "20151006033116.494402-420"
    .PARAMETER timestamp
    Timestamp in Active Directory [int64] filetime or WMI CIM_DATETIME format
#>
    [CmdletBinding()]
	param (
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$timestamp
    )
     
	try {
        if (($timestamp.Contains(".") -and $timestamp.Length -eq 25) -or ($timestamp.Length -eq 23 -and $timestamp.Contains("-"))){
            #seems to be a WMI date
            return [Management.ManagementDateTimeConverter]::ToDateTime($timestamp)
        } else {
		    return [system.DateTime]::FromFileTimeUtc($timestamp)
        }
	}
	catch {$datestring = "ERROR"}
}

function Find-Command
{
<#
    .SYNOPSIS
    Validate a command exists in the current context
    .DESCRIPTION
    Validate a command exists in the current context
    .LINK
    .EXAMPLE
    Find-Command -Command "Get-NonExistantCommand"
    False
    Find-Command -Command "Get-Command"
    True
    .PARAMETER Command
    The command to test for availability
#>
    [CmdletBinding()]
    param (
        [Alias("CMD","Name")]
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Command
    )
    return [bool](Get-Command -Name $Command -ErrorAction SilentlyContinue)
}

function Compare-ToNumeric {
<#
    .SYNOPSIS
    Determines if a given value is numeric using regex
    .DESCRIPTION
    Determines if a given value is numeric using regex
    .LINK
    .EXAMPLE
    Compare-ToNumeric -Value "ABC"
    False
    Compare-ToNumeric -Value "123"
    True
    .PARAMETER Command
    The command to test for availability
#>
    [CmdletBinding()]
    param (
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Value
    )
    return $Value -match "^[\d\.]+$"
}

function Get-PasswordFromPassSafe{
    <#
        .SYNOPSIS
        Retrieves a password from passwordstate for our current allowed password lists
        .DESCRIPTION
        We have api access to the passwordstate tool for the Compute, Storage, and Service account lists.
        This function can retrieve the password for a specified username from a specified list
        .LINK
        https://uc1t3npmt02:9119/
        http://www.clickstudios.com.au/api/
        .EXAMPLE
        Get-PasswordFromPassSafe -Username tasks_srv -PasswordList Service
            *returned password here*
        .PARAMETER UserName
        The Username of the password to retrieve
        .PARAMETER PasswordList
        The List the Username can be found in. Currently supports the Compute, Storage, and Service Accounts lists
        .Parameter location
        filter the possible results based on the location field
        .Parameter dnsname
        Filter the possible results based on the dnsname field
        .Parameter title
        Filter the possible results based on the title field
        .Parameter history
        Retrieves the most recent password that is not the current (N-1)
    #>
    param(
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$UserName,
        [parameter(Mandatory=$true)]
        [ValidateSet("Compute","Storage","Service")]
        [string]$PasswordList,
        [parameter(Mandatory=$false)]
        [string]$location = $null,
        [parameter(Mandatory=$false)]
        [string]$dnsname = $null,
        [parameter(Mandatory=$false)]
        [string]$title = $null,
        [switch]$history
    )
    $ApiInfo = Get-ApiInfoFromDB -KeyName $PasswordList
    if ($null -eq $ApiInfo){ Write-Log -Message "Unable to retrieve API Information for Password List $PasswordList" -Level Error; return $false }

    $passwordid = Get-PasswordIDFromPassSafe -UserName $UserName -location $location -dnsname $dnsname -title $title -PasswordList $ApiInfo.Related_ID -apikey $ApiInfo.API_Key

    if ($passwordID -eq $null){Write-Log -Message "Unable to retrieve password id" -Level Error; return $false}
    if ($history){
        $url = "https://uc1t3npmt02:9119/api/passwordhistory/$passwordid`?apikey=$($ApiInfo.Api_Key)"
    } else {
        $url = "https://uc1t3npmt02:9119/api/passwords/$passwordid`?apikey=$($ApiInfo.Api_Key)"
    }
    $webclient = new-object System.Net.WebClient
    [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
    try {
        $response = $webclient.DownloadString($url)
    } catch {
        if ($history){
            $url = "https://ny1t3npmt02:9119/api/passwordhistory/$passwordid`?apikey=$($ApiInfo.Api_Key)"
        } else {
            $url = "https://ny1t3npmt02:9119/api/passwords/$passwordid`?apikey=$($ApiInfo.Api_Key)"
        }
        $response = $webclient.DownloadString($url)
    }
    if ($null -eq $response){ Write-Log -Message "Response from PasswordState was empty" -Level Error; return $false }

    return ($response | ConvertFrom-Json).Password | select -first 1
}

function Get-PasswordIDFromPassSafe{
    <#
        .SYNOPSIS
        Internal Function
        Finds the ID of a given username in a given password list from the PasswordState tool.
        .DESCRIPTION
        We have api access to the passwordstate tool for the Compute, Storage, and Service account lists.
        This function will find the ID of a username in a specified password list. This is needed to retrieve the actual
        password for this User.
        .LINK
        https://uc1t3npmt02:9119/
        http://www.clickstudios.com.au/api/
        .EXAMPLE
        GetPasswordIDFromPassSafe -UserName tasks_srv -PasswordList Service -apikey $apikey
            239
        .PARAMETER UserName
        The Username of the password to retrieve
        .PARAMETER PasswordList
        The List the Username can be found in. Currently supports the Compute, Storage, and Service Accounts lists
        .PARAMETER ApiKey
        The API Key to use to connect to PasswordState.
    #>
    param(
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$UserName,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [int]$PasswordList,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$apikey,
        [parameter(Mandatory=$false)]
        [string]$location = $null,
        [parameter(Mandatory=$false)]
        [string]$dnsname = $null,
        [parameter(Mandatory=$false)]
        [string]$title = $null
    )

    [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
    $webclient = New-Object System.Net.WebClient
    try {
        $response = $webclient.DownloadString($url)
    } catch {
        $url = "https://ny1t3npmt02:9119/api/searchpasswords/$PasswordList`?apikey=$apikey&QueryAll&ExcludePassword=true"
        $response = $webclient.DownloadString($url)
    }
    $json = $response | ConvertFrom-Json

    $objs = $json | where {$_.UserName.Contains($UserName)}
    if (-not([string]::IsNullOrEmpty($location))){
        $objs = $objs | where {$_.GenericField1 -eq $location}
    }
    if (-not([string]::IsNullOrEmpty($title))){
        $objs = $objs | where {$_.Title -eq $title}
    }
    if (-not([string]::IsNullOrEmpty($dnsname))){
        $objs = $objs | where {$_.GenericField3 -eq $dnsname}
    }

    return ($objs | select -First 1).PasswordID

}

function Get-ApiInfoFromDB {
    <#
        .SYNOPSIS
        Internal Function
        Gets the API Key and associated list ID if applicable by name from the database.
        .DESCRIPTION
        Gets the API Key and associated list ID if applicable by name from the database.
        .LINK
        https://uc1t3npmt02:9119/
        http://www.clickstudios.com.au/api/
        .EXAMPLE
        Get-ApiInfoFromDB -KeyName Compute
        .PARAMETER KeyName
        Name associated with the API Key to retrieve
    #>
    param(
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$KeyName
    )
    return Invoke-SqlStoredProcedure -server $CTLSqlServer -database ToolingData -SP_Name Get_APIKey -parameters @{"Name"=$KeyName}
}



function Write-ZendeskTicketComment {
    <#
        .SYNOPSIS
        Writes a specified comment to a specified zendesk ticket
        .DESCRIPTION
        Writes a specified comment to a specified zendesk ticket
        .LINK
        https://developer.zendesk.com/rest_api/docs/core/tickets#updating-tickets
        .EXAMPLE
        Write-ZendeskTicketComment -username "eric.sheris@ctl.io" -password "79Fwp2MpU#QrcMH" -ticketid 1096495 -comment "test comment via powershell" -status open -private
        .PARAMETER username
        your Zendesk Username
        .PARAMETER password
        your Zendesk Password
        .PARAMETER ticketid
        Ticket ID to write the comment to.
        .PARAMETER comment
        Comment to write to the ticket
        .PARAMETER status
        Ticket Status. Can be open,pending,hold,solved,closed
        .PARAMETER private
        Use if comment should be private (internally viewable only)
    #>
    param(
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$username,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$password,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$ticketid,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$comment,
        [parameter(Mandatory=$true)]
        [ValidateSet("open","pending","hold","solved","closed")]
        [string]$status,
        [switch]$private
    )
    $base_url = "https://t3n.zendesk.com/api/v2/tickets" 
    $full_url = "$base_url/$ticketid.json"
    $obj = New-Object psobject
    $commentobj = new-object psobject
    $ticketobj = new-object psobject
    $commentobj | Add-Member -MemberType NoteProperty -Name public -Value $(-not $private)
    $commentobj | Add-Member -MemberType NoteProperty -Name body -Value $comment

    $ticketobj | Add-Member -MemberType NoteProperty -Name status -Value $status
    $ticketobj | Add-Member -MemberType NoteProperty -Name comment -Value $commentobj
    $obj | Add-Member -MemberType NoteProperty -Name ticket -Value $ticketobj
    $ticketUpdate = $obj | ConvertTo-Json

    $return = Invoke-RestMethod -Method Put -Uri $full_url -Body $ticketUpdate -Headers @{Authorization='Basic ' + [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("$($username):$($password)"));} -ContentType "application/json"
    return $return.ticket
}

function ConvertTo-Scriptblocks{
  <#
      .SYNOPSIS
      Create an array of scriptblocks from an array of string commands
      .DESCRIPTION
      Create an array of scriptblocks from an array of string commands
      .EXAMPLE
      Create-Scriptblocks -commands "Invoke-SSHCommand -ServerNameOrIP server1 -username test -password test -command "dmidecode | grep serial","Invoke-SSHCommand -ServerNameOrIP server2 -username test -password test -command "dmidecode | grep serial"
      .PARAMETER commands
      Array of commands to convert to scriptblocks
  #>
  param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
    [string[]]$commands
  )

  [scriptblock[]]$scriptblocks = @()
  foreach ($command in $commands){
    $scriptblocks += [scriptblock]::Create($command)
  }
  return $scriptblocks
}

Function Invoke-ScriptBlocks{
    <#
        .SYNOPSIS
        Creates a runspace pool and lanuches each command from the scriptblocks array into it's own runspace. returns an array of the results
        .DESCRIPTION
        Creates a runspace pool and lanuches each command from the scriptblocks array into it's own runspace. returns an array of the results
        .EXAMPLE
        $results = Create-Scriptblocks -commands "gwmi win32_computersystem -computer server1","gwmi win32_computersystem -computer server2" | Invoke-ScriptBlocks
        .PARAMETER scriptblocks
        An array of scriptblocks to Execute
        .PARAMETER ThrottleLimit
        number of threads to execute simultaneously 
    #>
[CmdletBinding()]
 param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
    [ValidateNotNullOrEmpty()]
    [ScriptBlock[]]$scriptblocks,

    [Parameter(Mandatory=$false)]
    [Int]$ThrottleLimit = 5
    )
Begin
{
    $Start = Get-Date
    $RunspaceCollection = @()
    $ProgressCounter = 0
    $results = @()
    $RunspacePool = [RunspaceFactory]::CreateRunspacePool(1,$ThrottleLimit,$host)
    $RunspacePool.Open()
 }
 Process
{
    Foreach($scriptblock in $scriptblocks)
    {
        #Create a PowerShell object to run and add the command.
        $Powershell = [PowerShell]::Create().AddScript("$pshome\profile.ps1").AddScript($ScriptBlock)
        #Specify runspace to use
        $Powershell.RunspacePool = $RunspacePool
        #Create Runspace collection
        [Collections.Arraylist]$RunspaceCollection += New-Object -TypeName PSObject -Property @{
            Runspace   = $PowerShell.BeginInvoke()
            PowerShell = $PowerShell
        }
    }

    While($RunspaceCollection){
        Foreach($Runspace in $RunspaceCollection.ToArray())
        {
             If($Runspace.Runspace.IsCompleted)
             {
                $ProgressCounter++
                Write-Progress -Activity "Collecting Event Logs" -Status "Last completed: $($Runspace.Computer)" -PercentComplete (($ProgressCounter/$scriptblocks.Count)*100)
                #Display results
                $results += $Runspace.PowerShell.EndInvoke($Runspace.Runspace)
                $Runspace.PowerShell.Dispose()
                $RunspaceCollection.Remove($Runspace)
             }
        }
    }
}
End{
    $RunspacePool.Close()
    $TimeSpan = New-TimeSpan -Start $Start -End (Get-Date)

    Write-Verbose "Script elapsed time : $($TimeSpan.Hours) hours $($TimeSpan.Minutes) minutes and $($TimeSpan.Seconds) seconds"
    return $results
    }
}

function Select-FileDialog
{
    <#
        .SYNOPSIS
        Creates and opens a OpenFileDialog windows for which returns a string containing the path of the selected file
        .DESCRIPTION
        Creates and opens a OpenFileDialog windows for which returns a string containing the path of the selected file
        .EXAMPLE
        Select-FileDialog -Title "Select a CSV" -Directory $PSScriptRoot -Filter "Text files (*.txt)|*.txt"
        .PARAMETER Title
        Dialog Title in the Title bar
        .PARAMETER Directory
        Base directory the dialog opens to, consider using $PSScriptRoot if you want to open in the same directory as your script or something like $env:Home
        .PARAMETER Filter
        For each filtering option, the filter string contains a description of the filter, followed by the vertical bar (|) and the filter pattern.
        The strings for different filtering options are separated by the vertical bar.
        You can add several filter patterns to a filter by separating the file types with semicolons, for example:
            Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*
                
        See the second link in the links section for more information on filters
        .LINK
        https://msdn.microsoft.com/en-us/library/system.windows.forms.openfiledialog(v=vs.110).aspx
        https://msdn.microsoft.com/en-us/library/system.windows.forms.filedialog.filter(v=vs.110).aspx
    #>
	param(
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Title,
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Directory,
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Filter
    )
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	if ($Show -eq "OK")
	{
		return $objForm.FileName
	}
	else
	{
		return $null
	}
}

function Save-FileDialog
{
    <#
        .SYNOPSIS
        Creates and opens a SaveFileDialog windows for which returns a string containing the save path
        .DESCRIPTION
        Creates and opens a SaveFileDialog windows for which returns a string containing the save path
        .EXAMPLE
        Save-FileDialog -Title "Select a CSV" -Directory $PSScriptRoot -Filter "Text files (*.txt)|*.txt"
        .PARAMETER Title
        Dialog Title in the Title bar
        .PARAMETER Directory
        Base directory the dialog opens to, consider using $PSScriptRoot if you want to open in the same directory as your script or something like $env:Home
        .PARAMETER Filter
        For each filtering option, the filter string contains a description of the filter, followed by the vertical bar (|) and the filter pattern.
        The strings for different filtering options are separated by the vertical bar.
        You can add several filter patterns to a filter by separating the file types with semicolons, for example:
            Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*

        See the second link in the links section for more information on filters
        .LINK
        https://msdn.microsoft.com/en-us/library/system.windows.forms.savefiledialog(v=vs.110).aspx
        https://msdn.microsoft.com/en-us/library/system.windows.forms.filedialog.filter(v=vs.110).aspx
    #>
	param(
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Title,
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Directory,
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Filter
    )
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.SaveFileDialog
	$objForm.ValidateNames = $true
	$objForm.Filter = $Filter
	$objForm.InitialDirectory = $Directory
	$objForm.AddExtension = $true
	$objForm.DefaultExt = $extension
	$objForm.Title = $Title
    $objForm.ShowHelp = $true
	$Show = $objForm.ShowDialog()
	if ($Show -eq "OK")
	{
		return $objForm.FileName
	}
	else
	{
		return $null
	}
}

function Get-HelpCode {
<#
    .SYNOPSIS
        Gets the Code from Comment Based Help Example section
    .PARAMETER  Example
        Help Example section from get-help
    .EXAMPLE
        Get-HelpCode -example $exampleblock
    .INPUTS
        System.String
    .OUTPUTS
        System.String
#>
    param (
        $Example
    )
    $codeAndRemarks = (($Example | Out-String) -replace ($Example.title), '').Trim() -split "`r`n"

    $code = New-Object "System.Collections.Generic.List[string]"
    for ($i = 0; $i -lt $codeAndRemarks.Length; $i++) {
        if ($codeAndRemarks[$i] -eq 'DESCRIPTION' -and $codeAndRemarks[$i + 1] -eq '-----------') {
            break
        }
        if (1 -le $i -and $i -le 2) {
            continue
        }
        $code.Add($codeAndRemarks[$i])
    }

    $code -join "`r`n"
}

function Get-HelpRemark {
<#
    .SYNOPSIS
        Gets the Remarks from Comment Based Help Example section
    .PARAMETER  Example
        Help Example section from get-help
    .EXAMPLE
        Get-HelpRemark -example $exampleblock
    .INPUTS
        System.String
    .OUTPUTS
        System.String
#>
    param (
        $Example
    )
    $codeAndRemarks = (($Example | Out-String) -replace ($Example.title), '').Trim() -split "`r`n"

    $isSkipped = $false
    $remark = New-Object "System.Collections.Generic.List[string]"
    for ($i = 0; $i -lt $codeAndRemarks.Length; $i++) {
        if (!$isSkipped -and $codeAndRemarks[$i - 2] -ne 'DESCRIPTION' -and $codeAndRemarks[$i - 1] -ne '-----------') {
            continue
        }
        $isSkipped = $true
        $remark.Add($codeAndRemarks[$i])
    }

    $remark -join "`r`n"
}

function Write-HelpMarkdown{
<#
    .SYNOPSIS
        Gets the comment-based help and converts to GitHub Flavored Markdown.
    .PARAMETER  Name
        A command name to get comment-based help.
    .EXAMPLE
        Write-HelpMarkdown -Name Create-HelpMarkdown | out-file "Create-HelpMarkdown.md" -encoding ascii
    .INPUTS
        System.String
    .OUTPUTS
        System.String
#>
param (
    [Parameter(Mandatory = $True)]
    $Name
)

try {
    if ($Host.UI.RawUI) {
      $rawUI = $Host.UI.RawUI
      $oldSize = $rawUI.BufferSize
      $typeName = $oldSize.GetType().FullName
      $newSize = New-Object $typeName (500, $oldSize.Height)
      $rawUI.BufferSize = $newSize
    }

    $full = Get-Help $Name -Full

@"
# $($full.Name)
## SYNOPSIS
$($full.Synopsis)
## SYNTAX
``````powershell
$((($full.syntax | Out-String) -replace "`r`n", "`r`n`r`n").Trim())
``````
## DESCRIPTION
$(($full.description | Out-String).Trim())
## PARAMETERS  $([environment]::NewLine)

"@ + $(foreach ($parameter in $full.parameters.parameter) {
@"
$([environment]::NewLine)### -$($parameter.name) &lt;$($parameter.type.name)&gt;
$(($parameter.description | Out-String).Trim())
``````
$(((($parameter | Out-String).Trim() -split "`r`n")[-5..-1] | % { $_.Trim() }) -join "`r`n")
``````
"@
}) + @"  
$([environment]::NewLine)## INPUTS
$($full.inputTypes.inputType.type.name)
$([environment]::NewLine)## NOTES
$(($full.alertSet.alert | Out-String).Trim())
$([environment]::NewLine)## EXAMPLES 
"@ + $(foreach ($example in $full.examples.example) {
@"
$([environment]::NewLine)### $(($example.title -replace '-*', '').Trim())
``````powershell
$(Get-HelpCode $example)
``````
$(Get-HelpRemark $example)
"@
}) + @"
"@

} finally {
    if ($Host.UI.RawUI) {
      $rawUI = $Host.UI.RawUI
      $rawUI.BufferSize = $oldSize
    }
}
}

function Update-HelpDocumentation{
<#
    .SYNOPSIS
        Gets all commands (functions) in the module and runs Create-HelpMarkdown for each. Creates a file for each function in markdown format
    .EXAMPLE
        Update-HelpDocumentation
    .OUTPUTS
        System.String
#>
    $basepath = "$PSScriptRoot\docs"
    $commands = Get-Command -Module PlatCoreLibrary
    $baseDocPath = "https://github.com/Tier3/PlatCoreLibrary/blob/master/docs"
    $docindex = New-Item -Path $PSScriptRoot -Name "DocIndex.md" -ItemType file -Value "#Index  $([environment]::NewLine)" -Force
    foreach ($command in $commands.Name){
        $leaf = split-path (ls function:\ | where {$_.Name -eq $command}).ScriptBlock.File.ToString() -Leaf
        $leaf = $leaf.Replace("PlatCoreLibrary-","").Replace(".psm1","")
        if (-not (Test-Path -Path "$basepath\$leaf")){
            $throwaway = New-Item -Path "$basepath" -Name $leaf -ItemType Directory
        }
        "[$command]($baseDocPath/$leaf/$command.md)  " | Out-File -FilePath $docindex.FullName -Encoding ascii -Append
        Write-HelpMarkdown -Name $command | Out-File -FilePath "$basepath\$leaf\$command.md" -Encoding ascii -Force
    }
}
