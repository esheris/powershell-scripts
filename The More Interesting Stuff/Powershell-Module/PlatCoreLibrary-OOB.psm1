function Test-OOBType{
<#
 .SYNOPSIS
  Determine if a specified IP Address is an HP iLo, Dell iDrac, or supermicro bmc
  .DESCRIPTION
  Determine if a specified IP Address is an HP iLo, Dell iDrac, or supermicro bmc
  .EXAMPLE
  Test-OOBType -IPAddress 10.88.9.203
  .PARAMETER IPAddress
  IP Address of device to test
 #>
    param(
        
        [ValidateNotNullOrEmpty()]
        [string]$IPAddress
    )
    begin {
        $HPurl = "https://$IPAddress/xmldata?item=all"
        $DellUrl = "https://$IPAddress/cgi-bin/discover"
        $superURL = "http://$IPAddress/cgi/login.cgi"
        $manufacturer = $null
    }
    process {
        [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
        if (-not (Test-Connection -ComputerName $IPAddress -Quiet)){
            Write-Warning "Failed to ping $IPAddress"
            $manufacturer = $null
            continue;
        }
        $webclient = New-Object System.Net.WebClient
        try {
            $response = $webclient.DownloadString($HPurl)
            $manufacturer = "HP"
        } catch {
            #Not an HP
            try {
                $response = $webclient.DownloadString($DellUrl)
                $manufacturer = "DELL"
            } catch {
                try {
                    $response = $webclient.DownloadString($superURL)
                    $manufacturer = "SUPERMICRO"
                }
                catch {
                    $manufacturer = "UNKNOWN"
                }
            }
        }
    }
    end {
        return $manufacturer
    }
}

function Add-OOBUser{
<#
 .SYNOPSIS
  Adds a User to an out of band device
  .DESCRIPTION
  Tests an oob device to determine the type then uses appropriate commands to add a new administrative user
  .EXAMPLE
  Add-OOBUser -ipaddress $ip -username $user -password $pass -newOOBUser $newUser -newOOBPassword $newuserpass
  .PARAMETER IPAddress
  IP Address of the Out of Band device
  .PARAMETER UserName
  Existing Login Username for OOB device
  .PARAMETER Password
  Existing Login Password for OOB device
  .PARAMETER NewOOBUser
  Username of new user
  .PARAMETER NewOOBPassword
  Password for new user
 #>
    param(
        [ValidateNotNullOrEmpty()]
        [string]$IPAddress,
        [ValidateNotNullOrEmpty()]
        [string]$UserName,
        [ValidateNotNullOrEmpty()]
        [string]$Password,
        [ValidateNotNullOrEmpty()]
        [string]$NewOOBUser,
        [ValidateNotNullOrEmpty()]
        [string]$NewOOBPassword
    )

    $manufacturer = Test-OOBType -IPAddress $IPAddress

    Switch ($manufacturer){
        "HP" {
            return Add-HPILOUser -username $username -password $password -IPAddress $IPAddress -newIloUser $NewOOBUser -newIloPassword $NewOOBPassword

        }
        "DELL" {
            return Add-DellDracUser -username $username -password $password -IPAddress $IPAddress -newDracUser $NewOOBUser -newDracPassword $NewOOBPassword
        }
        "SUPERMICRO" {}
        default {}
    }
}

function Test-IloUserExists{
<#
 .SYNOPSIS
  Connects to an ILO and checks if a given user already exists
  .DESCRIPTION
  Connects to an ILO and checks if a given user already exists
  .EXAMPLE
  Test-IloUserExists -ipaddress $ip -username $user -password $pass -LookupUserName oobie
  .PARAMETER IPAddress
  IP Address of the Out of Band device
  .PARAMETER UserName
  Existing Login Username for OOB device
  .PARAMETER Password
  Existing Login Password for OOB device
  .PARAMETER LookupUserName
  Username to test for
 #>
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$IPAddress,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Username,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Password,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$LookupUserName
    )
    $results = Invoke-SSHCommand -ServerNameOrIP $IPAddress -username $Username -password $Password -command "show /map1/accounts1/$($LookupUserName.Trim())"
    if ($results.contains("status=2")){
        return $false
    } else {
        return $true
    }
}

function Add-HPILOUser{
<#
 .SYNOPSIS
  Adds a User to an HP Ilo
  .DESCRIPTION
  Tests an oob device to determine the type then uses appropriate commands to add a new administrative user
  .EXAMPLE
  Add-HPILOUser -ipaddress $ip -username $user -password $pass -newOOBUser $newUser -newOOBPassword $newuserpass
  .PARAMETER IPAddress
  IP Address of the Out of Band device
  .PARAMETER UserName
  Existing Login Username for OOB device
  .PARAMETER Password
  Existing Login Password for OOB device
  .PARAMETER NewOOBUser
  Username of new user
  .PARAMETER NewOOBPassword
  Password for new user
 #>
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$username,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$password,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$IPAddress,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$newIloUser,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$newIloPassword
    )
    if (Test-IloUserExists -Username $username -Password $password -IPAddress $ipaddress -LookupUserName $newIloUser){
        write-log -Message "Ilo User $newIloUser was found on system at $IPAddress" -Level Info
        $command = "set /map1/accounts1/$newIloUser password=$newIloPassword"
    } else {
        $command = "create /map1/accounts1 username=$newIloUser password=$newIloPassword name=oobie group=admin,config,oemhp_vm,oemhp_rc,oemhp_power"
    }
    $return = Invoke-SSHCommand -ServerNameOrIP $IPAddress -username $username -password $password -command $command

    return Test-IloUserExists -Username $username -Password $password -IPAddress $IPAddress -LookupUserName $newIloUser
}

function Add-DellDracUser{
<#
 .SYNOPSIS
  Adds a User to a Dell Drac
  .DESCRIPTION
  Tests an oob device to determine the type then uses appropriate commands to add a new administrative user
  .EXAMPLE
  Add-DellDracUser -ipaddress $ip -username $user -password $pass -newOOBUser $newUser -newOOBPassword $newuserpass
  .PARAMETER IPAddress
  IP Address of the Out of Band device
  .PARAMETER UserName
  Existing Login Username for OOB device
  .PARAMETER Password
  Existing Login Password for OOB device
  .PARAMETER NewOOBUser
  Username of new user
  .PARAMETER NewOOBPassword
  Password for new user
 #>
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$IPAddress,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$username,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$password,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$newDracUser,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$newDracPassword
    )
    $userID = Test-DellDracUserExists -Username $username -Password $password -IPAddress $ipaddress -LookupUserName $newDracUser
    if ($userID -ne $false){
        write-log -Message "Drac User $newDracUser was found on system at $IPAddress" -Level Warn
        $command = "racadm config -g cfgUserAdmin -o cfgUserAdminPassword -i $userID $newDracPassword"
        $return = Invoke-SSHCommand -ServerNameOrIP $IPAddress -username $username -password $password -command $command
        if ($return.contains("successfully")){
            return $true
        }
        return $false
    } else {
        $emptyID = Get-FirstEmptyDracUser -Username $username -Password $password -IPAddress $ipaddress
        if ($emptyID -eq $null){Write-Log -Message "Unable to find empty user index on $IPAddress" -Level Error}
        $commands = @()
        $commands += "racadm config -g cfgUserAdmin -o cfgUserAdminUserName -i $emptyID $newDracUser"
        $commands += "racadm config -g cfgUserAdmin -o cfgUserAdminPassword -i $emptyID $newDracPassword"
        $commands += "racadm config -g cfgUserAdmin -o cfgUserAdminPrivilege -i $emptyID 0x1ff"
        $commands += "racadm config -g cfgUserAdmin -o cfgUserAdminEnable -i $emptyID 1"

        $revertCommands = @()
        $revertCommands += "racadm config -g cfgUserAdmin -o cfgUserAdminUserName -i $emptyID `"`""
        $revertCommands += "racadm config -g cfgUserAdmin -o cfgUserAdminPassword -i $emptyID `"`""
        $revertCommands += "racadm config -g cfgUserAdmin -o cfgUserAdminPrivilege -i $emptyID 0x0"
        $revertCommands += "racadm config -g cfgUserAdmin -o cfgUserAdminEnable -i $emptyID 0"
        $sshclient = New-SSHConnection -ServerNameOrIP $IPAddress -username $username -password $password
        foreach ($command in $commands){
            $result = Invoke-SSHCommand -sshClient $sshclient -command $command
            if ($result -eq "Object value modified successfully"){
                continue
            } else {
                Write-Log -Message "Error running command: '$command' attempting Revert" -Level Warn
                $index = $commands.IndexOf($command)
                for ($i = $index;$i -gt 0; $i--){
                    $throwaway = Invoke-SSHCommand -sshClient $sshclient -command $revertCommands[$i]
                }
            }
        }
    }
    Remove-SSHConnection -Client $sshclient
}

function Test-DellDracUserExists{
<#
 .SYNOPSIS
  Connects to a Drac and checks if a given user already exists
  .DESCRIPTION
  Connects to a Drac and checks if a given user already exists
  .EXAMPLE
  Test-DellDracUserExists -ipaddress $ip -username $user -password $pass -LookupUserName oobie
  .PARAMETER IPAddress
  IP Address of the Out of Band device
  .PARAMETER UserName
  Existing Login Username for OOB device
  .PARAMETER Password
  Existing Login Password for OOB device
  .PARAMETER LookupUserName
  Username to test for
 #>
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Username,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Password,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$IPAddress,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$LookupUserName
    )
    $sshclient = New-SSHConnection -ServerNameOrIP $IPAddress -username $Username -password $Password
    if ($sshclient -eq $null){ Write-Log -Message "Error creating ssh connection" -Level Error; return $null }
    foreach ($i in (1..16)){
        $command = "racadm getconfig -g cfgUserAdmin -i $i"
        $return = Invoke-SSHCommand -sshClient $sshclient -command $command
        if ($return.ToLower().contains("cfguseradminusername=$($LookupUserName.ToLower())")){
            Remove-SSHConnection -Client $sshclient
            return $i
        }
    }
    Remove-SSHConnection -Client $sshclient
    return $false
}

function Get-FirstEmptyDracUser{
<#
 .SYNOPSIS
  Returns the first empty user slot on a Dell Drac
  .DESCRIPTION
  Returns the first empty user slot on a Dell Drac
  .EXAMPLE
  Get-FirstEmptyDracUser -ipaddress $ip -username $user -password $pass
  .PARAMETER IPAddress
  IP Address of the Out of Band device
  .PARAMETER UserName
  Existing Login Username for OOB device
  .PARAMETER Password
  Existing Login Password for OOB device
  .PARAMETER LookupUserName
  Username to test for
 #>
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Username,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Password,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$IPAddress
    )
    $sshclient = New-SSHConnection -ServerNameOrIP $IPAddress -username $Username -password $Password
    foreach ($i in (2..16)){
        # dell doesn't allow changing the user at index 1 on some firmware version so may as well not check it.
        $command = "racadm getconfig -g cfgUserAdmin -i $i"
        $return = Invoke-SSHCommand -sshClient $sshclient -command $command
        $lines = $return.split("`r`n")
        foreach ($line in $lines){
            if ($line.ToLower().Trim() -eq "cfguseradminusername="){
                Remove-SSHConnection -Client $sshclient
                return $i
            }
        }
    }
    Remove-SSHConnection -Client $sshclient
    return $null
}

function Restart-ServerViaOOB{
<#
 .SYNOPSIS
  Restarts a server via its out of band
  .DESCRIPTION
  Tests the provided IP Address and issues the correct reboot command based on the type of out of band device (iLo,Drac,etc)
  .EXAMPLE
  Restart-ServerViaOOB -ipaddress $ip -username $user -password $pass
  .PARAMETER IPAddress
  IP Address of the Out of Band device
  .PARAMETER UserName
  Existing Login Username for OOB device
  .PARAMETER Password
  Existing Login Password for OOB device
  .PARAMETER LookupUserName
  Username to test for
 #>
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$IPAddress,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$UserName,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Password
    )
    if (-not(Test-Connection -ComputerName $IPAddress -Quiet)){
        Write-Log -Message "Unable to connect to $IPAddress" -Level Warn
        return $false
    }
    $manufacturer = Test-OOBType -IPAddress $IPAddress
    $command = [String]::Empty
    switch ($manufacturer.ToUpper()){
        "HP" {
            $command = "power reset"
            break;
        }
        "DELL" {
            $command = "racadm serveraction hardreset"
            break;
        }
        "SUPERMICRO"{
            $command = [string]::Empty
            break;
        }
        default {
            $command = [string]::Empty
            break;
        }
    }
    if ($command -eq [string]::Empty){
        Write-Log -Message "Unable to determine OOB type for $IPAddress" -Level Warn
        return $false
    }
    $result = Invoke-SSHCommand -ServerNameOrIP $IPAddress -username $UserName -password $Password -command $command
    if ($result.ToLower().contains("success")){
        return $true
    }
    return $false
}

function Set-OOBSNMP{
<#
 .SYNOPSIS
  Sets the SNMP community string for specified OOB device
  .DESCRIPTION
  Tests the provided IP Address and issues the correct command to set the SNMP community string
  .EXAMPLE
  Set-OOBSNMP -ipaddress $ip -username $user -password $pass -CommunityString public
  .PARAMETER IPAddress
  IP Address of the Out of Band device
  .PARAMETER UserName
  Existing Login Username for OOB device
  .PARAMETER Password
  Existing Login Password for OOB device
  .PARAMETER CommunityString
  Community String to set
 #>
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$IPAddress,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$UserName,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Password,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$CommunityString
    )
    $manufacturer = Test-OOBType -IPAddress $IPAddress
    $command = [String]::Empty
    switch ($manufacturer.ToUpper()){
        "HP" {
            $command = "set /map1/snmp1 readcom1=`"$CommunityString`""
            break;
        }
        "DELL" {
            $command = "racadm config -g cfgOobSnmp -o cfgOobSnmpAgentCommunity $CommunityString"
            break;
        }
        "SUPERMICRO"{
            $command = [string]::Empty
            break;
        }
        default {
            $command = [string]::Empty
            break;
        }
    }
    if ($command -eq [string]::Empty){
        Write-Log -Message "Unable to determine OOB type for $IPAddress" -Level Warn
        return $false
    }
    $result = Invoke-SSHCommand -ServerNameOrIP $IPAddress -username $UserName -password $Password -command $command
    if ($result.ToLower().contains("success")){
        return $true
    }
    return $false
}

function Start-DellDiagnotics{
<#
 .SYNOPSIS
  Issues commands to a Drac to reboot and start running diagnostics, then monitors the diagnostics
  run to completion and issues the command to export the results to a share
  .DESCRIPTION
  Issues commands to a Drac to reboot and start running diagnostics, then monitors the diagnostics
  run to completion and issues the command to export the results to a share
  .EXAMPLE
  Restart-ServerViaOOB -ipaddress $ip -username $user -password $pass
  IP Address of the Out of Band device
  .PARAMETER UserName
  Existing Login Username for OOB device
  .PARAMETER Password
  Existing Login Password for OOB device
  .PARAMETER CommunityString
  Community String to set
 #>
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$IPAddress,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$UserName,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Password,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$sharepath
    )

    if ($sharepath.Contains("\")){$sharepath = $sharepath.Replace("\","/")}
    $runcommand = "racadm diagnostics run -m 0 -r forced -s TIME_NOW -e TIME_NA"
    $exportCommand = "racadm diagnostics export -f $($ipaddress)_diagnostics -l $sharepath"
    $result = Invoke-SSHCommand -ServerNameOrIP $IPAddress -username $UserName -password $Password -command $command
}

function Register-HPRemoteSupport {
<#
  .SYNOPSIS
  uses the ILO Api and RibCL to add a HP server to HP Remote Support  
  .EXAMPLE
  Register-HPRemoteSupport -iloIP 10.0.10.20 -iloUser oobie -iloPass <oobie password>
  .PARAMETER iloIP
  IP or DNS name of the ILO to register
  .PARAMETER ilouser
  Existing Login Username for ILO device
  .PARAMETER iloPass
  Existing Login Password for ILO device
 #>
    param(
        [Parameter(Mandatory = $true)]
        [string]$iloIP,
        [Parameter(Mandatory = $true)]
        [string]$ilouser,
        [Parameter(Mandatory = $true)]
        [string]$ilopass
    )
    $passportuser = "infraeng@ctl.io"
    $passportpass = Get-PasswordFromPassSafe -UserName "infraeng@ctl.io" -PasswordList Compute
$RegServerXML = @"
<RIBCL VERSION="2.23">
    <LOGIN USER_LOGIN="__ilouser__" PASSWORD="__ilopass__">
        <RIB_INFO MODE="write">
            <SET_ERS_DIRECT_CONNECT>
               <ERS_HPP_USER_ID value="__passportuser__"/>
               <ERS_HPP_PASSWORD value="__passportpass__"/>
           </SET_ERS_DIRECT_CONNECT>
        </RIB_INFO>
    </LOGIN>
</RIBCL>
"@.Replace("__ilouser__",$ilouser).Replace("__ilopass__",$ilopass).Replace("__passportuser__",$passportuser).Replace("__passportpass__",$passportpass)

$RegCompleteXML = @"
<RIBCL VERSION="2.23">
    <LOGIN USER_LOGIN="__ilouser__" PASSWORD="__ilopass__">
        <RIB_INFO MODE="write">
            <DC_REGISTRATION_COMPLETE/>
        </RIB_INFO>
    </LOGIN>
</RIBCL>
"@.Replace("__ilouser__",$ilouser).Replace("__ilopass__",$ilopass)

    if(-not(Test-HPRemoteSupportEnabled -iloIP $iloIP -ilouser $ilouser -ilopass $ilopass)){
        $regResponse = Execute-HPRIBCL -iloip $iloIP -ribcl $RegServerXML
        if ($response -eq $null){ write-log -Message "Error Executing RIBCL against $iloIP" -Level Warn; return $false}
        $statuses = $regResponse.split("`r`n") | Select-String "STATUS"
        foreach ($status in $statuses){
            if ($status.Trim() -ne 'STATUS="0x0000"'){
                write-log -Message "Error Registering $iloip for HP Remote Support. Please verify internet connectivity and try again" -Level Warn
                return $false
            }
        }
        $completeResponse = [string]$webclient.UploadString($iloURL, "POST", $RegCompleteXML)
        $completeStatuses = $completeResponse.split("`r`n") | Select-String "STATUS"
        foreach ($status in $completeStatuses){
            if ($status.Trim() -ne 'STATUS="0x0000"'){
                write-log -Message "Error Completing registration for $iloip for HP Remote Support. Please verify internet connectivity and try again" -Level Warn
                return $false
            }
        }
        return $true
    }
}

function Invoke-HPRIBCL{
<#
  .SYNOPSIS
  Invokes commands against an ILO using RIBCL
  .Description
  Invokes commands against an ILO using RIBCL. RIBCL is an XML style language the ILO parses and executes
  .LINKS
  http://h20564.www2.hpe.com/hpsc/doc/public/display?docId=c03334058
  .EXAMPLE
  Invoke-HPRIBCL -iloIP 10.0.10.20 -riblc $ribcl
  .PARAMETER iloIP
  IP or DNS name of the ILO to register
  .PARAMETER ribcl
  RIBCL XML as a string
  .NOTES
  ribcl is best created using a here-string
 #>
  param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [string]$iloIP,
    [Parameter(Mandatory=$true)]
    [string]$ribcl
  )
  $iloURL = "https://$iloIP/ribcl"
  [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
  $webclient = New-Object System.Net.WebClient
  $response = [string]$webclient.UploadString($iloURL, "POST", $RegServerXML)
  return $response
}

function Test-HPRemoteSupportEnabled {
    param(
        [Parameter(Mandatory = $true)]
        [string]$iloIP,
        [Parameter(Mandatory = $true)]
        [string]$ilouser,
        [Parameter(Mandatory = $true)]
        [string]$ilopass
    )

$GetRegStatus = @"
<RIBCL VERSION="2.23">
    <LOGIN USER_LOGIN="__ilouser__" PASSWORD="__ilopass__">
        <RIB_INFO MODE="read">
            <GET_ERS_SETTINGS />
        </RIB_INFO>
    </LOGIN>
</RIBCL>
"@.Replace("__ilouser__",$ilouser).Replace("__ilopass__",$ilopass)

    $iloURL = "https://$iloIP/ribcl"
    [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
    $webclient = New-Object System.Net.WebClient
    $response = $webclient.UploadString($iloURL, "POST", $GetRegStatus)
    if ($response.Contains("<ERS_STATE VALUE=`"1`"/>")){
        return $true
    }
    return $false
}

function Get-HPAHSReport{
<#
  .SYNOPSIS
  Generates an HP AHS Report from an ILO and downloads it to the specified savepath
  .LINKS
  https://github.com/HewlettPackard/python-proliant-sdk/wiki/iLO-4-REST-API-Data-Model-(HpiLOActiveHealthSystem-Resource-Type)
  .EXAMPLE
  Get-HPAHSReport -ipaddress 10.0.10.20 -username oobie -password <oobiepassword> -all -savepath "c:\users\eric\desktop\hpahsreport"
  
  Retrieves the entire report from the ILO. Warning, May Be large
  .EXAMPLE
  Get-HPAHSReport -ipaddress 10.0.10.20 -username oobie -password <oobiepassword> -startdate "1/14/2016" -enddate "12/30/2015" -savepath "c:\users\eric\desktop\hpahsreport"

  Retrieves the report from between the given dates
  .EXAMPLE
  Get-HPAHSReport -ipaddress 10.0.10.20 -username oobie -password <oobiepassword> -savepath "c:\users\eric\desktop\hpahsreport"

  If no date range is given and -all is not used, the HP AHS report will be generated for the last 7 days
  .PARAMETER IpAddress
  IP or DNS name of the ILO to register
  .PARAMETER Username
  Ilo Username
  .PARAMETER Password
  ILO Password
  .PARAMETER ALL
  Switch Paramter. If -all is used, no start date or end date will be accepted
  .PARAMETER startdate
  Date on one side of the range. Defaults to the current date
  .PARAMETER enddate
  Date on the other side of the required range. Defaults to 7 days ago
 #>
    [CmdletBinding(DefaultParameterSetName="Date")]
	param (
            [Parameter(ParameterSetName="All",Mandatory=$true)]
            [Parameter(ParameterSetName="Date")]
			[ValidateNotNullOrEmpty()]
			[string] $Ipaddress,
            [Parameter(ParameterSetName="All",Mandatory=$true)]
            [Parameter(ParameterSetName="Date")]
			[ValidateNotNullOrEmpty()]
			[string] $Username,
            [Parameter(ParameterSetName="All",Mandatory=$true)]
            [Parameter(ParameterSetName="Date")]
			[ValidateNotNullOrEmpty()]
			[string] $Password,
            [Parameter(ParameterSetName="All",Mandatory=$false)]
            [switch]$All,
            [Parameter(ParameterSetName="Date",Mandatory=$false)]
            [datetime]$startdate = (get-date),
            [Parameter(ParameterSetName="Date",Mandatory=$false)]
            [datetime]$enddate = (get-date).adddays(-7),
            [Parameter(ParameterSetName="All",Mandatory=$true)]
            [Parameter(ParameterSetName="Date")]
			[ValidateNotNullOrEmpty()]
			[string] $SavePath
		)

		#
		# Form  the Microsoft path to save the AHS report.
		#

		$ahsRptOutputPth = $SavePath.ToLower().Trim()
		$ahsRptOutputPth = $ahsRptOutputPth  -replace "\\$"

		if(!(Test-Path -Path $ahsRptOutputPth )){
			New-Item -ItemType directory -Path $ahsRptOutputPth
		}

		# Header information returned from the iLO
		$iLOHeader = @{}

		$bsCredCmdLn = "$($Username.Trim()):$($Password.Trim())"

		# Encode the string to UTF8 which is form accepted by the ToBase64String funciton
		$bytesCmLn = [System.Text.Encoding]::UTF8.GetBytes($bsCredCmdLn)
		# Encode the string to the RFC2045-MIME variant of Base64, except not limited to 76 char/line.
		$bs64CrdCmdLn = [System.Convert]::ToBase64String($bytesCmLn)
		# Create the Auth value as the method, a space, and then the encoded pair Method Base64String
		$basicAuthValueCmdLn = "Basic $bs64CrdCmdLn"
		# Create the header Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
		$headersCmdLn = @{ Authorization = $basicAuthValueCmdLn }
		# Token Authentication
		$BodyCmdLn = @{
			UserName=$Username
			Password=$Password
		}
		# Invoke-WebRequest and Invoke-RestMethod require the credentials to be in
		# Json Format.
		$json = $BodyCmdLn | ConvertTo-Json
		#
		# With the WEB request acquire the iLO header information that will contain the formed X-Auth-Token
		# based on the json converted username and password.
		#
		$url1 = "https://$ipaddress/rest/v1/Sessions"
		#
		# To prevent --> The underlying connection was closed: Could not establish trust relationship for the SSL/TLS secure channel.
		#
		$clbckstate = [System.Net.ServicePointManager]::ServerCertificateValidationCallback
		[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

		Try {
			$lg1 = Invoke-WebRequest -Uri $url1 -ContentType "application/json" -Body $json -Method Post
			[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$clbckstate}
		}
		Catch
		{
			Write-Host "Caught a system exception while attempting to access $url1"
			write-host " "
			$_.Exception.Response
			write-host " "
			$_.Exception
			write-host " "
			[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$clbckstate}
			return
		}

		#
		# With the iLo header information form a hash table of the returned data.

		#
		# Split on carriage returns and line feeds.
		#

		$lg1Ary = $lg1.RawContent.split("`n|`r")

		Try {
			foreach ($item in $lg1Ary){
				#
				# As the script splits on :, need to replace : with ~ on any lines with https: or http:
				#
				$item = $item.ToLower() -replace "https:","https~"
				$item = $item.ToLower() -replace "http:","http~"
				#
				# Replace any remaining carriage returns and line feeds with a blank
				#
				$itemTrim = $item -replace '`n|`r',''
				#
				# split on "=:
				#
				$aryEntry = $itemTrim.Split(":")
				$aryEntry[0] = $aryEntry[0].Trim()
    	$iLOHeader[$aryEntry[0]] = $aryEntry[1]
		}
	}
	Catch
	{
			$_.Exception.Response
			write-host " "
			$_.Exception
			write-host " "
			[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$clbckstate}
			return
		}

		#
		# The URI used to create this connection is stored in "location" and this is need to close the connection.
		#
		# During the process of converting this iLO head information to a hash table, the : was used to split the data.
		# To prevent the split on https: or http:, the colon for these were replaced with ~
		# Need to put the : back
		#
		$iLOHeader["Location"] = $iLOHeader["Location"].ToLower() -replace "https~","https:"
		$iLOHeader["Location"] = $iLOHeader["Location"].ToLower() -replace "http~","http:"
		#
		# The contents of the location index of the hash table is the session ID need for the delete
		# session call.
		#
		$logouturl = $iLOHeader["Location"]

		# The https://${ipaddress}/rest/v1/Systems/1 location of the iLO
		# includes the serial number which this scripts uses to form the file name.
		#
		$hwurl = "https://${ipaddress}/rest/v1/Systems/1"
		$header = @{}
		#
		# Extract the "X-Auth-Token" from the iLOs web header information retured from the invoke-webrequest
		#
		$header.Add("X-Auth-Token", $iLOHeader["X-Auth-Token"])

		Try{
			$hwsn = Invoke-RestMethod -Uri $hwurl -Method Get -Headers $header -ContentType "application/json"
		}
		Catch [system.exception]
		{
			Write-Host "Caught a system exception while attempting to access $hwurl"
			write-host " "
			$_.Exception.Response
			write-host " "
			$_.Exception
			write-host " "
			$nodelogout = Invoke-RestMethod -Uri $logouturl -Method DELETE -Headers $header -ContentType "application/json"
			[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$clbckstate}
			return
		}

		#
		# Extract the "iLOs" from the serial number RestMethod call header information
		#
		$today = get-date -Format yyyy-MM-dd
		$serial = $hwsn.SerialNumber.trim()
		#
		# Logout the session using the Token Login
		#
		Try{
			$nodelogout = Invoke-RestMethod -Uri $logouturl -Method DELETE -Headers $header -ContentType "application/json"
			[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$clbckstate}
		}
		Catch [system.exception]
		{
			Write-Host "Caught a system exception while attempting to access $logouturl"
			write-host " "
			write-host " "
			$_.Exception.Response
			write-host " "
			$_.Exception
			write-host " "
			[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$clbckstate}
			return
		}
		#
		# Use Basic Authentication to retrieve the AHS logs
		#
    	$headerBsc = @{}
    	$headerBsc =  $headersCmdLn
			#
			# Form the  Start and End date to search for AHS report
			#
			

			#
			# Form the URL to acquire the AHS report
			#
			
            if ($PSCmdlet.ParameterSetName -eq "All"){
                $AHSUrl = "https://${ipaddress}/ahsdata/HP.ahs?downloadAll=1"
                $destination = $ahsRptOutputPth+"\HP_${serial}.ahs"
            } else {
                $today = get-date -date $startdate -Format yyyy-MM-dd
			    $destination = $ahsRptOutputPth+"\HP_${serial}_${today}.ahs"
                $xdaysago= "{0:yyyy-MM-dd}" -f (get-date -date $enddate)
			    $AHSUrl = "https://${ipaddress}/ahsdata/HP.ahs?from=${xdaysago}&to=${today}"
            }

 		Write-Host " -----------------------------"
 	Write-Host " "
 	Write-Host "AHS report being download to:        "   $destination
 	Write-Host " "
 	Write-Host " -----------------------------"


 	Try{
				$AHSDump = Invoke-WebRequest -Uri $AHSUrl -Method Get -Headers $($headerBsc) -ContentType "application/json" -UseBasicParsing -OutFile $destination
				[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$clbckstate}
			}
 		Catch
		{
			Write-Host "Caught a system exception while attempting to access $AHSUrl"
			write-host " "
			write-host " "
			$_.Exception.Response
			write-host " "
			$_.Exception
			write-host " "
			$nodelogout = Invoke-RestMethod -Uri $logouturl -Method DELETE -Headers $header -ContentType "application/json"
			[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$clbckstate}
			return
		}

		return $destination
}

function Update-HPIloFirmware {
    Param(
        [Parameter(Mandatory=$true)]
        [string]$IloIp,   
        [Parameter(Mandatory=$false)]
        [string]$FirmwarePath = "http://wa1t3nesiis02.t3n.dom/ilo4_240.bin"
    )
    $servertype = Test-OOBType -IPAddress $iloIP
    if ($servertype -ne "HP"){
        Write-Log -Message "$iloip appears to be a $servertype. Skipping..." -level Warn
        return
    }
    $username = "oobie"
    $password = (Get-PasswordFromPassSafe -UserName $username -PasswordList Compute)
    Invoke-SSHCommand -ServerNameOrIp $IloIp -username $username -password $password -command "load /map1/firmware1 -source $firmwarePath"   
}

# Below here strings are for reference when i talk to tinman team. will remove later
#$successregresponse = @"
#PS C:\Users\eric.sheris>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#"@
#
#$regfailure = @"
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x4008"
#    MESSAGE='Client Receive Error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#"@
#
#
#$getRegStatus = @"
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#<GET_ERS_SETTINGS>
#    <ERS_STATE VALUE="1"/>
#    <ERS_CONNECT_MODEL VALUE="1"/>
#    <ERS_HPP_USERNAME VALUE="infraeng@ctl.io"/>
#    <ERS_REGISTRATION_COMPLETE VALUE="1"/>
#    <ERS_DESTINATION_URL VALUE="services.isee.hp.com"/>
#    <ERS_DESTINATION_PORT VALUE="443"/>
#    <ERS_WEB_PROXY_URL VALUE=""/>
#    <ERS_WEB_PROXY_PORT VALUE="0"/>
#    <ERS_WEB_PROXY_USERNAME VALUE=""/>
#    <ERS_LAST_TRANSMISSION_TYPE VALUE="3"/>
#    <ERS_LAST_TRANSMISSION_DATE VALUE="2015-11-16T23:19:05"/>
#    <ERS_LAST_TRANSMISSION_ERRNO VALUE="No Error"/>
#    <ERS_COLLECTION_FREQUENCY VALUE="P30D"/>
#</GET_ERS_SETTINGS>
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#<?xml version="1.0"?>
#<RIBCL VERSION="2.23">
#<RESPONSE
#    STATUS="0x0000"
#    MESSAGE='No error'
#     />
#</RIBCL>
#"@
