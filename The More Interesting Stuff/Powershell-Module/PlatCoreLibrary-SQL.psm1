﻿function Get-SqlData {
<#
  .Synopsis
    Returns tabular data for an ad-hoc Select
  .Description
    Returns tabular data for an ad-hoc Select
  .Parameter server
    SQL server to connect to
  .Parameter query
    SQL query to run
  .Parameter database
    Database to run the query against
  .Parameter user
    Username to log into the database with
  .Parameter password
    Password to log into the database with
#>
    Param (
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$server,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$query,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$database,
        [parameter(Mandatory=$false)]
        [string]$user,
        [parameter(Mandatory=$false)]
        [string]$password
    )

    if ($query.Trim().ToUpper().StartsWith("SELECT")){
	    $connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
        if ([string]::IsNullOrEmpty($user)){
        $connString.psbase.IntegratedSecurity = $true
        } else {
	    $connString.psbase.UserID = $user
	    $connString.psbase.Password = $password
	    $connString.psbase.IntegratedSecurity = $false
        }
	    $connString.psbase.DataSource = $Server
	    $connString.psbase.InitialCatalog = $database

	    $conn = New-Object System.Data.SqlClient.SqlConnection($connString)
        $conn.Open()
        $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
        $sqlCmd.CommandText = $query
        $sqlCmd.Connection = $conn
        $table = New-Object System.Data.DataTable
	    $table.load($sqlCmd.ExecuteReader())
	    $conn.Close()
	    return $table
    } else {
        Write-Log -Message "Query '$query' does not appear to be a select query. For Insert,Update, or Delete queries please use New-SqlData"
        return $null
    }
}

function New-SqlData {
<#
  .Synopsis
    Executes Insert, Update and Delete Queries
  .Description
    Executes Insert, Update and Delete Queries
  .Parameter server
    SQL server to connect to
  .Parameter query
    SQL query to run
  .Parameter database
    Database to run the query against
  .Parameter user
    Username to log into the database with
  .Parameter password
    Password to log into the database with
#>
    Param (
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$server,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$query,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$database,
        [parameter(Mandatory=$false)]
        [string]$user,
        [parameter(Mandatory=$false)]
        [string]$password
    )
    begin {
        if ($query.Trim().ToUpper().StartsWith("SELECT")){
            Write-log -Message "'$query' appears to be a select query, please use Get-SqlData" -level Warn
            return $null
        }
        $connString = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
        if ([string]::IsNullOrEmpty($user)){
        $connString.psbase.IntegratedSecurity = $true
        } else {
	    $connString.psbase.UserID = $user
	    $connString.psbase.Password = $password
	    $connString.psbase.IntegratedSecurity = $false
        }
        $connString.psbase.DataSource = $Server
        $connString.psbase.InitialCatalog = $database
    }
    process {
        $conn = New-Object System.Data.SqlClient.SqlConnection($connString)
        $conn.Open()
        $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
        $sqlCmd.CommandText = $query
        $sqlCmd.Connection = $conn
        $result = $sqlCmd.ExecuteNonQuery()
    }
    end {
        $result
    }
}

function Invoke-SqlStoredProcedure {
<#
    .Synopsis
      Executes the specified stored proceedure
    .Description
      Executes the specified stored proceedure
    .Parameter server
      SQL server to connect to
    .Parameter SP_Name
      Name of stored proceedure to execute
    .Parameter database
      Database to run the query against
    .Parameter user
      Username to log into the database with
    .Parameter password
      Password to log into the database with
  #>
    [CmdletBinding(DefaultParameterSetName="Default")]
    Param (
        [parameter(ParameterSetName="Default",Mandatory=$true)]
        [parameter(ParameterSetName="UserName")]
        [ValidateNotNullOrEmpty()]
        [string]$server,
        [parameter(ParameterSetName="Default",Mandatory=$true)]
        [parameter(ParameterSetName="UserName")]
        [ValidateNotNullOrEmpty()]
        [string]$database,
        [parameter(ParameterSetName="Default",Mandatory=$true)]
        [parameter(ParameterSetName="UserName")]
        [ValidateNotNullOrEmpty()]
        [string]$SP_Name,
        [parameter(ParameterSetName="Default",Mandatory=$false)]
        [parameter(ParameterSetName="UserName")]
        [HashTable]$parameters = $null,
        [parameter(ParameterSetName="UserName")]
        [string]$UserName,
        [parameter(ParameterSetName="UserName")]
        [string]$Password
    )
    begin {
        $SqlConnection = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
        $SqlConnection.psbase.IntegratedSecurity = $true
        if ($PSCmdlet.ParameterSetName -eq "UserName"){
            $SqlConnection.psbase.IntegratedSecurity = $false
            $SqlConnection.psbase.UserID = $UserName
            $SqlConnection.psbase.Password = $Password
        }
        $SqlConnection.psbase.DataSource = $server
        $SqlConnection.psbase.InitialCatalog = $database
        $SqlCmd = New-Object System.Data.SqlClient.SqlCommand
        $SqlCmd.CommandText = $SP_Name
        $SqlCmd.CommandType = [System.Data.CommandType]::StoredProcedure
        $SqlCmd.Connection = New-Object System.Data.SqlClient.SqlConnection($SqlConnection)
        if ($parameters -ne $null){
            foreach ($param in $parameters.GetEnumerator()){
                $SqlCmd.Parameters.AddWithValue("@$($param.Key)",$($param.Value)) | Out-Null
            }
        }
    }
    process {
        $SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
        $SqlAdapter.SelectCommand = $SqlCmd
        $DataSet = New-Object System.Data.DataSet
        $fillret = $SqlAdapter.Fill($DataSet)
    }
    end {
        $SqlCmd.Connection.Close()
        return $DataSet.Tables[0]
    }
}