﻿function New-SSHConnection{
<#
  .Synopsis
    Opens an SSH connection to the specified IP Address
  .Description
    Opens an SSH Connection to a specified IP Address. Useful if running multiple commands against the same machine
    as the connection stays open until closed
  .Parameter ServerNameOrIP
    SQL server to connect to
  .Parameter username
    Username to log in with
  .Parameter password
    Password to log in with
  .Example
    $sshClient = New-SSHConnection -ServerNameOrIP 10.10.0.20 -username root -password calvin
#>
    param(
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$ServerNameOrIP,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$username,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$password
    )
    
    try{
        $sshClient = New-Object Renci.SshNet.SshClient($ServerNameOrIP,$username,$password)
        $sshClient.Connect()
        if ($sshClient.IsConnected){
            $global:sshClient = $sshClient
            return $sshClient
        } else {
            Write-Log -message "Unable to connect to $ServerNameOrIP" -Level Warn
            return $null
        }
    } catch {
        Write-Log -message "Unable to connect to $ServerNameOrIP" -Level Warn
        return $null
    }
}

function Invoke-SSHCommand{
<#
  .Synopsis
    Executes commands against remote machines via SSH
  .Description
    If provided an IP address, this command connects via ssh, executes the command, and disconnects.
    If provided an existing SSHClient, the existing ssh connection is used to execute the command and the
    connection is NOT closed upon completion
  .Parameter ServerNameOrIP
    SQL server to connect to
  .Parameter username
    Username to log in with
  .Parameter password
    Password to log in with
  .Parameter sshClient
    An ssh client generated using New-SSHConnection
  .Parameter command
    Command to execute
  .Example
    Invoke-SSHCommand -ServerNameOrIP 10.10.0.20 -username root -password calvin -command "racadm getnicinfo"
  .Example
    $sshclient = New-SshConnection -ServerNameOrIP 10.10.0.20 -username root -password calvin
    $nicinfo = Invoke-SSHCommand -sshClient $sshclient -command "racadm getnicinfo"
    $sysinfo = Invoke-SSHCommand -sshClient $sshclient -command "racadm getsysinfo"
    Remove-SshConnection -sshClient $sshclient
#>
    param(
        [parameter(ParameterSetName="Username",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$ServerNameOrIP,
        [parameter(ParameterSetName="Username",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$username,
        [parameter(ParameterSetName="Username",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$password,
        [parameter(ParameterSetName="Client",Mandatory=$true,ValueFromPipeline=$true)]
        [ValidateNotNullOrEmpty()]
        [Renci.SshNet.SshClient]$sshClient,
        [parameter(ParameterSetName="Client",Mandatory=$true)]
        [parameter(ParameterSetName="Username",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$command
    )
    begin {
        if ($PSCmdlet.ParameterSetName -eq "Username"){
        try{
            $sshClient = New-SSHConnection -ServerNameOrIP $ServerNameOrIP -username $username -password $password
            if ($sshClient -eq $null){ continue }
            } catch {
                write-log -message "Unable to connect using .net, trying Putty" -level Warn
                Invoke-Expression -Command "echo y | $psscriptroot\lib\plink.exe $ServerNameOrIP -l $username -pw $password exit" | out-null
                Invoke-Expression -Command "$psscriptroot\lib\plink.exe $ServerNameOrIP -l $username -pw $password $command"
                continue
            }
        }
    }
    process{
        $sshCommand = $sshClient.RunCommand($command)
        if ($sshCommand.ExitStatus -eq 0){
            $retval = $sshCommand.Result
        } else {
            Write-Error "$($sshCommand.Error)"
            $retval = $null
        }
    }
    end {
        if ($pscmdlet.ParameterSetName -eq "Username"){
            $sshClient.Disconnect()
            $sshClient.Dispose()
        }
        return $retval
    }
}

function Remove-SSHConnection{
<#
  .Synopsis
    Disconnects an existing ssh connection
  .Description
    Disconnects an existing ssh connection
  .Parameter sshClient
     ssh client from New-SSHConnection to clean up
#>
    param (
        [Parameter(Position=0,Mandatory=$true,ValueFromPipeline=$true)]
        [Renci.SshNet.SshClient]$sshClient
    )
    Try {
        $Hostname = $sshClient.ConnectionInfo.Host
        $Global:sshClient = $null
    }
    Catch {
        Throw "Error deconnecting. $($_.Exception.Message)"
    }
    Finally {
        $Client.Disconnect()
        $Client.Dispose()
    }
}
