﻿$CTLSqlServer = "wa1t3nessql01.t3n.dom";
function Get-CLCServer{
<#
     .SYNOPSIS
    Retrieves detailed server information from the asset database including location, warranty and vcenter information
    .EXAMPLE
    Get-CLCServer -Hostname wa1t3nesx02
    .EXAMPLE
    Get-CLCServer Serial 5PVG1T1
    .EXAMPLE
    Get-CLCServer -OOBIP 10.0.10.20
    .PARAMETER Hostname
    Hostname of the machine to look up
    .PARAMETER Serial
    Serial Number of the machine to look up
    .PARAMETER OOBIP
    Out of band (ILO or Drac) IP of the machine to look up
    .PARAMETER ID
    Row ID of machine to look up
    .NOTES
    Currently the only machines in the database are Physical ESX servers.
 #>
    [CmdletBinding()]
    param(
        [parameter(ParameterSetName="HostName",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $HostName,
        [parameter(ParameterSetName="Serial",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $Serial,
        [parameter(ParameterSetName="OOBIP",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $OOBIP,
        [parameter(ParameterSetName="ID",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $ID
    )
    
    switch ($PSCmdlet.ParameterSetName){
        "ID" {
            return Invoke-SqlStoredProcedure -server $CTLSqlServer -database "ToolingData" -SP_Name "Get_ServerByID" -parameters @{"ID"=$ID}
        }
        "HostName"{
            return Invoke-SqlStoredProcedure -server $CTLSqlServer -database "ToolingData" -SP_Name "Get_ServerByHostName" -parameters @{"HostName"=$HostName}
        }
        "Serial" {
            return Invoke-SqlStoredProcedure -server $CTLSqlServer -database "ToolingData" -SP_Name "Get_ServerBySerial" -parameters @{"Serial"=$Serial}
        }
        "OOBIP" {
            return Invoke-SqlStoredProcedure -server $CTLSqlServer -database "ToolingData" -SP_Name "Get_ServerByOOBIP" -parameters @{"OOBIP"=$OOBIP}
        }
    }
}

function Get-CLCServers{
<#
     .SYNOPSIS
    Retrieves detailed server information from the asset database including location, warranty and vcenter information for servers matching the criteria
    .EXAMPLE
    Get-CLSCservers
    Returns all servers in the database
    .EXAMPLE
    Get-CLCServers -Datacenter VA1
    Returns all servers in the database with a datacenter of VA1
    .EXAMPLE
    Get-CLCServers -Cluster VA1CLSA
    Returns all servers in the database listed as belonging to the specified cluster
    .EXAMPLE
    Get-CLCServers -Manufacturer DELL
    Returns all servers in the database listed as Dell
    .EXAMPLE
    Get-CLCServers -vCenter wa1t3nvc02
    Returns all servers in the database listed as belonging to the specified vcenter
    .PARAMETER Datacenter
    Datacenter to filter by
    .PARAMETER Cluster
    vCenter Cluster to filter by
    .PARAMETER Manufacturer
    Manufacturer to filter by
    vCenter to filter by
    .NOTES
    Currently the only machines in the database are Physical ESX servers.
 #>
    [CmdletBinding(DefaultParameterSetName="None")]
    param(
        [parameter(ParameterSetName="Datacenter",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $Datacenter,
        [parameter(ParameterSetName="Cluster",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $Cluster,
        [parameter(ParameterSetName="Manufacturer",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $Manufacturer,
        [parameter(ParameterSetName="vCenter",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $vCenter
    )
    switch ($PSCmdlet.ParameterSetName){
        "Datacenter" {
            return Invoke-SqlStoredProcedure -server $CTLSqlServer -database "ToolingData" -SP_Name "Get_ServersByDatacenter" -parameters @{"Datacenter"=$Datacenter}
        }
        "Cluster"{
            return Invoke-SqlStoredProcedure -server $CTLSqlServer -database "ToolingData" -SP_Name "Get_ServersByCluster" -parameters @{"Cluster"=$Cluster}
        }
        "Manufacturer" {
            return Invoke-SqlStoredProcedure -server $CTLSqlServer -database "ToolingData" -SP_Name "Get_ServersByManufacturer" -parameters @{"Manufacturer"=$Manufacturer}
        }
        "vCenter" {
            return Invoke-SqlStoredProcedure -server $CTLSqlServer -database "ToolingData" -SP_Name "Get_ServersByvCenter" -parameters @{"vCenter"=$vCenter}
        }
        default {
            return Invoke-SqlStoredProcedure -server $CTLSqlServer -database "ToolingData" -SP_Name "Get_Servers"
        }
    }
}

function Get-WarrantyInformation{
<#
  .SYNOPSIS
  Gathers warranty information for HP or Dell servers
  .DESCRIPTION
  Gathers warranty information for HP or Dell servers
  .EXAMPLE
  Get-WarrantyInformation -SerialNumber H7XG6X1
  .EXAMPLE
  Get-WarrantyInformation -SerialNumber MXQ351053C -ProductID "654081-B21" -Model "ProLiant DL360p Gen8"
  .PARAMETER SerialNumber
  The Server Serial Number
  .PARAMETER ProductID
  HP Servers only - The Product ID from the server
  .PARAMETER Model
  HP Servers only - The Server Model (e.g. Proliant DL360p Gen8, Proliant DL360 G7
#>
    [CmdletBinding()]
    param(
        [parameter(ParameterSetName="Dell",Mandatory=$true)]
        [parameter(ParameterSetName="HP",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $SerialNumber,
        [parameter(ParameterSetName="HP",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $ProductID,
        [parameter(ParameterSetName="HP",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $Model
    )
    begin{
        $SerialNumber = $SerialNumber.Trim()
        $baseURL = "http://wa1t3nesiis02.t3n.dom/api/Warranty"
    }
    process {
        switch ($PSCmdlet.ParameterSetName){ 
            "Dell" {
                $url = "$baseURL/$SerialNumber/"
                try {
                    $warrantyinfo = Invoke-WebRequest -Method Get -Uri $url -ErrorAction Stop
                    break;
                } catch {
                    Write-Log -Message "Error retrieving warranty information for $serialnumber ($url)" -Level Error
                    $warrantyinfo = $null
                }
            }
            "HP" {
                $ProductID = $ProductID.Trim()
                $Model = $Model.Trim().Replace(" ","%20")
                $url = "$baseUrl/$SerialNumber/$ProductID/$Model/"
                try {
                    $warrantyinfo = Invoke-WebRequest -Method Get -Uri $url -ErrorAction Stop
                    break;
                } catch {
                    Write-Log -Message "Error retrieving warranty information for $SerialNumber - $productid - $model ($url)" -Level Error
                    $warrantyinfo = $null
                }
            }
        }
    }
    end {
        if ($null -eq $warrantyinfo){ return $null }
        return $warrantyinfo | ConvertFrom-Json
    }
}

function Set-MonitoringStatus{
<#
  .SYNOPSIS
  Puts a server into maintenance mode or removes it from maintenance mode
  .EXAMPLE 
  Set a device in maintenance mode

  Set-MonitoringStatus -DeviceName UC1-VC-C02.t3n.dom-EVENT -Datacenter UC1 -Suppress -Reason "Example"
  .EXAMPLE 
  Set a device back to production monitoring

  Set-MonitoringStatus -DeviceName UC1-VC-C02.t3n.dom-EVENT -Datacenter UC1 -Monitor -Reason "Maintenance Complete
  .PARAMETER DeviceName
  Name of the device in ZenOSS
  .PARAMETER Datacenter
  Datacenter device is located in
  .PARAMETER Suppress
  Switch Parameter. Suppresses the device
  .PARAMETER Monitor
  Switch Parameter. Places the device back into production monitoring
  .PARAMETER reason
  Reason for device state change
#>
[CmdletBinding()]
    param(
        [parameter(ParameterSetName="Suppress",Mandatory=$true)]
        [parameter(ParameterSetName="Monitor",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$DeviceName,
        [parameter(ParameterSetName="Suppress",Mandatory=$true)]
        [parameter(ParameterSetName="Monitor",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Datacenter,
        [parameter(ParameterSetName="Suppress",Mandatory=$true)]
        [switch]$Suppress,
        [parameter(ParameterSetName="Monitor",Mandatory=$true)]
        [switch]$Monitor,
        [parameter(ParameterSetName="Suppress",Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$reason
    )
    switch ($PSCmdlet.ParameterSetName){
        "Suppress"{
            $state = "300"
            break
        }
        "Monitor"{
            $state = "1000"
            break
        }
    }
    $device = Get-MonitoringStatus -DeviceName $DeviceName -Datacenter $Datacenter

    if ([string]::IsNullOrEmpty($device.Name)){
        Write-Log -Message "$DeviceName could not be found in $Datacenter in ZenOSS" -Level Warn
        return $false
    }
    
    if ($device.ProductionState -eq $state){
        Write-Log -Message "Device is already set to requested state" -level Info
        return $true
    }
    
    $base_url = "http://10.128.131.39:8080/api"
    $url = "$base_url/devices/$datacenter/$($device.Name)/setProdState/$state`?reason=$reason"

    $result = Invoke-RestMethod -Method Put -Uri $url
    if (($result | where {$_.step -eq "updateZNodes"}).code -eq 200){
        $device = Get-MonitoringStatus -DataCenter $Datacenter -DeviceName $($device.Name)
        if ($Monitor.IsPresent -and ($Device.ProductionState -eq "1000")){ return $true }
        if ($Suppress.IsPresent -and ($Device.ProductionState -eq "300")) { return $true }
        return $false
    } else {
        Write-Log -Message "Failed Setting device in maintenance mode" -Level Warn
        return $false
    }
}

function Get-MonitoringStatus{
<#
  .SYNOPSIS
  Gets the current status of a device in ZenOSS
  .EXAMPLE 
  Get Data for specific device

  Get-MonitoringStatus -Datacenter UC1 -DeviceName UC1-VC-C02.t3n.dom-EVENT
  .EXAMPLE 
  Get data for all devices in the datacenter

  Get-MonitoringStatus -Datacenter UC1
  .PARAMETER Datacenter
  Datacenter device is located in
  .PARAMETER DeviceName
  Name of the device in ZenOSS
#>
[CmdletBinding()]
    param(
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$DataCenter,
        [parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        [string]$DeviceName
    )
    $base_url = "http://10.128.131.39:8080/api"
    if ([string]::IsNullOrEmpty($DeviceName)){
        $url = "$base_url/devices/$Datacenter"
    } else {
        $url = "$base_url/devices/$Datacenter/$DeviceName"
    }

    $device = Invoke-RestMethod -Method Get -Uri $url
    if ([string]::IsNullOrEmpty($device.mapping)){
        Write-Log -Message "$DeviceName could not be found in $Datacenter in ZenOSS" -Level Warn
        return $null
    }

    return New-Object psobject -Property @{
        Name = $device.Name
        ProductionState = $device.mapping.productionState
        zNode = $device.mapping.znode
    }
}