﻿function Connect-SPOSite {
 <#
     .SYNOPSIS
    Generates Sharepoint Credentials and connects to sharepoint
    .EXAMPLE
    Connect-SPOSite
    .EXAMPLE
    Connect-SPOSite -url "https://ctl243.sharepoint.com"
    .PARAMETER Url
    Url of the sharepoint site to connect to. Defaults to our sharepoint (https://ctl243.sharepoint.com)
 #>
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$false, ValueFromPipeline=$true, Position=0)]
        $Url = "https://ctl243.sharepoint.com"
    )

    begin {

    }
    process {
        if ($global:spoCred -eq $null) {
            $cred = Get-Credential -Message "Enter your credentials for SharePoint Online:"
            $global:spoCred = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($cred.UserName, $cred.Password)
        }
        $ctx = New-Object Microsoft.SharePoint.Client.ClientContext $Url
        $ctx.Credentials = $spoCred

        if (!$ctx.ServerObjectIsNull.Value) { 
            Write-Host "Connected to site: '$Url'" -ForegroundColor Green
        }
        return $ctx
    }
    end {
    }
}

function New-ChangeRequest{
 <#
     .SYNOPSIS
    Creates a new Change Request on Sharepoint
    .EXAMPLE
    New-ChangeRequest -list $list -title $rfc.Title -changeType $rfc.ChangeType -startDate $rfc.ScheduledStart -endDate $rfc.ScheduledEnd -RequesterID $requesterID -AssigneeID $AssigneeID -ChangeArea $rfc.ChangeArea -Datacenters $rfc.Datacenters -Emergency $rfc.Emergency -CSAT $rfc.CSATnotify -Security $rfc.SecurityNotify `
    -Platform $rfc.Platform -PlatformCore $rfc.PlatformCore -Networking $rfc.Networking -impactType $rfc.ImpactType -QTE $rfc.QTE -reasonForChange $rfc.Reason -AffectedInfra $rfc.AffectedInfra -ExpectedImpact $rfc.ExpectedImpact -Risks $rfc.Risks -DetailedSteps $rfc.DetailedSteps -TestPlan $rfc.TestPlan `
    -RollbackPlan $rfc.RollbackPlan -Validation $rfc.Validation -supporters $rfc.Supporters -Notes $rfc.Notes -approverid $ApproverID
    .NOTES
    Please use the RFCTemplate.csv file in the lib directory of the location of the PlatCoreLibrary module
 #>
    param(
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        $list,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$title,
        [Parameter(Mandatory=$true)]
        [validateset("Normal","Standard","Baseline","Control")]
        [string]$changeType,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$startDate,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$endDate,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$AssigneeID,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$RequesterID,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$ChangeArea,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string[]]$Datacenters,
        [Parameter(Mandatory=$false)]
        [string]$Emergency,
        [Parameter(Mandatory=$false)]
        [string]$CSAT,
        [Parameter(Mandatory=$false)]
        [string]$Security,
        [Parameter(Mandatory=$false)]
        [string]$Platform,
        [Parameter(Mandatory=$false)]
        [string]$PlatformCore,
        [Parameter(Mandatory=$false)]
        [string]$Networking,
        [Parameter(Mandatory=$true)]
        [validateset("Non-Disruptive","Distruptive","Potentially Disruptive")]
        [string]$impactType,
        [Parameter(Mandatory=$false)]
        [string]$QTE,
        [Parameter(Mandatory=$true)]
        [String]$reasonForChange,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$AffectedInfra,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$ExpectedImpact,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$Risks,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$DetailedSteps,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$TestPlan,
        [Parameter(Mandatory=$false)]
        [string]$RollbackPlan,
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [string]$Validation,
        [Parameter(Mandatory=$false)]
        [string]$supporters,
        [Parameter(Mandatory=$false)]
        [string]$Notes,
        [Parameter(Mandatory=$false)]
        [validatenotnullorempty()]
        [int]$approverid

    )

    if ([string]::IsNullOrEmpty($Emergency)){$Emergency = "False"}
    if ([string]::IsNullOrEmpty($QTE)){$QTE = "False"}
    if ([string]::IsNullOrEmpty($CSAT)){$CSAT = "False"}
    if ([string]::IsNullOrEmpty($Security)){$Security = "False"}
    if ([string]::IsNullOrEmpty($Platform)){$Platform = "False"}
    if ([string]::IsNullOrEmpty($PlatformCore)){$PlatformCore = "False"}
    if ([string]::IsNullOrEmpty($Networking)){$Networking = "False"}

    $listItemCreationInfo = New-Object Microsoft.SharePoint.Client.ListItemCreationInformation
    $newItem = $list.AddItem($listItemCreationInfo);
    $newItem["Title"] = $rfc.Title.Trim()
    $newItem["Change_x0020_Type"] = (Get-Culture).TextInfo.ToTitleCase($changeType.Trim())
    $newItem["Scheduled_x0020_Start"] = $startDate
    $newItem["Scheduled_x0020_End"] = $endDate
    $newItem["Assignee"] = $AssigneeID
    $newItem["Requester"] = $RequesterID
    $newItem["ChangeArea"] = @($ChangeArea.Split(",") | foreach {(Get-Culture).TextInfo.ToTitleCase($_.Trim())})
    $newItem["Datacenters"] = @($Datacenters.Split(",")  | foreach {$_.Trim().ToUpper()})
    $newItem["Qualified_x0020_Testing_x0020_En"] = (Get-Culture).TextInfo.ToTitleCase($QTE.Trim())
    $newItem["Emergency"] = (Get-Culture).TextInfo.ToTitleCase($Emergency.Trim())
    $newItem["CSAT"] = (Get-Culture).TextInfo.ToTitleCase($CSAT.Trim())
    $newItem["Security"] = (Get-Culture).TextInfo.ToTitleCase($Security.Trim())
    $newItem["Platform"] = (Get-Culture).TextInfo.ToTitleCase($Platform.Trim())
    $newItem["Platform_x002d_Core"] = (Get-Culture).TextInfo.ToTitleCase($PlatformCore.Trim())
    $newItem["Networking"] = (Get-Culture).TextInfo.ToTitleCase($Networking.Trim())
    
    $newItem["Change_x0020_Impact_x0020_Type"] = (Get-Culture).TextInfo.ToTitleCase($impactType.Trim())
    $newItem["Reason_x0020_for_x0020_Change"] = $ReasonForChange.Trim()
    $newItem["Affected_x0020_Infrastructure"] = $AffectedInfra.Trim()
    $newItem["Expected_x0020_Customer_x0020_Im"] = $ExpectedImpact.Trim()
    $newItem["Risks"] = $Risks.Trim()
    $newItem["Detailed_x0020_Steps"] = $DetailedSteps.Trim()
    $newItem["Change_x0020_Test_x0020_Plan"] = $TestPlan.Trim()
    $newItem["Rollback_x0020_Plan"] = $RollbackPlan.Trim()
    $newItem["Validation_x0020_Criteria"] = $Validation.Trim()
    if (-not [string]::IsNullOrEmpty($supporters)){
        $newItem["Supporters"] = $Supporters.Trim()
    }
    $newItem["Notes"] = $Notes
    if (-not [string]::IsNullOrEmpty($approverid)){
        $newItem["Approver"] = $approverID
    }
    
    $newItem.Update()
    $context.ExecuteQuery()
    $x
}

function Confirm-ChangeRequests{
 <#
     .SYNOPSIS
    Validation function. Used to validate change requests prior to calling New-ChangeRequest
    .PARAMETER ChangeRequests
    An array of change request data from import-csv
    .NOTES
    Please use the RFCTemplate.csv file in the lib directory of the location of the PlatCoreLibrary module
 #>
    param(
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        $ChangeRequests
    )
    $validChangeTypes = "normal","standard","baseline","control"
    $validChangeAreas = "compute","network","other","storage"
    $validDatacenters = "CA1","CA2","CA3","DE1","DE2","GB1","GB3","IL1","IL2","LB1","LB2","LB3","NE1","NJ1","NY1","PD1","QA1","QA2","QA3","SG1","UC1","UT1","VA1","WA1"
    $validImpactTypes = "non-disruptive","potentially disruptive","disruptive"
    $allerrors = @()
    for ($i=0;$i -lt $ChangeRequests.Length; $i++){
        $change = $ChangeRequests[$i]
        $startDate = (get-date -Date $change.ScheduledStart.Trim())
        $endDate = (get-date -Date $change.ScheduledEnd.Trim())
        $localTimeZone = [System.TimeZoneInfo]::Local
        if ($localTimeZone.ID -ne "Pacific Standard Time"){
            $pst = [System.TimeZoneInfo]::FindSystemTimeZoneById("Pacific Standard Time")
            $startDate = [System.TimeZoneInfo]::ConvertTime($startDate,$pst,$localTimeZone)
            $endDate = [System.TimeZoneInfo]::ConvertTime($endDate,$pst,$localTimeZone)
        }
        $rowErrors = @()
        try {
            $sd = get-date -Date $startDate -ErrorAction Stop
            if ($sd -lt (get-date)){
                $rowErrors += "Start Date is prior to NOW"
            }
        } catch {
            $rowErrors += "Start Date format is invalid. Expected format is MM/DD/YYYY hh:mm AM/PM."
        }
        try {
            $ed = get-date -Date $endDate -ErrorAction Stop
            if ($ed -lt (get-date)){
                $rowErrors += "End Date is prior to NOW"
            }
        } catch {
            $rowErrors += "End Date format is invalid. Expected format is MM/DD/YYYY hh:mm AM/PM."
        }
        if ((get-date -date $startDate) -gt (get-date -date $endDate)){
            $rowErrors += "Start Date is later then End Date"
        }
        if ($change.ChangeType.ToLower() -notin $validChangeTypes){
            $rowErrors += "ChangeType - $($change.ChangeType.ToLower()) is invalid"
        }
        $badchangeArea = Compare-Object -ReferenceObject $change.ChangeArea.trim().split(",") -DifferenceObject $validChangeAreas | where {$_.SideIndicator -eq "<="}
        if (($badchangeArea.count -ne 0)){
            $rowErrors += "ChangeArea - $($badchangeArea -join ',') are invalid"
        }
        $baddatacenters = Compare-Object -ReferenceObject $change.Datacenters.trim().split(",") -DifferenceObject $validDatacenters | where {$_.SideIndicator -eq "<="}
        if (($baddatacenters.count -ne 0)){
            $rowErrors += "Datacenters - $($baddatacenters -join ',') are invalid"
        }
        if ($change.ImpactType.ToLower() -notin $validImpactTypes){
            $rowErrors += "ImpactType - $($change.ImpactType.ToLower()) is invalid"
        }
        if ($rowErrors.Count -gt 0){
            $allerrors += "CSV Row $($i + 2) contains errors in the following fields: $($rowErrors -join ',')"
        }
        $rowErrors = $null
    }
    if ($allerrors.Count -eq 0){ return $true }
    $allerrors | foreach { Write-Log -Message $_ -Level Warn }
    return $false
}

function Import-ChangeRequests{
 <#
     .SYNOPSIS
    Wrapper function for validating and creating change requests. Takes a path to a CSV or prompts if no path is provided. Validates all rows of the CSV prior to beginning import. Returns information on any errors found
    .PARAMETER csvPath
    Path to the CSV containing the RFC information. If not provided a select file dialog will appear to browse for the file.d
    .EXAMPLE
    Import-ChangeRequests -csvPath c:\users\eric\desktop\rfcs.csv
    Imports the CSV
    .EXAMPLE
    Import-ChangeRequests
    Will display a select file dialog to browse for the file
    .NOTES
    Please use the RFCTemplate.csv file in the lib directory of the location of the PlatCoreLibrary module
 #>
    param(
        [Parameter(Mandatory = $false)]
        [string]$csvPath
    )
    if ([string]::IsNullOrEmpty($csvpath)){
        $csvpath = Select-FileDialog -Title "Change Request CSV" -Directory $PSScriptRoot -Filter "CSV Files (*.csv)|*.csv"
    }
    if ([string]::IsNullOrEmpty($csvpath)){
        return "No file was selected"
    }

    [array]$csvRfcs = Import-Csv -Path $csvpath
    if ($csvRfcs.Count -lt 1){ return "No data was successfully imported for parsing" }

    if (Confirm-ChangeRequests -ChangeRequests $csvRfcs){
        $context = Connect-SPOSite
        
        $list = $context.Web.Lists.GetByTitle("Change Control Library")
        $users = $context.Web.SiteUsers
        $context.Load($users)
        $context.ExecuteQuery()
        $userlist = $users | foreach { New-Object psobject -Property @{ ID = $_.ID; Email = $_.Email } }

        foreach ($rfc in $csvRfcs){
            $requesterID = ($userlist | where {$_.Email.tolower() -eq $($rfc.Requester)} | select -first 1).ID
            $AssigneeID = ($userlist | where {$_.Email.tolower() -eq $($rfc.Assignee)} | select -first 1).ID
            if (-not [string]::IsNullOrEmpty($rfc.Approver)){
                $ApproverID = ($userlist | where {$_.Email.tolower() -eq $($rfc.Approver)} | select -first 1).ID
            }
            $startDate = (get-date -Date $rfc.ScheduledStart.Trim())
            $endDate = (get-date -Date $rfc.ScheduledEnd.Trim())
            $localTimeZone = [System.TimeZoneInfo]::Local
            if ($localTimeZone.ID -ne "Pacific Standard Time"){
                $pst = [System.TimeZoneInfo]::FindSystemTimeZoneById("Pacific Standard Time")
                $startDate = [System.TimeZoneInfo]::ConvertTime($startDate,$pst,$localTimeZone)
                $endDate = [System.TimeZoneInfo]::ConvertTime($endDate,$pst,$localTimeZone)
            }
            New-ChangeRequest -list $list -title $rfc.Title -changeType $rfc.ChangeType -startDate $startDate.ToString("s") -endDate $endDate.ToString("s") -RequesterID $requesterID -AssigneeID $AssigneeID -ChangeArea $rfc.ChangeArea -Datacenters $rfc.Datacenters -Emergency $rfc.Emergency -CSAT $rfc.CSATnotify -Security $rfc.SecurityNotify `
            -Platform $rfc.PlatformNotify -PlatformCore $rfc.PlatCoreNotify -Networking $rfc.NetworkNotify -impactType $rfc.ImpactType -QTE $rfc.QTE -reasonForChange $rfc.Reason -AffectedInfra $rfc.AffectedInfra -ExpectedImpact $rfc.ExpectedImpact -Risks $rfc.Risks -DetailedSteps $rfc.DetailedSteps -TestPlan $rfc.TestPlan `
            -RollbackPlan $rfc.RollbackPlan -Validation $rfc.Validation -supporters $rfc.Supporters -Notes $rfc.Notes -approverid $ApproverID
        }
    }
}

function Approve-ChangeRequest{
 <#
     .SYNOPSIS
    Approves a change request
    .PARAMETER ChangeRequestID
    ID of the change request from sharepoint. This is in the link to the specific rfc.
    .PARAMETER ApproverEmail
    Email Address of person who is approving. If not provided, the person running Approve-ChangeRequest will be used
    .EXAMPLE
    Approve-ChangeRequest -ChangeRequestID 1490
    Approves the change request using whatever credentials were used to log into sharepoint
    .EXAMPLE
    Approve-ChangeRequest -ChangeRequestID 1490 -ApproverEmail eric.sheris@ctl.io
    Approves the change request as the specified user
    .NOTES
    Please use the RFCTemplate.csv file in the lib directory of the location of the PlatCoreLibrary module
 #>
    param(
        [Parameter(Mandatory=$true)]
        [validatenotnullorempty()]
        [int]$ChangeRequestID,
        [Parameter(Mandatory=$false)]
        [string]$ApproverEmail = [string]::Empty
    )
    $context = Connect-SPOSite
    $list = $context.Web.Lists.GetByTitle("Change Control Library")
    $users = $context.Web.SiteUsers
    $context.Load($users)
    $context.ExecuteQuery()
    $userlist = $users | foreach { New-Object psobject -Property @{ ID = $_.ID; Email = $_.Email } }
    $listItem = $list.GetItemById($ChangeRequestID)
    if ($ApproverEmail -eq [string]::Empty){
        $ApproverID = ($userlist | where {$_.Email.tolower() -eq $($context.Credentials.UserName)} | select -first 1).ID
    } else {
        $ApproverID = ($userlist | where {$_.Email.tolower() -eq $($ApproverEmail)} | select -first 1).ID
    }
    $listItem["Approver"] = $approverID
    $listItem.Update()
    $context.ExecuteQuery()
    
}
