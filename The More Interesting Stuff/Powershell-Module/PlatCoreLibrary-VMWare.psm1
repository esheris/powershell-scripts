﻿function Enter-VMHostMaintenanceMode
{
<#
 .SYNOPSIS
  Place a VMHost in Maintenance Mode in vCenter
  .DESCRIPTION
  Place a VMHost in Maintenance Mode in vCenter
  .EXAMPLE
  Enter-VMHostMaintenanceMode -vCenter wa1t3nvcn02.t3n.dom -vmhost wa1t3nesx01.t3n.dom -maintenanceNote "Bad Ram"
  .PARAMETER vCenter
  Name of the vCenter the host is attached to
  .PARAMETER vmhost
  Name of the vmHost to place in maintenance mode
  .PARAMETER maintenanceNote
  Note to set in the Maint-Note field
 #>
	[CmdletBinding()]
    param(
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$vCenter,
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$vmhost,
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
		[string]$maintenanceNote
    )
    if (-not $vCenter.Trim().EndsWith(".t3n.dom")){ $vCenter = "$vCenter.t3n.dom" }
    Connect-VIServer -Server $vcenter | out-null
    if (-not $vmhost.Trim().EndsWith(".t3n.dom")){ $vmhost = "$vmhost.t3n.dom" }
    $host = Get-VMHost -Name $vmhost
    $host | Set-VMHost -State Maintenance
	$host | Set-Annotation -CustomAttribute "Maint-Note" -value $maintenanceNote
    Disconnect-VIServer -Server $vcenter -confirm:$false
}

function Remove-VMHostMaintenanceMode
{
<#
 .SYNOPSIS
  Remove a VMHost in Maintenance Mode in vCenter
  .DESCRIPTION
  Remove a VMHost in Maintenance Mode in vCenter
  .EXAMPLE
  Remove-VMHostMaintenanceMode -vCenter wa1t3nvcn02.t3n.dom -vmhost wa1t3nesx01.t3n.dom
  .PARAMETER vCenter
  Name of the vCenter the host is attached to
  .PARAMETER vmhost
  Name of the vmHost to remove in maintenance mode
 #>
	[CmdletBinding()]
    param(
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$vcenter,
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$vmhost
    )
    if (-not $vCenter.trim().EndsWith(".t3n.dom")){ $vCenter = "$vCenter.t3n.dom" }
    Connect-VIServer -Server $vcenter | out-null
    if (-not $vmhost.trim().EndsWith(".t3n.dom")){ $vmhost = "$vmhost.t3n.dom" }
    $myhost = Get-VMHost -Name $vmhost
    $myhost | Set-VMHost -State Connected
	$myhost | Set-Annotation -CustomAttribute "Maint-Note" -value ""
    Disconnect-VIServer -Server $vcenter -confirm:$false
}

function Join-Domain {
<#
 .SYNOPSIS
  Used by build scripts to handle joining a vCenter to the domain
 #>
param(
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [pscredential]$DomainCredential,
    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [pscredential]$LocalCredential,
    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$VM,
   	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$DomainName,
    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [string]$VmType
    )
    # Vars
    $CurrentVmIp = (get-vm $vm).Guest.IPAddress | where {$_ -notmatch "::"}
    if ($CurrentVmIp -eq $Null) {
        write-host "VM is not powered on or does not have an IP"
        exit 1
    }
    $JoinedDomain = "False"
    $I = 0

   while ($JoinedDomain -eq "False"){
        # Test domain credential
        try {          
            $ClearDrive = Remove-PSDrive -name Test -ErrorAction SilentlyContinue
    	    $Drive = New-PSDrive -Credential $domaincredential -PSProvider FileSystem -name Test -root "\\$($CurrentVmIp)\c$" -ErrorAction stop
            write-host "We have verified domain authentication"
            $JoinedDomain = "True"
        } catch {
            if ($i -gt 9){
                write-host "Aborting after 10 attempts to join domain"
                break
                }
             # Join Domain
            write "Attempting to join $($Vm.name) to $($DomainName) domain"
            $Command = {Add-Computer -DomainName $using:DomainName -OUPath "OU=$Using:VmType Servers,OU=Servers,DC=t3n,DC=dom" -Credential $using:DomainCredential}
            Invoke-Command -ScriptBlock $Command -Credential $LocalCredential -ComputerName $CurrentVmIp -ErrorAction Continue
    
            # Restart the VM
            stop-clcvm -vm $Vm
            $StartVM = Start-VM -vm $Vm
            $WaitTools = wait-tools -vm $Vm
            $I++

        } 
    }    
}

function Get-VMHostWSManInstance {
<#
 .SYNOPSIS
  Retrieves data from a WSMan Instance
  .DESCRIPTION
  Retrieves data from a WSMan instance
  .EXAMPLE
  Get-VMHostWSManInstance -VMHost $vmh -class OMC_IPMIIPProtocolEndpoint -ignoreCertFailures -erroraction 'silentlycontinue'|Select IPv4Address, MACAddress, SerialNumber
  .PARAMETER VMHost
  Name of the VM Host to retrieve data from
  .PARAMETER class
  WSMan Class to read from
  .PARAMETER ignoreCertFailures
  [switch] parameter to ignore cert failures
  .PARAMETER credential
  [pscredential] object to use for authentication
 #>
    #
    param (
    [Parameter(Mandatory=$TRUE,HelpMessage="VMHosts to probe")]
    [VMware.VimAutomation.Client20.VMHostImpl[]]
    $VMHost,
 
    [Parameter(Mandatory=$TRUE,HelpMessage="Class Name")]
    [ValidateNotNullOrEmpty()]
    [string]
    $class,
 
    [switch]
    $ignoreCertFailures,
 
    [System.Management.Automation.PSCredential]
    $credential=$null
    )
 
    $omcBase = "http://schema.omc-project.org/wbem/wscim/1/cim-schema/2/"
    $dmtfBase = "http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/"
    $vmwareBase = "http://schemas.vmware.com/wbem/wscim/1/cim-schema/2/"
 
    if ($ignoreCertFailures) {
            $option = New-WSManSessionOption -SkipCACheck -SkipCNCheck -SkipRevocationCheck
    } else {
            $option = New-WSManSessionOption
    }
    foreach ($H in $VMHost) {
        if ($credential -eq $null) {
                $hView = $H | Get-View -property Value
                $ticket = $hView.AcquireCimServicesTicket()
                $password = convertto-securestring $ticket.SessionId -asplaintext -force
                $credential = new-object -typename System.Management.Automation.PSCredential -argumentlist $ticket.SessionId, $password
        }
        $uri = "https`://" + $h.Name + "/wsman"
        if ($class -cmatch "^CIM") {
                $baseUrl = $dmtfBase
        } elseif ($class -cmatch "^OMC") {
                $baseUrl = $omcBase
        } elseif ($class -cmatch "^VMware") {
                $baseUrl = $vmwareBase
        } else {
                throw "Unrecognized class"
        }
        Get-WSManInstance -Authentication basic -ConnectionURI $uri -Credential $credential -Enumerate -Port 443 -UseSSL -SessionOption $option -ResourceURI "$baseUrl/$class"
    }
}

Function Enable-MemHotAdd{
<#
 .SYNOPSIS
  Enables Memory Hot Add for a specified VM
  .DESCRIPTION
  Enables Memory Hot Add for a specified VM
  .EXAMPLE
  Enable-MemHotAdd -vm "VirtualMachineName"
  .PARAMETER VM
  Name of the VM to enable memory hot add for
 #>
    param(
        [Parameter(Mandatory=$FALSE,HelpMessage="vCenter VM is located in")]
        [string]$vCenter,
        [Parameter(Mandatory=$TRUE,Position=0,HelpMessage="VM to enable memory hot add for")]
        [ValidateNotNullOrEmpty()]
        [string]$vm
    )
    if (![String]::isnullorempty($vCenter)){
	Connect-VIServer -Server $vcenter | out-null 
	}
    $vmview = Get-vm $vm | Get-View 
    $vmConfigSpec = New-Object VMware.Vim.VirtualMachineConfigSpec

    $extra = New-Object VMware.Vim.optionvalue
    $extra.Key="mem.hotadd"
    $extra.Value="true"
    $vmConfigSpec.extraconfig += $extra

    $vmview.ReconfigVM($vmConfigSpec)
    if (![String]::isnullorempty($vCenter)){
	Disconnect-VIServer -Server $vcenter -confirm:$false -Force
	}
}

Function Enable-vCpuHotAdd{
<#
 .SYNOPSIS
  Enables CPU Hot Add for a specified VM
  .DESCRIPTION
  Enables CPU Hot Add for a specified VM
  .EXAMPLE
  Enable-vCpuHotAdd -vm "VirtualMachineName"
  .PARAMETER VM
  Name of the VM to enable memory hot add for
 #>
    param(
        [Parameter(Mandatory=$False,HelpMessage="vCenter VM is located in")]
        [string]$vCenter,
        [Parameter(Mandatory=$TRUE,Position=0,HelpMessage="VM to enable CPU hot add for")]
        [ValidateNotNullOrEmpty()]
        [string]$vm
    )
    
	if (![String]::isnullorempty($vCenter)){
	Connect-VIServer -Server $vcenter | out-null 
	}
	
	$vmview = Get-vm $vm | Get-View 
    $vmConfigSpec = New-Object VMware.Vim.VirtualMachineConfigSpec

    $extra = New-Object VMware.Vim.optionvalue
    $extra.Key="vcpu.hotadd"
    $extra.Value="true"
    $vmConfigSpec.extraconfig += $extra

    $vmview.ReconfigVM($vmConfigSpec)
    if (![String]::isnullorempty($vCenter)){
	Disconnect-VIServer -Server $vcenter -confirm:$false -Force
	}
}

Function Test-WebClientStatus {
	<#
	.SYNOPSIS
	Test the Web client status on the vCenter VCenter
	.DESCRIPTION
	Test the Web client status on the vCenter VCenter. It checks the https://vcenter_fqdn:9443/ web uri
	.EXAMPLE
	Test-WebClientStatus -vcenter vcenter_name
	.PARAMETER Vcenter_name
	Name of the VM you will create (not FQDN)
	#>

	[CmdletBinding()]
	param(
		[Parameter(Mandatory=$True)]
		[ValidateNotNullOrEmpty()]
		$vcenter
	)

	## a custom type for ignore SSL certificate chains

add-type @"
	using System.Net;
	using System.Security.Cryptography.X509Certificates;
	public class IDontCarePolicy : ICertificatePolicy {
		public IDontCarePolicy() {}
		public bool CheckValidationResult(ServicePoint sPoint, X509Certificate cert, WebRequest wRequest, int certProb) {
			return true;
		}
	}
"@

	# I don't care cert
	[System.Net.ServicePointManager]::CertificatePolicy = new-object IDontCarePolicy

	# wait for vmware web client is fully up 
	
	$webURLAddress = "https://" + $vcenter + ":9443/"
	$webURLChkResult = "False"
	
	$Counter = 0
	
	write-host "Waiting for vmware web client to start up.  One . per 20 seconds.  Aborting at 10 minutes."
	while ($webURLChkResult -eq "False" ) {
		write-host "." -NoNewline
		
		try {
			$webRequestResult = invoke-webrequest $webURLAddress 
			$webURLChkResult = "true"
			sleep 20
			
			$Counter++
			
			$webRequestResult = invoke-webrequest $($webURLAddress).replace("t3n.dom","") -UseBasic
			return $webURLChkResult		
		} catch {
			if ($Counter -ge 30) {
				return "vmware web client failed to start"
			}
		
			$webURLChkResult = "false"
		}
	}
}

#Verify the database server is ready
Function Test-SQLServerConnection
{
<#
 .SYNOPSIS
  Test the SQL Server Connection
  .DESCRIPTION
  Test SQL Connection status using the System DSN Name
  .EXAMPLE
  Test-SQLServerConnection -dsnname sqldsn_name -SQLServername servername -SQLUsername sql_username -SQLPassword sql_password -VCenterServer vc_name -Server guest-server -Guest_Username username -Guest_Password password
  .PARAMETER dsnname_name
  Name of the ODBC DSN
  .PARAMETER SQLServer_name
  Name of the SQL server to connect for the verification
  .PARAMETER Sql_username
  SQL Server connection username
  .PARAMETER Sql_password
  SQL Server connection password
  .PARAMETER VcenterServer_name
  Name of the Vcenter server where VM reside
  .PARAMETER server_name
  Name of the Guest Server from where SQL connectivity test is executing
  .PARAMETER Guest_Username
  Guest Server username
  .PARAMETER Guest_Password
  Guest Server connection password
#>
[CmdletBinding()]

param(
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
		[string]$sqldsn_name,
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
		[string]$SQLServername,
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
		[string]$sql_username,
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
		[string]$sql_password,
        [Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
		[string]$Guest_Server,
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
        [string]$Guest_Username,
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
        [string]$Guest_Password
	)
		
    $ConnectionString = "`"Driver={SQL Server Native Client 11.0};SERVER=$($SQLServername);DSN=$sqldsn_name;Uid=$sql_username;Pwd=$($sql_password)`""
    $Scriptblock = 
@"
    try
    {
        [System.Data.IDbConnection]`$connection = `$null;
        [System.Data.IDbCommand]`$command = `$null;
        `$query = "SELECT @@SERVERNAME;";
        
        `$connection = new-object System.Data.Odbc.OdbcConnection($ConnectionString);
        `$command = new-object System.Data.Odbc.OdbcCommand(`$query, `$Connection); 

        `$connection.Open();
        `$result = `$command.ExecuteScalar();
        `$connection.Close();
        return `$true
    }
    catch [Exception]
    {
        return `$false
    }
"@ 	

    $SQLTestResult = Invoke-VMScript -ScriptText $ScriptBlock -vm $Guest_Server -GuestUser $Guest_Username -GuestPassword $Guest_Password -ScriptType Powershell
    
	if ($($SQLTestResult.ScriptOutput) -match "False")
    {
        
        return $SQLTestResult
		
        }
	else {
		
		return $SQLTestResult
		
	}
	

}

#Copy Directory from a file share in a VM. It checks the path is already there.
Function Copy-Directory 
{
<#
 .SYNOPSIS
  Copy Directory from file share to a VM
  .DESCRIPTION
  Copy Directory from file share to a VM
  .EXAMPLE
  Copy-Directory -vCenter vc_name -Credential credObject -DirectoryPath DirPath -VMName vm_name -SourceUNCPath FileServerUNCPath
  .PARAMETER vCenter
  Name of the vCenter where VM reside
  .PARAMETER Credential
  Credential object to Access VCenter and VM
  .PARAMETER DirectoryPath
  Path for directory to copy
  .PARAMETER VMName
  VM Servername for the copy
  .PARAMETER SourceUNCPath
  Directory source UNC path
 #>
[CmdletBinding()]

    param(
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
		[string]$vCenter,
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
		[PSCredential]$Cred,
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
		[string]$DirectoryPath,
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
		[string]$VMName,
		[Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
        [string]$SourceUNCPath
	)
	# Verify if the if we have connection to VI Server
	if ($DefaultVIServer.name -eq $null){
			try {
				$connect = Connect-VIServer -Server $vCenter -Credential $cred -ErrorAction Stop
			}
			catch{
				return $False
			}
		}
	
	#Setting up the parameters for copy
	$DirToCopy = split-path "$SourceUNCPath" -Leaf
	$DirPathToTest = $DirectoryPath + "\" + $DirToCopy
	$cmdTestPath = "Test`-Path `"$DirPathToTest`""
    $WaitTools = Wait-Tools -VM $VMname
	
	#Check if we could run the invoke-vmscript and return Failed if issue with invoke-vmscript
	try {
		$resInvScriptPath = Invoke-VMScript -ScriptText $cmdTestPath -VM $VMName -GuestCredential $cred -ErrorAction Stop
		if ($($resInvScriptPath.ScriptOutput) -match "False"){
			
			# Folders not there. Copying folder.
			$cmdCopyItem = "copy-item -path `"$SourceUNCPath`" -Destination `"$DirectoryPath`" -Force -Recurse"
			$resInvScript = Invoke-VMScript -ScriptText $cmdCopyItem -VM $VMName -GuestCredential $cred
			
			if ($connect -ne $null){
					Disconnect-VIServer * -Confirm:$false
			}
			return $True
		}
		else {
			if ($connect -ne $null){
				Disconnect-VIServer * -Confirm:$false
			}
			return $True
		}
	}
	catch{
		return $False
	}
}

#Add Datacenter in provided vCenter
Function Add-Datacenter
{

<#
 .SYNOPSIS
  Add Datacenter VCenter
  .DESCRIPTION
  Add Datacenter folder on the currently connected VCenter.
  .EXAMPLE
  Add-Datacenter -Datacenter datacenter_name
  .PARAMETER datacenter_name
  Datacenter name that appears in vCenter
  
#>
[CmdletBinding()]
param (
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$Datacenter
)

$folder = Get-Folder -NoRecursion

$DatacenterExists = Get-Datacenter -Name $Datacenter -ErrorAction SilentlyContinue

write "Creating datacenter - $($Datacenter)"

if ($DatacenterExists -eq $null) {
	New-Datacenter -Location $folder -Name $Datacenter | Out-Null
} else {
	write "The datacenter already exists"
}
}

#Add Cluster in provided vCenter
Function Add-Cluster
{
<#
 .SYNOPSIS
  Add Datacenter VCenter
  .DESCRIPTION
  Add Datacenter folder on the currently connected VCenter.
  .EXAMPLE
  Add-Datacenter -Datacenter datacenter_name
  .PARAMETER datacenter_name
  Datacenter name that appears in vCenter
  .PARAMETER cluster_name
  Cluster name that appears in vCenter
  
#>
[CmdletBinding()]
param (
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$Datacenter,
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$Cluster
)

$Datacenter = Get-Datacenter -Name $Datacenter

$ClusterExists = Get-Cluster -Name $Cluster -ErrorAction SilentlyContinue

write "Creating cluster - $($Cluster)"

if ($ClusterExists -eq $null) {
	New-Cluster -Location $Datacenter -Name $Cluster -DRSEnabled -DRSAutomationLevel FullyAutomated -HAEnabled -Confirm:$false | Out-Null
} else {
	write "The cluster already exists"
}

write "`n"
}

#Add vCenter license
Function Add-vcLicense
{
#script to install license on a vcenter.  
<#
 .SYNOPSIS
  Add vCenter License Key
  .DESCRIPTION
  Add vCenter License Key on the currently connected VCenter.
  .EXAMPLE
  Add-vcLicense -LicenseKey LicenceKey
  .PARAMETER LicenseKey
  License Key in vCenter
  
#>
[CmdletBinding()]
param (
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$licensekey
)

write "Adding licensekey - $($licensekey)"

#Add Licenses
$VcLicMgr=$DefaultVIServer
$LicMgr = Get-View $VcLicMgr
$AddLic= Get-View $LicMgr.Content.LicenseManager
# If the key is already installed, don't try to install it again
$isLicensePresent = $addlic.Licenses.licenseKey.Contains($licensekey)
if ($isLicensePresent -eq $false){
    
	$licenseOp = $AddLic.AddLicense($licensekey,$null)

# Assign the license to the vcenter	
	$LicenseMgr = Get-View -Id 'LicenseManager-LicenseManager'
	$LicenseAssignMgr = Get-View -Id 'LicenseAssignmentManager-LicenseAssignmentManager'
	$LicenseType = $LicenseMgr.DecodeLicense($LicenseKey)

	$Uuid = (Get-View ServiceInstance).Content.About.InstanceUuid
	$licAssign = $licenseAssignMgr.UpdateAssignedLicense($Uuid, $LicenseKey,$null)

} else {
    write-host "License $($LicenseKey) is already installed"
}

}


#Add ESXi host license
Function Add-ESXiLicense
{
<#
 .SYNOPSIS
  Add ESXi License Key
  .DESCRIPTION
  Add ESXi License Key on the currently connected VCenter.
  .EXAMPLE
  Add-ESXiLicense -LicenseKey LicenceKey
  .PARAMETER LicenseKey
  License Key for ESXi
#>
[CmdletBinding()]
param (
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$licensekey
)

write "Adding licensekey - $($licensekey)"

#Add Licenses
$VcLicMgr=$DefaultVIServer
$LicMgr = Get-View $VcLicMgr
$AddLic= Get-View $LicMgr.Content.LicenseManager
# If the key is already installed, don't try to install it again
$isLicensePresent = $addlic.Licenses.licenseKey.Contains($licensekey)
if ($isLicensePresent -eq $false){
    $licenseOp = $AddLic.AddLicense($licensekey,$null)

} else {
    write-host "License $($LicenseKey) already installed"
}
}

#Add Monitoring role. 
Function Add-MonitoringRole
{
<#
 .SYNOPSIS
  Add VCenter Monitoring Role
  .DESCRIPTION
  Add VCenter Monitoring Role for user T3N\svc_zenoss
  .EXAMPLE
  Add-MonitoringRole
#>
[CmdletBinding()]
param(
    [string]$monitoringuser = "T3N\svc_zenoss"
)
$roleexists = $null
$entity = get-folder -norecursion
$sviewpriv = (Get-VIPrivilege -privilegeitem | where { $_.id -match "StorageViews.View"})
try {
	$roleexists = Get-VIRole -Name *monitoring*
}
catch {
	#[System.Exception]
	write "monitoring Role doesn't exist yet"
}	
if ($roleexists -eq $null){
	#create new role and permission
	write "creating Role monitoring"
	$role = New-VIRole -Name monitoring -Privilege $sviewpriv -WarningAction SilentlyContinue
}	
else {
	write "role exists - skipping"
}	
$monitoringrole = Get-VIRole -Name monitoring
write "setting $monitoringuser to role monitoring"
$Perm = new-vipermission -Role $monitoringrole -Entity $entity -Principal $monitoringuser -Propagate $true -Confirm:$false -WarningAction SilentlyContinue
}

#Add vCenter Permission
Function Add-vcPermissions
{
<#
 .SYNOPSIS
  Add VCenter Permission
  .DESCRIPTION
  Add VCenter Permission to user provided
  .EXAMPLE
  Add-vcPermissions -user user-name -role role_type
  .PARAMETER user_name
  Name of the user from AD - t3n\veeam_svc
  .PARAMETER role_type
  Type of the role Admin or ReadOnly 
#>
[CmdletBinding()]
param (
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	[string]$user,
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	[string]$role
)

write "Creating new user - $($user)"

$entity = get-folder -norecursion

$entityexists = get-vipermission -Entity $entity -Principal $user -ErrorAction SilentlyContinue

if ($entityexists -eq $null) {
	$perm = new-vipermission -Role $Role -Entity $entity -Principal $user -Propagate $true -Confirm:$false -WarningAction SilentlyContinue
} else {
	write "$($user) already exists"
}

write "Setting role for $($user) to $($role)"

$Perm = get-vipermission -Entity $entity -Principal $user  | Set-VIPermission -Role $Role -WarningAction SilentlyContinue

}

#Set vCenter SNMP
Function Set-vcSNMP
{
<#
 .SYNOPSIS
  Set VCenter SNMP
  .DESCRIPTION
  Set VCenter SNMP Settings
  .EXAMPLE
  Set-vcSNMP -vCenter vCenter_name -snmpuri snmpuri
  .PARAMETER vCenter_name
  Name of the vCenter
  .PARAMETER snmpuri
  SNMPURI address 
#>
[CmdletBinding()]
param (
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]    
	$vcenter,
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$snmpuri
)

write "Setting vcenter snmp settings"

Get-AdvancedSetting -Entity $vcenter -Name snmp.receiver.1.community | Set-AdvancedSetting -Value tier3-snmp -Confirm:$false | Out-Null
Get-AdvancedSetting -Entity $vcenter -Name snmp.receiver.1.enabled | Set-AdvancedSetting -Value $true -Confirm:$false | Out-Null
Get-AdvancedSetting -Entity $vcenter -Name snmp.receiver.1.name | Set-AdvancedSetting -Value $snmpuri -Confirm:$false | Out-Null

}

#Add VDSwitch
Function Add-vcVDSwitch
{
<#
 .SYNOPSIS
  Add VCenter Virtual Distributed Switch
  .DESCRIPTION
  Add Virtual Distributed Switch in current vCenter with provided export file 
  .EXAMPLE
  Add-vcVDSwitch -VCSite vCenter_Site -Datacenter Datacenter_name -VDSFile VDSExportFile
  .PARAMETER vCSite
  Name of the vCenter site
  .PARAMETER Datacenter
  Datacenter name in the Current vCenter
  .PARAMETER VDSFile
  Virtual Distributed Switch export file
#>
[CmdletBinding()]
param (
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$VCSite,
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$DataCenter,
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$VDSFile
)

$VCDatacenter = Get-Datacenter -Name $DataCenter

$VDSwitchExists = Get-VDSwitch -Name "$($VCSite)-vds" -ErrorAction SilentlyContinue

if ($VDSwitchExists -eq $null) {
	$VDSwitch = "$($VCSite)-vds"

	write "Importing VDSwitch - $VDSwitch"
	
	$VCDatacenter = Get-Datacenter -Name $DataCenter

	New-VDSwitch -Name $VDSwitch -Location $VCDatacenter -BackupPath $VDSFile | Out-Null
} else {
	write "$($VCSite)-vds already exists"
}
}

# Add Extra PortGroups on VDS Switch
Function Add-ExtraPG_VDSwitch
{
<#
 .SYNOPSIS
  Add Extra PortGroups
  .DESCRIPTION
  Add Extra PortGroups in current vCenter
  .EXAMPLE
  Add-ExtraPG-VDSwitch -PGName PGName -PGVLAN PGVLAN_ID
  .PARAMETER PGName
  Name of the Port Group
  .PARAMETER PGVLAN
  Port Group VLAN ID
  
#>
[CmdletBinding()]
param (
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$PGName,
    [ValidateNotNullOrEmpty()]
	[Parameter(Mandatory=$True)]
	$PGVLAN
)

$VDSwitch = get-vdswitch

$VDPortGroupExists = get-vdportgroup | where {$_.name -eq $PGName}

if ($VDPortGroupExists -eq $null) {
	$NewVDPortgroup = New-VDPortgroup -VDSwitch $VDSwitch -Name $PGName -PortBinding Static -NumPorts 12 -VlanId $PGVLAN

	$NewVDPortgroupSecurityPolicy = Get-VDSecurityPolicy -VDPortgroup $NewVDPortgroup
	Set-VDSecurityPolicy -Policy $NewVDPortgroupSecurityPolicy -MacChanges $true -ForgedTransmits $true -AllowPromiscuous $true | Out-Null

	$NewVDPortgroupUplinkTeamingPolicy = Get-VDUplinkTeamingPolicy -VDPortgroup $NewVDPortgroup
	Set-VDUplinkTeamingPolicy -Policy $NewVDPortgroupUplinkTeamingPolicy -LoadBalancingPolicy LoadBalanceIP | Out-Null
}
}

# Add SDRS Clusters and add datastores to them
Function Add-vcSDRSClusters
{
<#
 .SYNOPSIS
  Add SDRS Cluster in vCenter
  .DESCRIPTION
  Add SDRS Cluster in vCenter out of SolidFire datastores
  .EXAMPLE
  Add-vcSDRSClusters
#>
[CmdletBinding()]

$alldatastores = Get-Datastore
$datastoreclusterlocation = Get-Datacenter

foreach ($datastore in $alldatastores) {
	if ($datastore.name -match "sfire" -and $datastore.name -notmatch "template" -and $datastore.name -notmatch "vmclone") {	
		$datastoreclustername = $datastore.name -split ("-")
		$datastoreclustername = "$($datastoreclustername[0])-$($datastoreclustername[1])" + "-ISCSI-" + "$($datastoreclustername[4])-$($datastoreclustername[5])"
		
		$datastoreclusterexists = Get-DatastoreCluster -Name $datastoreclustername -ErrorAction SilentlyContinue
		
		if ($datastoreclusterexists -eq $null) {
#			write "Creating DatastoreCluster $($datastoreclustername)"
			
			New-DatastoreCluster -Name $datastoreclustername -Location $datastoreclusterlocation | Out-Null
			Set-DatastoreCluster -DatastoreCluster $datastoreclustername -SdrsAutomationLevel FullyAutomated -IOLoadBalanceEnabled 1 | Out-Null
		}
		
		$datastorecluster = Get-DatastoreCluster -Name $datastoreclustername
		
		$isclustermember = Get-Datastore $datastore.name | Get-DatastoreCluster
	
		if ($isclustermember -eq $null) {
			write "Adding $($Datastore.name) to SDRS Cluster $($datastorecluster.name)"
			
			Move-Datastore $datastore -Destination $datastorecluster | Out-Null	
		} else {
			write "$($Datastore.name) already part of a cluster"
		}
	}
}

}


# Get vmIP. This will wait till it gets IP from the DHCP server.
Function Get-vmIP
{
<#
 .SYNOPSIS
  Gets the IP Address of a VM
 #>
    param (
        [Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
        [string]$VIServer, 
        [Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
        [string]$vmname
    )
    
	#Connect the vi server where VM is hosted
	Connect-VIServer -Server $VIServer | out-null
	
	do {
	   $VMInfo = Get-VM $vmname | select Name,@{N="IP";E={@($_.guest.IPAddress[0])}}
	   sleep 10	
    }  while ($VMInfo.IP -eq $null -or $VMinfo.IP -match "^169")
	Disconnect-VIServer * -Confirm:$false
	return $VMInfo.IP
}


function Stop-CLCVM {
<#
 .SYNOPSIS
  Do a soft shutdown on a virtual machine
  .DESCRIPTION
  This script sends a soft ACPI compliant shutdown request to the guest OS, then waits for it to be powered off in vCenter
  .EXAMPLE
  shutdown-vm -vm MyVMName
  .PARAMETER VM
  Virtual Machine Name
#>
    param ( 
        [Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
        [string]$VM 
    )
    write "Shutting down $($VM.name)"
    $ShutdownSuccess = $False
    do { try {
            $Shutdown = stop-vmguest -vm $VM -Confirm:$false -ErrorAction Stop
            $ShutDownSuccess = $True
        } catch { 
            write-host "Error shutting down.  Retrying"
            sleep 5
        }
    } while ($ShutdownSuccess = $False)

    do {
        sleep 5
        $PowerState = (Get-VM $VM).PowerState
        write-host "Power state = $($Powerstate)"
    } while ($Powerstate -eq "PoweredOn")
}

Function Confirm-VMHost{
<#
  .SYNOPSIS
  Validate a host is consistent with other hosts in its cluster
  .DESCRIPTION
  Connects to the specified vCenter and gathers data for the specified host and 1 other random powered on and connected host
   and compares the networks, datastores, advanced settings, syslog settings and ntp settings
  .EXAMPLE
  Confirm-VMHost -RepairedHost wa1t3nesx24.t3n.dom -vCenter wa1t3nvcn02.t3n.dom
  .PARAMETER RepairedHost
  The host that has been repaired/rebuilt and needs validation
  .PARAMETER vCenter
  The vCenter the cluster the repaired host belongs to
#>
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$RepairedHost,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$vCenter
    )
    begin{
        $baseUrl = "http://wa1t3nesiis02.t3n.dom/api/HostValidation"
        $RepairedHost = $RepairedHost.Trim()
        $vCenter = $vCenter.Trim()
        $url = "$baseUrl/$RepairedHost/$vCenter/"
    }
    Process{
        $data = Invoke-WebRequest -Method Get -Uri $url
    }
    End{
        if ($data -eq $null) {Write-Log -Message "Did not receive data from API" -Level Error; return $null}
        return $data | ConvertFrom-Json
    }
}

# Get SSL Thumbprint of the given URL. This is used for xVCVmotion.
Function Get-SSLThumbprint {  
<#
 .SYNOPSIS
  Gets the SSL Certificate Thumbprint
 #>
[CmdletBinding()]  
    param(  
        [Parameter(Mandatory = $true, ValueFromPipeline = $true, Position = 0)]  
        [string]$URL,  
        [Parameter(Position = 1)]  
        [ValidateRange(1,65535)]  
        [int]$Port = 443,  
        [Parameter(Position = 2)]  
        [Net.WebProxy]$Proxy,  
        [Parameter(Position = 3)]  
        [int]$Timeout = 15000,  
        [switch]$UseUserContext  
    )  
Add-Type @"  
using System;  
using System.Net;  
using System.Security.Cryptography.X509Certificates;  
namespace PKI {  
    namespace Web {  
        public class WebSSL {  
            public Uri OriginalURi;  
            public Uri ReturnedURi;  
            public X509Certificate2 Certificate;  
            //public X500DistinguishedName Issuer;  
            //public X500DistinguishedName Subject;  
            public string Issuer;  
            public string Subject;  
            public string[] SubjectAlternativeNames;  
            public bool CertificateIsValid;  
            //public X509ChainStatus[] ErrorInformation;  
            public string[] ErrorInformation;  
            public HttpWebResponse Response;  
        }  
    }  
}  
"@  
    $ConnectString = "https://$url`:$port"  
    $WebRequest = [Net.WebRequest]::Create($ConnectString)  
    $WebRequest.Proxy = $Proxy  
    $WebRequest.Credentials = $null  
    $WebRequest.Timeout = $Timeout  
    $WebRequest.AllowAutoRedirect = $true  
    [Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}  
    try {$Response = $WebRequest.GetResponse()}  
    catch {}  
    if ($WebRequest.ServicePoint.Certificate -ne $null) {  
        $Cert = [Security.Cryptography.X509Certificates.X509Certificate2]$WebRequest.ServicePoint.Certificate.Handle  
        try {$SAN = ($Cert.Extensions | Where-Object {$_.Oid.Value -eq "2.5.29.17"}).Format(0) -split ", "}  
        catch {$SAN = $null}  
        $chain = New-Object Security.Cryptography.X509Certificates.X509Chain -ArgumentList (!$UseUserContext)  
        [void]$chain.ChainPolicy.ApplicationPolicy.Add("1.3.6.1.5.5.7.3.1")  
        $Status = $chain.Build($Cert)  
        New-Object PKI.Web.WebSSL -Property @{  
            OriginalUri = $ConnectString;  
            ReturnedUri = $Response.ResponseUri;  
            Certificate = $WebRequest.ServicePoint.Certificate;  
            Issuer = $WebRequest.ServicePoint.Certificate.Issuer;  
            Subject = $WebRequest.ServicePoint.Certificate.Subject;  
            SubjectAlternativeNames = $SAN;  
            CertificateIsValid = $Status;  
            Response = $Response;  
            ErrorInformation = $chain.ChainStatus | ForEach-Object {$_.Status}  
        }  
        $chain.Reset()  
        [Net.ServicePointManager]::ServerCertificateValidationCallback = $null  
    } else {  
        Write-Error $Error[0]  
    }  
}  

# Move VM across vCenter 6.
Function Move-CLCVM_xvmotion {
	<#
	.SYNOPSIS
	   This script migrate running Virtual Machine 
	   live between two vCenter Servers which are NOT part of the
	   same vCenter SSO Domain which is only available using the vSphere 6.0 API. This script needs vCenter 6.0 and atleast one ESXi 6 host in a cluster
	.NOTES
	   Author     : Nilan and Jasper for CLC
	   Version    : 1.0
	   Known Bugs : Sometimes requires $service.instanceUuid = $destVCConn.InstanceUuid.ToUpper(), sometimes ToUpper will break it
	.LINK
		http://www.virtuallyghetto.com/2015/02/did-you-know-of-an-additional-cool-vmotion-capability-in-vsphere-6-0.html
	.LINK
	   https://github.com/lamw

	.INPUTS
	   sourceVC, destVC, [PSCredential], [datastorename], [clustername], [vmhostname], [vmnetworkname],
	   vmname, [logpath]
	.OUTPUTS
	   Console output
	.EXAMPLE
	   Move-CLCVM_xvmotion -sourceVC ca2-vc-m01 -destVC ca2-vc-m02 -vmname "tst-xvmotion"
	.PARAMETER sourceVC
	   The hostname the source vCenter Server
	.PARAMETER destVC
	   The hostname the destination vCenter Server
	.PARAMETER PSCredential
	   The domain credential from T3N domain which has rights on source and target VC
	.PARAMETER datastorename
	   The destination vSphere Datastore where the VM will be migrated to
	.PARAMETER clustername
	   The destination vSphere Cluster where the VM will be migrated to
	.PARAMETER vmhostname
	   The destination vSphere ESXi host where the VM will be migrated to
	.PARAMETER vmnetworkname
	   The destination vSphere VM Portgroup where the VM will be migrated to
	.PARAMETER vmname
	   The name of the source VM to be migrated
	.PARAMETER logPath
	   The logpath for the migration   
	   #>
	param
	(  
	   [Parameter(Mandatory=$true)]
	   [string]
	   $sourceVC,
	   [Parameter(Mandatory=$true)]
	   [string]
	   $destVC,
	   [Parameter(Mandatory=$false)]
	   $cred = $null,
	   [Parameter(Mandatory=$false)]
	   [string]
	   $datastorename = $null,
	   [Parameter(Mandatory=$false)]
	   [string]
	   $clustername = $null,
	   [Parameter(Mandatory=$false)]
	   [string]
	   $vmhostname =$null,
	   [Parameter(Mandatory=$false)]
	   [string]
	   $vmnetworkname =$null,
	   [Parameter(Mandatory=$true)]
	   [string]
	   $vmname,
	   [Parameter(Mandatory=$false)]
	   [string]
	   $logPath =$null
	);

	<#
	## DEBUGGING

	$vmname = "chris-test" # VM name

	$sourceVC = "lb1-vc-std01.t3n.dom"
	$sourceVCUsername = "administrator@vsphere.local"
	$sourceVCPassword = "redacted"
	$destVC = "lb1-vc-std02.t3n.dom" 
	$destVCUsername = "administrator@vsphere.local"
	$destVCpassword = "redacted"
	$datastorename = "LB1-STD02-CL01-SFIRE-SSD-STD-01"
	$clustername = "LB1STD02CL01" 
	$vmhostname = "lb1-r2-esx-std02-01.t3n.dom"
	$destVCThumbprint = "E2:7C:58:85:2B:8C:44:A0:A6:07:ED:5E:FD:D0:D0:51:13:E7:6D:42"
	$vmnetworkname = "vlan_202_admin"
	  
	<#
	  $sourceVC = "lb1-vc-std02.t3n.dom"
	  $sourceVCUsername = "administrator@vsphere.local"
	  $sourceVCPassword = "redacted"
	  $destVC = "lb1-vc-std01.t3n.dom" 
	  $destVCUsername = "administrator@vsphere.local"
	  $destVCpassword = "redacted"
	  $datastorename = "LB1-STD01-CL01-SFIRE-SSD-STD-01"
	  $clustername = "LB1STD01CL01" 
	  $vmhostname = "lb1-r2-esx-std01-01.t3n.dom"
	  $destVCThumbprint = "75:C3:D9:55:B2:A3:56:FD:D5:32:F8:76:7E:95:B7:CE:3D:19:4F:0C"
	  $vmnetworkname = "vlan_50_10.240.15"
	  #>
	
	# Get Domain credential for cross vCenter vMotion
	if (!$cred){   
	$cred = get-credential -Message "Please entry the T3N domain credential who has access to both vCenters"
	}
	
	# Convert the credential to username and password
	$vcUsername = $cred.UserName
	$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($cred.password)
	$vcPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
	
	# Find if we are already connected to VI servers
	foreach ($serverObj in $global:DefaultVIServers) {
	if ($($serverObj.name) -like $sourceVC) { 
		Write-Host "Source VC $sourceVC is already connected"
		$sourceVCConn = $serverObj
		}
	if ($($serverObj.name) -like $destVC) { 
		Write-Host "Destination VC $destVC is already connected"
		$destVCConn = $serverObj
		}
	}

	if (!$sourceVCConn){
	# Connect to Source vCenter Server
	$sourceVCConn = Connect-VIServer -Server $sourceVC -user $vcUsername -password $vcPassword -WarningAction SilentlyContinue
	$srcConnVC = $true
	}
	if (!$destVCConn ){
	# Connect to Destination vCenter Server
	$destVCConn = Connect-VIServer -Server $destVC -user $vcUsername -password $vcPassword -WarningAction SilentlyContinue
	$desConnVC = $true
	}

	# Get Source VM Object
	$vmObj = Get-VM -Server $sourceVC -Name $vmname

	#Start logging
	if (!$logPath){
	$logPath = "xvmotion_$($vmObj.name)_$(Get-Date -format 'yyyy-MM-dd_HH-mm-ss').txt"
	}
	#Start-Transcript -path $logPath -append	
	Tee-Object -InputObject "Starting xVmotion of $($vmObj.name) by $vcUsername" -FilePath ".\$logPath" -Append

	#Get thumbprint from the destination
	$port = 443
	$ConnectString = "https://$destVC`:$port"
	$WebRequest = [Net.WebRequest]::Create($ConnectString)
	$cert = [Security.Cryptography.X509Certificates.X509Certificate2]$WebRequest.ServicePoint.Certificate.Handle
	$tp = $cert.Thumbprint
	$global:destVCTh = ($tp -split '(..)' | where {$_}) -join ':'
	$destVCThumbprint=$destVCTh

	Tee-Object -InputObject "Thumbprint for destination vCenter is $destVCThumbprint" -FilePath ".\$logPath" -Append
	
	
	# Source VM folder name
	$folderName = (get-vm -Server $sourceVC -Name $vmname | select folder).folder.Name

	Tee-Object -InputObject "Source Folder is $folderName " -FilePath ".\$logPath" -Append

	# Create Folder if the foldername is not present at destination vc
	try {
	$folderGetOutput = Get-Folder -Server $destVC -Name $folderName -ErrorAction Stop
	Tee-Object -InputObject "VM Folder is already there" -FilePath ".\$logPath" -Append
	}
	catch {
	$parentFolder = (Get-Datacenter -Server $destVC).name
	Tee-Object -InputObject "Datacenter is $parentFolder" -FilePath ".\$logPath" -Append
	$folderCreateOutput = (Get-View -Server $destVC (Get-View -viewtype datacenter -filter @{"name"=$parentFolder}).vmfolder).CreateFolder($folderName)
	Tee-Object -InputObject "VM Folder was Not already there and $folderName folder created" -FilePath ".\$logPath" -Append
	}
	
	# Source VM to migrate
	$vm = Get-View (Get-VM -Server $sourceVC -Name $vmname) -Property Config.Hardware.Device

	# Find Source VM's Host version
	if (($vmObj | Get-VMHost | select version).version -like "6*") {
		Tee-Object -InputObject "VM $vmObj.name is already on ESXi 6" -FilePath ".\$logPath" -Append
		}
	else {
		$vmCluster = $vmObj | Get-VMHost | Get-Cluster
		$vmHostMostFree = $vmCluster | get-vmhost | where {$_.version -like "6*" -and $_.ConnectionState -like "Connected" -and ($_.MemoryTotalGB - $_.MemoryUsageGB) -gt 50} | select Name, @{Name="MemoryFreeGB"; Expression={$_.MemoryTotalGB - $_.MemoryUsageGB}} | sort MemoryFreeGB -Descending | select -first 1
		if ($vmHostMostFree) {
		$MoveToEsxi6 = Move-vm -vm $vmname -Destination $vmHostMostFree.Name
		Tee-Object -InputObject "VM $vmObj.name is moved to ESXi 6 host" -FilePath ".\$logPath" -Append
		}
		else {
		Tee-Object -InputObject "ESXi host doesn't have enough Memory free" -FilePath ".\$logPath" -Append
		}
	}	

	# Find Current VM Datastore Name
	$vmDisk = ($vmObj | Get-HardDisk | select Filename).Filename
	$vmDS = $vmDisk -split "]" | select -first 1

	# Dest Datastore to migrate VM to
	if (!$datastorename){
		if ($vmDS -like "*sfire*"){
			if ($vmDS -like "*-prem-*" ){
				$destDS = get-datastore -Server $destVC |where {$_.Name -like "*SFIRE*" -and $_.name -like "*-PREM-*"}  | Sort-Object FreeSpaceGB -Descending | select -first 1
			}
			elseif ($vmDS -like "*-std-*" ){
				$destDS = get-datastore -Server $destVC  |where {$_.Name -like "*SFIRE*" -and $_.name -like "*-STD-*"}  | Sort-Object FreeSpaceGB -Descending | select -first 1
			}
			elseif ($vmDS -like "*template*" ){
				$destDS = get-datastore -Server $destVC  |where {$_.Name -like "*SFIRE*" -and $_.name -like "*template*"}  | Sort-Object FreeSpaceGB -Descending | select -first 1
			}
			elseif ($vmDS -like "*clones*" ){
				$destDS = get-datastore -Server $destVC  |where {$_.Name -like "*SFIRE*" -and $_.name -like "*clones*"}  | Sort-Object FreeSpaceGB -Descending | select -first 1
			}
		}
		elseif ($vmDS -like "*neta*"){
			if ($vmDS -like "*-prem-*" ){
				$destDS = get-datastore -Server $destVC  |where {$_.Name -like "*NETA*" -and $_.name -like "*-PREM-*"}  | Sort-Object FreeSpaceGB -Descending | select -first 1
			}
			elseif ($vmDS -like "*-std-*" ){
				$destDS = get-datastore -Server $destVC  |where {$_.Name -like "*NETA*" -and $_.name -like "*-STD-*"}  | Sort-Object FreeSpaceGB -Descending | select -first 1
			}
		}
		elseif ($vmDS -like "*NMBL*"){
			if ($vmDS -like "*-prem-*" ){
				$destDS = get-datastore -Server $destVC  |where {$_.Name -like "*SFIRE*" -and $_.name -like "*-PREM-*"}  | Sort-Object FreeSpaceGB -Descending | select -first 1
			}
			elseif ($vmDS -like "*-std-*" ){
				$destDS = get-datastore -Server $destVC  |where {$_.Name -like "*SFIRE*" -and $_.name -like "*-STD-*"}  | Sort-Object FreeSpaceGB -Descending | select -first 1
			}
		}
		else{
			Tee-Object -InputObject "The datastores are not Netapp or Solidfire" -FilePath ".\$logPath" -Append
			$destDs = $null
			exit 1
		}
		$datastore = $destDS
	}
	else{
	$datastore = (Get-Datastore -Server $destVC -Name $datastorename)
	}
	Tee-Object -InputObject "The datastore name $datastore" -FilePath ".\$logPath" -Append
	
	# Dest ESXi host to migrate VM to
	if (!$vmhostname){
		$vmDestCluster = get-cluster -Server $destVC | where {$_.name -notlike "*bkup*" -and $_.name -notlike "*bl*"} | select -first 1
		$vmDest6HostMostFree = $vmDestCluster | get-vmhost -Server $destVC | where {$_.version -like "6*" -and $_.ConnectionState -like "Connected" -and ($_.MemoryTotalGB - $_.MemoryUsageGB) -gt 50} | select Name, @{Name="MemoryFreeGB"; Expression={$_.MemoryTotalGB - $_.MemoryUsageGB}} | sort MemoryFreeGB -Descending | select -first 1
		$vmhost = $vmDest6HostMostFree
	}
	else {
		$vmhost = Get-VMHost -Server $destVC -Name $vmhostname | where {$_.version -like "6*" -and $_.ConnectionState -like "Connected" -and ($_.MemoryTotalGB - $_.MemoryUsageGB) -gt 50}
	}
	
	if ($vmhost){
		$vmhostname = $vmhost.Name
		Tee-Object -InputObject "Destination ESXi host is $vmhostname" -FilePath ".\$logPath" -Append
	}
	else {
		Tee-Object -InputObject "ESXi host doesn't have enough Memory free" -FilePath ".\$logPath" -Append
		exit 1
	}
	
	# Dest Cluster to migrate VM to
	if (!$clustername){
	$cluster = Get-VMHost -Server $destVC | where {$_.name -match $vmhost.Name} | Get-Cluster
	}
	else {
	$cluster = (Get-Cluster -Server $destVC -Name $clustername)
	}

	#Current VM network name
	$vmNet = ($vmObj | Get-NetworkAdapter | select NetworkName).NetworkName

	# Dest ESXi VM Portgroup

	$PortGroup = @()
	if (!$vmnetworkname){
		foreach ($nic in $vmNet){
			try{
				$PortGroup += Get-VirtualPortGroup -Name $nic -VMHost $vmhost -ErrorAction Stop
			}
			catch{
				Tee-Object -InputObject "Network port is not present in destination host $vmhost" -FilePath ".\$logPath" -Append
				Exit 1
			}
		}
	}
	else {
		try{
			$PortGroup[0] = Get-VirtualPortGroup -Name $vmnetworkname -VMHost $vmhost -ErrorAction Stop
		}
		catch{
			Tee-Object -InputObject "Network port is not present in destination host $vmhost" -FilePath ".\$logPath" -Append
			Exit 1
		}
		
	}
	# Find Ethernet Device on VM to change VM Networks
	$devices = $vm.Config.Hardware.Device

	$vmNetworkAdapter = @()
	foreach ($device in $devices) {
	   if($device -is [VMware.Vim.VirtualEthernetCard]) {
		  $vmNetworkAdapter += $device
	   }
	}

	# Relocate Spec for Migration
	$spec = New-Object VMware.Vim.VirtualMachineRelocateSpec
	$spec.datastore = $datastore.Id
	$spec.host = $vmhost.Id
	$spec.pool = $cluster.ExtensionData.ResourcePool
	# New Service Locator required for Destination vCenter Server when not part of same SSO Domain
	$service = New-Object VMware.Vim.ServiceLocator
	$credential = New-Object VMware.Vim.ServiceLocatorNamePassword
	$credential.username = $vcUsername
	$credential.password = $vcPassword
	$service.credential = $credential
	#$service.instanceUuid = $destVCConn.InstanceUuid.ToUpper()
	$service.instanceUuid = $destVCConn.InstanceUuid
	$service.sslThumbprint = $destVCThumbprint
	$service.url = "https://$destVC"
	$spec.service = $service

	# Nic settings
	$k=0
	$spec.deviceChange = New-Object VMware.Vim.VirtualDeviceConfigSpec[] (5)

	foreach ($vmNetAdapter in $vmNetworkAdapter) {
		$spec.deviceChange[$k] = New-Object VMware.Vim.VirtualDeviceConfigSpec
		$spec.deviceChange[$k].Operation = "edit"
		$spec.deviceChange[$k].Device = $vmNetAdapter
		# Adjust script based on what type of portgroup it is
		if ($PortGroup[$k] -is [VMware.VimAutomation.ViCore.Impl.V1.Host.Networking.DistributedPortGroupImpl]){
			$vdswitch = Get-VirtualSwitch -Distributed -VMHost $vmhost
			$spec.deviceChange[$k].Device.backing = New-Object VMware.Vim.VirtualEthernetCardDistributedVirtualPortBackingInfo
			$spec.deviceChange[$k].Device.backing.port = New-Object VMware.Vim.DistributedVirtualSwitchPortConnection
			$spec.deviceChange[$k].Device.backing.port.SwitchUuid = $vdswitch.extensiondata.uuid
			$spec.deviceChange[$k].Device.backing.port.PortgroupKey = $PortGroup[$k].key
		} else {
			#Write-Host " I am in right loop"
			$spec.deviceChange[$k].Device.backing.deviceName = $PortGroup[$k].Name
		}
		$k++
	}
		
	Tee-Object -InputObject "`nMigrating $vmname from $sourceVC to $destVC ...`n" -FilePath ".\$logPath" -Append	
	# Issue Cross VC-vMotion 
	try{
		$task = $vm.RelocateVM_Task($spec,"defaultPriority") 
		$task1 = Get-Task -Id ("Task-$($task.value)")
		$task1 | Wait-Task -ErrorAction Stop
		
		#Move vm to right host in destination vCenter
		$vmObjDest = Get-VM -Server $destVC -Name $vmname

		# Find the lowest utilized host at destination and move VM there
		$vmDestHostMostFree = $cluster | get-vmhost -Server $destVC -State Connected  | where {($_.MemoryTotalGB - $_.MemoryUsageGB) -gt 50} | select Name, @{Name="MemoryFreeGB"; Expression={$_.MemoryTotalGB - $_.MemoryUsageGB}} | sort MemoryFreeGB -Descending | select -first 1
		$MoveToDestEsxiHost = Move-vm -Server $destVC -vm $vmname -Destination $($vmDestHostMostFree).Name
		Tee-Object -InputObject "VM $vmObjDest is moved to $($vmDestHostMostFree.Name) host" -FilePath ".\$logPath" -Append	
			
		#Move to destination folder
		$destFolder = get-folder -Server $destVC -name $folderName
		$MoveToDestFolder = Move-vm -Server $destVC -vm $vmname -Destination $destFolder
		Tee-Object -InputObject "VM $vmObjDest is moved to $destFolder" -FilePath ".\$logPath" -Append	
		
		# Disconnect from Source/Destination VC
		if ($srcConnVC){
		Disconnect-VIServer -Server $sourceVC -Confirm:$false -Force
		}
		if ($desConnVC){
		Disconnect-VIServer -Server $destVC -Confirm:$false -Force
		}
	
	return $true	
	}
	catch {
	Tee-Object -InputObject "$vmname can't be moved" -FilePath ".\$logPath" -Append	
	return $false
	
	}
}
function Update-VendorESXiImage {

<#
 .SYNOPSIS
  Updates a vendor provided image with VMWare updates
  .DESCRIPTION
  Updates a vendor provided image with VMWare updates
  .EXAMPLE
  Update-VendorESXiImage -VendorZip "d:\VMWare-HP-ESXi_3247720.zip" -ExportFolder "d:\downloads" -Vendor HP
  .PARAMETER VendorZip
  Path to the vendor esxi image zip file
  .PARAMETER ExportFolder
  Path to folder where ESXi image should be exported
  .PARAMETER Vendor
  Hardware vendor like HP or Dell, used to identify which isos map to which hardware
 #>
	param(
	[Parameter(Mandatory=$TRUE,HelpMessage="Path to Vendor provided ESXi Zip")]
	$VendorZip,
	
	[Parameter(Mandatory=$TRUE,HelpMessage="Path to ISO output including iso file name")]
	$ExportFolder,
	
	[Parameter(Mandatory=$TRUE,HelpMessage="Vendor Name, like HP or Dell")]
	$Vendor
	)

#Testing parameters
$PathTest = Test-path $ExportFolder 
if ( $PathTest -eq $False ) { write-host "Not a real folder, yo.  $($ExportFolder) is not."; exit 1 }

$FileTest = Test-path $VendorZip
if ( $FileTest -eq $False ) { write-host "Not a real file, yo.  $($VendorZip) is not."; exit 1 }
	
# Define VMWare update repo (this shouldn't change but it may, ymmv)
$VMWareRepo = "https://hostupdate.vmware.com/software/VUM/PRODUCTION/main/vmw-depot-index.xml"

# First import the ESXi image and the custom depot
$softwareDepotAdd = Add-EsxSoftwareDepot $VendorZip
$VendorBasePackage = get-esxsoftwarepackage -name esx-base -newest
write-host "Vendor esx-base package version is $($VendorBasePackage.Version)"

# Create new image profile
$newESXProfile = New-EsxImageProfile -CloneProfile (Get-EsxImageProfile)[0] -name CLC-CustomBuild -AcceptanceLevel CommunitySupported -Vendor CenturyLink

# Find our package we want to add
$VMWareRepoAdd = Add-EsxSoftwareDepot $VMWareRepo
$NewBasePackage = get-esxsoftwarepackage -name esx-base -newest
write-host "Latest VMWare esx-base is $($NewBasePackage.Version)"

# Add our custom image, remove bad package
$packageAdd = Add-EsxSoftwarePackage -ImageProfile CLC-CustomBuild -SoftwarePackage $NewBasePackage

# Export Iso
$MajorVersion = $NewBasePackage.Version.Split(".")[0] + "." + $NewBasePackage.Version.Split(".")[1]
$BuildNumber = $NewBasePackage.Version.Split(".")[-1]
$ExportPath = $ExportFolder + "\" + "VMWare-ESXi-$($MajorVersion)-$($BuildNumber)-$($Vendor).iso"

write-host "Exporting ISO to $($ExportPath)"
$isoExport = Export-EsxImageProfile -ImageProfile CLC-CustomBuild -ExportToIso -FilePath $ExportPath
}

# Move/Add vmotion to separate TCP/IP Stack
Function Add-vmk_vmotion
{
<#
 .SYNOPSIS
  Add vmk vmotion on VMHost
  .DESCRIPTION
  Add vmk vmotion on VMHost
  .EXAMPLE
  Add-vmk_vmotion -esxihost "lb1-r1-esx-m01-02.t3n.dom" -vmotionIP "10.240.1.142" -subnetmask "255.255.255.128" -gateway "10.240.1.129" -priDNS "10.240.15.19" -secDNS "10.240.15.20" -vlanid 203
  .EXAMPLE
  Add-vmk_vmotion -esxihost "lb1r1b1s1n1.t3n.dom" -vmotionIP "10.240.1.151" -subnetmask "255.255.255.128" -gateway "10.240.1.129" -priDNS "10.240.15.19" -secDNS "10.240.15.20"
  .PARAMETER esxihost
  Name of the host updating the vmotion vmk
  .PARAMETER vmotionIP
  IP of the vmotion Nic
  .PARAMETER subnetmask
  subnet mask of the vmotion ip
  .PARAMETER gateway
  IP of the gateway vmotion vmk
  .PARAMETER priDNS
  IP of the primary DNS
  .PARAMETER secDNS
  IP of the secondary DNS
  .PARAMETER vlanID
  vlan ID if the vmotion nic is on the standard switch
 #>
	[CmdletBinding()]
    param(
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$esxihost,
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$vmotionIP,
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
		[string]$subnetmask,
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$gateway,
		[parameter(Mandatory=$false)]
        [string]$priDNS,
		[parameter(Mandatory=$false)]
        [string]$secDNS,
		[parameter(Mandatory=$false)]
        [int]$vlanID
    )

	
#Check if the hostname is FQDN
if ($esxihost.Contains(".t3n.dom")){
    Write-host "we have the FQDN Esxi hostname" 
}
else {
    Write-host "we DON'T have FQDN Esxi hostname" 
    $esxihost = "$esxihost.t3n.dom"
    Write-Host "FQDN Esxi hostname is $esxihost"
}
	
#Find out if the vmk0 is on standard switch or distributed switch
#If vmk0 is on the standard switch vmotion will keep it on the standard switch.
$vmk0Net = get-vmhost -name $esxihost | Get-VMHostNetworkAdapter -name vmk0
$vmk0PG = get-vmhost -name $esxihost | Get-VirtualPortGroup -name $vmk0net.PortGroupName

if ($vmk0PG.GetType().name -eq "VirtualPortGroupImpl"){
	write-host "vmk port is on standard switch"
	$switchType = "STD"
	$switchName = $vmk0PG.VirtualSwitch.Name
	write-host "vSwitch Name is $switchName"
}
else{
	write-host "vmk port is on Distributed switch switch"
	$switchName = $vmk0PG.VirtualSwitch.Name
	$switchType = "DVS"
}
	
	
#Get some parameter
#Initialize VLAN Id
if (!$vlanID){
$vlanID = 0
}

#Get CIDR value for subnet mast
[int]$cidrvalue = ConvertTo-MaskLength $subnetmask
if (!$cidrvalue){
$cidrvalue = 24
}

#Get the route network for IpAddress
$routeNet = Get-NetworkAddress -IPAddress $vmotionIP -SubnetMask $subnetmask
write-host "Route network is $routeNet"

#Get host network system
$hostSystem = get-view -ViewType HostSystem -Filter @{"Name" = $esxihost}
$hostConfigManager = $hostSystem.get_ConfigManager()
$hostNetworkSystem = $hostConfigManager.get_NetworkSystem()
$hostid = $hostNetworkSystem.ToString()
write-host "host id is $hostid "

if ($switchType -eq "DVS"){
#Get vmkernel vmotion port group
$vmotionswtch = Get-VDPortgroup -name vmotion-vmk
$vmotiondvsuuid = $vmotionswtch.VDSwitch.key
$vmotionkey = $vmotionswtch.key
write-host "vmotionid is $vmotionkey "
}
#Check if the vmotion tcpip stack is already assigned 
$vmkNicReq = $false
$vmnetadapter = Get-VMHost -name $esxihost | Get-VMHostNetworkAdapter -VMKernel | where {$_.Portgroupname -match "vmotion-vmk"}

if ($vmnetadapter) {
	write-host "Vmotion adapter vmotion-vmk is already present. Checking if it is in vmotion tcpip stack"
	if ($vmnetadapter.ExtensionData.Spec.NetStackInstanceKey -eq "vmotion"){
	write-host "Vmotion adapter vmotion-vmk is already present and it is already in vmotion tcpip stack"
	$vmkNicReq = $false
	}
	else {
	write-host "Updating the Vmotion adapter vmotion-vmk to vmotion tcpip stack"
	Remove-VMHostNetworkAdapter -Nic $vmnetadapter -Confirm:$false | Out-Null
	$vportgroup = Get-VirtualPortGroup -VMHost $esxihost -VirtualSwitch $switchName -Name "vmotion-vmk"
		if ($switchType -eq "STD") {
		Remove-VirtualPortGroup -VirtualPortGroup $vportgroup -Confirm:$false | Out-Null
		}
	
	$vmkNicReq = $true	
	}
}
else {
	write-host "Vmotion adapter vmotion-vmk is Not present. Adding the vmotion-vmk and putting it in vmotion tcpip stack"
	$vmkNicReq = $true
}

if ($vmkNicReq -eq $true -and $switchType -eq "DVS") {
# ------- AddVirtualNic -------

	$portgroup = ''
	$nic = New-Object VMware.Vim.HostVirtualNicSpec
	$nic.mtu = 9000
	$nic.distributedVirtualPort = New-Object VMware.Vim.DistributedVirtualSwitchPortConnection
	$nic.distributedVirtualPort.portgroupKey = $vmotionkey
	$nic.distributedVirtualPort.switchUuid = $vmotiondvsuuid
	$nic.netStackInstanceKey = 'vmotion'
	$nic.ip = New-Object VMware.Vim.HostIpConfig
	$nic.ip.subnetMask = $subnetmask
	$nic.ip.dhcp = $false
	$nic.ip.ipV6Config = New-Object VMware.Vim.HostIpConfigIpV6AddressConfiguration
	$nic.ip.ipV6Config.ipV6Address = New-Object VMware.Vim.HostIpConfigIpV6Address[] (0)
	$nic.ip.ipV6Config.dhcpV6Enabled = $false
	$nic.ip.ipV6Config.autoConfigurationEnabled = $false
	$nic.ip.ipAddress = $vmotionIP
	$_this = Get-View -Id $hostid
	$_this.AddVirtualNic($portgroup, $nic)


}
	
if ($vmkNicReq -eq $true -and $switchType -eq "STD") {

	#Add vmotion vmk stack on standard switch
	$config = New-Object VMware.Vim.HostNetworkConfig
	$config.vnic = New-Object VMware.Vim.HostVirtualNicConfig[] (1)
	$config.vnic[0] = New-Object VMware.Vim.HostVirtualNicConfig
	$config.vnic[0].spec = New-Object VMware.Vim.HostVirtualNicSpec
	$config.vnic[0].spec.mtu = 9000
	$config.vnic[0].spec.netStackInstanceKey = 'vmotion'
	$config.vnic[0].spec.ip = New-Object VMware.Vim.HostIpConfig
	$config.vnic[0].spec.ip.subnetMask = $subnetmask
	$config.vnic[0].spec.ip.dhcp = $false
	$config.vnic[0].spec.ip.ipV6Config = New-Object VMware.Vim.HostIpConfigIpV6AddressConfiguration
	$config.vnic[0].spec.ip.ipV6Config.ipV6Address = New-Object VMware.Vim.HostIpConfigIpV6Address[] (0)
	$config.vnic[0].spec.ip.ipV6Config.dhcpV6Enabled = $false
	$config.vnic[0].spec.ip.ipV6Config.autoConfigurationEnabled = $false
	$config.vnic[0].spec.ip.ipAddress = $vmotionIP
	$config.vnic[0].portgroup = 'vmotion-vmk'
	$config.vnic[0].changeOperation = 'add'
	$config.portgroup = New-Object VMware.Vim.HostPortGroupConfig[] (1)
	$config.portgroup[0] = New-Object VMware.Vim.HostPortGroupConfig
	$config.portgroup[0].spec = New-Object VMware.Vim.HostPortGroupSpec
	$config.portgroup[0].spec.name = 'vmotion-vmk'
	$config.portgroup[0].spec.vswitchName = $switchName
	$config.portgroup[0].spec.policy = New-Object VMware.Vim.HostNetworkPolicy
	$config.portgroup[0].spec.vlanId = $vlanID
	$config.portgroup[0].changeOperation = 'add'
	$changeMode = 'modify'
	$_this = Get-View -Id $hostid
	$_this.UpdateNetworkConfig($config, $changeMode)

}	

if ($vmkNicReq -eq $true) {
# Update default route configuration

	$config = New-Object VMware.Vim.HostNetworkConfig
	$config.netStackSpec = New-Object VMware.Vim.HostNetworkConfigNetStackSpec[] (1)
	$config.netStackSpec[0] = New-Object VMware.Vim.HostNetworkConfigNetStackSpec
	$config.netStackSpec[0].operation = 'edit'
	$config.netStackSpec[0].netStackInstance = New-Object VMware.Vim.HostNetStackInstance
	$config.netStackSpec[0].netStackInstance.routeTableConfig = New-Object VMware.Vim.HostIpRouteTableConfig
	$config.netStackSpec[0].netStackInstance.routeTableConfig.ipRoute = New-Object VMware.Vim.HostIpRouteOp[] (1)
	$config.netStackSpec[0].netStackInstance.routeTableConfig.ipRoute[0] = New-Object VMware.Vim.HostIpRouteOp
	$config.netStackSpec[0].netStackInstance.routeTableConfig.ipRoute[0].route = New-Object VMware.Vim.HostIpRouteEntry
	$config.netStackSpec[0].netStackInstance.routeTableConfig.ipRoute[0].route.gateway = '0.0.0.0'
	$config.netStackSpec[0].netStackInstance.routeTableConfig.ipRoute[0].route.deviceName = 'vmk1'
	$config.netStackSpec[0].netStackInstance.routeTableConfig.ipRoute[0].route.prefixLength = $cidrvalue
	$config.netStackSpec[0].netStackInstance.routeTableConfig.ipRoute[0].route.network = $routeNet
	$config.netStackSpec[0].netStackInstance.routeTableConfig.ipRoute[0].changeOperation = ''
	$config.netStackSpec[0].netStackInstance.congestionControlAlgorithm = 'newreno'
	$config.netStackSpec[0].netStackInstance.requestedMaxNumberOfConnections = 11000
	$config.netStackSpec[0].netStackInstance.name = 'vmotion'
	$config.netStackSpec[0].netStackInstance.ipRouteConfig = New-Object VMware.Vim.HostIpRouteConfig
	$config.netStackSpec[0].netStackInstance.ipRouteConfig.defaultGateway = $gateway
	$config.netStackSpec[0].netStackInstance.dnsConfig = New-Object VMware.Vim.HostDnsConfig
	$config.netStackSpec[0].netStackInstance.dnsConfig.address = New-Object String[] (2)
	$config.netStackSpec[0].netStackInstance.dnsConfig.address[0] = $priDNS
	$config.netStackSpec[0].netStackInstance.dnsConfig.address[1] = $secDNS
	$config.netStackSpec[0].netStackInstance.dnsConfig.searchDomain = New-Object String[] (1)
	$config.netStackSpec[0].netStackInstance.dnsConfig.searchDomain[0] = 't3n.dom'
	$config.netStackSpec[0].netStackInstance.dnsConfig.hostName = ''
	$config.netStackSpec[0].netStackInstance.dnsConfig.dhcp = $false
	$config.netStackSpec[0].netStackInstance.dnsConfig.domainName = ''
	$config.netStackSpec[0].netStackInstance.key = 'vmotion'
	$changeMode = 'modify'
	$_this = Get-View -Id $hostid
	$_this.UpdateNetworkConfig($config, $changeMode)

}

}
# Create local user in Windows server and add it in local administrators group
# It will get the password for the user from the password state

Function Add-LocalComputerAdminAcct {
<#
 .SYNOPSIS
  Create Local computer account and add it to administrators group
  .DESCRIPTION
  Create Local computer account and add it to administrators group
  .EXAMPLE
  Add-LocalComputerAdminAcct -Computername "WindowsServerName" -Username "Username_to_add"
  .PARAMETER Computername
  Windows Computer Name
  .PARAMETER Username
  new username to create
   
#>
[CmdletBinding()]
param (
	[Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
	$Computername,
	[Parameter(Mandatory=$True)]
	[ValidateNotNullOrEmpty()]
	$Username
	
)
try {
$ADSIComp = [adsi]"WinNT://$Computername"
$NewUser = $ADSIComp.Create('User',$Username)
$Password = Get-PasswordFromPassSafe -UserName $Username -PasswordList Compute -ErrorAction Stop
$NewUser.SetPassword($Password)
$NewUser.SetInfo()
$NewUser.UserFlags = 65536
$NewUser.SetInfo()
$group = [ADSI]("WinNT://$Computername/administrators,group")
$group.add("WinNT://$Username,user")
}
catch {
write-host "Couldn't create the $Username on $ComputerName"
}

}

function Set-ScratchLocation {
<#
 .SYNOPSIS
  Updates the scratch partition location
  .DESCRIPTION
  Updates the scratch partition location
  .EXAMPLE
  Set-ScratchLocation -EsxiHost "lb1-r1-esx-m01-02.t3n.dom" -scratchDSName "lb1-sfire-m01-vmtemplates"
  .PARAMETER EsxiHost
  Hostname of the ESXi host
  .PARAMETER scratchDSName
  Scratch partition datastore name.
  
 #>
	param(
	[Parameter(Mandatory=$TRUE,HelpMessage="Esxi Host Name to update scratch partion")]
	$EsxiHost,
	[Parameter(Mandatory=$TRUE,HelpMessage="Scratch Data Store Name")]
	$scratchDSName
	
	)
	$hostDirectory = ($esxiHost).Replace(".t3n.dom","")
	$dc = Get-Datacenter
	$dcValue = $dc.ExtensionData.MoRef.value
	$dcType = $dc.ExtensionData.MoRef.type

	$nameHostDir = "[$scratchDSName]/$hostDirectory"
	$nameHostScratch = "[$scratchDSName]/$hostDirectory/.locker"
	$datacenter = New-Object VMware.Vim.ManagedObjectReference
	$datacenter.value = $dcValue
	$datacenter.type = $dcType
	$createParentDirectories = $false
	$_this = Get-View -Id 'FileManager-FileManager'
	$_this.MakeDirectory($nameHostDir, $datacenter, $createParentDirectories)
	$_this.MakeDirectory($nameHostScratch, $datacenter, $createParentDirectories)

	#get host view object
	$hostview = get-vmhost -name $esxiHost | get-view
	$hostOptionAdvance = $hostview.ConfigManager.AdvancedOption.ToString()

	$changedValue = New-Object VMware.Vim.OptionValue[] (1)
	$changedValue[0] = New-Object VMware.Vim.OptionValue
	$changedValue[0].value = "/vmfs/volumes/$scratchDSName/$hostDirectory/.locker"
	$changedValue[0].key = 'ScratchConfig.ConfiguredScratchLocation'
	$_this = Get-View -Id $hostOptionAdvance
	$_this.UpdateOptions($changedValue)	

}
Function Update-EsxiService {

<#
 .SYNOPSIS
  Restart the service on ESXi host
  .DESCRIPTION
  Restart the service on ESXi host
  .EXAMPLE
  Update-EsxiService -EsxiHost "lb1-r1-esx-m01-02.t3n.dom" -dc "Location" -user "root" -svcName "vpxa"
  .PARAMETER EsxiHost
  FQDN Hostname of the ESXi host
  .PARAMETER dc
  Location of the ESXi host
  .PARAMETER user
  User name of the Esxi host
  .PARAMETER svcName
  Service name to update
 #>
	param(
	[Parameter(Mandatory=$TRUE,HelpMessage="Esxi Host Name to update the service")]
	$esxihost,
	[Parameter(Mandatory=$FALSE,HelpMessage="Location of the Host")]
	$dc,
	[Parameter(Mandatory=$FALSE,HelpMessage="Esxi Host UserName")]
	$user,
	[Parameter(Mandatory=$TRUE,HelpMessage="Esxi service Name to update")]
	$svcName,
    [Parameter(Mandatory=$FALSE,HelpMessage="Esxi service Status-Stop, Start, Restart")]
    [validateset ("Start","Stop","Restart")]
	$svcStatus
	)

# if Location of the host is not provided, we will get it from the first 3 letters from the host name
if(!$dc){
$dc = $esxihost.Substring(0,3)
}
# if username is note provided, we will use root
if(!$user){
$user = "root"
}
# if service status to update is note provided we will use restart
if(!$svcStatus){
$svcStatus = "Restart"
}

Start-Transcript -Path ".\vpxa_restart.log" -Append -Force

$Password = Get-PasswordFromPassSafe -UserName $user -PasswordList Compute -location $dc -ErrorAction Stop

Connect-VIServer $esxihost -user $user -password $Password
$_this = Get-View -Id 'HostServiceSystem-serviceSystem'

Switch ($svcStatus) 
    { 
        "Start" {$_this.StartService($svcName)} 
        "Stop" {$_this.StopService($svcName)} 
        "Restart" {$_this.RestartService($svcName)} 
        default {Write-host "The Service status provided is unknown."}
    }


disconnect-viserver $esxihost -force -Confirm:$false

Stop-Transcript

}