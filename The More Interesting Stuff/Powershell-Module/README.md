# PlatCoreLibrary - Command Reference

## An Index to the FULL Documentation an be found [HERE]()  
This document includes details and examples of commonly used commands from PlatCoreLibrary. It also includes some more advanced examples of using multiple functions together.

# Useful PowerShell one-liners

## Get-command -module PlatCoreLibrary

 This will list all of the current cmdlets in the library. The majority of the cmdlets have help files.

## Get-Help <cmdletName>

 Shows help information obviously, but also will show more detailed information on how to use a cmdlet that does not have help.

## (Get-Command <cmdletName>).definition

 Occasionally you want to check the source of a cmdlet in the terminal. Here's how (sometimes). Use to see how a cmdlet works if it's not a C# cmdlet.

# Interesting Library Commands

 I'm listing just some of the more generally useful cmdlets day to day cmdlets in the library. Please feel free to use the Get-Command line from the top of the file to explore the library and ask me or Nilan questions (He wrote most of the vmware stuff)

## New-DellServiceRequest
-DellUserName <TechDirect Username> -DellPassword <TechDirect Password> -ServiceTag <ServiceTag> -Datacenter <Datacenter of server> -PrimaryContactName <Name> -SecondaryContactName <Name> -Part <Part Code from Get-ValidDellParts> -onsite <$true or $false> -TroubleshootingNotes <Information on how the issue was validated>

    New-DellServiceRequest -DellUserName <username> -DellPassword <Tech Direct Password> -ServiceTag <servicetag> -Datacenter WA1 -PrimaryContactName <contact> -SecondaryContactName <contact -Part OTR -onsite $true -TroubleshootingNotes "Failed Bootable SD CARD (8gb) in DRAC"

Creates A new Service Request with Dell for a specified part. To find valid part codes, please run Get-ValidDellParts. More information on the command will be below. For failed SDCards please use OTR and specify that it is a failed SDCard with the size in the TroubleshootingNotes

## Get-ValidDellParts
-DellUserName <TechDirect Username> -DellPassword <TechDirect Password> -ServiceTag <ServiceTag>

    Example: Get-ValidDellParts -DellUserName <username> -DellPassword <password> -ServiceTag <servicetag>

Returns a list of valid parts for the specified ServiceTag. For failed SDCards please use OTR and specify that it is a failed SDCard with the size in the TroubleshootingNotes of New-DellServiceRequest
## Confirm-VMHost

-RepairedHost "RecentlyFixedServer" -vCenter "vCenter it is in"

    Example: Confirm-VMHost -RepairedHost <server> -vCenter <vcenter>

 Connects to the specified vCenter and gathers data for the specified host and 1 other random powered on and connected host and compares the networks, datastores, advanced settings, syslog settings and NTP settings. Returns status for each of the comparisons, an overall status, and lists any missing items on the repaired host.

## Get-PasswordFromPassSafe

 -Username "UsernameToLookUp" -PasswordList [Compute,Service,Storage] -location "e.g CA1" -title "" -dnsname ""

    Example: Get-PasswordFromPassSafe -Username <username> -PasswordList Compute

-Username and -PasswordList are required. The other 3 fields are to narrow the search when the username exists multiple times in the password list. FYI -PasswordList will TabComplete.

## Get-CLCServer

-Serial  -Hostname -OobIP

    Example: Get-CLCServer -Serial <serial>

 Currently only returns data for ESX Hosts. Returns known information about the specific server such as Vendor and Model, Warranty information and vcenter information.

## Get-CLCServers

 -Datacenter -Cluster -vCenter -Manufacturer

    Example: Get-CLCServers -Manufacturer HP

Currently only returns data for ESX Hosts. Returns the same information as the singular command (Get-CLCServer) but returns an array of objects instead of one.

## Restart-ServerViaOOB

 -IPAddress "DracOrIloIP" -Username "Username" -password "Password" (try using get-passwordfrompasssafe here for extra credit!)

    Example:
    Restart-ServerViaOOB -IPAddress <ipaddress> -Username <username>
    -Password $(Get-PasswordFromPassSafe -username <username> -PasswordList Compute)

Connects to the specified IP (DNS name for the IP is also valid) and determines what kind of Out Of Band device it is (Drac, ILO) and issues the appropriate HARD reboot command.

## ConvertTo-Scriptblocks

 -Commands "An","Array","Of","Text","Commands","To","Convert"

    Example:
    $sb = ConvertTo-Scriptblocks -Commands "Get-Process -Computername comp1","Get-Process -ComputerName "Comp2"

Returns an array of Scriptblocks from the provided array of strings. Really only useful with the next cmdlet and a few other native PowerShell cmdlets that accept scriptblocks.

## Invoke-Scriptblocks

 -scriptblocks <Array of Scriptblocks from ConvertTo-Scriptblocks> -ThrottleLimit <Number of concurrent processes to run>

    Example:
    Invoke-ScriptBlocks -ScriptBlocks $sb -throttleLimit 10

Don't confuse this command with the similar Invoke-ScriptBlock from the Cloud-LSE module, It works differently. I will probably rename mine in the future.

Returns whatever data your array of commands return. ThrottleLimit is not required and defaults to 5. Since execution is in parallel, the results can come back in a different order the the commands were issued. I will include a more indepth example of how to use ConvertTo-Scriptblock and Invoke-Scriptblock at the end.

## Get-WarrantyInformation
 For Dell: -SerialNumber <ServiceTag>
 For HP: -SerialNumber <SerialNumber> -ProductID <HP Product ID> -Model <HP Model>

    Example:
    Get-WarrantyInformation -SerialNumber <serial>

Use this command to get warranty information ad-hoc and if the machine is not found via Get-CLCServer as Get-CLCServer returns this information plus some for machines it knows of and is updated daily.

## Get-HPAHSReport

 -IpAddress <IP of iLo> -Username <Username> -Password <Password> -StartDate <date> -enddate <date>

    Example (Default, downloads last 7 days of logs)
    Get-HPAHSReport -IpAddress <ipaddress> -Username <username>
    -Password $(Get-PasswordFromPassSafe -username <username> -PasswordList Compute)

    Example (Download full log):
    Get-HPAHSReport -IpAddress <ipaddress> -Username <username>
    -Password $(Get-PasswordFromPassSafe -username <username> -PasswordList Compute)
    -All

    Example (Download Date Range):
    Get-HPAHSReport -IpAddress <ipaddress> -Username <username>
    -Password $(Get-PasswordFromPassSafe -username <username> -PasswordList Compute)
    -StartDate "12/15/2015" -EndDate "12/30/2015"

Required Parameters are IPAddress, Username, and Password. By default the cmdlet gets the report for the last 7 days. Use -StartDate and -EndDate to increase or modify the range

## Invoke-SSHCommand

 -ServernameOrIP <ipaddress> -username <username> -password <password> -command <command text>

    Example: Invoke-SSHCommand -ServerNameOrIP <ipaddress> -username <username> -password
    $(Get-PasswordFromPassSafe -username <username> -PasswordList Compute)
    -command "show system1/network1/Integrated\_NICs"

 Runs an SSH Command against a specified IP address and returns the results as a [string]

## Enter-VMHostMaintenanceMode

 -vCenter <vcenter of host> -vmhost <vm host name> -maintenanceNote <note related to why the host is in maintenance>

    Example: Enter-VMHostMaintenanceMode -vcenter <vcenter> -vmhost <host> -maintenancenote "Failed Ram"

## Remove-VMHostMaintenanceMode

 -vcenter <vcenter of host> -vmhost <vm host name>

    Example: Remove-VMHostMaintenanceMode -vcenter <vcenter> -vmhost <host>

# Examples

## ConvertTo-Scriptblock and Invoke-ScriptBlock

First lets make some commands that we want to execute in parallel, perhaps against a list of servers. Here is a recent example where Jasper needed to reboot a bunch of hosts via the iLO/Drac. Jasper's list is in a text file names jaspersreboots.txt

    $serverlist = get-content ".\jaspersreboots.txt"
    #imagine we now have a list of 30 server names in $serverlist

    $user = "<username>"
    $password = $(Get-PasswordFromPassSafe -username $user -PasswordList Compute)

    #Lets make some commands for the reboots
    $strCommands = $serverlist | foreach { "Restart-ServerViaOOB -IPAddress $_ -username $user -Password $password " }

    #Now we should have an array of strings for the commands to run. Lets convert them to scriptblocks
    $sb = ConvertTo-Scriptblocks -commands $strCommands

    #Now we have an array of scriptblocks, lets run them

    # Calling invoke-scriptblocks using the default throttle limit of 5 at a time
    Invoke-Scriptblocks -scriptblocks $sb

## Random Network Related cmdlet examples

I didn't specify any of these above, but I will put in a few examples of the network and ip address related cmdlets in the library. These were found on the internet  but are pretty useful sometimes.

    Get-BroadcastAddress -IPAddress 192.168.0.1 -SubnetMask 255.255.255.224
Returns 192.168.0.31

    Get-NetworkRange -IP 192.168.0.1 -Mask 255.255.255.224
Returns an array of all ip's between 192.168.0.5 and 192.168.0.30

    ConvertTo-Mask -MaskLength 27
Returns 255.255.255.224

    ConvertTo-MaskLength -SubnetMask 255.255.255.192
Returns 26
