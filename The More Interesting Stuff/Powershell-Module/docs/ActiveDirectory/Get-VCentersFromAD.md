# Get-VCentersFromAD
## SYNOPSIS
Uses an LDAP query to locate our Vcenter Servers within AD
## SYNTAX
```powershell
Get-VCentersFromAD [[-Type] <String>] [[-datacenter] <String>] [<CommonParameters>]
```
## DESCRIPTION
Performs an LDAP query of the local Active Directory to locate all non disabled computer objects,
whose description contains the phrase "vcent".
This allows us to automatically discover the Virtual Center Servers within our environment
## PARAMETERS  


### -Type &lt;String&gt;
vCenters, vCenterDatabase, or All
```
Required?                    false
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -datacenter &lt;String&gt;

```
Required?                    false
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES
Requires domain connectivity.

## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Get-VcentersFromAD -type vCenter

Datacenter      : IL1
ServerName      : IL1-VC-STD04
Description     : vCenter:prod
OperatingSystem : Windows Server 2008 R2 Standard
IPv4            : 10.98.120.32

Datacenter      : IL1
ServerName      : IL1-VC-M01
Description     : vCenter:prod
OperatingSystem : Windows Server 2008 R2 Standard
IPv4            : 10.98.120.34
....
```

