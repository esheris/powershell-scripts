# Connect-ToPlatform
## SYNOPSIS
Generates an authenticated session with Control
## SYNTAX
```powershell
Connect-ToPlatform [-ControlUsername] <Object> [-ControlPassword] <Object> [<CommonParameters>]
```
## DESCRIPTION
Generates an authenticated session with Control
## PARAMETERS  


### -ControlUsername &lt;Object&gt;
Control Username
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -ControlPassword &lt;Object&gt;
Control Password
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES
Requires the user to have valid Control API Access

## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Connect-ToPlatform -ControlUsername $user -ControlPassword $pass
```

