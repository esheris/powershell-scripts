# Get-ServerCredentials
## SYNOPSIS

Get-ServerCredentials [-VMObject] <VirtualMachine> [<CommonParameters>]

## SYNTAX
```powershell
syntaxItem                                                                                                       

----------                                                                                                       

{@{name=Get-ServerCredentials; CommonParameters=True; WorkflowCommonParameters=False; parameter=System.Object[]}}
```
## DESCRIPTION

## PARAMETERS  


### -VMObject &lt;VirtualMachine&gt;

```
Position?                    0
Accept pipeline input?       false
Parameter set name           (All)
Aliases                      None
Dynamic?                     false
```
## INPUTS
None


## NOTES


## EXAMPLES 
