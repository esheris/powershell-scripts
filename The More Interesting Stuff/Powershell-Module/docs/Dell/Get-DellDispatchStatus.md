# Get-DellDispatchStatus
## SYNOPSIS
Retrieves the current status of a specified dispatch from Dell
## SYNTAX
```powershell
Get-DellDispatchStatus [-SRNumber] <String> [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -SRNumber &lt;String&gt;
Dell Service Request Number
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES
Requires a valid Dell TechDirect account

## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Get-DellDispatchStatus -SRNumber SR123455
```

