# Get-DellDispatches
## SYNOPSIS
Gets all dispatches from the dispatch database
## SYNTAX
```powershell
Get-DellDispatches [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


## INPUTS


## NOTES
Dispatch Database information is updated nightly and may not always show the most recent data

## EXAMPLES 
