# Get-ValidDellParts
## SYNOPSIS
Retrieves a list of valid parts for a given service tag
## SYNTAX
```powershell
Get-ValidDellParts [-ServiceTag] <String> [<CommonParameters>]
```
## DESCRIPTION
Retrieves a list of valid parts for a given service tag
## PARAMETERS  


### -ServiceTag &lt;String&gt;
The service tag to retrieve parts for
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
Get-ValidDellParts -ServiceTag <ServiceTag for server here>
```

