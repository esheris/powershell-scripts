# New-DellServiceRequest
## SYNOPSIS
Create a new Dell Service Request
## SYNTAX
```powershell
New-DellServiceRequest [-ServiceTag] <String> [-Datacenter] <String> [-PrimaryContactName] <String> [-SecondaryContactName] <String> [-Part] <String> [-onsite] <Boolean> [-TroubleshootingNotes] <String> [<CommonParameters>]
```
## DESCRIPTION
Create a new Dell Service Request for hardware replacement
## PARAMETERS  


### -ServiceTag &lt;String&gt;
The service tag to retrieve parts for
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Datacenter &lt;String&gt;
The Datacenter the server is located in. Valid values are: CA1,CA2,CA3,DE1,DE2,GB1,GB3,IL1,IL2,LB1,LB2,LB3,NE1,NJ1,NY1,PD1,SG1,UC1,UT1,VA1,WA1
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -PrimaryContactName &lt;String&gt;
First and Last name of the primary contact (normally yourself) Current valid values are Ray Salamon, Kevin Kang, Kelly Mendenhall, Eric Sheris
The name is used for a database lookup to find the appropriate email address and phone number
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -SecondaryContactName &lt;String&gt;
Same as PrimaryContactName
```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Part &lt;String&gt;
Valid Part Code from Get-ValidDellParts
```
Required?                    true
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -onsite &lt;Boolean&gt;
$true if you want to have dell schedule a tech to go to the datacenter to replace the part, $false to just send the part to the datacenter
```
Required?                    true
Position?                    6
Default value                False
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -TroubleshootingNotes &lt;String&gt;
Notes on what was done to diagnose the problem. Dell will accept or reject the request depending on this. Also put information here if your part is OTR (other)
```
Required?                    true
Position?                    7
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>New-DellServiceRequest -ServiceTag 5HTJVH1  -Datacenter WA1 -PrimaryContactName "Ray Salamon" -SecondaryContactName "Kevin Kang" -Part OTR -onsite $true -TroubleshootingNotes "Failed Bootable SD CARD (8gb) in DRAC"
```

