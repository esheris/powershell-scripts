# Update-DellDispatchStatus
## SYNOPSIS
Updates the Dispatch status to the specified status
## SYNTAX
```powershell
Update-DellDispatchStatus [-RowID] <Int32> [-status] <String> [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -RowID &lt;Int32&gt;
SQL Row ID to update
```
Required?                    true
Position?                    1
Default value                0
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -status &lt;String&gt;
Status text to update the Status column to.
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
