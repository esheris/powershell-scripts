# ConvertTo-BinaryIP
## SYNOPSIS
Converts a Decimal IP address into a binary format.
## SYNTAX
```powershell
ConvertTo-BinaryIP [-IPAddress] <IPAddress> [<CommonParameters>]
```
## DESCRIPTION
ConvertTo-BinaryIP uses System.Convert to switch between decimal and binary format. The output from this function is dotted binary.
## PARAMETERS  


### -IPAddress &lt;IPAddress&gt;
An IP Address to convert.
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       true (ByValue)
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
