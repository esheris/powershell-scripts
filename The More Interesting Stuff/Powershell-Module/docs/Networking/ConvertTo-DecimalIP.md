# ConvertTo-DecimalIP
## SYNOPSIS
Converts a Decimal IP address into a 32-bit unsigned integer.
## SYNTAX
```powershell
ConvertTo-DecimalIP [-IPAddress] <IPAddress> [<CommonParameters>]
```
## DESCRIPTION
ConvertTo-DecimalIP takes a decimal IP, uses a shift-like operation on each octet and returns a single UInt32 value.
## PARAMETERS  


### -IPAddress &lt;IPAddress&gt;
An IP Address to convert.
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       true (ByValue)
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
