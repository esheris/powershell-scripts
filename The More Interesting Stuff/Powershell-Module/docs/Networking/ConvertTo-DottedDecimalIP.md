# ConvertTo-DottedDecimalIP
## SYNOPSIS
Returns a dotted decimal IP address from either an unsigned 32-bit integer or a dotted binary string.
## SYNTAX
```powershell
ConvertTo-DottedDecimalIP [-IPAddress] <String> [<CommonParameters>]
```
## DESCRIPTION
ConvertTo-DottedDecimalIP uses a regular expression match on the input string to convert to an IP address.
## PARAMETERS  


### -IPAddress &lt;String&gt;
A string representation of an IP address from either UInt32 or dotted binary.
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       true (ByValue)
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
