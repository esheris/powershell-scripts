# ConvertTo-Mask
## SYNOPSIS
Returns a dotted decimal subnet mask from a mask length.
## SYNTAX
```powershell
ConvertTo-Mask [-MaskLength] <Object> [<CommonParameters>]
```
## DESCRIPTION
ConvertTo-Mask returns a subnet mask in dotted decimal format from an integer value ranging 
between 0 and 32. ConvertTo-Mask first creates a binary string from the length, converts 
that to an unsigned 32-bit integer then calls ConvertTo-DottedDecimalIP to complete the operation.
## PARAMETERS  


### -MaskLength &lt;Object&gt;
The number of bits which must be masked.
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       true (ByValue)
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
