# ConvertTo-MaskLength
## SYNOPSIS
Returns the length of a subnet mask.
## SYNTAX
```powershell
ConvertTo-MaskLength [-SubnetMask] <IPAddress> [<CommonParameters>]
```
## DESCRIPTION
ConvertTo-MaskLength accepts any IPv4 address as input, however the output value 
only makes sense when using a subnet mask.
## PARAMETERS  


### -SubnetMask &lt;IPAddress&gt;
A subnet mask to convert into length
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       true (ByValue)
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
