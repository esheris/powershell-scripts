# Get-BroadcastAddress
## SYNOPSIS
Takes an IP address and subnet mask then calculates the broadcast address for the range.
## SYNTAX
```powershell
Get-BroadcastAddress [-IPAddress] <IPAddress> [-SubnetMask] <IPAddress> [<CommonParameters>]
```
## DESCRIPTION
Get-BroadcastAddress returns the broadcast address for a subnet by performing a bitwise AND 
operation against the decimal forms of the IP address and inverted subnet mask. 
Get-BroadcastAddress expects both the IP address and subnet mask in dotted decimal format.
## PARAMETERS  


### -IPAddress &lt;IPAddress&gt;
Any IP address within the network range.
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       true (ByValue)
Accept wildcard characters?  false
``` 
### -SubnetMask &lt;IPAddress&gt;
The subnet mask for the network.
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
