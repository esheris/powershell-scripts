# Get-NetworkRange
## SYNOPSIS
Returns an array of all ip's in a given subnet
## SYNTAX
```powershell
Get-NetworkRange [[-IP] <String>] [[-Mask] <String>] [<CommonParameters>]
```
## DESCRIPTION
Returns an array of all ip's in a given subnet
## PARAMETERS  


### -IP &lt;String&gt;

```
Required?                    false
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Mask &lt;String&gt;

```
Required?                    false
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
