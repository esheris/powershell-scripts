# Add-DellDracUser
## SYNOPSIS
Adds a User to a Dell Drac
## SYNTAX
```powershell
Add-DellDracUser [-IPAddress] <String> [-username] <String> [-password] <String> [-newDracUser] <String> [-newDracPassword] <String> [<CommonParameters>]
```
## DESCRIPTION
Tests an oob device to determine the type then uses appropriate commands to add a new administrative user
## PARAMETERS  


### -IPAddress &lt;String&gt;
IP Address of the Out of Band device
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -username &lt;String&gt;
Existing Login Username for OOB device
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -password &lt;String&gt;
Existing Login Password for OOB device
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -newDracUser &lt;String&gt;

```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -newDracPassword &lt;String&gt;

```
Required?                    true
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-DellDracUser -ipaddress $ip -username $user -password $pass -newOOBUser $newUser -newOOBPassword $newuserpass
```

