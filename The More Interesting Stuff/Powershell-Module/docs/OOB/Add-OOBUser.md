# Add-OOBUser
## SYNOPSIS
Adds a User to an out of band device
## SYNTAX
```powershell
Add-OOBUser [[-IPAddress] <String>] [[-UserName] <String>] [[-Password] <String>] [[-NewOOBUser] <String>] [[-NewOOBPassword] <String>] [<CommonParameters>]
```
## DESCRIPTION
Tests an oob device to determine the type then uses appropriate commands to add a new administrative user
## PARAMETERS  


### -IPAddress &lt;String&gt;
IP Address of the Out of Band device
```
Required?                    false
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -UserName &lt;String&gt;
Existing Login Username for OOB device
```
Required?                    false
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Password &lt;String&gt;
Existing Login Password for OOB device
```
Required?                    false
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -NewOOBUser &lt;String&gt;
Username of new user
```
Required?                    false
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -NewOOBPassword &lt;String&gt;
Password for new user
```
Required?                    false
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-OOBUser -ipaddress $ip -username $user -password $pass -newOOBUser $newUser -newOOBPassword $newuserpass
```

