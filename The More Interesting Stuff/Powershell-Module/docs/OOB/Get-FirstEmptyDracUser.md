# Get-FirstEmptyDracUser
## SYNOPSIS
Returns the first empty user slot on a Dell Drac
## SYNTAX
```powershell
Get-FirstEmptyDracUser [-Username] <String> [-Password] <String> [-IPAddress] <String> [<CommonParameters>]
```
## DESCRIPTION
Returns the first empty user slot on a Dell Drac
## PARAMETERS  


### -Username &lt;String&gt;
Existing Login Username for OOB device
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Password &lt;String&gt;
Existing Login Password for OOB device
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -IPAddress &lt;String&gt;
IP Address of the Out of Band device
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Get-FirstEmptyDracUser -ipaddress $ip -username $user -password $pass
```

