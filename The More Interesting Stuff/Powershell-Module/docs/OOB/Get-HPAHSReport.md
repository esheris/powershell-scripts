# Get-HPAHSReport
## SYNOPSIS

Get-HPAHSReport [-Ipaddress <string>] [-Username <string>] [-Password <string>] [-startdate <datetime>] [-enddate <datetime>] [-SavePath <string>] [<CommonParameters>]

Get-HPAHSReport -Ipaddress <string> -Username <string> -Password <string> -SavePath <string> [-All] [<CommonParameters>]

## SYNTAX
```powershell
syntaxItem                                                                                                                                                                                                            

----------                                                                                                                                                                                                            

{@{name=Get-HPAHSReport; CommonParameters=True; WorkflowCommonParameters=False; parameter=System.Object[]}, @{name=Get-HPAHSReport; CommonParameters=True; WorkflowCommonParameters=False; parameter=System.Object[]}}
```
## DESCRIPTION

## PARAMETERS  


### -All &lt;switch&gt;

```
Position?                    Named
Accept pipeline input?       false
Parameter set name           All
Aliases                      None
Dynamic?                     false
``` 
### -Ipaddress &lt;string&gt;

```
Position?                    Named
Accept pipeline input?       false
Parameter set name           Date, All
Aliases                      None
Dynamic?                     false
``` 
### -Password &lt;string&gt;

```
Position?                    Named
Accept pipeline input?       false
Parameter set name           Date, All
Aliases                      None
Dynamic?                     false
``` 
### -SavePath &lt;string&gt;

```
Position?                    Named
Accept pipeline input?       false
Parameter set name           Date, All
Aliases                      None
Dynamic?                     false
``` 
### -Username &lt;string&gt;

```
Position?                    Named
Accept pipeline input?       false
Parameter set name           Date, All
Aliases                      None
Dynamic?                     false
``` 
### -enddate &lt;datetime&gt;

```
Position?                    Named
Accept pipeline input?       false
Parameter set name           Date
Aliases                      None
Dynamic?                     false
``` 
### -startdate &lt;datetime&gt;

```
Position?                    Named
Accept pipeline input?       false
Parameter set name           Date
Aliases                      None
Dynamic?                     false
```
## INPUTS
None


## NOTES


## EXAMPLES 
