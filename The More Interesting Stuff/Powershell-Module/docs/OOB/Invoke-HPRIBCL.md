# Invoke-HPRIBCL
## SYNOPSIS

Invoke-HPRIBCL [-iloIP] <string> [-ribcl] <string> [<CommonParameters>]

## SYNTAX
```powershell
syntaxItem                                                                                                

----------                                                                                                

{@{name=Invoke-HPRIBCL; CommonParameters=True; WorkflowCommonParameters=False; parameter=System.Object[]}}
```
## DESCRIPTION

## PARAMETERS  


### -iloIP &lt;string&gt;

```
Position?                    0
Accept pipeline input?       true (ByValue)
Parameter set name           (All)
Aliases                      None
Dynamic?                     false
``` 
### -ribcl &lt;string&gt;

```
Position?                    1
Accept pipeline input?       false
Parameter set name           (All)
Aliases                      None
Dynamic?                     false
```
## INPUTS
System.String


## NOTES


## EXAMPLES 
