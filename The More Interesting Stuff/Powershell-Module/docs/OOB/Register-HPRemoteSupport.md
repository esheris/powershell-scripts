# Register-HPRemoteSupport
## SYNOPSIS
uses the ILO Api and RibCL to add a HP server to HP Remote Support
## SYNTAX
```powershell
Register-HPRemoteSupport [-iloIP] <String> [-ilouser] <String> [-ilopass] <String> [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -iloIP &lt;String&gt;
IP or DNS name of the ILO to register
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -ilouser &lt;String&gt;
Existing Login Username for ILO device
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -ilopass &lt;String&gt;
Existing Login Password for ILO device
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
Register-HPRemoteSupport -iloIP 10.0.10.20 -iloUser oobie -iloPass <oobie password>
```

