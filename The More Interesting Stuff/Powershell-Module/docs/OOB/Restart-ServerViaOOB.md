# Restart-ServerViaOOB
## SYNOPSIS
Restarts a server via its out of band
## SYNTAX
```powershell
Restart-ServerViaOOB [-IPAddress] <String> [-UserName] <String> [-Password] <String> [<CommonParameters>]
```
## DESCRIPTION
Tests the provided IP Address and issues the correct reboot command based on the type of out of band device (iLo,Drac,etc)
## PARAMETERS  


### -IPAddress &lt;String&gt;
IP Address of the Out of Band device
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -UserName &lt;String&gt;
Existing Login Username for OOB device
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Password &lt;String&gt;
Existing Login Password for OOB device
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Restart-ServerViaOOB -ipaddress $ip -username $user -password $pass
```

