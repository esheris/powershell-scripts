# Set-OOBSNMP
## SYNOPSIS
Sets the SNMP community string for specified OOB device
## SYNTAX
```powershell
Set-OOBSNMP [-IPAddress] <String> [-UserName] <String> [-Password] <String> [-CommunityString] <String> [<CommonParameters>]
```
## DESCRIPTION
Tests the provided IP Address and issues the correct command to set the SNMP community string
## PARAMETERS  


### -IPAddress &lt;String&gt;
IP Address of the Out of Band device
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -UserName &lt;String&gt;
Existing Login Username for OOB device
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Password &lt;String&gt;
Existing Login Password for OOB device
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -CommunityString &lt;String&gt;
Community String to set
```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Set-OOBSNMP -ipaddress $ip -username $user -password $pass -CommunityString public
```

