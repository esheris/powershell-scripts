# Start-DellDiagnotics
## SYNOPSIS
Issues commands to a Drac to reboot and start running diagnostics, then monitors the diagnostics
run to completion and issues the command to export the results to a share
## SYNTAX
```powershell
Start-DellDiagnotics [-IPAddress] <String> [-UserName] <String> [-Password] <String> [-sharepath] <String> [<CommonParameters>]
```
## DESCRIPTION
Issues commands to a Drac to reboot and start running diagnostics, then monitors the diagnostics
run to completion and issues the command to export the results to a share
## PARAMETERS  


### -IPAddress &lt;String&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -UserName &lt;String&gt;
Existing Login Username for OOB device
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Password &lt;String&gt;
Existing Login Password for OOB device
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -sharepath &lt;String&gt;

```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Restart-ServerViaOOB -ipaddress $ip -username $user -password $pass

IP Address of the Out of Band device
```

