# Test-IloUserExists
## SYNOPSIS
Connects to an ILO and checks if a given user already exists
## SYNTAX
```powershell
Test-IloUserExists [-IPAddress] <String> [-Username] <String> [-Password] <String> [-LookupUserName] <String> [<CommonParameters>]
```
## DESCRIPTION
Connects to an ILO and checks if a given user already exists
## PARAMETERS  


### -IPAddress &lt;String&gt;
IP Address of the Out of Band device
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Username &lt;String&gt;
Existing Login Username for OOB device
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Password &lt;String&gt;
Existing Login Password for OOB device
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -LookupUserName &lt;String&gt;
Username to test for
```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Test-IloUserExists -ipaddress $ip -username $user -password $pass -LookupUserName oobie
```

