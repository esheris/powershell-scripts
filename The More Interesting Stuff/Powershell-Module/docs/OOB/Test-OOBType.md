# Test-OOBType
## SYNOPSIS
Determine if a specified IP Address is an HP iLo, Dell iDrac, or supermicro bmc
## SYNTAX
```powershell
Test-OOBType [[-IPAddress] <String>] [<CommonParameters>]
```
## DESCRIPTION
Determine if a specified IP Address is an HP iLo, Dell iDrac, or supermicro bmc
## PARAMETERS  


### -IPAddress &lt;String&gt;
IP Address of device to test
```
Required?                    false
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Test-OOBType -IPAddress 10.88.9.203
```

