# Update-HPIloFirmware
## SYNOPSIS

Update-HPIloFirmware [-IloIp] <string> [[-FirmwarePath] <string>] [<CommonParameters>]

## SYNTAX
```powershell
syntaxItem                                                                                                      

----------                                                                                                      

{@{name=Update-HPIloFirmware; CommonParameters=True; WorkflowCommonParameters=False; parameter=System.Object[]}}
```
## DESCRIPTION

## PARAMETERS  


### -FirmwarePath &lt;string&gt;

```
Position?                    1
Accept pipeline input?       false
Parameter set name           (All)
Aliases                      None
Dynamic?                     false
``` 
### -IloIp &lt;string&gt;

```
Position?                    0
Accept pipeline input?       false
Parameter set name           (All)
Aliases                      None
Dynamic?                     false
```
## INPUTS
None


## NOTES


## EXAMPLES 
