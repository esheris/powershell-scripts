# Invoke-SqlStoredProcedure
## SYNOPSIS
Executes the specified stored proceedure
## SYNTAX
```powershell
Invoke-SqlStoredProcedure -server <String> -database <String> -SP_Name <String> [-parameters <Hashtable>] [<CommonParameters>]

Invoke-SqlStoredProcedure [-server <String>] [-database <String>] [-SP_Name <String>] [-parameters <Hashtable>] [-UserName <String>] [-Password <String>] [<CommonParameters>]
```
## DESCRIPTION
Executes the specified stored proceedure
## PARAMETERS  


### -server &lt;String&gt;
SQL server to connect to
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -database &lt;String&gt;
Database to run the query against
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -SP_Name &lt;String&gt;
Name of stored proceedure to execute
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -parameters &lt;Hashtable&gt;

```
Required?                    false
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -UserName &lt;String&gt;

```
Required?                    false
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Password &lt;String&gt;
Password to log into the database with
```
Required?                    false
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
