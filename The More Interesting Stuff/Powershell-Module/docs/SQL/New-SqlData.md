# New-SqlData
## SYNOPSIS
Executes Insert, Update and Delete Queries
## SYNTAX
```powershell
New-SqlData [-server] <String> [-query] <String> [-database] <String> [[-user] <String>] [[-password] <String>] [<CommonParameters>]
```
## DESCRIPTION
Executes Insert, Update and Delete Queries
## PARAMETERS  


### -server &lt;String&gt;
SQL server to connect to
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -query &lt;String&gt;
SQL query to run
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -database &lt;String&gt;
Database to run the query against
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -user &lt;String&gt;
Username to log into the database with
```
Required?                    false
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -password &lt;String&gt;
Password to log into the database with
```
Required?                    false
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
