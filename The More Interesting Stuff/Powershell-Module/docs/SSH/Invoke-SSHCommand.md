# Invoke-SSHCommand
## SYNOPSIS
Executes commands against remote machines via SSH
## SYNTAX
```powershell
Invoke-SSHCommand -ServerNameOrIP <String> -username <String> -password <String> -command <String> [<CommonParameters>]

Invoke-SSHCommand -sshClient <SshClient> -command <String> [<CommonParameters>]
```
## DESCRIPTION
If provided an IP address, this command connects via ssh, executes the command, and disconnects.
If provided an existing SSHClient, the existing ssh connection is used to execute the command and the
connection is NOT closed upon completion
## PARAMETERS  


### -ServerNameOrIP &lt;String&gt;
SQL server to connect to
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -username &lt;String&gt;
Username to log in with
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -password &lt;String&gt;
Password to log in with
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -sshClient &lt;SshClient&gt;
An ssh client generated using New-SSHConnection
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       true (ByValue)
Accept wildcard characters?  false
``` 
### -command &lt;String&gt;
Command to execute
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Invoke-SSHCommand -ServerNameOrIP 10.10.0.20 -username root -password calvin -command "racadm getnicinfo"
```
 
### EXAMPLE 2
```powershell
PS C:\>$sshclient = New-SshConnection -ServerNameOrIP 10.10.0.20 -username root -password calvin

$nicinfo = Invoke-SSHCommand -sshClient $sshclient -command "racadm getnicinfo"
$sysinfo = Invoke-SSHCommand -sshClient $sshclient -command "racadm getsysinfo"
Remove-SshConnection -sshClient $sshclient
```

