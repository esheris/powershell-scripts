# New-SSHConnection
## SYNOPSIS
Opens an SSH connection to the specified IP Address
## SYNTAX
```powershell
New-SSHConnection [-ServerNameOrIP] <String> [-username] <String> [-password] <String> [<CommonParameters>]
```
## DESCRIPTION
Opens an SSH Connection to a specified IP Address. Useful if running multiple commands against the same machine
as the connection stays open until closed
## PARAMETERS  


### -ServerNameOrIP &lt;String&gt;
SQL server to connect to
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -username &lt;String&gt;
Username to log in with
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -password &lt;String&gt;
Password to log in with
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>$sshClient = New-SSHConnection -ServerNameOrIP 10.10.0.20 -username root -password calvin
```

