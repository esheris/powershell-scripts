# Remove-SSHConnection
## SYNOPSIS
Disconnects an existing ssh connection
## SYNTAX
```powershell
Remove-SSHConnection [-sshClient] <SshClient> [<CommonParameters>]
```
## DESCRIPTION
Disconnects an existing ssh connection
## PARAMETERS  


### -sshClient &lt;SshClient&gt;
ssh client from New-SSHConnection to clean up
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       true (ByValue)
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
