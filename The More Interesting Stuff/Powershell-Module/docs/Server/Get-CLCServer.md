# Get-CLCServer
## SYNOPSIS
Retrieves detailed server information from the asset database including location, warranty and vcenter information
## SYNTAX
```powershell
Get-CLCServer -HostName <Object> [<CommonParameters>]

Get-CLCServer -Serial <Object> [<CommonParameters>]

Get-CLCServer -OOBIP <Object> [<CommonParameters>]

Get-CLCServer -ID <Object> [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -HostName &lt;Object&gt;
Hostname of the machine to look up
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Serial &lt;Object&gt;
Serial Number of the machine to look up
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -OOBIP &lt;Object&gt;
Out of band (ILO or Drac) IP of the machine to look up
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -ID &lt;Object&gt;
Row ID of machine to look up
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES
Currently the only machines in the database are Physical ESX servers.

## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Get-CLCServer -Hostname wa1t3nesx02
```
 
### EXAMPLE 2
```powershell
PS C:\>Get-CLCServer Serial 5PVG1T1
```
 
### EXAMPLE 3
```powershell
PS C:\>Get-CLCServer -OOBIP 10.0.10.20
```

