# Get-CLCServers
## SYNOPSIS
Retrieves detailed server information from the asset database including location, warranty and vcenter information for servers matching the criteria
## SYNTAX
```powershell
Get-CLCServers [<CommonParameters>]

Get-CLCServers -Datacenter <Object> [<CommonParameters>]

Get-CLCServers -Cluster <Object> [<CommonParameters>]

Get-CLCServers -Manufacturer <Object> [<CommonParameters>]

Get-CLCServers -vCenter <Object> [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -Datacenter &lt;Object&gt;
Datacenter to filter by
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Cluster &lt;Object&gt;
vCenter Cluster to filter by
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Manufacturer &lt;Object&gt;
Manufacturer to filter by
vCenter to filter by
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vCenter &lt;Object&gt;

```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES
Currently the only machines in the database are Physical ESX servers.

## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Get-CLSCservers

Returns all servers in the database
```
 
### EXAMPLE 2
```powershell
PS C:\>Get-CLCServers -Datacenter VA1

Returns all servers in the database with a datacenter of VA1
```
 
### EXAMPLE 3
```powershell
PS C:\>Get-CLCServers -Cluster VA1CLSA

Returns all servers in the database listed as belonging to the specified cluster
```
 
### EXAMPLE 4
```powershell
PS C:\>Get-CLCServers -Manufacturer DELL

Returns all servers in the database listed as Dell
```
 
### EXAMPLE 5
```powershell
PS C:\>Get-CLCServers -vCenter wa1t3nvc02

Returns all servers in the database listed as belonging to the specified vcenter
```

