# Get-MonitoringStatus
## SYNOPSIS
Gets the current status of a device in ZenOSS
## SYNTAX
```powershell
Get-MonitoringStatus [-DataCenter] <String> [[-DeviceName] <String>] [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -DataCenter &lt;String&gt;
Datacenter device is located in
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -DeviceName &lt;String&gt;
Name of the device in ZenOSS
```
Required?                    false
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Get Data for specific device

Get-MonitoringStatus -Datacenter UC1 -DeviceName UC1-VC-C02.t3n.dom-EVENT
```
 
### EXAMPLE 2
```powershell
PS C:\>Get data for all devices in the datacenter

Get-MonitoringStatus -Datacenter UC1
```

