# Get-WarrantyInformation
## SYNOPSIS
Gathers warranty information for HP or Dell servers
## SYNTAX
```powershell
Get-WarrantyInformation -SerialNumber <Object> -ProductID <Object> -Model <Object> [<CommonParameters>]

Get-WarrantyInformation -SerialNumber <Object> [<CommonParameters>]
```
## DESCRIPTION
Gathers warranty information for HP or Dell servers
## PARAMETERS  


### -SerialNumber &lt;Object&gt;
The Server Serial Number
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -ProductID &lt;Object&gt;
HP Servers only - The Product ID from the server
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Model &lt;Object&gt;
HP Servers only - The Server Model (e.g. Proliant DL360p Gen8, Proliant DL360 G7
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Get-WarrantyInformation -SerialNumber H7XG6X1
```
 
### EXAMPLE 2
```powershell
PS C:\>Get-WarrantyInformation -SerialNumber MXQ351053C -ProductID "654081-B21" -Model "ProLiant DL360p Gen8"
```

