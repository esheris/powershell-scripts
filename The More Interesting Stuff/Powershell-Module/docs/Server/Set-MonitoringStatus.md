# Set-MonitoringStatus
## SYNOPSIS
Puts a server into maintenance mode or removes it from maintenance mode
## SYNTAX
```powershell
Set-MonitoringStatus -DeviceName <String> -Datacenter <String> -Monitor [<CommonParameters>]

Set-MonitoringStatus -DeviceName <String> -Datacenter <String> -Suppress -reason <String> [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -DeviceName &lt;String&gt;
Name of the device in ZenOSS
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Datacenter &lt;String&gt;
Datacenter device is located in
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Suppress &lt;SwitchParameter&gt;
Switch Parameter. Suppresses the device
```
Required?                    true
Position?                    named
Default value                False
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Monitor &lt;SwitchParameter&gt;
Switch Parameter. Places the device back into production monitoring
```
Required?                    true
Position?                    named
Default value                False
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -reason &lt;String&gt;
Reason for device state change
```
Required?                    true
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Set a device in maintenance mode

Set-MonitoringStatus -DeviceName UC1-VC-C02.t3n.dom-EVENT -Datacenter UC1 -Suppress -Reason "Example"
```
 
### EXAMPLE 2
```powershell
PS C:\>Set a device back to production monitoring

Set-MonitoringStatus -DeviceName UC1-VC-C02.t3n.dom-EVENT -Datacenter UC1 -Monitor -Reason "Maintenance Complete
```

