# Approve-ChangeRequest
## SYNOPSIS
Approves a change request
## SYNTAX
```powershell
Approve-ChangeRequest [-ChangeRequestID] <Int32> [[-ApproverEmail] <String>] [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -ChangeRequestID &lt;Int32&gt;
ID of the change request from sharepoint. This is in the link to the specific rfc.
```
Required?                    true
Position?                    1
Default value                0
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -ApproverEmail &lt;String&gt;
Email Address of person who is approving. If not provided, the person running Approve-ChangeRequest will be used
```
Required?                    false
Position?                    2
Default value                [string]::Empty
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES
Please use the RFCTemplate.csv file in the lib directory of the location of the PlatCoreLibrary module

## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Approve-ChangeRequest -ChangeRequestID 1490

Approves the change request using whatever credentials were used to log into sharepoint
```
 
### EXAMPLE 2
```powershell
PS C:\>Approve-ChangeRequest -ChangeRequestID 1490 -ApproverEmail eric.sheris@ctl.io

Approves the change request as the specified user
```

