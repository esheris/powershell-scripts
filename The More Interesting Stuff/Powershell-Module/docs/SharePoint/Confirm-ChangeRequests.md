# Confirm-ChangeRequests
## SYNOPSIS
Validation function. Used to validate change requests prior to calling New-ChangeRequest
## SYNTAX
```powershell
Confirm-ChangeRequests [-ChangeRequests] <Object> [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -ChangeRequests &lt;Object&gt;
An array of change request data from import-csv
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES
Please use the RFCTemplate.csv file in the lib directory of the location of the PlatCoreLibrary module

## EXAMPLES 
