# Connect-SPOSite
## SYNOPSIS
Generates Sharepoint Credentials and connects to sharepoint
## SYNTAX
```powershell
Connect-SPOSite [[-Url] <Object>] [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -Url &lt;Object&gt;
Url of the sharepoint site to connect to. Defaults to our sharepoint (https://ctl243.sharepoint.com)
```
Required?                    false
Position?                    1
Default value                https://ctl243.sharepoint.com
Accept pipeline input?       true (ByValue)
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Connect-SPOSite
```
 
### EXAMPLE 2
```powershell
PS C:\>Connect-SPOSite -url "https://ctl243.sharepoint.com"
```

