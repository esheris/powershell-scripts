# Import-ChangeRequests
## SYNOPSIS
Wrapper function for validating and creating change requests. Takes a path to a CSV or prompts if no path is provided. Validates all rows of the CSV prior to beginning import. Returns information on any errors found
## SYNTAX
```powershell
Import-ChangeRequests [[-csvPath] <String>] [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -csvPath &lt;String&gt;
Path to the CSV containing the RFC information. If not provided a select file dialog will appear to browse for the file.d
```
Required?                    false
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES
Please use the RFCTemplate.csv file in the lib directory of the location of the PlatCoreLibrary module

## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Import-ChangeRequests -csvPath c:\users\eric\desktop\rfcs.csv

Imports the CSV
```
 
### EXAMPLE 2
```powershell
PS C:\>Import-ChangeRequests

Will display a select file dialog to browse for the file
```

