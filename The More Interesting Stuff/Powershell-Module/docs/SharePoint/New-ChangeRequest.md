# New-ChangeRequest
## SYNOPSIS
Creates a new Change Request on Sharepoint
## SYNTAX
```powershell
New-ChangeRequest [-list] <Object> [-title] <String> [-changeType] <String> [-startDate] <String> [-endDate] <String> [-AssigneeID] <String> [-RequesterID] <String> [-ChangeArea] <String> [-Datacenters] <String[]> [[-Emergency] <String>] [[-CSAT] <String>] [[-Security] <String>] [[-Platform] <String>] [[-PlatformCore] <String>] [[-Networking] <String>] [-impactType] <String> [[-QTE] <String>] [-reasonForChange] <String> [-AffectedInfra] <String> [-ExpectedImpact] <String> [-Risks] <String> 

[-DetailedSteps] <String> [-TestPlan] <String> [[-RollbackPlan] <String>] [-Validation] <String> [[-supporters] <String>] [[-Notes] <String>] [[-approverid] <Int32>] [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -list &lt;Object&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -title &lt;String&gt;

```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -changeType &lt;String&gt;

```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -startDate &lt;String&gt;

```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -endDate &lt;String&gt;

```
Required?                    true
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -AssigneeID &lt;String&gt;

```
Required?                    true
Position?                    6
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -RequesterID &lt;String&gt;

```
Required?                    true
Position?                    7
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -ChangeArea &lt;String&gt;

```
Required?                    true
Position?                    8
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Datacenters &lt;String[]&gt;

```
Required?                    true
Position?                    9
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Emergency &lt;String&gt;

```
Required?                    false
Position?                    10
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -CSAT &lt;String&gt;

```
Required?                    false
Position?                    11
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Security &lt;String&gt;

```
Required?                    false
Position?                    12
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Platform &lt;String&gt;

```
Required?                    false
Position?                    13
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -PlatformCore &lt;String&gt;

```
Required?                    false
Position?                    14
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Networking &lt;String&gt;

```
Required?                    false
Position?                    15
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -impactType &lt;String&gt;

```
Required?                    true
Position?                    16
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -QTE &lt;String&gt;

```
Required?                    false
Position?                    17
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -reasonForChange &lt;String&gt;

```
Required?                    true
Position?                    18
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -AffectedInfra &lt;String&gt;

```
Required?                    true
Position?                    19
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -ExpectedImpact &lt;String&gt;

```
Required?                    true
Position?                    20
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Risks &lt;String&gt;

```
Required?                    true
Position?                    21
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -DetailedSteps &lt;String&gt;

```
Required?                    true
Position?                    22
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -TestPlan &lt;String&gt;

```
Required?                    true
Position?                    23
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -RollbackPlan &lt;String&gt;

```
Required?                    false
Position?                    24
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Validation &lt;String&gt;

```
Required?                    true
Position?                    25
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -supporters &lt;String&gt;

```
Required?                    false
Position?                    26
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Notes &lt;String&gt;

```
Required?                    false
Position?                    27
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -approverid &lt;Int32&gt;

```
Required?                    false
Position?                    28
Default value                0
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES
Please use the RFCTemplate.csv file in the lib directory of the location of the PlatCoreLibrary module

## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>New-ChangeRequest -list $list -title $rfc.Title -changeType $rfc.ChangeType -startDate $rfc.ScheduledStart -endDate $rfc.ScheduledEnd -RequesterID $requesterID -AssigneeID $AssigneeID -ChangeArea $rfc.ChangeArea -Datacenters $rfc.Datacenters -Emergency $rfc.Emergency -CSAT $rfc.CSATnotify -Security $rfc.SecurityNotify `

-Platform $rfc.Platform -PlatformCore $rfc.PlatformCore -Networking $rfc.Networking -impactType $rfc.ImpactType -QTE $rfc.QTE -reasonForChange $rfc.Reason -AffectedInfra $rfc.AffectedInfra -ExpectedImpact $rfc.ExpectedImpact -Risks $rfc.Risks -DetailedSteps $rfc.DetailedSteps -TestPlan $rfc.TestPlan `
-RollbackPlan $rfc.RollbackPlan -Validation $rfc.Validation -supporters $rfc.Supporters -Notes $rfc.Notes -approverid $ApproverID
```

