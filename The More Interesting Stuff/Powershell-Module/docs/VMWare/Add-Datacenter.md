# Add-Datacenter
## SYNOPSIS
Add Datacenter VCenter
## SYNTAX
```powershell
Add-Datacenter [-Datacenter] <Object> [<CommonParameters>]
```
## DESCRIPTION
Add Datacenter folder on the currently connected VCenter.
## PARAMETERS  


### -Datacenter &lt;Object&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-Datacenter -Datacenter datacenter_name
```

