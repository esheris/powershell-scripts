# Add-ESXiLicense
## SYNOPSIS
Add ESXi License Key
## SYNTAX
```powershell
Add-ESXiLicense [-licensekey] <Object> [<CommonParameters>]
```
## DESCRIPTION
Add ESXi License Key on the currently connected VCenter.
## PARAMETERS  


### -licensekey &lt;Object&gt;
License Key for ESXi
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-ESXiLicense -LicenseKey LicenceKey
```

