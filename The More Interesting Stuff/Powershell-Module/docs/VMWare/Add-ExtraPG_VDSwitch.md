# Add-ExtraPG_VDSwitch
## SYNOPSIS
Add Extra PortGroups
## SYNTAX
```powershell
Add-ExtraPG_VDSwitch [-PGName] <Object> [-PGVLAN] <Object> [<CommonParameters>]
```
## DESCRIPTION
Add Extra PortGroups in current vCenter
## PARAMETERS  


### -PGName &lt;Object&gt;
Name of the Port Group
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -PGVLAN &lt;Object&gt;
Port Group VLAN ID
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-ExtraPG-VDSwitch -PGName PGName -PGVLAN PGVLAN_ID
```

