# Add-LocalComputerAdminAcct
## SYNOPSIS
Create Local computer account and add it to administrators group
## SYNTAX
```powershell
Add-LocalComputerAdminAcct [-Computername] <Object> [-Username] <Object> [<CommonParameters>]
```
## DESCRIPTION
Create Local computer account and add it to administrators group
## PARAMETERS  


### -Computername &lt;Object&gt;
Windows Computer Name
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Username &lt;Object&gt;
new username to create
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-LocalComputerAdminAcct -Computername "WindowsServerName" -Username "Username_to_add"
```

