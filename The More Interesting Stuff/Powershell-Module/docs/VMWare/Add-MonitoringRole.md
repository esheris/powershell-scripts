# Add-MonitoringRole
## SYNOPSIS
Add VCenter Monitoring Role
## SYNTAX
```powershell
Add-MonitoringRole [[-monitoringuser] <String>] [<CommonParameters>]
```
## DESCRIPTION
Add VCenter Monitoring Role for user T3N\svc_zenoss
## PARAMETERS  


### -monitoringuser &lt;String&gt;

```
Required?                    false
Position?                    1
Default value                T3N\svc_zenoss
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-MonitoringRole
```

