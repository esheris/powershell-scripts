# Add-vcLicense
## SYNOPSIS

Add-vcLicense [-licensekey] <Object> [<CommonParameters>]

## SYNTAX
```powershell
syntaxItem                                                                                               

----------                                                                                               

{@{name=Add-vcLicense; CommonParameters=True; WorkflowCommonParameters=False; parameter=System.Object[]}}
```
## DESCRIPTION

## PARAMETERS  


### -licensekey &lt;Object&gt;

```
Position?                    0
Accept pipeline input?       false
Parameter set name           (All)
Aliases                      None
Dynamic?                     false
```
## INPUTS
None


## NOTES


## EXAMPLES 
