# Add-vcPermissions
## SYNOPSIS
Add VCenter Permission
## SYNTAX
```powershell
Add-vcPermissions [-user] <String> [-role] <String> [<CommonParameters>]
```
## DESCRIPTION
Add VCenter Permission to user provided
## PARAMETERS  


### -user &lt;String&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -role &lt;String&gt;

```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-vcPermissions -user user-name -role role_type
```

