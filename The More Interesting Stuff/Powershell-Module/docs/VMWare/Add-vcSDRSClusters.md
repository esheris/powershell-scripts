# Add-vcSDRSClusters
## SYNOPSIS
Add SDRS Cluster in vCenter
## SYNTAX
```powershell
Add-vcSDRSClusters [<CommonParameters>]
```
## DESCRIPTION
Add SDRS Cluster in vCenter out of SolidFire datastores
## PARAMETERS  


## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-vcSDRSClusters
```

