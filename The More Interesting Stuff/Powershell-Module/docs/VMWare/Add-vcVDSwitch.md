# Add-vcVDSwitch
## SYNOPSIS
Add VCenter Virtual Distributed Switch
## SYNTAX
```powershell
Add-vcVDSwitch [-VCSite] <Object> [-DataCenter] <Object> [-VDSFile] <Object> [<CommonParameters>]
```
## DESCRIPTION
Add Virtual Distributed Switch in current vCenter with provided export file
## PARAMETERS  


### -VCSite &lt;Object&gt;
Name of the vCenter site
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -DataCenter &lt;Object&gt;
Datacenter name in the Current vCenter
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -VDSFile &lt;Object&gt;
Virtual Distributed Switch export file
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-vcVDSwitch -VCSite vCenter_Site -Datacenter Datacenter_name -VDSFile VDSExportFile
```

