# Add-vmk_vmotion
## SYNOPSIS
Add vmk vmotion on VMHost
## SYNTAX
```powershell
Add-vmk_vmotion [-esxihost] <String> [-vmotionIP] <String> [-subnetmask] <String> [-gateway] <String> [[-priDNS] <String>] [[-secDNS] <String>] [[-vlanID] <Int32>] [<CommonParameters>]
```
## DESCRIPTION
Add vmk vmotion on VMHost
## PARAMETERS  


### -esxihost &lt;String&gt;
Name of the host updating the vmotion vmk
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vmotionIP &lt;String&gt;
IP of the vmotion Nic
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -subnetmask &lt;String&gt;
subnet mask of the vmotion ip
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -gateway &lt;String&gt;
IP of the gateway vmotion vmk
```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -priDNS &lt;String&gt;
IP of the primary DNS
```
Required?                    false
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -secDNS &lt;String&gt;
IP of the secondary DNS
```
Required?                    false
Position?                    6
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vlanID &lt;Int32&gt;
vlan ID if the vmotion nic is on the standard switch
```
Required?                    false
Position?                    7
Default value                0
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Add-vmk_vmotion -esxihost "lb1-r1-esx-m01-02.t3n.dom" -vmotionIP "10.240.1.142" -subnetmask "255.255.255.128" -gateway "10.240.1.129" -priDNS "10.240.15.19" -secDNS "10.240.15.20" -vlanid 203
```
 
### EXAMPLE 2
```powershell
PS C:\>Add-vmk_vmotion -esxihost "lb1r1b1s1n1.t3n.dom" -vmotionIP "10.240.1.151" -subnetmask "255.255.255.128" -gateway "10.240.1.129" -priDNS "10.240.15.19" -secDNS "10.240.15.20"
```

