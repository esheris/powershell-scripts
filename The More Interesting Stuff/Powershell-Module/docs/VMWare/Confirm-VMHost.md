# Confirm-VMHost
## SYNOPSIS
Validate a host is consistent with other hosts in its cluster
## SYNTAX
```powershell
Confirm-VMHost [-RepairedHost] <String> [-vCenter] <String> [<CommonParameters>]
```
## DESCRIPTION
Connects to the specified vCenter and gathers data for the specified host and 1 other random powered on and connected host
 and compares the networks, datastores, advanced settings, syslog settings and ntp settings
## PARAMETERS  


### -RepairedHost &lt;String&gt;
The host that has been repaired/rebuilt and needs validation
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vCenter &lt;String&gt;
The vCenter the cluster the repaired host belongs to
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Confirm-VMHost -RepairedHost wa1t3nesx24.t3n.dom -vCenter wa1t3nvcn02.t3n.dom
```

