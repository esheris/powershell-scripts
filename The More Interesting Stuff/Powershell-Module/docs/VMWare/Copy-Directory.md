# Copy-Directory
## SYNOPSIS
Copy Directory from file share to a VM
## SYNTAX
```powershell
Copy-Directory [-vCenter] <String> [-Cred] <PSCredential> [-DirectoryPath] <String> [-VMName] <String> [-SourceUNCPath] <String> [<CommonParameters>]
```
## DESCRIPTION
Copy Directory from file share to a VM
## PARAMETERS  


### -vCenter &lt;String&gt;
Name of the vCenter where VM reside
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Cred &lt;PSCredential&gt;

```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -DirectoryPath &lt;String&gt;
Path for directory to copy
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -VMName &lt;String&gt;
VM Servername for the copy
```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -SourceUNCPath &lt;String&gt;
Directory source UNC path
```
Required?                    true
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Copy-Directory -vCenter vc_name -Credential credObject -DirectoryPath DirPath -VMName vm_name -SourceUNCPath FileServerUNCPath
```

