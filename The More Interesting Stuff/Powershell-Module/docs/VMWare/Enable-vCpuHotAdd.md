# Enable-vCpuHotAdd
## SYNOPSIS
Enables CPU Hot Add for a specified VM
## SYNTAX
```powershell
Enable-vCpuHotAdd [-vCenter <String>] [-vm] <String> [<CommonParameters>]
```
## DESCRIPTION
Enables CPU Hot Add for a specified VM
## PARAMETERS  


### -vCenter &lt;String&gt;

```
Required?                    false
Position?                    named
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vm &lt;String&gt;
Name of the VM to enable memory hot add for
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Enable-vCpuHotAdd -vm "VirtualMachineName"
```

