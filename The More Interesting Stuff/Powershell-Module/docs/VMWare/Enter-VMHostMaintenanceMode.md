# Enter-VMHostMaintenanceMode
## SYNOPSIS
Place a VMHost in Maintenance Mode in vCenter
## SYNTAX
```powershell
Enter-VMHostMaintenanceMode [-vCenter] <String> [-vmhost] <String> [-maintenanceNote] <String> [<CommonParameters>]
```
## DESCRIPTION
Place a VMHost in Maintenance Mode in vCenter
## PARAMETERS  


### -vCenter &lt;String&gt;
Name of the vCenter the host is attached to
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vmhost &lt;String&gt;
Name of the vmHost to place in maintenance mode
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -maintenanceNote &lt;String&gt;
Note to set in the Maint-Note field
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Enter-VMHostMaintenanceMode -vCenter wa1t3nvcn02.t3n.dom -vmhost wa1t3nesx01.t3n.dom -maintenanceNote "Bad Ram"
```

