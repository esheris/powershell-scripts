# Get-SSLThumbprint
## SYNOPSIS
Gets the SSL Certificate Thumbprint
## SYNTAX
```powershell
Get-SSLThumbprint [-URL] <String> [[-Port] <Int32>] [[-Proxy] <WebProxy>] [[-Timeout] <Int32>] [-UseUserContext] [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -URL &lt;String&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       true (ByValue)
Accept wildcard characters?  false
``` 
### -Port &lt;Int32&gt;

```
Required?                    false
Position?                    2
Default value                443
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Proxy &lt;WebProxy&gt;

```
Required?                    false
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Timeout &lt;Int32&gt;

```
Required?                    false
Position?                    4
Default value                15000
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -UseUserContext &lt;SwitchParameter&gt;

```
Required?                    false
Position?                    named
Default value                False
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
