# Get-VMHostWSManInstance
## SYNOPSIS
Retrieves data from a WSMan Instance
## SYNTAX
```powershell
Get-VMHostWSManInstance [-VMHost] <VMHostImpl[]> [-class] <String> [-ignoreCertFailures] [[-credential] <PSCredential>] [<CommonParameters>]
```
## DESCRIPTION
Retrieves data from a WSMan instance
## PARAMETERS  


### -VMHost &lt;VMHostImpl[]&gt;
Name of the VM Host to retrieve data from
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -class &lt;String&gt;
WSMan Class to read from
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -ignoreCertFailures &lt;SwitchParameter&gt;
[switch] parameter to ignore cert failures
```
Required?                    false
Position?                    named
Default value                False
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -credential &lt;PSCredential&gt;
[pscredential] object to use for authentication
```
Required?                    false
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Get-VMHostWSManInstance -VMHost $vmh -class OMC_IPMIIPProtocolEndpoint -ignoreCertFailures -erroraction 'silentlycontinue'|Select IPv4Address, MACAddress, SerialNumber
```

