# Get-vmIP
## SYNOPSIS
Gets the IP Address of a VM
## SYNTAX
```powershell
Get-vmIP [-VIServer] <String> [-vmname] <String> [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -VIServer &lt;String&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vmname &lt;String&gt;

```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
