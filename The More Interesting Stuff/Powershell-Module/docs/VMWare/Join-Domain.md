# Join-Domain
## SYNOPSIS
Used by build scripts to handle joining a vCenter to the domain
## SYNTAX
```powershell
Join-Domain [-DomainCredential] <PSCredential> [-LocalCredential] <PSCredential> [-VM] <String> [-DomainName] <String> [-VmType] <String> [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -DomainCredential &lt;PSCredential&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -LocalCredential &lt;PSCredential&gt;

```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -VM &lt;String&gt;

```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -DomainName &lt;String&gt;

```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -VmType &lt;String&gt;

```
Required?                    true
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
