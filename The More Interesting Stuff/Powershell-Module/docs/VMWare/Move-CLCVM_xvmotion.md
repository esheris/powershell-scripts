# Move-CLCVM_xvmotion
## SYNOPSIS
This script migrate running Virtual Machine 
live between two vCenter Servers which are NOT part of the
same vCenter SSO Domain which is only available using the vSphere 6.0 API. This script needs vCenter 6.0 and atleast one ESXi 6 host in a cluster
## SYNTAX
```powershell
Move-CLCVM_xvmotion [-sourceVC] <String> [-destVC] <String> [[-cred] <Object>] [[-datastorename] <String>] [[-clustername] <String>] [[-vmhostname] <String>] [[-vmnetworkname] <String>] [-vmname] <String> [[-logPath] <String>] [<CommonParameters>]
```
## DESCRIPTION

## PARAMETERS  


### -sourceVC &lt;String&gt;
The hostname the source vCenter Server
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -destVC &lt;String&gt;
The hostname the destination vCenter Server
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -cred &lt;Object&gt;

```
Required?                    false
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -datastorename &lt;String&gt;
The destination vSphere Datastore where the VM will be migrated to
```
Required?                    false
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -clustername &lt;String&gt;
The destination vSphere Cluster where the VM will be migrated to
```
Required?                    false
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vmhostname &lt;String&gt;
The destination vSphere ESXi host where the VM will be migrated to
```
Required?                    false
Position?                    6
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vmnetworkname &lt;String&gt;
The destination vSphere VM Portgroup where the VM will be migrated to
```
Required?                    false
Position?                    7
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vmname &lt;String&gt;
The name of the source VM to be migrated
```
Required?                    true
Position?                    8
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -logPath &lt;String&gt;
The logpath for the migration
```
Required?                    false
Position?                    9
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS
sourceVC, destVC, [PSCredential], [datastorename], [clustername], [vmhostname], [vmnetworkname],
vmname, [logpath]

## NOTES
Author     : Nilan and Jasper for CLC
Version    : 1.0
Known Bugs : Sometimes requires $service.instanceUuid = $destVCConn.InstanceUuid.ToUpper(), sometimes ToUpper will break it

## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Move-CLCVM_xvmotion -sourceVC ca2-vc-m01 -destVC ca2-vc-m02 -vmname "tst-xvmotion"
```

