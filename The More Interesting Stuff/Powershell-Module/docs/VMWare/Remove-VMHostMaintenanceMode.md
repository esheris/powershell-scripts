# Remove-VMHostMaintenanceMode
## SYNOPSIS
Remove a VMHost in Maintenance Mode in vCenter
## SYNTAX
```powershell
Remove-VMHostMaintenanceMode [-vcenter] <String> [-vmhost] <String> [<CommonParameters>]
```
## DESCRIPTION
Remove a VMHost in Maintenance Mode in vCenter
## PARAMETERS  


### -vcenter &lt;String&gt;
Name of the vCenter the host is attached to
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vmhost &lt;String&gt;
Name of the vmHost to remove in maintenance mode
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Remove-VMHostMaintenanceMode -vCenter wa1t3nvcn02.t3n.dom -vmhost wa1t3nesx01.t3n.dom
```

