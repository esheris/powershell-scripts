# Set-ScratchLocation
## SYNOPSIS
Updates the scratch partition location
## SYNTAX
```powershell
Set-ScratchLocation [-EsxiHost] <Object> [-scratchDSName] <Object> [<CommonParameters>]
```
## DESCRIPTION
Updates the scratch partition location
## PARAMETERS  


### -EsxiHost &lt;Object&gt;
Hostname of the ESXi host
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -scratchDSName &lt;Object&gt;
Scratch partition datastore name.
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Set-ScratchLocation -EsxiHost "lb1-r1-esx-m01-02.t3n.dom" -scratchDSName "lb1-sfire-m01-vmtemplates"
```

