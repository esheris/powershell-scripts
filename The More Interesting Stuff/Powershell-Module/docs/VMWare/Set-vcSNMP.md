# Set-vcSNMP
## SYNOPSIS
Set VCenter SNMP
## SYNTAX
```powershell
Set-vcSNMP [-vcenter] <Object> [-snmpuri] <Object> [<CommonParameters>]
```
## DESCRIPTION
Set VCenter SNMP Settings
## PARAMETERS  


### -vcenter &lt;Object&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -snmpuri &lt;Object&gt;
SNMPURI address
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Set-vcSNMP -vCenter vCenter_name -snmpuri snmpuri
```

