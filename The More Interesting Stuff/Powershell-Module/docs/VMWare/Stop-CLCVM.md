# Stop-CLCVM
## SYNOPSIS
Do a soft shutdown on a virtual machine
## SYNTAX
```powershell
Stop-CLCVM [-VM] <String> [<CommonParameters>]
```
## DESCRIPTION
This script sends a soft ACPI compliant shutdown request to the guest OS, then waits for it to be powered off in vCenter
## PARAMETERS  


### -VM &lt;String&gt;
Virtual Machine Name
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>shutdown-vm -vm MyVMName
```

