# Test-SQLServerConnection
## SYNOPSIS
Test the SQL Server Connection
## SYNTAX
```powershell
Test-SQLServerConnection [-sqldsn_name] <String> [-SQLServername] <String> [-sql_username] <String> [-sql_password] <String> [-Guest_Server] <String> [-Guest_Username] <String> [-Guest_Password] <String> [<CommonParameters>]
```
## DESCRIPTION
Test SQL Connection status using the System DSN Name
## PARAMETERS  


### -sqldsn_name &lt;String&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -SQLServername &lt;String&gt;

```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -sql_username &lt;String&gt;
SQL Server connection username
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -sql_password &lt;String&gt;
SQL Server connection password
```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Guest_Server &lt;String&gt;

```
Required?                    true
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Guest_Username &lt;String&gt;
Guest Server username
```
Required?                    true
Position?                    6
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Guest_Password &lt;String&gt;
Guest Server connection password
```
Required?                    true
Position?                    7
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Test-SQLServerConnection -dsnname sqldsn_name -SQLServername servername -SQLUsername sql_username -SQLPassword sql_password -VCenterServer vc_name -Server guest-server -Guest_Username username -Guest_Password password
```

