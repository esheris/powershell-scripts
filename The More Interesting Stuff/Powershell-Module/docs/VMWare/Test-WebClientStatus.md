# Test-WebClientStatus
## SYNOPSIS
Test the Web client status on the vCenter VCenter
## SYNTAX
```powershell
Test-WebClientStatus [-vcenter] <Object> [<CommonParameters>]
```
## DESCRIPTION
Test the Web client status on the vCenter VCenter. It checks the https://vcenter_fqdn:9443/ web uri
## PARAMETERS  


### -vcenter &lt;Object&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Test-WebClientStatus -vcenter vcenter_name
```

