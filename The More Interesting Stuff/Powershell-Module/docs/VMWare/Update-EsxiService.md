# Update-EsxiService
## SYNOPSIS
Restart the service on ESXi host
## SYNTAX
```powershell
Update-EsxiService [-esxihost] <Object> [[-dc] <Object>] [[-user] <Object>] [-svcName] <Object> [[-svcStatus] <Object>] [<CommonParameters>]
```
## DESCRIPTION
Restart the service on ESXi host
## PARAMETERS  


### -esxihost &lt;Object&gt;
FQDN Hostname of the ESXi host
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -dc &lt;Object&gt;
Location of the ESXi host
```
Required?                    false
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -user &lt;Object&gt;
User name of the Esxi host
```
Required?                    false
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -svcName &lt;Object&gt;
Service name to update
```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -svcStatus &lt;Object&gt;

```
Required?                    false
Position?                    5
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Update-EsxiService -EsxiHost "lb1-r1-esx-m01-02.t3n.dom" -dc "Location" -user "root" -svcName "vpxa"
```

