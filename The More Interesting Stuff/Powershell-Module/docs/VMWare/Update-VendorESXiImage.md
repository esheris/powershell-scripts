# Update-VendorESXiImage
## SYNOPSIS
Updates a vendor provided image with VMWare updates
## SYNTAX
```powershell
Update-VendorESXiImage [-VendorZip] <Object> [-ExportFolder] <Object> [-Vendor] <Object> [<CommonParameters>]
```
## DESCRIPTION
Updates a vendor provided image with VMWare updates
## PARAMETERS  


### -VendorZip &lt;Object&gt;
Path to the vendor esxi image zip file
```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -ExportFolder &lt;Object&gt;
Path to folder where ESXi image should be exported
```
Required?                    true
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -Vendor &lt;Object&gt;
Hardware vendor like HP or Dell, used to identify which isos map to which hardware
```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Update-VendorESXiImage -VendorZip "d:\VMWare-HP-ESXi_3247720.zip" -ExportFolder "d:\downloads" -Vendor HP
```

