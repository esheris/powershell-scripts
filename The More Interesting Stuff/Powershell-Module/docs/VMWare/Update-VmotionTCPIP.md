# Update-VmotionTCPIP
## SYNOPSIS
Update Vmotion vmk nic to separate routeable TCP/IP stack supported on vSphere 6
## SYNTAX
```powershell
Update-VmotionTCPIP [-VMHost] <Object> [[-cred] <Object>] [-newvMotionIP] <String> [-vMotionDefaultGW] <String> [<CommonParameters>]
```
## DESCRIPTION
Update the Vmotion vmk nic to separate TCP/IP stack with its own default gateway IP on all ESXi 6 hosts.
## PARAMETERS  


### -VMHost &lt;Object&gt;

```
Required?                    true
Position?                    1
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -cred &lt;Object&gt;

```
Required?                    false
Position?                    2
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -newvMotionIP &lt;String&gt;

```
Required?                    true
Position?                    3
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
``` 
### -vMotionDefaultGW &lt;String&gt;

```
Required?                    true
Position?                    4
Default value
Accept pipeline input?       false
Accept wildcard characters?  false
```
## INPUTS


## NOTES


## EXAMPLES 
### EXAMPLE 1
```powershell
PS C:\>Update-VmotionTCPIP -VMHost "ESXiHostName" -newvMotionIP "IP_Address_Vmotion" -vMotionDefaultGW "IP_Gateways"
```

