USE [ServerBuilds]
GO

/****** Object:  Table [dbo].[BuildSummary]    Script Date: 10/19/2011 03:38:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BuildSummary](
	[i_ID] [int] IDENTITY(1,1) NOT NULL,
	[v_ReservationID] [nvarchar](100) NULL,
	[d_DateAdded] [date] NULL,
	[v_Project] [nvarchar](100) NULL,
	[v_AskType] [nvarchar](100) NULL,
	[v_Service] [nvarchar](100) NULL,
	[v_OpsOwner] [nvarchar](100) NULL,
	[v_Requestor] [nvarchar](100) NULL,
	[v_Order] [nvarchar](100) NULL,
	[i_Qty] [int] NULL,
	[d_RequestedDate] [date] NULL,
	[v_RequestedDomain] [nvarchar](100) NULL,
	[i_LineItem] [int] NULL,
	[v_ReservationServerName] [nvarchar](100) NULL,
	[v_Type] [nvarchar](100) NULL,
	[v_Short] [nvarchar](100) NULL,
	[v_SKU] [nvarchar](100) NULL,
	[v_ImagingBatch] [nvarchar](100) NULL,
	[v_ImagedServerName] [nvarchar](15) NOT NULL,
	[v_OSRequested] [nvarchar](100) NOT NULL,
	[v_Domain] [nvarchar](100) NULL,
	[v_DataCenter] [nvarchar](100) NULL,
	[v_Rack] [nvarchar](100) NULL,
	[i_U] [int] NULL,
	[si_U-Height] [smallint] NULL,
	[v_Asset] [int] NOT NULL,
	[v_Serial] [nvarchar](100) NOT NULL,
	[v_OutOfBandIp] [nvarchar](15) NOT NULL,
	[v_BeMAC] [nvarchar](12) NOT NULL,
	[i_BeVLAN] [int] NOT NULL,
	[v_BEIP] [nvarchar](15) NOT NULL,
	[v_BESubnet] [nvarchar](15) NOT NULL,
	[v_BEGateway] [nvarchar](15) NOT NULL,
	[v_FeMac] [nvarchar](12) NULL,
	[v_FEVLAN] [nvarchar](100) NULL,
	[v_FEIP] [nvarchar](100) NULL,
	[v_FESubnet] [nvarchar](15) NULL,
	[v_FEGateway] [nvarchar](15) NULL,
	[v_TFSTASK] [nvarchar](10) NULL,
	[v_BuildStatus] [nvarchar](100) NULL,
	[v_Manufacturer] [nvarchar](100) NOT NULL,
	[v_Model] [nvarchar](100) NULL,
	[v_Role] [nvarchar](100) NULL,
	[v_ImageName]  AS (case when ([v_OSRequested]='2008 SP2' OR [v_OSRequested]='2k8 SP2') AND substring([v_ImagedServerName],(10),(3))='IIS' then '2K8SP2_IIS' when ([v_OSRequested]='2008 SP2' OR [v_OSRequested]='2k8 SP2') AND substring([v_ImagedServerName],(10),(3))='INH' then '2K8SP2_INH' when ([v_OSRequested]='2008 SP2' OR [v_OSRequested]='2k8 SP2') AND substring([v_ImagedServerName],(10),(3))='CCH' then '2K8SP2_INH' when ([v_OSRequested]='2008 SP2' OR [v_OSRequested]='2k8 SP2') AND substring([v_ImagedServerName],(10),(3))='SQL' then '2K8SP2_SQL' when ([v_OSRequested]='2008 R2' OR [v_OSRequested]='2k8 R2') AND substring([v_ImagedServerName],(10),(3))='IIS' then '2K8R2_IIS' when ([v_OSRequested]='2008 R2' OR [v_OSRequested]='2k8 R2') AND substring([v_ImagedServerName],(10),(3))='INH' then '2K8R2_INH' when ([v_OSRequested]='2008 R2' OR [v_OSRequested]='2k8 R2') AND substring([v_ImagedServerName],(10),(3))='CCH' then '2K8R2_INH' when ([v_OSRequested]='2008 R2' OR [v_OSRequested]='2k8 R2') AND substring([v_ImagedServerName],(10),(3))='SQL' then '2K8R2_SQL' else '2K8_SP2_INH' end),
	[v_DiskConfig] [nvarchar](100) NULL,
	[v_smsSiteCode] [nvarchar](10) NULL,
	[v_DomainName] [nvarchar](100) NULL,
	[v_DomainPassword]  AS (case when [v_DomainName]='<domain>' then '<oldpassword>' when [v_DomainName]='<domain>' then '<new password>' else '<old password>' end),
	[v_DomainUsername] [nvarchar](100) NULL,
	[v_staticroutes] [nvarchar](100) NULL,
	[v_TeamConfigFile] [nvarchar](100) NULL,
	[v_BeDefaultGatewayMetric] [nvarchar](100) NULL,
	[v_BeDnsServerSearchOrders] [nvarchar](100) NULL,
	[v_BeDnsServerSearchOrder] [nvarchar](100) NULL,
	[v_BeDnsServerSearchOrder2] [nvarchar](100) NULL,
	[v_BeDhcpEnabled] [nvarchar](100) NULL,
	[v_BeDnsRegistrationEnabled] [bit] NULL,
	[v_BeEnabled] [bit] NULL,
	[v_BeName]  AS ('Team-'+[v_BEIP]),
	[v_BeNetBiosConfig] [bit] NULL,
	[v_BePnPDeviceID]  AS (case when [v_Manufacturer]='HP' then 'ROOT\CQ_CPQTEAMMP\0000' when [v_Manufacturer]='DELL' then 'ROOT\BRCM_BLFM\0000' else 'NONE' end),
	[v_BeDnsSuffixRegistrationEnabled] [nvarchar](100) NULL,
	[v_BeInterfaceMetric] [nvarchar](100) NULL,
	[v_FeDefaultGatewayMetric] [nvarchar](100) NULL,
	[v_FeDhcpEnabled] [nvarchar](100) NULL,
	[v_FeDnsRegistrationEnabled] [nvarchar](100) NULL,
	[v_FeEnabled] [nvarchar](100) NULL,
	[v_FeName]  AS ('Team-'+[v_FEIP]),
	[v_FeNetBiosConfig] [nvarchar](100) NULL,
	[v_FePnPDeviceID]  AS (case when [v_Manufacturer]='HP' then 'ROOT\CQ_CPQTEAMMP\0001' when [v_Manufacturer]='DELL' then 'ROOT\BRCM_BLFM\0001' else 'NONE' end),
	[v_FeDnsSuffixRegistrationEnabled] [nvarchar](100) NULL,
	[v_FeDnsSuffix] [nvarchar](100) NULL,
	[v_FenterfaceMetric] [nvarchar](100) NULL,
	[v_OOBDefaultHostname]  AS (case when [v_Manufacturer]='Dell' then 'RAC-'+[v_ImagedServerName] when [v_Manufacturer]='HP' then 'ILO-'+[v_ImagedServerName] else [v_ImagedServerName] end),
	[v_OOBDhcpEnabled] [nvarchar](100) NULL,
	[v_oobActivationKey] [nvarchar](100) NULL,
	[v_oobIpAddress] [nvarchar](100) NULL,
	[v_oobDefaultGateway] [nvarchar](100) NULL,
	[v_oobSubnetMask] [nvarchar](100) NULL,
 CONSTRAINT [PK_BuildSummary] PRIMARY KEY CLUSTERED
(
	[i_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED
(
	[v_ImagedServerName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED
(
	[v_Asset] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED
(
	[v_BEIP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED
(
	[v_ReservationServerName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED
(
	[v_FEIP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED
(
	[v_BeMAC] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED
(
	[v_Serial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[BuildSummary] ADD  CONSTRAINT [DF_BuildSummary_v_BeDefaultGatewayMetric]  DEFAULT ((20)) FOR [v_BeDefaultGatewayMetric]
GO

ALTER TABLE [dbo].[BuildSummary] ADD  CONSTRAINT [DF_BuildSummary_v_BeDhcpEnabled]  DEFAULT (N'STATIC') FOR [v_BeDhcpEnabled]
GO

ALTER TABLE [dbo].[BuildSummary] ADD  CONSTRAINT [DF_BuildSummary_v_BeDnsRegistrationEnabled]  DEFAULT ((1)) FOR [v_BeDnsRegistrationEnabled]
GO

ALTER TABLE [dbo].[BuildSummary] ADD  CONSTRAINT [DF_BuildSummary_v_BeEnabled]  DEFAULT ((1)) FOR [v_BeEnabled]
GO

ALTER TABLE [dbo].[BuildSummary] ADD  CONSTRAINT [DF_BuildSummary_v_oobActivationKey]  DEFAULT (N'35QWKTN3DN7T3C9BXGJQZ3V3R') FOR [v_oobActivationKey]
GO
