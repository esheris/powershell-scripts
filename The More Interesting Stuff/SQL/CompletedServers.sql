select tasks.ServerName, Tasks.TFS_Task, Tasks.Comment, completed.[Collection], Completed.Comment From
(select vrs.Name0 as ServerName, va.AdvertisementName as TFS_Task, va.Comment as Comment from v_ClientAdvertisementStatus as cas
join v_R_System as vrs on cas.ResourceID = vrs.ResourceID
join v_Advertisement as va on cas.AdvertisementID = va.AdvertisementID
where va.AdvertisementName like 'TFS_Task_%') as tasks
join 
(select vrs.Name0 as ServerName, va.AdvertisementName as [Collection], va.Comment as Comment from v_ClientAdvertisementStatus as cas
join v_R_System as vrs on cas.ResourceID = vrs.ResourceID
join v_Advertisement as va on cas.AdvertisementID = va.AdvertisementID
where va.AdvertisementName = 'Master_Completed') as completed on completed.ServerName = tasks.ServerName
order by ServerName