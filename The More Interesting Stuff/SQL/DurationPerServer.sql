select VRS.Name0 as Name, Temp.StartExecutionTime as StartTime, Temp.EndExecutionTime as EndTime, Temp.Hr, Temp.MIN
from  v_R_System(nolock) as VRS 
join (
select RSET.ResourceID,RSET.StartExecutionTime,REET.EndExecutionTime,DATEDIFF( MINUTE ,RSET.StartExecutionTime,REET.EndExecutionTime)/60 HR,DATEDIFF( MINUTE ,RSET.StartExecutionTime,REET.EndExecutionTime)%60 MIN
from (
select ResourceID,max(ExecutionTime) StartExecutionTime from v_TaskExecutionStatus
where LastStatusMessageID=11140 and Step=0 
and AdvertisementID in ( 'Q012008F')
and ExecutionTime >= DATEADD(DAY,-30,CURRENT_TIMESTAMP)
group by ResourceID) as RSET

join
(
select ResourceID,max(ExecutionTime) EndExecutionTime from v_TaskExecutionStatus
where LastStatusMessageID=11143 
and AdvertisementID in ( 'Q0120091' )
and ExecutionTime >= DATEADD(DAY,-30,CURRENT_TIMESTAMP)
group by ResourceID
) as REET
on (RSET.ResourceID=REET.ResourceID)
) as Temp on VRS.ResourceID=Temp.ResourceID
order by Hr,MIN