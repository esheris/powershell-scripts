use SMS_Q01
DECLARE @in TABLE (
	ID INT IDENTITY(1,1),
	ResourceID INT,
	Name VARCHAR(16),
	Caption VARCHAR(100),
	OOBIP VARCHAR(100)
)
DECLARE @out TABLE (
	Name VARCHAR(16),
	Caption VARCHAR(100),
	MAC0 varchar(100),
	IPAdress0 VARCHAR(96),
	IPSubnet0 VARCHAR(120),
	DefaultIPGateway0 VARCHAR(32),
	Mac1 varchar(100),
	IPAdress1 VARCHAR(96),
	IPSubnet1 VARCHAR(120),
	DefaultIPGateway1 VARCHAR(32),
	OOBIP VARCHAR(100)
)

INSERT INTO @in
SELECT s.ResourceID, s.Name0, os.Caption0, dp.AccessInfo0 FROM v_R_System s WITH (NOLOCK)
JOIN v_GS_OPERATING_SYSTEM os WITH (NOLOCK)
ON s.ResourceID = os.ResourceID
join v_GS_Dell_DRAC_Properties0 as dp on dp.ResourceID = s.ResourceID

INSERT INTO @in
SELECT s.ResourceID, s.Name0, os.Caption0, ip.IPAddress0 FROM v_R_System s WITH (NOLOCK)
JOIN v_GS_OPERATING_SYSTEM os WITH (NOLOCK)
ON s.ResourceID = os.ResourceID
join v_GS_HP_iLO_Properties0 as ip on ip.ResourceID = s.ResourceID

DECLARE @coun INT = 1
WHILE (@coun <= (SELECT COUNT(*) FROM @in))
	BEGIN
		DECLARE @name VARCHAR(16) = '', @caption VARCHAR(100) = '', @OOBIP VARCHAR(100) = '', @IPAdress0 VARCHAR(96) = '', @IPSubnet0 VARCHAR(120) = '', @MAC0 VARCHAR(100) = '',
		@DefaultIPGateway0 VARCHAR(32) = '', @IPAdress1 VARCHAR(96) = '', @IPSubnet1 VARCHAR(120) = '', @DefaultIPGateway1 VARCHAR(32) = '', @MAC1 VARCHAR(100) = ''
		SELECT @name = Name, @caption = Caption, @OOBIP = OOBIP FROM @in WHERE ID = @coun
		
		CREATE TABLE ##temp (
			ID INT IDENTITY(1,1),
			MacAddress0 VARCHAR(100),
			IPAdress0 VARCHAR(96),
			IPSubnet0 VARCHAR(120),
			DefaultIPGateway0 VARCHAR(32)
		)
		INSERT INTO ##temp
		SELECT MACAddress0, IPAddress0, IPSubnet0, DefaultIPGateway0 FROM v_GS_NETWORK_ADAPTER_CONFIGUR WITH (NOLOCK)
		WHERE IPEnabled0 = 1
		AND ResourceID = (SELECT ResourceID FROM @in WHERE ID = @coun)
		
		SELECT @Mac0 = MacAddress0, @IPAdress0 = IPAdress0, @IPSubnet0 = IPSubnet0, @DefaultIPGateway0 = DefaultIPGateway0 FROM ##temp WHERE ID = 1
		SELECT @MAC1 = MacAddress0, @IPAdress1 = IPAdress0, @IPSubnet1 = IPSubnet0, @DefaultIPGateway1 = DefaultIPGateway0 FROM ##temp WHERE ID = 2
		
		INSERT INTO @out
		SELECT @name, @caption, REPLACE(@Mac0, ':', ''), @OOBIP, @IPAdress0, @IPSubnet0, @DefaultIPGateway0, REPLACE(@mac1, ':', ''), @IPAdress1, @IPSubnet1, @DefaultIPGateway1
		
		DROP TABLE ##temp
		
		SET @coun += 1
	END

SELECT * FROM @out