USE [master]
GO

/****** Object:  Database [Manifest]    Script Date: 08/22/2011 22:25:37 ******/
CREATE DATABASE [Manifest] ON  PRIMARY 
( NAME = N'Manifest', FILENAME = N'H:\MSSQL\DATA\Manifest.mdf' , SIZE = 10240KB , MAXSIZE = UNLIMITED, FILEGROWTH = 204800KB )
 LOG ON 
( NAME = N'Manifest_log', FILENAME = N'O:\MSSQL\DATA\Manifest_log.ldf' , SIZE = 13312KB , MAXSIZE = 2048GB , FILEGROWTH = 102400KB )
GO

ALTER DATABASE [Manifest] SET COMPATIBILITY_LEVEL = 100
GO



USE [Manifest]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
-- Create Vlans Table
CREATE TABLE [dbo].[T_Vlans](
	[I_VlanID] [int] IDENTITY(1,1) NOT NULL,
	[I_VlanNumber] [int] NOT NULL,
	[B_FrontEnd] [bit] NOT NULL, --Bit for True/False. If Bit is 1 we know its a FE vlan and can use its gateway for handling static routes
	[C_Gateway] [char](15) NOT NULL,
	[C_Mask] [char](15) NOT NULL,
 CONSTRAINT [PK_T_Vlans] PRIMARY KEY CLUSTERED 
(
	[I_VlanID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

-- Create Network Environment Table
CREATE TABLE [dbo].[T_NetEnv](
	[I_NetEnvID] [int] IDENTITY(1,1) NOT NULL,
	[VC_NetEnvName] [varchar](50) NOT NULL,
	[C_DnsServer1] [char](15) NOT NULL,
	[C_DnsServer2] [char](15) NOT NULL,
	[C_DnsServer3] [char](15) NULL,
	[C_DnsServer4] [char](15) NULL,
 CONSTRAINT [PK_T_NetEnv] PRIMARY KEY CLUSTERED 
(
	[I_NetEnvID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

-- Create Windows Table
CREATE TABLE [dbo].[T_Windows](
	[I_WindowsID] [int] IDENTITY(1,1) NOT NULL,
	[VC_DomainName] [varchar](20) NOT NULL,
 CONSTRAINT [PK_T_Windows] PRIMARY KEY CLUSTERED 
(
	[I_WindowsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
-- Create License Table
CREATE TABLE [dbo].[T_License](
	[I_LicenseID] [int] IDENTITY(1,1) NOT NULL,
	[VC_License] [varchar](50) NOT NULL,
 CONSTRAINT [PK_T_License] PRIMARY KEY CLUSTERED 
(
	[I_LicenseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
-- Create Discovery Table
CREATE TABLE [dbo].[T_Discovery](
	[I_DiscoveryID] [int] IDENTITY(1,1) NOT NULL,
	[DT_Created] [datetime] NULL,
	[DT_LastModified] [datetime] NULL,
	[VC_SystemModel] [varchar](20) NOT NULL,
	[VC_SerialNumber] [varchar](20) NOT NULL,
	[VC_AssetTag] [varchar](10) NULL,
	[VC_Nic1MacAddress] [varchar](12) NOT NULL,
	[VC_Nic1Description] [varchar](100) NOT NULL,
	[VC_Nic2MacAddress] [varchar](12) NOT NULL,
	[VC_Nic2Description] [varchar](100) NOT NULL,
	[VC_Nic3MacAddress] [varchar](12) NULL,
	[VC_Nic3Description] [varchar](100) NULL,
	[VC_Nic4MacAddress] [varchar](12) NULL,
	[VC_Nic4Description] [varchar](100) NULL,
	[I_TotalRam] [int] NOT NULL,
	[I_NumberOfDisks] [smallint] NOT NULL,
	[BI_FirstDiskSize] [bigint] NOT NULL,
	[VC_ProcessorModel] [varchar](50) NOT NULL,
	[VC_ProcessorSpeed] [varchar](10) NOT NULL,
	[SI_ProcessorCount] [smallint] NOT NULL,
	[B_DisksConfigured] [bit] NOT NULL,
 CONSTRAINT [PK_T_Discovery] PRIMARY KEY CLUSTERED 
(
	[I_DiscoveryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
-- Create Unique Constraint on Mac Address 1
UNIQUE NONCLUSTERED 
(
	[VC_Nic1MacAddress] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[T_Credential](
	[I_CredentialID] [int] IDENTITY(1,1) NOT NULL,
	[VC_Description] [varchar](15) NOT NULL, --SQL, Windows, etc
	[VC_Environment] [varchar](20),
	[VC_UserName] [varchar](20) NULL,
	[VC_Password] [varchar](50) NULL,
 CONSTRAINT [PK_T_Credential] PRIMARY KEY CLUSTERED 
(
	[I_CredentialID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[T_Images](
	[I_ImageID] [int] IDENTITY(1,1) NOT NULL,
	[VC_Description] [varchar](15) NOT NULL, --SQL, Windows, etc
	[VC_Name] [varchar](20),
	[VC_UserName] [varchar](20) NULL,
	[VC_Password] [varchar](50) NULL,
 CONSTRAINT [PK_T_Image] PRIMARY KEY CLUSTERED 
(
	[I_ImageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[T_Manifest](
	[I_ManifestID] [int] IDENTITY(1,1) NOT NULL,
	[VC_TaskID] [varchar](20) NULL,
	[VC_Asset] [varchar](15) NOT NULL,
	[VC_Rack] [varchar](20) NOT NULL, --AR413, S3-1 12, etc
	[I_Ustop] [tinyint] NOT NULL,
	[I_ImageID] [int] NOT NULL,
	[VC_StorageConfig] [varchar](30) NOT NULL,
	[VC_NetworkConfig] [varchar](30) NOT NULL,
	[I_Vlan1ID] [int] NULL,-- Vlan Number for Join
	[C_IPAddress1] [char](15) NULL, --Nullable in case of dhcp configuration
	[I_Vlan2ID] [int] NULL,
	[C_IPAddress2] [char](15) NULL,
	[I_Vlan3ID] [int] NULL,
	[C_IPAddress3] [char](15) NULL,
	[I_Vlan4ID] [int] NULL,
	[C_IPAddress4] [char](15) NULL,
	[I_OOBVlanID] [int] NULL,
	[C_OOBIpAddress] [char](15) NOT NULL,
 CONSTRAINT [PK_T_Manifest] PRIMARY KEY CLUSTERED 
(
	[I_ManifestID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO
-- Create Inserted and Modified Triggers
CREATE TRIGGER tr_Discovery_Data_CreateDate ON T_Discovery
FOR INSERT 
AS
UPDATE T_Discovery SET T_Discovery.DT_Created=getdate()
FROM T_Discovery INNER JOIN Inserted ON T_Discovery.I_DiscoveryID = Inserted.I_DiscoveryID 

go 

CREATE TRIGGER tr_Discovery_Data_LastModifiedDate ON T_Discovery
FOR UPDATE 
AS
UPDATE T_Discovery SET T_Discovery.DT_LastModified=getdate()
FROM T_Discovery INNER JOIN Inserted ON T_Discovery.I_DiscoveryID= Inserted.I_DiscoveryID

go
