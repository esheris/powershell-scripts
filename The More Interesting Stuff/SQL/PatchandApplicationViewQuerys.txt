All Installed Applications VIEW
SELECT     vrs.ResourceID, vrs.Netbios_Name0 AS ServerName, arp.Publisher0 AS Publisher, arp.DisplayName0 AS ApplicationName, arp.Version0 AS Version, 
                      arp.InstallDate0 AS InstallDate
FROM         dbo.v_Add_Remove_Programs AS arp INNER JOIN
                      dbo.v_R_System AS vrs ON arp.ResourceID = vrs.ResourceID
					  
All Needed Patches Detailed VIEW
SELECT     rs.ResourceID, rs.Netbios_Name0 AS Servername, rs.Resource_Domain_OR_Workgr0 AS Domain, updates.Title, updates.ArticleID, updates.BulletinID, 
                      csr.CustomName, CONVERT(varchar(10), updates.DatePosted, 101) AS DatePosted, CONVERT(varchar(10), updates.DateRevised, 101) AS DateRevised
FROM         dbo.v_ClientCollectionMembers AS ccm INNER JOIN
                      dbo.v_R_System AS rs ON rs.ResourceID = ccm.ResourceID LEFT OUTER JOIN
                      dbo.v_UpdateComplianceStatus AS UCS ON UCS.ResourceID = ccm.ResourceID INNER JOIN
                      dbo.v_CICategories_All AS catall2 ON catall2.CI_ID = UCS.CI_ID INNER JOIN
                      dbo.v_CategoryInfo AS catinfo2 ON catall2.CategoryInstance_UniqueID = catinfo2.CategoryInstance_UniqueID AND 
                      catinfo2.CategoryTypeName = 'UpdateClassification' INNER JOIN
                      dbo.v_UpdateInfo AS updates ON UCS.CI_ID = updates.CI_ID LEFT OUTER JOIN
                      dbo.CustomSeverityReference AS csr ON updates.Severity = csr.SeverityID
WHERE     (ccm.CollectionID = 'SMS00001') AND (catinfo2.CategoryInstanceName LIKE 'Security Updates') AND (UCS.Status = 2) AND (updates.IsExpired = 0) AND 
                      (updates.IsSuperseded = 0)
					  
Servers Out Of Compliance VIEW
SELECT     rs.ResourceID, rs.Netbios_Name0 AS [PC Name], rs.Resource_Domain_OR_Workgr0 AS Domain, SUM(CASE WHEN UCS.status = 2 THEN 1 ELSE 0 END) 
                      AS [Updates Needed], asite.SMS_Assigned_Sites0 AS [SCCM Site], DATEDIFF(D, ws.LastHWScan, GETDATE()) AS [Last Hardware Scan (Days)], DATEDIFF(D, 
                      USS.LastScanTime, GETDATE()) AS [Last Updates Scan (days)], CONVERT(VarChar(10), OS.LastBootUpTime0, 101) AS [Last Boot Date], DATEDIFF(D, 
                      OS.LastBootUpTime0, GETDATE()) AS [Last Boot (Days)], MAX(OU.System_OU_Name0) AS [AD OU]
FROM         dbo.v_ClientCollectionMembers AS ccm INNER JOIN
                      dbo.v_R_System AS rs ON rs.ResourceID = ccm.ResourceID LEFT OUTER JOIN
                      dbo.v_UpdateComplianceStatus AS UCS ON UCS.ResourceID = ccm.ResourceID INNER JOIN
                      dbo.v_CICategories_All AS catall2 ON catall2.CI_ID = UCS.CI_ID INNER JOIN
                      dbo.v_CategoryInfo AS catinfo2 ON catall2.CategoryInstance_UniqueID = catinfo2.CategoryInstance_UniqueID AND 
                      catinfo2.CategoryTypeName = 'UpdateClassification' LEFT OUTER JOIN
                      dbo.v_RA_System_SMSAssignedSites AS asite ON asite.ResourceID = ccm.ResourceID LEFT OUTER JOIN
                      dbo.v_GS_WORKSTATION_STATUS AS ws ON ws.ResourceID = rs.ResourceID LEFT OUTER JOIN
                      dbo.v_GS_OPERATING_SYSTEM AS OS ON ws.ResourceID = OS.ResourceID LEFT OUTER JOIN
                      dbo.v_RA_System_SMSAssignedSites AS sites ON ws.ResourceID = sites.ResourceID LEFT OUTER JOIN
                      dbo.v_UpdateCIs AS updates ON UCS.CI_ID = updates.CI_ID LEFT OUTER JOIN
                      dbo.v_RA_System_SystemOUName AS OU ON rs.ResourceID = OU.ResourceID LEFT OUTER JOIN
                      dbo.v_UpdateScanStatus AS USS ON rs.ResourceID = USS.ResourceID
WHERE     (ccm.CollectionID = 'SMS00001') AND (catinfo2.CategoryInstanceName LIKE 'Security Updates') AND (updates.IsExpired = 0) AND (updates.IsSuperseded = 0) AND 
                      (updates.DateRevised < DATEADD(day, - 30, GETDATE())) AND (UCS.Status = 2)
GROUP BY rs.Netbios_Name0, rs.User_Name0, asite.SMS_Assigned_Sites0, rs.Client_Version0, ws.LastHWScan, OS.LastBootUpTime0, rs.Resource_Domain_OR_Workgr0, 
                      USS.LastScanTime, rs.ResourceID
					  
