select Netbios_Name0, AdvertisementName from vSMS_ClientAdvertisementStatus as cas
inner join v_R_System as vrs on vrs.ResourceID = cas.ResourceID
inner join v_Advertisement as va on va.AdvertisementID = cas.AdvertisementID
where LastStatusMessageID = 11170 and LastStatusTime > DATEADD(MINUTE,-35,GETDATE())

select Netbios_Name0, AdvertisementName from vSMS_ClientAdvertisementStatus as cas
inner join v_R_System as vrs on vrs.ResourceID = cas.ResourceID
inner join v_Advertisement as va on va.AdvertisementID = cas.AdvertisementID
where LastStatusMessageID = 11171 and LastStatusTime > DATEADD(MINUTE,-35,GETDATE()) and cas.AdvertisementID in ('CEN20004','CEN20003')

select col.Name, fcm.Name from v_FullCollectionMembership as fcm
inner join v_Collection as col on col.CollectionID = fcm.CollectionID
where col.Name in (
'1a_Disk_Array',
'1b_Base_Image',
'2_Drivers_And_firmware',
'3_NICTeam_And_IP',
'4_SQL_Config'
)