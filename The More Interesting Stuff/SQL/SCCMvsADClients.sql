DECLARE @sccmclt as decimal
DECLARE @adclt as decimal
DECLARE @percent as decimal (18,2)
 

SET @sccmclt = (Select COUNT (Distinct sys.ResourceID)
 FROM v_R_System sys
 Full outer join v_GS_COMPUTER_SYSTEM cs on sys.resourceID = cs.ResourceID
 WHERE Client0 = 1 and Active0 =1 and Domain0 like '%prod%'
)
 
SET @adclt = (Select COUNT (cn)
 
From openquery ("prod",
'SELECT cn
FROM ''LDAP://<domain>''
WHERE objectCategory = ''Computer''')
)
 
SET @percent = CASE WHEN @sccmclt = 0 Then 0 ELSE ((@sccmclt/@adclt)*100) END 

Select @sccmclt as 'Sccm Clients (Active)', @adclt as 'AD Client' , @percent as 'Sccm client %'

exec sp_dropserver 'Prod'
