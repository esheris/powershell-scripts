SELECT 
rs.Netbios_name0 as 'PC Name', 
case 
	when (sum(case when UCS.status=2 then 1 else 0 end))>0 then (cast(sum(case when UCS.status=2 then 1 else 0 end)as varchar(10))+ ' Security Updates Missing') 
	else 'Up To Date' 
end as 'Updates Status', 
rs.User_Name0 as 'Last Logged On User', 
asite.SMS_Assigned_Sites0 as 'SMS Site', 
rs.Client_Version0 as 'Client Version', 
ws.lasthwscan as 'Last Hardware Scan', 
uss.LastScanPackageLocation as 'Last Scan Location', 
uss.LastScanPackageVersion as 'Last Scan Package', 
st.StateName as 'Status', 
Convert(VarChar(10), OS.LastBootUpTime0, 101) 'Last Boot Date', 
DateDiff(D, OS.LastBootUpTime0, GetDate()) 'Last Boot (Days)' 

from v_ClientCollectionMembers ccm 
join v_R_System rs on rs.ResourceID = ccm.ResourceID 
left outer join v_UpdateComplianceStatus  UCS on UCS.ResourceID = ccm.ResourceID 
join v_CICategories_All catall2 on catall2.CI_ID=UCS.CI_ID 
join v_CategoryInfo catinfo2 on catall2.CategoryInstance_UniqueID = catinfo2.CategoryInstance_UniqueID and catinfo2.CategoryTypeName='UpdateClassification' 
left join v_RA_System_SMSAssignedSites asite on asite.ResourceID = ccm.ResourceID 
left join v_gs_workstation_status ws on ws.resourceid=rs.resourceid 
left join v_UpdateScanStatus uss on ws.resourceid = uss.ResourceID 
left join v_StateNames st on st.TopicType = 501 and st.StateID = (case when (isnull(uss.LastScanState, 0)=0 and Left(isnull(rs.Client_Version0, '4.0'), 
1)<'4') then 7 else isnull(uss.LastScanState, 0) end) 
left join v_Gs_Operating_System OS on ws.resourceid = OS.ResourceID
 
where 
ccm.CollectionID='sms00001' 
and catinfo2.CategoryInstanceName like 'Security Updates'

Group By 
rs.Netbios_name0, 
rs.User_Name0 , 
asite.SMS_Assigned_Sites0 , 
rs.Client_Version0, 
ws.lasthwscan, 
uss.LastScanPackageLocation, 
uss.LastScanPackageVersion , 
st.StateName , 
OS.LastBootUpTime0


