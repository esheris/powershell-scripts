select 
case
	when ps.[Updates Status] = 0 then 'Up To Date'
	when ps.[Updates Status] between 1 and 10 then '1 - 10 Missing Updates'
	when ps.[Updates Status] between 11 and 20 then '11 - 20 Missing Updates'
	when ps.[Updates Status] between 21 and 30 then '21 - 30 Missing Updates'
	when ps.[Updates Status] between 31 and 40 then '31 - 40 Missing Updates'
	when ps.[Updates Status] between 41 and 50 then '41 - 50 Missing Updates'
	when ps.[Updates Status] between 51 and 60 then '51 - 60 Missing Updates'
	when ps.[Updates Status] between 61 and 70 then '61 - 70 Missing Updates'
	when ps.[Updates Status] between 71 and 80 then '71 - 80 Missing Updates'
	when ps.[Updates Status] between 81 and 90 then '81 - 90 Missing Updates'
	when ps.[Updates Status] between 91 and 100 then '91 - 100 Missing Updates'
	when ps.[Updates Status] > 100 then '100+ Missing Updates'
end as 'Missing Updates'
, COUNT(ps.[Updates Status]) as 'Server Count' from uv_patchstatus ps
group by case
	when ps.[Updates Status] = 0 then 'Up To Date'
	when ps.[Updates Status] between 1 and 10 then '1 - 10 Missing Updates'
	when ps.[Updates Status] between 11 and 20 then '11 - 20 Missing Updates'
	when ps.[Updates Status] between 21 and 30 then '21 - 30 Missing Updates'
	when ps.[Updates Status] between 31 and 40 then '31 - 40 Missing Updates'
	when ps.[Updates Status] between 41 and 50 then '41 - 50 Missing Updates'
	when ps.[Updates Status] between 51 and 60 then '51 - 60 Missing Updates'
	when ps.[Updates Status] between 61 and 70 then '61 - 70 Missing Updates'
	when ps.[Updates Status] between 71 and 80 then '71 - 80 Missing Updates'
	when ps.[Updates Status] between 81 and 90 then '81 - 90 Missing Updates'
	when ps.[Updates Status] between 91 and 100 then '91 - 100 Missing Updates'
	when ps.[Updates Status] > 100 then '100+ Missing Updates'
end
order by 'Missing Updates'
