select v_R_System.Name0, v_Advertisement.AdvertisementName from v_TaskExecutionStatus
inner join v_R_System on v_R_System.ResourceID = v_TaskExecutionStatus.ResourceID
inner join v_Advertisement on v_Advertisement.AdvertisementID = v_TaskExecutionStatus.AdvertisementID
where v_TaskExecutionStatus.Step = '1' 
and (v_Advertisement.AdvertisementName in ( '1_Build_OS_v1.2_Part_1', '1_Build_OS_v1.2_Part_2', '2_NICTeam_and_Patch_v1.2', '3_Storage_and_SQL_v2.0' ))
and v_TaskExecutionStatus.ExecutionTime >= DATEADD(day,-7,CURRENT_TIMESTAMP)