SELECT 
sou.System_OU_Name0,
COUNT (distinct sou.ResourceID) as 'Total Computers'
 
From  v_R_System usr
  left join v_RA_System_SystemOUName sou on usr.ResourceID = sou.ResourceID
  left join v_AgentDiscoveries disc on usr.ResourceID = disc.ResourceID
  Inner Join 
      ( SELECT ResourceID, Max(System_OU_Name0) as ou2
         FROM v_RA_System_SystemOUName Group By ResourceID
        ) maxou
        on sou.ResourceID = maxou.ResourceID and sou.System_OU_Name0 = maxou.ou2
 
Where DATEDIFF(d,disc.AgentTime,Getdate()) <= 1
Group By sou.System_OU_Name0
Order By 'Total Computers' desc


