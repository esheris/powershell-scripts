select vrs.name0 as ServerName, pcb.serialnumber0 as Serial, sysE.SMBIOSAssetTag0 as Asset from SMS_Q01.dbo.v_GS_SYSTEM_ENCLOSURE as sysE
join v_r_system as vrs on vrs.resourceid = sysE.ResourceID
join v_gs_pc_bios as pcb on pcb.resourceid = sysE.ResourceID
order by pcb.serialnumber0
