
DECLARE @adstaleclt as decimal
DECLARE @adclt as decimal
DECLARE @percent as decimal (18,2)
 
 
SET @adstaleclt=(Select COUNT (cn)
From openquery ("ADSI",  
'SELECT cn,
operatingSystem,
operatingSystemServicePack,
lastLogonTimestamp,
pwdLastSet
FROM ''LDAP:/''
WHERE objectCategory = ''Computer''')
 
WHERE DATEDIFF(D,DATEADD(mi,(cast(lastlogontimestamp as bigint) / 600000000) - 157258080
+ DATEDIFF(Minute,GetUTCDate(),GetDate()),0),GETDATE()) >= 90
and DATEDIFF(D,DATEADD(mi,(cast(pwdlastset as bigint) / 600000000) - 157258080
+ DATEDIFF(Minute,GetUTCDate(),GetDate()),0),GETDATE()) >= 90
)
 
 
SET @adclt=(Select COUNT (cn)
 
From openquery ("ADSI",  
'SELECT cn
FROM ''LDAP:/''
WHERE objectCategory = ''Computer''')
)
 
SET @percent = CASE WHEN @adstaleclt = 0 THEN 0 ELSE ((@adstaleclt/@adclt)*100) END
 
Select ', @adstaleclt as 'AD Stale Clients (90 Days)', @adclt as 'AD Client' , @percent as 'Stale Clients %'