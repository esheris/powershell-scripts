declare @ratio table (id int NULL,failures bigint NULL, sucesses bigint NULL)
insert into @ratio (id) values (1)
update @ratio
set failures = (
select COUNT(v_TaskExecutionStatus.ResourceID) as Failures from v_TaskExecutionStatus 
join v_Advertisement on v_Advertisement.AdvertisementID = v_TaskExecutionStatus.AdvertisementID
join v_R_System on v_R_System.ResourceID = v_TaskExecutionStatus.ResourceID
where v_TaskExecutionStatus.ExecutionTime >= DATEADD(DAY,-730,CURRENT_TIMESTAMP)
and LastStatusMessageID = '11141')
where id = 1
update @ratio
set sucesses = (
select COUNT(v_taskexecutionstatus.ResourceID) as Failures from v_TaskExecutionStatus 
join v_Advertisement on v_Advertisement.AdvertisementID = v_TaskExecutionStatus.AdvertisementID
join v_R_System on v_R_System.ResourceID = v_TaskExecutionStatus.ResourceID
where v_TaskExecutionStatus.ExecutionTime >= DATEADD(DAY,-730,CURRENT_TIMESTAMP)
and LastStatusMessageID = '11143')
where id = 1
select failures, sucesses, cast(cast((sucesses / cast(failures as numeric)) as decimal(8,4)) * 100 as decimal(8,2)) as SuccessPercent from @ratio
