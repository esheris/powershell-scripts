Select v_R_System.Name0 as ServerName, v_Collection.Name as CollectionName
From v_R_System
Left join v_FullCollectionMembership on v_FullCollectionMembership.ResourceId = v_R_System.ResourceId
Left join v_Collection on v_FullCollectionMembership.CollectionId = v_Collection.CollectionId
Where Name0 in (
'<server1>',
'<server2>'
) and v_collection.Name not like 'all%' and v_Collection.Name not like 'HP%Servers'
order by CollectionName, ServerName
