SELECT     rs.ResourceID, rs.Netbios_Name0 AS [PC Name], rs.Resource_Domain_OR_Workgr0 AS Domain, SUM(CASE WHEN UCS.status = 2 THEN 1 ELSE 0 END) 
                      AS [Updates Needed], asite.SMS_Assigned_Sites0 AS [SCCM Site], Datediff(D, ws.LastHWScan, GETDATE()) AS [Last Hardware Scan], 
                      CONVERT(VarChar(10), 
                      OS.LastBootUpTime0, 101) AS [Last Boot Date], DATEDIFF(D, OS.LastBootUpTime0, GETDATE()) AS [Last Boot (Days)], MAX(OU.System_OU_Name0) AS [AD OU]
FROM         dbo.v_ClientCollectionMembers AS ccm INNER JOIN
                      dbo.v_R_System AS rs ON rs.ResourceID = ccm.ResourceID LEFT OUTER JOIN
                      dbo.v_UpdateComplianceStatus AS UCS ON UCS.ResourceID = ccm.ResourceID INNER JOIN
                      dbo.v_CICategories_All AS catall2 ON catall2.CI_ID = UCS.CI_ID INNER JOIN
                      dbo.v_CategoryInfo AS catinfo2 ON catall2.CategoryInstance_UniqueID = catinfo2.CategoryInstance_UniqueID AND 
                      catinfo2.CategoryTypeName = 'UpdateClassification' LEFT OUTER JOIN
                      dbo.v_RA_System_SMSAssignedSites AS asite ON asite.ResourceID = ccm.ResourceID LEFT OUTER JOIN
                      dbo.v_GS_WORKSTATION_STATUS AS ws ON ws.ResourceID = rs.ResourceID LEFT OUTER JOIN
                      dbo.v_GS_OPERATING_SYSTEM AS OS ON ws.ResourceID = OS.ResourceID LEFT OUTER JOIN
                      dbo.v_RA_System_SMSAssignedSites AS sites ON ws.ResourceID = sites.ResourceID Left Outer Join
                      dbo.v_UpdateCIs as updates on UCS.CI_ID = updates.CI_ID LEFT OUTER JOIN
                      dbo.v_RA_System_SystemOUName as OU on rs.ResourceID = OU.ResourceID
WHERE     (ccm.CollectionID = 'SMS00001') AND (catinfo2.CategoryInstanceName LIKE 'Security Updates') and (updates.DateRevised <= DATEADD(day,-7,GETDATE())) 
GROUP BY rs.Netbios_Name0, rs.User_Name0, asite.SMS_Assigned_Sites0, rs.Client_Version0, ws.LastHWScan,  
                      OS.LastBootUpTime0, rs.Resource_Domain_OR_Workgr0, rs.ResourceID
                      
                      
                      
                      
                  