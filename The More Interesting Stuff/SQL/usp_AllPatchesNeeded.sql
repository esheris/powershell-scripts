USE [SMS_CEN]
GO

/****** Object:  StoredProcedure [dbo].[usp_AllPatchesNeeded]    Script Date: 01/07/2013 01:16:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AllPatchesNeeded] 
	-- Add the parameters for the stored procedure here
	@daysAgoLower int = 0, 
	@daysAgoUpper int = 1825
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT      rs.Netbios_Name0 as [Servername], rs.Resource_Domain_OR_Workgr0 as [Domain], updates.Title, updates.ArticleID, updates.BulletinID,
			csr.CustomName,convert(varchar(10),updates.DatePosted,101) as [DatePosted], convert(varchar(10),updates.DateRevised,101) as [DateRevised]
FROM         dbo.v_ClientCollectionMembers AS ccm INNER JOIN
                      dbo.v_R_System AS rs ON rs.ResourceID = ccm.ResourceID LEFT OUTER JOIN
                      dbo.v_UpdateComplianceStatus AS UCS ON UCS.ResourceID = ccm.ResourceID INNER JOIN
                      dbo.v_CICategories_All AS catall2 ON catall2.CI_ID = UCS.CI_ID INNER JOIN
                      dbo.v_CategoryInfo AS catinfo2 ON catall2.CategoryInstance_UniqueID = catinfo2.CategoryInstance_UniqueID AND 
                      catinfo2.CategoryTypeName = 'UpdateClassification' inner join
                      v_UpdateInfo updates on ucs.CI_ID = updates.CI_ID
                      left outer join CustomSeverityReference csr on updates.Severity = csr.SeverityID
WHERE       (ccm.CollectionID = 'SMS00001') AND (catinfo2.CategoryInstanceName LIKE 'Security Updates') and UCS.status = 2 and updates.IsExpired = 0 and updates.IsSuperseded = 0
and updates.DateRevised between DATEADD(day,-@daysAgoUpper,getdate()) and DATEADD(DAY,-@daysAgoLower,GETDATE())
END

GO

