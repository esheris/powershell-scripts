USE [SMS_CEN]
GO

/****** Object:  StoredProcedure [dbo].[usp_AvgBuildTimePerDay]    Script Date: 01/07/2013 01:15:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AvgBuildTimePerDay] 
	-- Add the parameters for the stored procedure here
	@day int = -30
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select convert(varchar,pb.v_checkpoint01,101) [Day], AVG(DATEDIFF(minute,pb.v_Checkpoint01, pb.v_Checkpoint10)) [Build Time]
from vSMS_ClientAdvertisementStatus as cas
inner join v_R_System as vrs on vrs.ResourceID = cas.ResourceID
inner join v_Advertisement as va on va.AdvertisementID = cas.AdvertisementID
join Root_Of_All_Evil.dbo.Pandoras_Box as pb on pb.v_ImagedServerName = Netbios_Name0
where (LastStatusMessageID = 11171 or LastStatusMessageID = 10040) and LastStatusTime > DATEADD(DAY,@day,GETDATE()) and (cas.AdvertisementID = 'CEN20024' or cas.AdvertisementID = 'CEN20047') 
and DATEDIFF(minute,pb.v_Checkpoint01, pb.v_Checkpoint10) < 4000
group by convert(varchar,pb.v_checkpoint01,101)
END

GO

