USE [SMS_CEN]
GO

/****** Object:  StoredProcedure [dbo].[usp_BuildBatchStatus]    Script Date: 01/07/2013 01:16:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_BuildBatchStatus] 
	-- Add the parameters for the stored procedure here
	@Batch varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Netbios_Name0 as [Server Name], 
case 
	when vcol.Name = '4_Fat_Image_Completed' then 'Complete' 
	when vcol.Name = '4_SQL_Config' then 'SQL Config' 
	when vcol.Name = 'Post_Build_Patching' then 'Patching' 
	when vcol.Name = '3_NICTeam_And_IP' then 'Teaming'
	when vcol.Name = '2_Drivers_And_Firmware' then 'Drivers and Firmware'
	when vcol.Name = '1b_Base_Image' then 'Deploying Operating System'
	when vcol.Name = '1a_Disk_Array' then 'Configuring Disk Array'
	when vcol.Name = 'Nucleon_Compute_Sled' then 'Nucleon Step 1'
	when vcol.Name = 'Nucleon_Compute_Sled_Part_2' then 'Nucleon Step 2'
	when vcol.Name = 'Nucleon_Compute_Sled_Completed' then 'Nucleon Completed'
	when vcol.Name = 'Decom_Computer_Object' then 'Decommisioning'
	when vcol.Name = '1_Disk_Configuration' then 'Config. Arrays'
	when vcol.Name = '2_OS_Image_And_Domain' then 'Deploying OS'
	when vcol.Name = '3_Drivers_Firmware_And_HyperV' then 'Drivers/Firmware'
	when vcol.Name = '4_NICTeam_And_IP' then 'Network Config.'
	when vcol.Name = '5_SQL_Configuration' then 'SQL Config.'
	when vcol.Name = '6_Post_Build_Patching' then 'Patching'
	when vcol.Name = '7_OS_Deployment_Completed' then 'Complete'
	
end as [Status],
pb.v_MachineStatus as [Details],
datediff(minute,pb.d_LastUpdated,GETDATE()) as [LastUpdated]
from v_R_System as vrs
inner join v_FullCollectionMembership as vfcm on vfcm.ResourceID = vrs.ResourceID
inner join v_Collection as vcol on vcol.CollectionID = vfcm.CollectionID
left join Root_Of_All_Evil.dbo.Pandoras_Box pb on pb.v_ImagedServerName = vrs.Netbios_Name0
where vfcm.CollectionID in ('CEN00021','CEN00022','CEN00023','CEN00024','CEN00025','CEN00026','CEN0009C','CEN0008A','CEN0008C','CEN0008B','CEN00096','CEN000C4','CEN000C5','CEN000C6','CEN000C7','CEN000C8','CEN000CA','CEN000CB','CEN00096') 
and (vrs.Netbios_Name0 in (
	select v_imagedServerName from Root_Of_All_Evil.dbo.Pandoras_Box
	where v_ImagingBatchID = @Batch
)
or vrs.Netbios_Name0 in (
	select v_PriorServerName from Root_Of_All_Evil.dbo.Pandoras_Box
	where v_ImagingBatchID = @Batch
	)
)
order by [status],[Details],[Server Name],[LastUpdated]
END

GO

