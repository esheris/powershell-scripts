USE [SMS_CEN]
GO

/****** Object:  StoredProcedure [dbo].[usp_InstalledApplications]    Script Date: 01/07/2013 01:17:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_InstalledApplications] 
	-- Add the parameters for the stored procedure here
	@Server varchar(15) = '', 
	@Program varchar(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Netbios_Name0 as ServerName, Publisher0 as Publisher, ProductName0 as ApplicationName, ProductVersion0 as 'Version' from v_R_System vrs
inner join v_GS_INSTALLED_SOFTWARE installed on vrs.ResourceID = installed.ResourceID
where Netbios_Name0 like '%' + @Server + '%' and ProductName0 like '%' + @Program + '%'
END

GO

