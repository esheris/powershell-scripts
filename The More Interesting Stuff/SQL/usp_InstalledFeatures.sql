USE [SMS_CEN]
GO

/****** Object:  StoredProcedure [dbo].[usp_InstalledFeatures]    Script Date: 01/07/2013 01:17:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_InstalledFeatures]
	-- Add the parameters for the stored procedure here
	@ServerName varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Netbios_Name0, sf.ID0, sf.ParentID0, sf.Name0 from v_R_System vrs
inner join v_GS_Server_Feature0 sf on vrs.ResourceID = sf.ResourceID
where Netbios_Name0 = @ServerName
END

GO

