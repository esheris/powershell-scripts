USE [SMS_CEN]
GO

/****** Object:  StoredProcedure [dbo].[usp_PatchingStatusByRFC]    Script Date: 01/07/2013 01:18:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PatchingStatusByRFC]
	-- Add the parameters for the stored procedure here
	@rfc bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select sps.ServerName, sps.StartTime,sps.ResultType as ScriptResult, sps.ResultText as ScriptStatus,  
	SccmStatus = case when cas.LastStatusTime > sps.StartTime then cas.LastStateName else 'No Status' end, 
	LastUpdate = case when cas.LastStatusTime > sps.StartTime then cas.LastStatusTime else null end
from root_of_all_evil.dbo.SccmPatchingStatus sps
left outer join v_ClientAdvertisementStatus cas on sps.SccmResourceID = cas.ResourceID
where AdvertisementID = 'CEN20016' and sps.RFC = @rfc
order by SccmStatus, ResultType, cas.LastStatusTime

END

GO

