USE [SMS_CEN]
GO

/****** Object:  StoredProcedure [dbo].[usp_NonNucleon_Completed]    Script Date: 01/07/2013 01:23:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Eric Sheris
-- Create date: 06/26/2012
-- Description:	Retrieves all Completed NonNucleons. Defaults to the last 7 days
-- =============================================
CREATE PROCEDURE [dbo].[usp_NonNucleon_Completed]
	-- Add the parameters for the stored procedure here
	@days int = -7 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select Netbios_Name0 as [Server Name], DATEDIFF(minute,pb.v_Checkpoint01, pb.v_Checkpoint10) [Build Time], pb.v_ImagingBatchID as Batch
from vSMS_ClientAdvertisementStatus as cas
inner join v_R_System as vrs on vrs.ResourceID = cas.ResourceID
inner join v_Advertisement as va on va.AdvertisementID = cas.AdvertisementID
join Root_Of_All_Evil.dbo.Pandoras_Box as pb on pb.v_ImagedServerName = Netbios_Name0
where (LastStatusMessageID = 11171 or LastStatusMessageID = 10040) and pb.v_checkpoint10 > DATEADD(DAY,@days,GETDATE()) and (cas.AdvertisementID = 'CEN20024' or cas.AdvertisementID = 'CEN20047')
order by Batch
END

GO

