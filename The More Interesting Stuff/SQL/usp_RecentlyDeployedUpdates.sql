USE [SMS_CEN]
GO

/****** Object:  StoredProcedure [dbo].[usp_RecentUpdates]    Script Date: 01/07/2013 01:19:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_RecentUpdates]
	-- Add the parameters for the stored procedure here
	@days int = 7
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select 
AssignmentName, DateCreated, DateLastModified, BulletinID, ArticleID, DatePosted as updatePosted, DateRevised as UpdateRevised, Title as UpdateTitle, ui.Description as UpdateDescriptoin
from v_CIAssignment a
join v_CIAssignmentToCI aci
   on aci.AssignmentID=a.AssignmentID
join v_UpdateInfo ui
   on ui.CI_ID=aci.CI_ID
where IsDeployed = 1 and DatePosted > DATEADD(day,-@days,GETDATE())
order by ui.ArticleID
END

GO

