' ===========================================================================================
' Constants
' ===========================================================================================
 
Const NETWORK_CONNECTIONS = &H31&
Const ForAppending        = 8
 
strScriptPath             = "."         ' Path where TCPdump.exe is
strComputer               = "." 
 
' ===========================================================================================
' Check Script is being run with CSCRIPT rather than WSCRIPT due to using stdout
' ===========================================================================================
 
If UCase(Right(Wscript.FullName, 11)) = "WSCRIPT.EXE" Then
    strPath = Wscript.ScriptFullName
    ' Wscript.Echo "This script must be run under CScript." & vbCrLf & vbCrLf & "Re-Starting Under CScript"
    strCommand = "%comspec% /K cscript //NOLOGO " & Chr(34) & strPath & chr(34)
    Set objShell = CreateObject("Wscript.Shell")
    objShell.Run(strCommand)
    Wscript.Quit
End If 
 
Set objShell = CreateObject("Wscript.Shell")
Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
Set objShellApp = CreateObject("Shell.Application")
Set objFSO = CreateObject("Scripting.FileSystemObject")
 
 
Set objFolder = objShellApp.Namespace(NETWORK_CONNECTIONS)
Set oAdapters = objWMIService.ExecQuery ("Select * from Win32_NetworkAdapterConfiguration where (IPenabled = True)")
 
i = 0   ' Counter of detected CDP-Informations
 
' Enumerate the results (list of NICS). "

	'strCommand = strScriptPath & "\windump -nn -v -s 1500 -i \Device\" & oAdapter.SettingID & " -c 1 ether[20:2] == 0x2000"
    strCommand = strScriptPath & "\windump -nn -v -s 1500 -c 1 ether[20:2] == 0x2000"    
 
    
    Wscript.echo strCommand & VbCrLF
 
    Set objExec = objShell.Exec(strCommand)
 
    count = 0
 
    Do Until objExec.Status
        count = count +1
        'Timeout to Deal with Non CDP Enabled Devices
        If count = 1200 then
            objExec.terminate
            wscript.quit
        End If
        Wscript.Sleep 250
    Loop   
 
    ' Loop through the output of TCPDUMP stored in stdout and retrieve required fields
    ' Namely switch name, IP and Port
 
    While Not objExec.StdOut.AtEndOfStream
        strLine = objExec.StdOut.ReadLine
        If Instr(UCASE(strLine),"DEVICE-ID") > 0 Then
            strDeviceID = Mid(strLine,(Instr(strLine,chr(39))+1),(Len(StrLine) - (Instr(strLine,chr(39))+1)))
        End If
        If Instr(UCASE(strLine),"ADDRESS ") > 0 Then
            strDeviceIP = Right(strLine,(Len(strLine) - (Instrrev(strLine,")")+1)))
        End If
        If Instr(UCASE(strLine),"PORT-ID") > 0 Then
            strPort = Mid(strLine,(Instr(strLine,chr(39))+1),(Len(StrLine) - (Instr(strLine,chr(39))+1)))
        End If
		If Instr(UCASE(strLine),"MANAGEMENT ADDRESSES ") > 0 Then
            strMgmtIP = Right(strLine,(Len(strLine) - (Instrrev(strLine,")")+1)))
        End If
		If Instr(UCASE(strLine),"NATIVE VLAN ID") > 0 Then
            strVLAN = Right(strLine,(Len(strLine) - (Instrrev(strLine,":")+1)))
        End If
    Wend   
 
 dim osd: set env = CreateObject("Microsoft.SMS.TSEnvironment")
 env("SwitchName") = strDeviceID
 env("SwitchIP") = strDeviceIP
 env("SwitchPort") = strPort
 env("SwitchPortVlan") = strVLAN
 env("SwitchMGMTIP") = strMgmtIP
    wscript.echo "Switch ID " & env("SwitchName")
	wscript.echo "Switch IP " & env("SwitchIP")
	wscript.echo "Connected Port " & env("SwitchPort")
	wscript.echo "Port Vlan " & env("SwitchPortVlan")
	wscript.echo "Switch MGMT IP " & env("SwitchMGMTIP")
	
