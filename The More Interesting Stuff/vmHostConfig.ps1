﻿param(
    [string]$server
)
Configuration SccmHyperVHost
{
    param ($MachineName)
    Node $MachineName
    {
        WindowsFeature InstallHyperV
        {
            Ensure = "Present"
            Name = "Hyper-V"
        }

        WindowsFeature Rsat-Hyperv
        {
            Ensure = "Present"
            Name = "RSAT-Hyper-V-Tools"
            IncludeAllSubFeature = "True"
            DependsOn = "[WindowsFeature]InstallHyperV"
        }

        Script SetVirtualHardDiskPath
        {
            TestScript = {
                ((Get-VMHost).VirtualHardDiskPath -eq "E:\VMS")
            }
            GetScript = {
                @{ Result = ((Get-VMHost).VirtualHardDiskPath)}
            }
            SetScript = {
                Set-VMHost -VirtualHardDiskPath E:\VMS
            }
            DependsOn = "[WindowsFeature]Rsat-Hyperv" 
        }
        Script SetVirtualMachinePath
        {
            TestScript = {
                ((Get-VMHost).VirtualMachinePath -eq "E:\VMS")
            }
            GetScript = {
                @{ Result = ((Get-VMHost).VirtualMachinePath)}
            }
            SetScript = {
                Set-VMHost -VirtualMachinePath E:\VMS
            }
            DependsOn = "[WindowsFeature]Rsat-Hyperv"
        }


        Script vSwitch
        {
            TestScript = {
                ((Get-VMSwitch -Name "vSwitch" -erroraction silentlycontinue).Name -eq "vSwitch")
            }
            GetScript = {
                @{ Result = (Get-VMSwitch -name "vSwitch").Name }
            }
            SetScript = {                
                $iface = Get-NetIPConfiguration | where { $_.NetIPv4Interface.DHCP -eq "Disabled" -and $_.DNSServer -ne $null}
                if ((Measure-object -InputObject $iface).Count -eq 1){
                    New-VMSwitch -Name "vSwitch" -NetAdapterName $iface.InterfaceAlias -AllowManagementOS:$true 
                }
            }

            DependsOn = "[WindowsFeature]Rsat-Hyperv"
        }
    }
}
SccmHyperVHost -MachineName $server