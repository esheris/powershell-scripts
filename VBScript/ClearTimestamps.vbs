dim testing, time, checkpoint, bemac, Server, Database
testing = 0
if wscript.arguments.count <> 3 then
	helptxt()
	Wscript.Quit 2
end if
bemac = wscript.arguments.item(0)
Server = wscript.arguments.item(1)
Database = wscript.arguments.item(2)

WriteToDatabase checkpoint,time,Server,Database
function WriteToDatabase(checkpointNumber, currentTime, dbServer, Database)
	dim objCN, strConnection, insertSQL, updateSQL
	set objCN = CreateObject("ADODB.Connection")
	strConnection = "Provider=SQLOLEDB;Data Source=" & Server & ";Initial Catalog=" & Database & ";Network Library=DBMSSOCN;User Id=build;Password=P@$$word;" 
	
	updateSQL = "Update Pandoras_Box "
	updateSQL = updateSQL & "Set v_checkpoint01 = NULL,"
	updateSQL = updateSQL & "v_checkpoint02 = NULL,"
	updateSQL = updateSQL & "v_checkpoint03 = NULL,"
	updateSQL = updateSQL & "v_checkpoint04 = NULL,"
	updateSQL = updateSQL & "v_checkpoint05 = NULL,"
	updateSQL = updateSQL & "v_checkpoint06 = NULL,"
	updateSQL = updateSQL & "v_checkpoint07 = NULL,"
	updateSQL = updateSQL & "v_checkpoint08 = NULL,"
	updateSQL = updateSQL & "v_checkpoint09 = NULL,"
	updateSQL = updateSQL & "v_checkpoint10 = NULL,"
	updateSQL = updateSQL & "v_checkpoint11 = NULL,"
	updateSQL = updateSQL & "v_checkpoint12 = NULL,"
	updateSQL = updateSQL & "v_checkpoint13 = NULL,"
	updateSQL = updateSQL & "v_checkpoint14 = NULL,"
	updateSQL = updateSQL & "v_checkpoint15 = NULL,"
	updateSQL = updateSQL & "v_checkpoint16 = NULL,"
	updateSQL = updateSQL & "v_checkpoint17 = NULL,"
	updateSQL = updateSQL & "v_checkpoint18 = NULL,"
	updateSQL = updateSQL & "v_checkpoint19 = NULL,"
	updateSQL = updateSQL & "v_checkpoint20 = NULL "
	updateSQL = updateSQL & "Where v_BEMac = '" & bemac & "'"
	if testing = 0 then
		objCN.open(strConnection)
		objCN.Execute(updateSQL)
		if Err.Number <> 0 then
			Wscript.echo("Error Writing to database")
		else
			Wscript.echo("Sucessfully wrote to database")
		end if
	else
		wscript.echo(strConnection)
		wscript.echo(updateSQL)
	end if
end function

sub helptxt()
	wscript.echo("Syntax:")
	wscript.echo("ClearTimestamps.vbs <Backend Mac address (adsadminmac)> <SQL Server> <Database>")
end sub