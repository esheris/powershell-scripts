Dim env, root, adsimage
SET env = CreateObject("Microsoft.SMS.TSEnvironment")
for each v in env.GetVariables
	if ucase(v) = "C" then
		root = env(v)
	end if

	if v = "adsImageName" then
		adsimage = env(v)
	end if
next
env("OSDTargetSystemDrive") = root
env("OSDDriverPath") = root & "\_SMSTaskSequence\drivers"
env("OSDTargetSystemRoot") = root & "\Windows"
env("OSDAnswerFilePath") = root & "\Windows\panther\unattend\unattend.xml"
env("OSDInstallType") = "Sysprep"

Select Case ucase(adsimage)
	case "2K8SP2_INH", "2K8SP2_IIS", "2K8SP2_SQL"
		env("OSVersionNumber") = "6.0"
		env("OSArchitecture") = "X64"
	case "2K8R2_INH", "2K8R2_IIS", "2K8R2_SQL"
		env("OSVersionNumber") = "6.1"
		env("OSArchitecture") = "X64"
End Select

WScript.Quit(0)
