' *************************************************************
' Copyright (c)2004, Microsoft Corporation, All Rights Reserved
' *************************************************************
'
' SetupSiteBoundaries.vbs
'
'
' This script will read from a list of IP Subnets or AD Site Names from a text files and add them to a specific site server.
' It uses several utility functions to manage a site control file session.
'
' INPUT FILE FORMAT: details , Type, SiteCode, DisplayName, Flags
Option Explicit

' ******************************************************************
'   Global Variables
' ******************************************************************

' --- Connection Information ---  (Enter your site server, site code, and connection credentials here)
Dim gsServer, gsUser, gsPass
gsServer = "<server>"			' Name of the Site Server machine
gsUser   = ""			' Admin account on the Site Server
gsPass   = ""			' Password for the admin account

' --- Global Variables ---
Dim gsSiteCode			' The site code of the site
Dim gsProvider			' The machine with the provider
Dim goWMIConnection		' SWbemServices connection to the SMS provider
Dim goWMIContext		' Context for WMI Calls
Dim gsSessionHandle		' Session Handle of the SCF we're manipulating
Dim filename

' Code execution begins here
' --------------------------------

Main

' *******************************************************************
' Main() - This is the main subroutine of the script
' *******************************************************************

Sub Main()
	Dim Boundary, aBoundaries
	
	filename = "siteboundaries.txt"
	Dim fso, FileExistFlag
	Set fso = CreateObject("Scripting.FileSystemObject")
	FileExistFlag = fso.FileExists(filename)
	If FileExistFlag = False Then
		MsgBox "Error: You must specify a file that exists."
		WScript.Quit()
	End If
	
	' Make sure the default information has been changed
	If gsUser = "USERNAME" Then 
		MsgBox "You should used notepad to edit the default connection information in this script before running it.", vbCritical, "Oops!"
		WScript.Quit()
	End If

	' Establish a connection to WMI
	Connect
	
	' Get a new SCF session handle
	gsSessionHandle = GetSessionHandle()
	
	' Build a context object
	BuildWMIContext gsSessionHandle
	
	' Refresh the site control file
	'RefreshSCF gsSiteCode

	'Read text file into array
	aBoundaries = ParseTextFile(fileName)
	
	'Walk through array calling SetupBoundary
	
	for each Boundary in aBoundaries
	
		' Modify the Site Assignment Boundary settings
		If Boundary <> "" Then
			SetupBoundary Boundary
		End If
	next
	
	' Commit the SCF changes
	'CommitSCF gsSiteCode
	
	' Release the session handle
	ReleaseSessionHandle gsSessionHandle
	
	wscript.echo "Successfully modified the Site Assignment settings on " & gsServer

End Sub

Function ParseTextFile(filename)
	Dim aIPsParsed()
	Dim fso, MyFile, i
	i = 0
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set MyFile = fso.OpenTextFile(filename)
	
	Do Until MyFile.AtEndOfStream
		Redim Preserve arrFileLines(i)
		arrFileLines(i) = MyFile.ReadLine
		i = i + 1
	Loop

	ParseTextFile = arrFileLines

End Function

' *******************************************************************
' SetupBoundary() - This sub makes the actual changes to the
'   Site Boundary site server component
' *******************************************************************

Sub SetupBoundary(strBoundary)
	Dim sRelPath, astrBoundaryPart
	'Alipka: divide the line of file to 3 parts: boundary, type, site
	astrBoundaryPart = Split(strBoundary, ",", -1, 0)
	'MsgBox astrBoundaryPart(4)
	gsSiteCode = astrBoundaryPart(4)
	RefreshSCF gsSiteCode

	' Get the Network Discovery site control item
	
	wscript.echo gsSiteCode
	'sRelPath = "SMS_SCI_SiteAssignment.FileType=2,ItemName=""Site Assignment"",ItemType=""Site Assignment"",SiteCode=""" & gsSiteCode & """"
	sRelPath = "SMS_SCI_RoamingBoundary.FileType=1,Itemtype='Roaming Boundary',SiteCode='" & gsSiteCode & "',ItemName='Roaming Boundary'"
	'wscript.echo sRelPath
	Dim oSCI_SiteAssign
	Set oSCI_SiteAssign = goWMIConnection.Get(sRelPath,, goWMIContext)
	
	' Get the current list of domains to search
	Dim aDomainArray
	Dim bDomainArray
	Dim cDomainArray
	Dim dDomainArray
	aDomainArray = oSCI_SiteAssign.Details
	bDomainArray = oSCI_SiteAssign.Types
	cDomainArray = oSCI_SiteAssign.DisplayNames
	dDomainArray = oSCI_SiteAssign.Flags
	'wscript.echo aDomainArray(0), bDomainArray(0), cDomainArray(0), dDomainArray(0)
	
	' Grow the array by 1
	ReDim Preserve aDomainArray(UBound(aDomainArray)+1)
	ReDim Preserve bDomainArray(UBound(bDomainArray)+1)
	ReDim Preserve cDomainArray(UBound(cDomainArray)+1)
	ReDim Preserve dDomainArray(UBound(dDomainArray)+1)


	' Add our new domain
	aDomainArray(UBound(aDomainArray)) = Replace(astrBoundaryPart(0), """", "")
	bDomainArray(UBound(bDomainArray)) = Replace(astrBoundaryPart(1), """", "")
	cDomainArray(UBound(cDomainArray)) = Replace(astrBoundaryPart(2), """", "")
	dDomainArray(UBound(dDomainArray)) = Replace(astrBoundaryPart(3), """", "")
	'wscript.echo aDomainArray(2), bDomainArray(2), cDomainArray(2), dDomainArray(2)
	
	' Save the changes to the SCI
	oSCI_SiteAssign.Details	= aDomainArray
	oSCI_SiteAssign.Types	= bDomainArray
	oSCI_SiteAssign.DisplayNames	= cDomainArray
	oSCI_SiteAssign.Flags	= dDomainArray

	oSCI_SiteAssign.Put_ ,goWMIContext

	' Commit the SCF changes
	CommitSCF gsSiteCode

End Sub

' *******************************************************************
' Connect() - This sub makes a connection to the SMS Provider
' *******************************************************************

Sub Connect()

	' Create a WMI Locator
	Dim oLocator
	Set oLocator = CreateObject("WbemScripting.SWbemLocator")

	' Trap the WMI Exceptions
	On Error Resume Next
		
	' Connect to the "root\sms" namespace on the site server
	Set goWMIConnection = oLocator.ConnectServer(gsServer, "root\sms")
	If Err.number <> 0 Then
		MsgBox "Failed to connect to the ""\\" & gsServer & "\root\sms"" namespace." & vbCrLf & "WMI ERROR: " & Err.Description & " (" & Err.Number & ") ", vbCritical, "Error"
		WScript.Quit()
	End if 
	
	' Query for the SMS_ProviderLocator instance for this site server
	Dim osQueryResults
	Set osQueryResults = goWMIConnection.ExecQuery("SELECT * FROM SMS_ProviderLocation WHERE ProviderForLocalSite=""TRUE""")
	
	' Loop through the results
	Dim oInst
	For Each oInst in osQueryResults
		
		' Get the Provider Machine & the Site Code
		gsProvider = oInst.Machine
		gsSiteCode = oInst.SiteCode
		Exit For
	Next
	
	' Make sure we didn't have any errors
	If Err.number <> 0 Then
		MsgBox "Failed to find the provider for the site """ & gsServer & """." & vbCrLf & "WMI ERROR: " & Err.Description & " (" & Err.Number & ") ", vbCritical, "Error"
		WScript.Quit()
	End if 
	
	' Connect to the "root\sms\site_ABC" namespace on the provider machine
	Set goWMIConnection = oLocator.ConnectServer(gsProvider, "root\sms\site_" & gsSiteCode, gsUser, gsPass)
	If Err.number <> 0 Then
		MsgBox "Failed to connect to the ""\\" & gsProvider & "\root\sms\site_" & gsSiteCode & """ namespace." & vbCrLf & "WMI ERROR: " & Err.Description & " (" & Err.Number & ") ", vbCritical, "Error"
		WScript.Quit()
	End if 

	' Turn exception handling back on
	On Error Goto 0
	
End Sub

' *******************************************************************
' BuildWMIContext() - This sub will create a new WMI Context object
' *******************************************************************

Sub BuildWMIContext(sSessionHandle)

	' Create a new context object
	Set goWMIContext = CreateObject("WbemScripting.SWbemNamedValueSet")
	
	' Add the session handle to the context object
	goWMIContext.Add "SessionHandle", sSessionHandle
	
End Sub

' *******************************************************************
' GetSessionHandle() - This function gets a new session handle for SCF manipulation
' *******************************************************************

Function GetSessionHandle()

    ' Get the SMS_SiteControlFile object
    Dim oSCFClass
    Set oSCFClass = goWMIConnection.Get("SMS_SiteControlFile")
    
    ' Call the GetSessionHandle method
    Dim oOutParams
    Set oOutParams = oSCFClass.ExecMethod_("GetSessionHandle")
    
    ' Get the session handle from the results
    GetSessionHandle = oOutParams.SessionHandle
    
End Function

' *******************************************************************
' ReleaseSessionHandle() - This function releases an existing session handle
' *******************************************************************

Sub ReleaseSessionHandle(sSessionHandle)

	' Get the SMS_SiteControlFile object
	Dim oSCFClass
	Set oSCFClass = goWMIConnection.Get("SMS_SiteControlFile")
	
	' Call the ReleaseSessionHandle method
	oSCFClass.ReleaseSessionHandle(sSessionHandle)
	
End Sub

' *******************************************************************
' RefreshSCF() - This function will refresh a specific SCF
' *******************************************************************

Sub RefreshSCF(sSiteCode)

	' Get the SMS_SiteControlFile object
	Dim oSCFClass
	Set oSCFClass = goWMIConnection.Get("SMS_SiteControlFile")
	
	' Get the InParams for the method
	Dim oInParams
	set oInParams = oSCFClass.Methods_("RefreshSCF").InParameters.Clone_
	
	' Set the InParams for the method
	oInParams.Properties_("SiteCode").Value = sSiteCode
	
	' Call the RefreshSCF method
	oSCFClass.ExecMethod_ "RefreshSCF", oInParams, , goWMIContext
	
End Sub

' *******************************************************************
' CommitSCF() - This function will commit any changes to a specific SCF
' *******************************************************************

Sub CommitSCF(sSiteCode)

	' Get the SMS_SiteControlFile object
	Dim oSCFClass
	Set oSCFClass = goWMIConnection.Get("SMS_SiteControlFile")
	
	' Get the InParams for the method
	Dim oInParams
	set oInParams = oSCFClass.Methods_("CommitSCF").InParameters.Clone_
	
	' Set the InParams for the method
	oInParams.Properties_("SiteCode").Value = sSiteCode
	
	' Call the CommitSCF method
	oSCFClass.ExecMethod_ "CommitSCF", oInParams, , goWMIContext
	
End Sub

' *******************************************************************


