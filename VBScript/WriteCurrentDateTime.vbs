dim testing, time, checkpoint, bemac, Server, Database
testing = 0
if wscript.arguments.count <> 4 then
	helptxt()
	Wscript.Quit 2
end if
checkpoint = wscript.arguments.item(0)
bemac = wscript.arguments.item(1)
Server = wscript.arguments.item(2)
Database = wscript.arguments.item(3)
time = Now()
WriteToDatabase checkpoint,time,Server,Database
function WriteToDatabase(checkpointNumber, currentTime, dbServer, Database)
	dim objCN, strConnection, insertSQL, updateSQL
	set objCN = CreateObject("ADODB.Connection")
	strConnection = "Provider=SQLOLEDB;Data Source=" & Server & ";Initial Catalog=" & Database & ";Network Library=DBMSSOCN;User Id=build;Password=P@$$word;" 
	
	updateSQL = "Update Pandoras_Box "
	updateSQL = updateSQL & "Set " & checkpoint & " = '" & time & "' "
	updateSQL = updateSQL & "Where v_BEMac = '" & bemac & "'"
	if testing = 0 then
		objCN.open(strConnection)
		objCN.Execute(updateSQL)
		if Err.Number <> 0 then
			Wscript.echo("Error Writing to database")
		else
			Wscript.echo("Sucessfully wrote to database")
		end if
	else
		wscript.echo(strConnection)
		wscript.echo(updateSQL)
	end if
end function

sub helptxt()
	wscript.echo("Syntax:")
	wscript.echo("WriteCurrentDateTime.vbs <checkpoint column name> <Backend Mac address (adsadminmac)> <SQL Server> <Database>")
end sub