'08/10/2000 script created by v-winman
'09/29/2000 added some array checking
'09/29/2000 fixed missing DNS servers
'09/29/2000 added counter that matches ipconfig /all NIC#
'10/17/2000 added DHCP info
'11/15/2000 cleaned up output
'04/11/2001 modIfied help msg
'01/25/2005 dlangdon - added NIC Speed/Duplex and drastically changed script flow.
'02/21/2005 dlangdon - added better Speed/Duplex handling.  Changed .Caption to .Description for MSNDIS speed section
'05/23/2006 dlangdon - added NC6136 detection (Fiber NIC)

Option Explicit

Dim lngCurrentServer, strVersion

strVersion = "2.0"

If WScript.Arguments.Count = 0 Then
	Call Usage(strVersion)
End If

For lngCurrentServer = 0 To WScript.Arguments.Count - 1
	If InStr(1, WScript.Arguments(lngCurrentServer), "?", 1) > 0 Then
		Call Usage(strVersion)
	Else
		Call GetNICSpeedDuplexSettings(WScript.Arguments(lngCurrentServer))
	End If
Next

Sub GetNICSpeedDuplexSettings(ByVal strComputer)

	Dim objService, objAdapter, objInstance, aryPNPDeviceID, objRegistry, objAdapterConfig, objInstanceConfig
	Dim strNetworkName, strCurrentControlNetwork, strKeyCurrentPath, ary4Digits, strLast4Digits
	Dim strSpeedDuplex, strTempValue, strMACADdress, strAdapterName, objWBEM, objSpeedDuplex, strTempDuplex, strTempSpeed
	Dim lngCurrentIPAddress, lngCurrentDNSServer, lngNICCount, strMAC, strSpeed, strDuplex, bFiber
	
	Const HKEY_LOCAL_MACHINE = &H80000002
	
	'On Error Resume Next
	
	strCurrentControlNetwork = "SYSTEM\CurrentControlSet\Control\Network\{4D36E972-E325-11CE-BFC1-08002bE10318}"
	strKeyCurrentPath =	"SYSTEM\CurrentControlSet\Control\Class\{4D36E972-E325-11CE-BFC1-08002bE10318}"
	lngNICCount = 0

	Set	objRegistry = GetObject("winmgmts:{impersonationLevel=impersonate}!\\"	& strComputer &	"\root\default:StdRegProv")
	Set	objService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
	Set objAdapter = objService.ExecQuery("SELECT * FROM Win32_NetworkAdapter WHERE MACADDRESS <> NULL")
	
	For Each objInstance In objAdapter

		If objInstance.PNPDeviceID <> Empty Then
			aryPNPDeviceID = Split(objInstance.PNPDeviceID,	"\", -1, 1)

			If aryPNPDeviceID(0) <> "ROOT" Or InStr(1, aryPNPDeviceID(1), "CPQTEAM", 1) <> 0 Then

				Set objAdapterConfig = objService.ExecQuery("SELECT * FROM Win32_NetworkAdapterConfiguration")
				For Each objInstanceConfig In objAdapterConfig
				
					If objInstanceConfig.Caption = objInstance.Caption Then
					
						lngNICCount = lngNICCount + 1
						bFiber = False

						'Get Adapter Name
						objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strCurrentControlNetwork & "\" & objInstanceConfig.SettingID & "\Connection", "Name", strAdapterName
						
						If InStr(1, objInstanceConfig.Description, "NC6136", 0) <> 0 Then
							bFiber = True
						End If

						WScript.Echo
						WScript.Echo "--------------------------------------------------------------------------------------"
						WScript.Echo "[" & objInstanceConfig.Description & "]" & " NIC #" & lngNICCount
						WScript.Echo
						WScript.Echo Left("  NIC Name:" & Space(30), 30) & strAdapterName

						strMAC = objInstanceConfig.MACAddress
						
						Do While InStr(1, strMAC, ":", 1) > 0
							strMAC = Left(strMAC, InStr(1, strMAC, ":", 1) - 1) & Right(strMAC, Len(strMAC) - InStr(1, strMAC, ":", 1))
						Loop

						WScript.Echo Left("  MAC Address:" & Space(30), 30) & strMAC

						If Not isNull(objInstanceConfig.IPaddress) Then
							
							WScript.Echo Left("  DNS Host Name:" & Space(30), 30) & objInstanceConfig.DNSHostName
							WScript.Echo Left("  DNS Domain:" & Space(30), 30) & objInstanceConfig.DNSDomain


							For lngCurrentIPAddress = LBound(objInstanceConfig.IPAddress) to UBound(objInstanceConfig.IPAddress)
								If lngCurrentIPAddress = 0 And LBound(objInstanceConfig.IPAddress)<> UBound(objInstanceConfig.IPAddress) Then
									WScript.Echo Left("  IPAddress #" & (lngCurrentIPAddress + 1) & ":" & " (Primary)" & Space(30), 30) & objInstanceConfig.IPAddress(lngCurrentIPAddress)
								ElseIf Left(strMAC, 4) = "02BF" Then
									WScript.Echo Left("  IPAddress #" & (lngCurrentIPAddress + 1) & ":" & " (Cluster)" & Space(30), 30) & objInstanceConfig.IPAddress(lngCurrentIPAddress)
								Else
	   								WScript.Echo Left("  IPAddress #" & (lngCurrentIPAddress + 1) & ":" & Space(30), 30) & objInstanceConfig.IPAddress(lngCurrentIPAddress)
	   							End If
							Next
		
							WScript.Echo Left("  Subnet Mask:" & Space(30), 30) & objInstanceConfig.IPSubnet(0)
							WScript.Echo Left("  Default Gateway:" & Space(30), 30) & IIf(IsNull(objInstanceConfig.DefaultIPGateway), "", objInstanceConfig.DefaultIPGateway(0))

							If Not IsNull(objInstanceConfig.DNSServerSearchOrder) Then
								For lngCurrentDNSServer = LBound(objInstanceConfig.DNSServerSearchOrder) to UBound(objInstanceConfig.DNSServerSearchOrder)
	   								WScript.Echo Left("  DNS Server:" & Space(30), 30) & objInstanceConfig.DNSServerSearchOrder(lngCurrentDNSServer)
								Next
							End If
		
							If objInstanceConfig.WINSPrimaryServer <> "127.0.0.0" Then
								WScript.Echo Left("  Primary WINS:" & Space(30), 30) & objInstanceConfig.WINSPrimaryServer
							End If
							
							If objInstanceConfig.WINSSecondaryServer <> "127.0.0.0" Then
								WScript.Echo Left("  Secondary WINS:" & Space(30), 30) & objInstanceConfig.WINSSecondaryServer
							End If
							
							Select Case objInstanceConfig.TcpipNetbiosOptions
								Case "1"
									WScript.Echo Left("  NetBios over TCP/IP:" & Space(30), 30) & "ENABLED"
								Case "2"
									WScript.Echo Left("  NetBios over TCP/IP:" & Space(30), 30) & "DISABLED"
								Case "0"
									WScript.Echo Left("  NetBios:" & Space(30), 30) & "Using Netbios setting from DHCP Server"
							End Select

							If objInstanceConfig.DHCPEnabled = True Then
								WScript.Echo Left("  DHCP:" & Space(30), 30) & "Enabled" 
								WScript.Echo Left("  DHCP Server:" & Space(30), 30) & objInstanceConfig.DHCPServer
							Else
								WScript.Echo Left("  DHCP:" & Space(30), 30) & "DISABLED"
							End If
							
						End If

						ary4Digits = Split(objInstance.Caption, "]", -1, 1)
						strLast4Digits = Right(ary4Digits(0), 4)
						
						'1st try to get Duplex and Speed from registry.
						objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyCurrentPath & "\" & strLast4Digits, "SpeedDuplex", strTempValue
						objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyCurrentPath & "\" & strLast4Digits & "\Ndi\params\SpeedDuplex\enum", strTempValue, strSpeedDuplex

						If InStr(1, UCase(strSpeedDuplex), "AUTO", 1) > 0 Then
							'Ok, we know the NIC is running Auto.  Now we just need the speed.
							strDuplex = strSpeedDuplex
							strSpeedDuplex = ""

							'Try to get speed from Registry
							objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyCurrentPath & "\" & strLast4Digits, "MediaSpeed", strTempValue
							objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyCurrentPath & "\" & strLast4Digits & "\Ndi\params\MediaSpeed\enum", strTempValue, strSpeed

							If IsNull(strSpeed) Then
								'Couldn't get speed from registry.  Let's use WBEM instead.
								strSpeed = GetSpeed(strComputer, objInstanceConfig.Description)
							End If

							'Now we have Duplex (strDuplex) and Speed (strSpeed)
							
						ElseIf IsNull(strTempValue) Then
							'We need to try something else to get Duplex and Speed
							
							objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyCurrentPath & "\" & strLast4Digits, "Duplex", strTempValue
							objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyCurrentPath & "\" & strLast4Digits & "\Ndi\params\Duplex\enum", strTempValue, strDuplex

							If IsNull(strDuplex) Then
								
								objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyCurrentPath & "\" & strLast4Digits, "RequestedMediaType", strTempValue
								objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyCurrentPath & "\" & strLast4Digits & "\Ndi\params\RequestedMediaType\enum", strTempValue, strDuplex
								
							End If
							
							If IsNull(strDuplex) And bFiber Then
								'Ok, we have a Gig-E fiber NIC.
								strDuplex = "(Gig-E Fiber)"
							End If
							
							'Now we should have Duplex.							
							
							'Try to get speed from Registry
							objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyCurrentPath & "\" & strLast4Digits, "MediaSpeed", strTempValue
							objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyCurrentPath & "\" & strLast4Digits & "\Ndi\params\MediaSpeed\enum", strTempValue, strSpeed

							If IsNull(strSpeed) Then
								'Couldn't get speed from registry.  Let's use WBEM instead.
								strSpeed = GetSpeed(strComputer, objInstanceConfig.Description)
							End If
							
							'Now we have Duplex (strDuplex) and Speed (strSpeed)							
							
						Else
							'We have Duplex and Speed already.
							
						End If

						If strSpeedDuplex = "" Or IsNull(strSpeedDuplex) Then
							strSpeedDuplex = strSpeed & IIf(strDuplex <> "", " / ", "") & strDuplex
						End If

						'At this point we have the Speed/Duplex settings.  Let's record it.
						WScript.Echo Left("  Speed/Duplex:" & Space(30), 30) & strSpeedDuplex

					End If
					
				Next
				
			End If
			
		End If
		
	Next
	
	WScript.Echo
	
	On Error Goto 0
	
End Sub

Sub Usage(strVersion)

	WScript.Echo
	WScript.Echo "RIPCONFIG.VBS Version " & strVersion
	WScript.Echo	
	WScript.Echo "This script will give IPCONFIG /all information remotely"
	WScript.Echo "This script is designed for Windows 2000, but will work on NT4 with WMI installed and running"
	WScript.Echo
	WScript.Echo "Syntax: ripconfig COMPUTERNAME or IP Address"
	WScript.Echo
	WScript.Quit

End Sub

Function IIf(ByVal Expression, ByVal TruePart, ByVal FalsePart)

	If (Expression) Then
		IIf = TruePart
	Else
		IIf = FalsePart
	End If

End Function

Function GetSpeed(ByVal strComputer, ByVal strNICCaption)

	Dim objWBEM, strSpeed, objSpeedDuplex
	
	strSpeed = "N/A"
	
	Set objWBEM = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer  & "\root\wmi")
	
	'HP Nic Teaming doesn't supprt SWbemServicesEx
	On Error Resume Next
	Set objSpeedDuplex = objWBEM.Get("MSNdis_LinkSpeed='" & strNICCaption & "'")
	'Set objSpeedDuplex = objWBEM.Get("MSNdis_LinkSpeed='" & Mid(strNICCaption, InStr(1, strNICCaption, "]", 1) + 2) & "'")
	
	If Err.Number = 0 Then
		strSpeed = (objSpeedDuplex.NdisLinkSpeed / 10000)

		If strSpeed => 1000 Then
			strSpeed = (strSpeed / 1000) & " Gbps"
		Else
			strSpeed = strSpeed & " Mbps"
		End If
	Else
		Err.Clear
	End If
	
	GetSpeed = strSpeed

End Function