Dim objCN, strConnection, goSWbemLocator, objWMIService, goTSenv, sVarName, sVarValue, bMatchFound

'----------------------------------------------------------------------------------------
'Constants And Variables
'----------------------------------------------------------------------------------------
On Error Resume Next

Const ForAppending = 8
const HKEY_LOCAL_MACHINE = &H80000002
const REG_SZ = 1
const REG_EXPAND_SZ = 2
const REG_BINARY = 3
const REG_DWORD = 4
const REG_MULTI_SZ = 7

argLog = "c:\sql.log"
argSMSServer = "sccm"
bMatchFound = "0"

Dim gsPad
gsPad = Space(90)

Call CreateENV
Call Connects

WriteLog "Script processing started"

Set colAdapters = objWMIService.ExecQuery("SELECT * From Win32_NetworkAdapter")
	For Each objAdapter In colAdapters
		'WriteLog "Found MAC address - " & objAdapter.MACAddress
		strMAC = objAdapter.MACAddress
		NewMAC = Replace(strMAC,":","")
		WriteLog "Looking for this MAC address in the server manifest - " & NewMAC
		Call FindMAC(NewMAC)


	Next

If bMatchFound = "0" Then
	WriteLog "Failed finding MAC address in server manifest. Manifest or network team mac address may be wrong."
	Wscript.Quit(1)

Else If bMatchFound = "1" Then
	Wscript.Quit(0)
End If
End If


Function Connects

'Connect to SMS WMI
Err.Clear

'Set objSMS = goSWbemLocator.ConnectServer("<server>", "root\sms","<user>","<password>")
'Set Results = objSMS.ExecQuery("SELECT * From SMS_ProviderLocation WHERE ProviderForLocalSite = true")
'	For each Loc in Results
'		If Loc.ProviderForLocalSite = True Then
'			Set objSMS = goSWbemLocator.ConnectServer(Loc.Machine, "root\sms\site_" & Loc.SiteCode,"<user>","<password>")
'			strSMSSiteCode = Loc.SiteCode
'				If Not Err.number=0 Then
'					WriteLog "ERROR - Could not connect to SMS WMI provider - " & "Exit code: " & Err.Number
'				Else
'					WriteLog "Successfully connected to SMS WMI provider - " & "Exit code: " & Err.Number
'					WriteLog "Determined SMS site code as the following: " & strSMSSiteCode
'				End If
'		End If
'	Next

'Connect to CIMV2
Err.Clear
strComputer = "."
Set objWMIService = GetObject("winmgmts:\\" & strComputer)
	If Not Err.number=0 Then
		WriteLog "ERROR - Could not connect to the CIMV2 WMI namespace - " & "Exit code: " & Err.Number
	Else
		WriteLog "Successfully connected to CIMV2 WMI namespace - " & "Exit code: " & Err.Number
	End If

Set objCN = CreateObject("ADODB.Connection")

'strConnection = "Driver={SQLServer};Server=<server>;Database=Root_of_All_Evil;Uid=build;Pwd=P@$$word"

strConnection = "Provider=SQLOLEDB;Data Source=<server>;Initial Catalog=Root_of_All_Evil;Network Library=DBMSSOCN;User Id=build;Password=P@$$word;"
err.clear
'objCN.Open(strConnection)
objCN.Open(strConnection)

if (Err.Number <> 0) then
	wscript.echo("error connecting to sql server")
end if

End Function

Function CreateENV

    Set goWshShell = WScript.CreateObject("WScript.Shell")
        If (Err.Number <> 0) Or (StrComp(TypeName(goWshShell), "IWshShell3", 1) <> 0) Then
            SetError 1,3,"Could not load Wscript.Shell " & TypeName(goWshShell)
        End If

    Set objFSO = CreateObject("Scripting.FileSystemObject")
        If (Err.Number <> 0)  Or (StrComp(TypeName(goFSO), "FileSystemObject", 1) <> 0)  Then
'            SetError 1,3,"Could not load Scripting.FileSystemObject! " & TypeName(objFSO)
        End If

    Set goXmlDom = CreateObject("Microsoft.XMLDOM")
        If (Err.Number <> 0)  Or (StrComp(TypeName(goXmlDom), "DOMDocument", 1) <> 0)  Then
            SetError 1,3,"Could not load Microsoft.XMLDOM! " & TypeName(goXmlDom)
        End If
        goXmlDom.setProperty "SelectionLanguage", "XPath"

    Set goSysEnv = goWshShell.Environment("PROCESS")
        If (Err.Number <> 0)  Or (StrComp(TypeName(goSysEnv), "IWshEnvironment", 1) <> 0)  Then
            SetError 1,3,"Could not load goWshShell.Environment! " & TypeName(goSysEnv)
        End If

    Set goTSEnv = CreateObject("Microsoft.SMS.TSEnvironment")
        If (Err.Number <> 0)   Then
            SetError 1,3,"Could not load Microsoft.SMS.TSEnvironment! " & TypeName(goTSEnv)
        End If

    Set goSWbemLocator = CreateObject("WbemScripting.SWbemLocator")
        If (Err.Number <> 0)  Or (StrComp(TypeName(goSWbemLocator), "SWbemLocator", 1) <> 0)  Then
            SetError 1,3,"Could not load WMI SWbemLocator " & TypeName(goSWbemLocator)
        End If

End Function

Function FindMAC(strServerMAC)

Dim strSQLQuery
strSQLQuery = "Select * from v_Build_Data Where netNic1MacAddress = '" & strServerMAC & "'"
wscript.echo(strSqlQuery)
Dim objRSSet
err.clear
objRS=CreateObject("ADODB.Recordset")

if (err.number <> 0) then
	wscript.echo "error creating recordset"
end if
err.clear
Set objRS = objCN.Execute(strSQLQuery)
if (err.number <> 0) then
	wscript.echo "error executing sql statement"
end if

'modification mikelee 2011-09-07. bmatchfound is sometimes not getting set below when
'it should, thus causing false failures of the script. making sure it gets set when
'the query returns at least one record. TODO: fail when the query returns more than
'one record. To do that, have to set a static cursor type when creating the
'recordset object. Also, we could fail based on winhostname or other field not being populated
'so that we can stage manifest records and kick them off by filling in the trigger field.

If objRS("netNic1MacAddress") <> "" Then
	wscript.echo "Found record for computer " & objRS("winhostname") & " with MAC address " & objRS("netNic1MacAddress")
	bMatchFound = "1"


Do Until objRS.EOF

SetTaskSequenceVariable "winHostname", objRS.Fields("winHostname")
SetTaskSequenceVariable "netNic1MacAddress", objRS.Fields("netNic1MacAddress")
SetTaskSequenceVariable "formatscript", objRS.Fields("formatscript")
SetTaskSequenceVariable "adsimagename", objRS.Fields("adsimagename")
SetTaskSequenceVariable "smsSiteCode", objRS.Fields("smsSiteCode")
SetTaskSequenceVariable "winAdminPassword", objRS.Fields("winAdminPassword")
SetTaskSequenceVariable "winAdminUsername", objRS.Fields("winAdminUsername")
SetTaskSequenceVariable "winDomainName", objRS.Fields("winDomainName")
SetTaskSequenceVariable "staticroutes", objRS.Fields("staticroutes")
SetTaskSequenceVariable "netTeamConfigFile", objRS.Fields("netTeamConfigFile")
SetTaskSequenceVariable "netNic1IpAddress", objRS.Fields("netNic1IpAddress")
SetTaskSequenceVariable "netNic1SubnetMask", objRS.Fields("netNic1SubnetMask")
SetTaskSequenceVariable "netNic1DefaultGateway", objRS.Fields("netNic1DefaultGateway")
SetTaskSequenceVariable "netNic1DnsServerSearchOrders", objRS.Fields("netNic1DnsServerSearchOrders")
SetTaskSequenceVariable "netNic1DnsServerSearchOrder", objRS.Fields("netNic1DnsServerSearchOrder")
SetTaskSequenceVariable "netNic1DnsServerSearchOrder2", objRS.Fields("netNic1DnsServerSearchOrder2")
SetTaskSequenceVariable "netNic1DnsSuffixRegistrationEnabled", objRS.Fields("netNic1DnsSuffixRegistrationEnabled")
SetTaskSequenceVariable "netNic2IpAddress", objRS.Fields("netNic2IpAddress")
SetTaskSequenceVariable "netNic2SubnetMask", objRS.Fields("netNic2SubnetMask")
SetTaskSequenceVariable "netNic2DefaultGateway", objRS.Fields("netNic2DefaultGateway")
SetTaskSequenceVariable "netNic2DnsServerSearchOrder", objRS.Fields("netNic2DnsServerSearchOrder")
SetTaskSequenceVariable "netNic2MacAddress", objRS.Fields("netNic2MacAddress")
SetTaskSequenceVariable "netNic2DnsSuffixRegistrationEnabled", objRS.Fields("netNic2DnsSuffixRegistrationEnabled")
'SetTaskSequenceVariable "oobDefaultHostname", objRS.Fields("oobDefaultHostname")
'SetTaskSequenceVariable "oobDhcpEnabled", objRS.Fields("oobDhcpEnabled")
'SetTaskSequenceVariable "oobActivationKey", objRS.Fields("oobActivationKey")
SetTaskSequenceVariable "oobIpAddress", objRS.Fields("oobIpAddress")
SetTaskSequenceVariable "oobDefaultGateway", objRS.Fields("oobDefaultGateway")
SetTaskSequenceVariable "oobSubnetMask", objRS.Fields("oobSubnetMask")
SetTaskSequenceVariable "oobPrimaryDnsServer", objRS.Fields("oobPrimaryDnsServer")
SetTaskSequenceVariable "oobSecondaryDnsServer", objRS.Fields("oobSecondaryDnsServer")
'SetTaskSequenceVariable "oobAddPassword1", objRS.Fields("oobAddPassword1")
'SetTaskSequenceVariable "oobAddUserLogin1", objRS.Fields("oobAddUserLogin1")
SetTaskSequenceVariable "EnableHyperThreading", objRS.Fields("EnableHyperThreading")
SetTaskSequenceVariable "EnableHyperV", objRS.Fields("EnableHyperV")
SetTaskSequenceVariable "TFSTask", objRS.Fields("TFSTask")
SetTaskSequenceVariable "DiskCount", objRS.Fields("DiskCount")
SetTaskSequenceVariable "Role", objRS.Fields("Role")
SetTaskSequenceVariable "PandoraID", objRS.Fields("PandoraID")
SetTaskSequenceVariable "OSDComputerName", objRS.Fields("winHostname")



    objRS.MoveNext

bMatchFound = "1"

Loop
End If

End Function

Sub WriteLog(sText)

    if (sText <> "") then
        wscript.echo gsIndent & sText & gsPad
    Else
    End If
End Sub

Function SetTaskSequenceVariable(sVarName, sVarValue)

	Wscript.Sleep(3)
    Indent
    if (sVarName <> "" and sVarValue <> "") Then
        goTSEnv(sVarName) = sVarValue
        If (Err.number = 0) then
            WriteLog "SetTaskSequenceVariable: Environment Variable '"  & sVarName & "' to " & sVarValue
            SetTaskSequenceVariable = true
        Else
            WriteLog "SetTaskSequenceVariable: Environment Variable '"  & sVarName & " not found. "
            SetTaskSequenceVariable = False
        End If
    Else
        WriteLog "SetTaskSequenceVariable: Variable name passed in was empty - " & sVarName
        SetTaskSequenceVariable = False
    End If

    Err.Clear
    on error goto 0

    Outdent

End Function

Sub Indent
    gsIndent = gsIndent & " "
End Sub

Sub OutDent
    If (Len(gsIndent) > 0) then
       gsIndent = Left(gsIndent, Len(gsIndent)-1)
    End If
End Sub
