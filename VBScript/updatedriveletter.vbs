'//////////////////////////////////////////////////////////////////////
'// updatedriveletters.vbs '// Version: 1.0
'// Date: May 2, 2008
'// by Alex Lee and Andres Springborn
'// Script File:
'//
'//   updatedriveletter.vbs
'//
'//   WMI script to reset drive letters to match expected values for XBox Live Operations 
'//
'// Notes:
'//   This script expects Drive Letters P, Q, R, S, U are unused.  IF they are in use the script will break.
'//   Notes on how drive letters are used today at XBL
'//   Z,Y,X:  Updated Drive letters for CD and Virtual Media from DELL DRAC connections
'//   T,O,H,E:  Normally used drives for SQL
'//   I,J:  Sometimes used drives for SQL
'//   A-H:  Can be used by systems on start when initial drive letters are configured (avoid)
'//
'//
'//////////////////////////////////////////////////////////////////////
strServer = "."
Set objWMI = GetObject("winmgmts:\\" & strServer & "\root\cimv2")
Set objInstances = objWMI.ExecQuery("select * from Win32_Volume",,48)
For Each objInstance in objInstances
    wscript.echo objInstance.DriveLetter & vbTab & objInstance.Label
    Select Case objInstance.Label
        Case "SQL"
            objInstance.DriveLetter = "P:"
            objInstance.Put_
	Case "SQL_Data"
            objInstance.DriveLetter = "Q:"
            objInstance.Put_
        Case "SQL_Log"
            objInstance.DriveLetter = "R:"
            objInstance.Put_
        Case "TempDB"
            objInstance.DriveLetter = "S:"
            objInstance.Put_
        Case "SQL_Backup"
            objInstance.DriveLetter = "U:"
            objInstance.Put_
	Case "SQL_Data1"
            objInstance.DriveLetter = "M:"
            objInstance.Put_
        Case "SQL_Backup1"
            objInstance.DriveLetter = "N:"
            objInstance.Put_
    End Select
Next
Set objInstances = objWMI.ExecQuery("select * from Win32_Volume",,48)
For Each objInstance in objInstances
    wscript.echo objInstance.DriveLetter & vbTab & objInstance.Label
    Select Case objInstance.Label
        Case "SQL"
            objInstance.DriveLetter = "D:"
            objInstance.Put_
	Case "SQL_Data"
            objInstance.DriveLetter = "H:"
            objInstance.Put_
        Case "SQL_Log"
            objInstance.DriveLetter = "O:"
            objInstance.Put_
        Case "TempDB"
            objInstance.DriveLetter = "T:"
            objInstance.Put_
        Case "SQL_Backup"
            objInstance.DriveLetter = "E:"
            objInstance.Put_
	Case "SQL_Data1"
            objInstance.DriveLetter = "I:"
            objInstance.Put_
        Case "SQL_Backup1"
            objInstance.DriveLetter = "F:"
            objInstance.Put_
    End Select
Next
Set objInstances = objWMI.ExecQuery("select * from Win32_Volume",,48)
For Each objInstance in objInstances
    wscript.echo objInstance.DriveLetter & vbTab & objInstance.Label
    Select Case objInstance.Label
	Case "SQL_Data1"
            objInstance.Label = "SQL_Data"
            objInstance.Put_
        Case "SQL_Backup1"
            objInstance.Label = "SQL_Backup"
            objInstance.Put_
    End Select
Next